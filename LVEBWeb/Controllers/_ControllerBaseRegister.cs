﻿using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Localization;

namespace LVEBWeb.Controllers
{
    public class _ControllerBaseRegister : _ControllerBase
    {

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (CurrentUser.UserCode == null)
                context.Result = new RedirectResult("/");

            base.OnActionExecuting(context);
        }


    }
}