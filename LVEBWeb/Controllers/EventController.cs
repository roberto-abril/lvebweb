﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class EventController : _ControllerBase
    {

        [HttpGet("e/{code}")]
        [HttpGet("e/{code}/{name}")]
        public IActionResult Index(string code, string name)
        {
            var place = _EventsBR.EventGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("e/{code}/eventos/{name}")]
        public IActionResult Events(string code, string name)
        {
            var place = _EventsBR.EventGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("e/{code}/fotos/{name}")]
        public IActionResult Photos(string code, string name)
        {
            var place = _EventsBR.EventGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("e/{code}/videos/{name}")]
        public IActionResult Videos(string code, string name)
        {
            var place = _EventsBR.EventGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("e/{code}/favoritos/{name}")]
        public IActionResult Favorites(string code, string name)
        {
            var place = _EventsBR.EventGet(CurrentUser, code);
            return View(place);
        }

        [ResponseCache(Duration = 86400)]
        [HttpGet("ei/{placeCode}/{code}")]
        public FileResult EventPhoto(string placeCode, string code)
        {
            var path = App_Core.Common.Configuration.PathImagesEvents(placeCode, code);
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                string contentType = "image/jpeg";
                return base.File(image, contentType);
            }
            else
            {
                path = App_Core.Common.Configuration.PathImagePlaces(placeCode);
                if (System.IO.File.Exists(path))
                {
                    var image = System.IO.File.ReadAllBytes(path);
                    string contentType = "image/jpeg";
                    return base.File(image, contentType);
                }
                else
                    return null;
            }
        }
        #region Base
        private static IEventsBR _EventsBR;


        public EventController(IMemoryCache memoryCache)
        {
            _EventsBR = EventsBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
