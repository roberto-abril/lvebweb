﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;

using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {

        [HttpGet("Admin/Tools/Exceptions")]
        public IActionResult Tools_Exceptions()
        {
            return View("Tools/Exceptions");
        }
        [HttpGet("Admin/Tools/Exceptions_Select")]
        public JsonResult Tools_Exceptions_Select(GridFilterModel gridModel)
        {
            try
            {
                var items = ToolsBR.Singleton().Exceptions_Search(gridModel);
                var jsonData = new
                {
                    total = gridModel.PT,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "Admin.ToolsController.ExceptionWeb_Select");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }


        [HttpGet("Admin/Tools/Exceptions_Clear")]
        public IActionResult Tools_Exceptions_Clear()
        {
            ToolsBR.Singleton().Exceptions_Clear();
            return RedirectToAction("Tools_Exceptions");
        }



    }
}
