﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        [HttpGet("Admin/Events")]
        public ActionResult Events()
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            //ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias);
            return View("Events/Events");
        }


        [HttpGet("Admin/Events/Search")]
        public JsonResult EventsSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = EventsBR.Singleton(MemoryCache).EventsSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }





        [HttpGet("Admin/Events/Edit")]
        [HttpGet("Admin/Events/Edit/{code}")]
        public ActionResult EventsEdit(string code = "")
        {
            ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.EventCategories);

            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            /* var place = PlacesBR.Singleton().PlaceGet(CurrentUser, code);

             ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince(CurrentUser, place.AddProvinceCode);
             return View(place);*/

            //ActivityBE Activity;
            if (string.IsNullOrEmpty(code))
            {
                ViewData["Cities"] = new List<DICCityBE>();
                var activity = new EventBE() { DateStart = DateTime.Now };
                return View("Events/EventsEdit", activity);
            }
            else
            {
                var activity = EventsBR.Singleton(MemoryCache).EventGet(CurrentUser, code);
                ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince(CurrentUser, activity.AddProvinceCode);
                return View("Events/EventsEdit", activity);
            }
            // return View(Activity);
        }


        [HttpPost("Admin/Events/Edit/{code}")]
        [HttpPost("Admin/Events/Edit/")]
        public JsonResult ActivitiesEdit(EventBE model, IFormFile file, string code = "")
        {
            if (string.IsNullOrEmpty(model.Name) || string.IsNullOrEmpty(model.Description))
            {
                return Json(new JsonResultModel(false));
            }
            // var result = EventsBR.Singleton(MemoryCache).ActivityInsertOrUpdate(CurrentUser, Activity);

            if (!string.IsNullOrEmpty(code) && file != null && file.Length > 0)
            {
                var pathLogo = Configuration.PathImagesEvents(model.PlaceCode, model.Code);
                using (var stream = new FileStream(pathLogo, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                    Functions.ImagesCreateForEvents(pathLogo);
                }
                model.HavePoster = true;
            }

            var result = EventsBR.Singleton(MemoryCache).ActivityInsertOrUpdate(CurrentUser, model);
            return Json(result);
        }



        [HttpPost("Admin/Events/Delete/{code}")]
        public JsonResult EventsDelete(string code)
        {
            EventsBR.Singleton(MemoryCache).EventDelete(CurrentUser, code);
            return Json(new JsonResultModel(true));
        }

        [HttpGet("Admin/EventsTMP")]
        public ActionResult EventsTMP()
        {
            // ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            //ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias);
            return View("Events/EventsTMP");
        }


        [HttpGet("Admin/EventsTMP/Search")]
        public JsonResult EventsTMPSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = EventsBR.Singleton(MemoryCache).EventsTMPSelect(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

    }
}
