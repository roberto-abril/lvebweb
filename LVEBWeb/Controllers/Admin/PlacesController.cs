﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet("Admin/Places")]
        public ActionResult Places()
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias);
            ViewData["BaileTags"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalTagsBailes);
            //ViewData["ProfileTags"] = CommonBR.Singleton().ValuesList( CurrentUser, DICTables.LocalTagsProfiles );
            return View("Places/Places");
        }


        [HttpGet("Admin/Places/Search")]
        public JsonResult PlacesSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = PlacesBR.Singleton(MemoryCache).PlacesSelect(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

        [HttpGet("Admin/PlacesTMP")]
        public ActionResult PlacesTMP()
        {
            // ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            // ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias);
            // ViewData["BaileTags"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalTagsBailes);
            return View("Places/PlacesTMP");
        }


        [HttpGet("Admin/PlacesTMP/Search")]
        public JsonResult PlacesTMPSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = PlacesBR.Singleton(MemoryCache).PlacesTMPSelect(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

        [HttpGet("Admin/Places/Edit/{code}")]
        [HttpGet("Admin/Places/Edit")]
        public ActionResult PlacesEdit(string code, bool hideMenu)
        {
            ViewData["HideMenu"] = hideMenu;
            ViewData["LocalCategorias"] = Newtonsoft.Json.JsonConvert.SerializeObject(CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias));

            ViewData["LocalTagsBailes"] = Newtonsoft.Json.JsonConvert.SerializeObject(CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalTagsBailes));
            ViewData["LocalTagsProfiles"] = Newtonsoft.Json.JsonConvert.SerializeObject(CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalTagsProfiles));


            //ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias);
            //ViewData["BaileTags"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalTagsBailes);

            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            if (string.IsNullOrEmpty(code))
            {
                ViewData["Cities"] = new List<DICCityBE>();
                var place = new PlaceBE();
                return View("Places/PlacesEdit", place);
            }
            else
            {
                var place = PlacesBR.Singleton(MemoryCache).PlaceGet(CurrentUser, code);

                ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince(CurrentUser, place.AddProvinceCode);
                return View("Places/PlacesEdit", place);
            }
        }
        [HttpPost("Admin/Places/Edit")]
        [HttpPost("Admin/Places/Edit/{code}")]
        public ActionResult PlacesEdit(string code, PlaceBE place, IFormFile file, bool hideMenu)
        {
            ViewData["HideMenu"] = hideMenu;

            if (string.IsNullOrEmpty(place.Name) || string.IsNullOrEmpty(place.AddProvinceCode)
                // || place.Latitude == 0 || place.Longitude == 0
                || string.IsNullOrEmpty(place.Description))
            {
                return Json(new JsonResultModel(false));
            }
            if (file != null && file.Length > 0)
                place.HaveLogo = true;

            var result = PlacesBR.Singleton(MemoryCache).PlaceInsertOrUpdate(CurrentUser, place);

            if (result.NewCode == "") result.NewCode = code;
            var pathLogo = Configuration.PathImagePlaces(result.NewCode);
            if (file != null && file.Length > 0)
            {

                using (var stream = new FileStream(pathLogo, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                    Functions.ImagesCreateCopies(pathLogo);
                }
            }
            else
            {
                if (System.IO.File.Exists(pathLogo))
                {
                    System.IO.File.Delete(pathLogo);
                    System.IO.File.Delete(pathLogo + "100");
                    System.IO.File.Delete(pathLogo + "250");
                    System.IO.File.Delete(pathLogo + "500");
                    System.IO.File.Delete(pathLogo + "1000");
                }
            }
            return Json(result);

        }
        [HttpPost("Admin/Places/Delete/{code}")]
        public JsonResult PlacesDelete(string code)
        {
            PlacesBR.Singleton(MemoryCache).PlaceDelete(CurrentUser, code);
            return Json(new JsonResultModel(true));
        }

        [HttpGet("Admin/Places/PhotoList")]
        public JsonResult PlacePhotoList(string placeCode)
        {
            try
            {
                var items = PlacesBR.Singleton(MemoryCache).PlacePhotoList(CurrentUser, placeCode);
                var jsonData = new
                {
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }
        [HttpPost("Admin/Places/PhotoAdd/{placeCode}")]
        public ActionResult PlacePhotoAdd(string placeCode, IFormFile file)
        {

            var photoCode = Functions.CreateId();

            var pathLogo = Configuration.PathImagePlaces(photoCode);
            if (file != null && file.Length > 0)
            {

                using (var stream = new FileStream(pathLogo, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                    Functions.ImagesCreateCopies(pathLogo);
                }


                PlacesBR.Singleton(MemoryCache).PlacePhotoInsert(CurrentUser, new PlacePhotoBE() { PhotoCode = photoCode, PlaceCode = placeCode, Name = "" });

            }

            return Json(true);

        }
        [HttpPost("Admin/PlacePhoto/Delete/{code}")]
        public JsonResult PlacePhotoDelete(string code)
        {
            PlacesBR.Singleton(MemoryCache).PlacePhothoDelete(CurrentUser, code);

            var files = System.IO.Directory.GetFiles(Configuration.PathImagePlacesBase(code), code + "*");
            foreach (var file in files)
            {
                System.IO.File.Delete(file);
            }
            return Json(true);
        }
        [HttpGet("Admin/PlacePhoto/{code}")]
        public FileResult PlacePhoto(string code)
        {

            var path = System.IO.Path.Combine(Configuration.PathImagePlaces(code + "100"));
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                string contentType = "image/jpeg";
                return base.File(image, contentType);

            }
            else
                return null;
        }


        [HttpPost("Admin/PlacesFindCodes")]
        //[HttpGet("PlacesFindCodes/{term}")]
        [HttpGet("Admin/PlacesFindCodes")]
        public JsonResult PlacesFindCodes(string term)
        {
            var items = PlacesBR.Singleton(MemoryCache).PlacesFindCodes(CurrentUser, term);
            return Json(items);
        }
        [HttpPost("Admin/PlacesFindCodesFull")]
        //[HttpGet("PlacesFindCodes/{term}")]
        [HttpGet("Admin/PlacesFindCodesFull")]
        public JsonResult PlacesFindCodesFull(string term)
        {
            var items = PlacesBR.Singleton(MemoryCache).PlacesFindCodesFull(CurrentUser, term);
            return Json(items);
        }
    }
}
