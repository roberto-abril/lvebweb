﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet("Admin/Competitions")]
        public ActionResult Competitions()
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.UsuarioTypes);
            return View("Competitions/Competitions");
        }


        [HttpGet("Admin/Competitions/Search")]
        public JsonResult CompetitionsSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = CompetitionsBR.Singleton(MemoryCache).CompetitionsSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.CompetitionsSearch");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

        [HttpGet("Admin/Competitions/Edit")]
        [HttpGet("Admin/Competitions/Edit/{code}")]
        public ActionResult CompetitionsEdit(string code = "")
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);

            //ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.SocioTypes);
            //ViewData["TypeLegalID"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.ContactoTypeLegalID);
            CompetitionBE competition;
            if (code == "")
            {
                competition = new CompetitionBE() { DateStart = DateTime.Now, DateEnd = DateTime.Now.AddDays(2) };
                ViewData["Cities"] = new List<DICCityBE>();
            }
            else
            {
                competition = CompetitionsBR.Singleton(MemoryCache).CompetitionGet(CurrentUser, code);
                ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince(CurrentUser, competition.AddProvinceCode);
            }
            return View("Competitions/CompetitionsEdit", competition);
        }


        [HttpPost("Admin/Competitions/Edit/{code}")]
        [HttpPost("Admin/Competitions/Edit/")]
        public JsonResult CompetitionsEdit(CompetitionBE competition, IFormFile file, string code = "")
        {
            if (string.IsNullOrEmpty(competition.Name) || string.IsNullOrEmpty(competition.AddProvinceCode)
                || string.IsNullOrEmpty(competition.Description) || string.IsNullOrEmpty(competition.Modalities))
            {
                return Json(new JsonResultModel(false));
            }
            //this.Request.Form["HavePoster"]
            //competition.HavePoster = file != null && file.Length > 0;

            var hp = this.Request.Form["HavePoster"];

            competition.HavePoster = (hp == "on") || file != null && file.Length > 0;


            var result = CompetitionsBR.Singleton(MemoryCache).CompetitionInsertOrUpdate(CurrentUser, competition);

            if (file != null && file.Length > 0)
            {
                if (result.NewCode == "") result.NewCode = code;
                var path = System.IO.Path.Combine(Configuration.PathImagesCompetitions, result.NewCode);
                //if (!System.IO.Directory.Exists(path))
                //    System.IO.Directory.CreateDirectory(path);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                    Functions.ImagesCreateCopies(path);
                }
            }

            return Json(result);
        }


        [HttpPost("Admin/Competitions/Delete/{code}")]
        public JsonResult CompetitionsDelete(string code)
        {
            CompetitionsBR.Singleton(MemoryCache).CompetitionDelete(CurrentUser, code);
            return Json(new JsonResultModel(true));
        }

        [HttpGet("Admin/CompetitionsPoster/{code}")]
        public FileResult CompetitionsPoster(string code)
        {

            var path = System.IO.Path.Combine(Configuration.PathImagesCompetitions, code);
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                string contentType = "image/jpeg";
                return base.File(image, contentType);

            }
            else
                return null;
        }



    }
}
