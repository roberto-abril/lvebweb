﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet( "Admin/Messages" )]
        public ActionResult Messages()
        {
            return View( "Messages/Messages" );
        }


        [HttpGet( "Admin/Messages/Search" )]
        public JsonResult MessagesSearch( GridFilterModel gridModel )
        {
            try
            {
                var items = InternalMessagesBR.Singleton().MessagesSearch( CurrentUser, gridModel );
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json( jsonData );

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync( ex, "AdminController.UsersSelect" );
                return Json( new
                {
                    total = 0,
                    page = 0,
                    records = 0
                } );
            }
        }

        [HttpGet( "Admin/Messages/Edit" )]
        [HttpGet( "Admin/Messages/Edit/{code}" )]
        public ActionResult MessagesEdit( string code = "" )
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList( CurrentUser );

            //ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.SocioTypes);
            //ViewData["TypeLegalID"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.ContactoTypeLegalID);
            CompetitionBE socio;
            if (code == "")
            {
                socio = new CompetitionBE();
                ViewData["Cities"] = new List<DICCityBE>();
            }
            else
            {
                socio = CompetitionsBR.Singleton( MemoryCache ).CompetitionGet( CurrentUser, code );
                ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince( CurrentUser, socio.AddProvinceCode );
            }
            return View( "Messages/Edit", socio );
        }




    }
}
