﻿using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
namespace LVEBWeb.Controllers.Admin
{
    [LVEBWeb.Helpers.RedirectingActionSecureAdminAttribute]
    public partial class AdminController : Controller
    {

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
            ViewData["IsAdmin"] = CurrentUser.IsAdmin();
        }

        #region Variables
        /// <summary>
        /// Contiene el id del usuario en curso.
        /// </summary>
        SessionAdminUserModel _CurrentUser;
        /// <summary>
        /// Obtiene o establece el usuario en curso. Gestionado y almacenado en la Session.
        /// </summary>
        protected SessionAdminUserModel CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {


                    if (HttpContext.Session != null && HttpContext.Session.Get<SessionAdminUserModel>("CurrentAdminUser") != null)
                        _CurrentUser = HttpContext.Session.Get<SessionAdminUserModel>("CurrentAdminUser");
                    else
                    {
                        _CurrentUser = new SessionAdminUserModel();
                        if (Configuration.Environment == "DEV")
                        {
                            _CurrentUser = App_Core.Admin.BR.WithOutSessionBR.Singleton().Logon(Configuration.AdminUserEmail, Configuration.AdminUserPassword);
                            if (_CurrentUser.UserHasAccess())
                            {
                                HttpContext.Session.Set("CurrentAdminUser", _CurrentUser);

                                if (this.Request.Headers.ContainsKey("Referer"))
                                {
                                    // url = this.Request.Headers["Referer"].ToString();
                                    this.Redirect(this.Request.Headers["Referer"].ToString());
                                }
                                else
                                    this.Redirect("/Admin");
                            }
                        }
                    }
                }
                if (_CurrentUser.UserHasAccess())
                {
                    this.Redirect("/Admin");
                }
                return _CurrentUser;
            }
        }
        #endregion Variables

        IMemoryCache MemoryCache;
        public AdminController(IMemoryCache memoryCache)
        {
            MemoryCache = memoryCache;
        }



        private bool HaveAccess()
        {
            return CurrentUser.UserId > 0;
        }
    }
}