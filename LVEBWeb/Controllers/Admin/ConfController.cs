﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;

using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet( "Admin/Conf/Diccionaries" )]
        public ActionResult Diccionaries()
        {
            ViewData["Tables"] = ConfBR.Singleton().TableList( CurrentUser );
            return View( "Conf/Diccionaries" );
        }


        [HttpGet( "Admin/Conf/DiccionariesSearch" )]
        public JsonResult DiccionariesSearch( GridFilterModel gridModel )
        {
            try
            {
                var items = ConfBR.Singleton().TableValueList( CurrentUser, gridModel.FK1 );
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json( jsonData );

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync( ex, "AdminController.UsersSelect" );
                return Json( new
                {
                    total = 0,
                    page = 0,
                    records = 0
                } );
            }
        }

        [HttpPost( "Admin/Conf/DiccionarySave" )]
        public JsonResult DiccionarySave( DICTableValueBE tableValue )
        {
            return Json( new JsonResultModel( ConfBR.Singleton().TableValueSave( CurrentUser, tableValue ) ) );

        }



        [HttpGet( "Admin/Emails" )]
        public ActionResult Emails()
        {
            return View( "Emails" );
        }


        [HttpGet( "Admin/EmailsSearch" )]
        public JsonResult EmailsSearch( GridFilterModel gridModel )
        {
            try
            {
                var items = ConfBR.Singleton().EmailList( CurrentUser, gridModel );
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json( jsonData );

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync( ex, "AdminController.UsersSelect" );
                return Json( new
                {
                    total = 0,
                    page = 0,
                    records = 0
                } );
            }
        }


    }
}
