﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet("Admin/companies")]
        public ActionResult Companies()
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.CompanyType);
            return View("Businesses/Companies");
        }


        [HttpGet("Admin/Companies/CompaniesSearch")]
        public JsonResult CompaniesSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = CompaniesBR.Singleton().CompaniesSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.BusinessesSearch");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

        [HttpGet("Admin/Companies/CompanyEdit")]
        [HttpGet("Admin/Companies/CompanyEdit/{code}")]
        public ActionResult CompanyEdit(string code = "")
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            ViewData["Categories"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.LocalCategorias);


            ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.CompanyType);
            ViewData["TypeLegalID"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.CompanyTypeLegalID);
            CompanyBE socio;
            if (code == "")
            {
                socio = new CompanyBE();
                ViewData["Cities"] = new List<DICCityBE>();
            }
            else
            {
                socio = CompaniesBR.Singleton().CompanyGet(CurrentUser, code);
                if (string.IsNullOrEmpty(socio.Code))
                {
                    return Redirect("/Admin/Companies");
                }
                else
                    ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince(CurrentUser, socio.AddProvinceCode);
            }
            return View("Businesses/CompanyEdit", socio);
        }


        [HttpPost("Admin/Companies/CompanyEdit/{code}")]
        [HttpPost("Admin/Companies/CompanyEdit/")]
        public JsonResult CompanyEdit(CompanyBE company, string code = "")
        {
            if (string.IsNullOrEmpty(company.Name) || string.IsNullOrEmpty(company.AddProvinceCode))
            {
                return Json(new JsonResultModel(false));
            }
            var result = CompaniesBR.Singleton().CompanyInsertOrUpdate(CurrentUser, company);
            return Json(result);
        }



        [HttpPost("Admin/Companies/CompaniesFindCodes")]
        [HttpGet("Admin/Companies/CompaniesFindCodes")]
        public JsonResult CompaniesFindCodes(string term)
        {
            var items = CompaniesBR.Singleton().CompaniesFindCodes(CurrentUser, term);
            return Json(items);
        }

        [HttpGet("Admin/Companies/UsersByCompanyList")]
        public JsonResult UsersByCompanyList(CompanyBE company, string code = "")
        {
            var result = CompaniesBR.Singleton().CompanyUserList(CurrentUser, code, null);
            return Json(result);
        }


        [HttpPost("Admin/Companies/UserAdd")]
        public JsonResult CompaniesUserAdd(CompanyUserBE model)
        {
            var items = CompaniesBR.Singleton().CompanyUserInsert(CurrentUser, model);
            return Json(items);
        }

        [HttpPost("Admin/Companies/UserRemove")]
        public JsonResult CompaniesUserRemove(CompanyUserBE model)
        {
            var items = CompaniesBR.Singleton().CompanyUserDelete(CurrentUser, model);
            return Json(items);
        }


        [HttpGet("Admin/Companies/PlacesByCompanyList")]
        public JsonResult PlacesByCompanyList(CompanyBE company, string code = "")
        {
            var result = CompaniesBR.Singleton().CompanyPlaceList(CurrentUser, code, null);
            return Json(result);
        }

        [HttpPost("Admin/Companies/PlaceAdd")]
        public JsonResult CompaniesPlaceAdd(CompanyPlaceBE model)
        {
            var items = CompaniesBR.Singleton().CompanyPlaceInsert(CurrentUser, model);
            return Json(items);
        }

        [HttpPost("Admin/Companies/PlaceRemove")]
        public JsonResult CompaniesPlaceRemove(CompanyPlaceBE model)
        {
            var items = CompaniesBR.Singleton().CompanyPlaceDelete(CurrentUser, model);
            return Json(items);
        }

    }
}
