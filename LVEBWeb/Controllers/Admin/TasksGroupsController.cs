﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;

using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet("Admin/TasksGroups")]
        public ActionResult TasksGroups()
        {
            return View("TasksGroups/TasksGroups");
        }


        [HttpGet("Admin/TasksGroups/Search")]
        public JsonResult TasksGroups_Search(GridFilterModel gridModel)
        {
            try
            {
                var items = TasksBR.Singleton().TaskGroupsSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.TasksGroupsSearch");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }


        [HttpGet("Admin/TasksGroups/Edit")]
        [HttpGet("Admin/TasksGroups/Edit/{code}")]
        public ActionResult TasksGroups_Edit(string code = "")
        {
            ViewData["Status"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.TaskStatus);
            ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.TaskTypes);
            var model = TasksBR.Singleton().TaskGroupGet(CurrentUser, code);
            return View("TasksGroups/TasksGroupsEdit", model);
        }


        [HttpPost("Admin/TasksGroups/Edit/{code}")]
        [HttpPost("Admin/TasksGroups/Edit/")]
        public JsonResult TasksGroups_Edit(TaskGroupBE taskGroup, string code = "")
        {
            if (string.IsNullOrEmpty(taskGroup.Name))
            {
                return Json(new JsonResultModel(false));
            }
            var result = TasksBR.Singleton().TaskGroupInsertOrUpdate(CurrentUser, taskGroup);
            return Json(result);
        }


        [HttpPost("Admin/TasksGroups/Delete/{code}")]
        public JsonResult TasksGroups_Delete(string code)
        {
            TasksBR.Singleton().TaskGroupDelete(CurrentUser, code);
            return Json(new JsonResultModel(true));
        }

        [HttpGet("Admin/TasksGroups/TasksSearch")]
        public JsonResult TasksGroups_TasksSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = TasksBR.Singleton().TaskSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.TasksSearch");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }


    }
}
