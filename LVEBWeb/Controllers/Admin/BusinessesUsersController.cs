﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet("Admin/users")]
        public ActionResult Users()
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);
            ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.UsuarioTypes);
            return View("Businesses/Users");
        }


        [HttpGet("Admin/users/UsersSearch")]
        public JsonResult UsersSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = UsersBR.Singleton().UsersSeach(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.BusinessesSearch");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

        [HttpGet("Admin/users/UserEdit")]
        [HttpGet("Admin/users/UserEdit/{code}")]
        public ActionResult UserEdit(string code = "")
        {
            ViewData["Provinces"] = CommonBR.Singleton().ProvincesList(CurrentUser);

            ViewData["Types"] = CommonBR.Singleton().ValuesList(CurrentUser, DICTables.UsuarioTypes);
            //ViewData["TypeLegalID"] = CommonBR.Singleton().ValuesList( CurrentUser, DICTables.ContactoTypeLegalID );
            UserBE user;
            if (code == "")
            {
                user = new UserBE();
                ViewData["Cities"] = new List<DICCityBE>();
            }
            else
            {
                user = UsersBR.Singleton().UserGet(CurrentUser, code);
                ViewData["Cities"] = CommonBR.Singleton().CitiesListByProvince(CurrentUser, user.AddProvinceCode);
            }
            return View("Businesses/UserEdit", user);
        }


        [HttpPost("Admin/users/UserEdit/{code}")]
        [HttpPost("Admin/users/UserEdit/")]
        public JsonResult UserEdit(UserBE user, string code = "")
        {
            if (string.IsNullOrEmpty(user.Name) || (string.IsNullOrEmpty(user.Email) & string.IsNullOrEmpty(user.Phone)))
            {
                return Json(new JsonResultModel(false));
            }
            var result = UsersBR.Singleton().UserInsertOrUpdate(CurrentUser, user);
            return Json(result);
        }


        [HttpGet("Admin/users/CompaniesByUserList")]
        public JsonResult CompaniesByUserList(CompanyBE company, string code = "")
        {
            var result = CompaniesBR.Singleton().CompanyUserList(CurrentUser, null, code);
            return Json(result);
        }

        [HttpPost("Admin/users/UsersFindCodes")]
        [HttpGet("Admin/users/UsersFindCodes")]
        public JsonResult UsersFindCodes(string term)
        {
            var items = UsersBR.Singleton().UsersFindCodes(CurrentUser, term);
            return Json(items);
        }

        [HttpGet("Admin/users/UserEmulator")]
        public IActionResult Customer_UserEmulator(string userCode)
        {

            var user = UsersBR.Singleton().UserGet(CurrentUser, userCode);
            var companies = CompaniesBR.Singleton().CompanyUserList(CurrentUser, null, userCode);
            var sessionUser = new App_Core.Main.Model.SessionUserModel()
            {
                UserCode = user.Code,
                UserEmail = user.Email,
                UserName = user.Name,
                IsCustomer = companies.Count > 0,
                Companies = new HashSet<string>(companies.Select(x => x.CompanyCode).ToList())
            };
            //System.Web.Security.FormsAuthentication.SetAuthCookie(sessionUser.UserEmail, true);

            HttpContext.Session.Set("CurrentUser", sessionUser);

            return Redirect("/");
        }

    }
}
