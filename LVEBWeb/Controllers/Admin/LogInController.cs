﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;
using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public class AdminLogInController : Controller
    {

        [HttpGet("Admin/LogIn")]
        //[HttpGet("Admin/logon")]
        public ActionResult LogIn(string id = "")
        {
            //if (!string.IsNullOrEmpty(CurrentUser.Email))
            //	return RedirectToAction("Index", "Home");
            string url = Configuration.HOST_URL + "/Admin";

            if (Configuration.Environment == "DEV")
            {
                return LogIn(new LogOnModel() { UserName = Configuration.AdminUserEmail, Password = Configuration.AdminUserPassword }, url);
            }
            if (!string.IsNullOrEmpty(id))
            {
                id = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                string[] _p = id.Split('_');
                return LogIn(new LogOnModel() { UserName = _p[0], Password = _p[1] }, url);
            }

            return View();
        }


        [HttpPost("Admin/LogIn")]
        public ActionResult LogIn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var _user = WithOutSessionBR.Singleton().Logon(model.UserName, model.Password);

                switch (_user.Result)
                {
                    case 0:

                        HttpContext.Session.Set("CurrentAdminUser", _user);
                        return RedirectToAction("Index", "Admin");


                    case -1:
                        ModelState.AddModelError("", "User blocked. It has introduced 3 times wrong password, or the user is disabled.");
                        break;
                    case -2:
                        ModelState.AddModelError("", "User is disabled or missing. Please contact the administrator.");
                        break;
                    default:
                        ModelState.AddModelError("", "Access data error.");
                        break;
                }

            }
            return View(model);
        }

        [HttpGet("Admin/logoff")]
        public ActionResult LogOff()
        {
            HttpContext.Session.Remove("CurrentAdminUser");

            return RedirectToAction("Index", "Admin");

        }


        const string EmailRE = @"^(([^<>()\[\]\\.,;:\s@""]+(\.[^<>()\[\]\.,;:\s@""]+)*)|("".+""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$";

        /*
                [HttpGet("PasswordForgot")]
                public IActionResult PasswordForgot()
                {
                    ViewData["EmailRE"] = EmailRE;
                    return View();
                }
                [HttpPost("PasswordForgot")]
                public IActionResult PasswordForgot(PasswordRecoverModel model)
                {


                    var wasOk = WithOutSessionBR.Singleton().PasswordForgot_SendMail(model);
                    ViewData["WasOk"] = wasOk;

                    return View("PasswordForgotConfirmation");

                }



                [HttpGet("PasswordReset/{key}/{email}")]
                public IActionResult PasswordReset(string key, string email)
                {
                    ViewData["Result"] = "";
                    if (WithOutSessionBR.Singleton().PasswordForgot_Check(email, key))
                    {
                        ViewData["Email"] = email;
                        ViewData["Key"] = key;
                        return View();
                    }
                    else
                    {
                        return Redirect("/LogIn");
                    }
                }


                [HttpPost("PasswordReset/{key}/{email}")]
                public IActionResult PasswordReset(PasswordRecoverModel model)
                {
                    ViewData["Result"] = "";
                    if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.PasswordConfirmation))
                    {
                        ViewData["Result"] = "Write a correct password!";
                        return View();
                    }

                    if (model.Password == model.PasswordConfirmation && Functions.MatchGoodPassword(model.Password))
                    {
                        var pass = model.Password;
                        if (WithOutSessionBR.Singleton().PasswordForgot_Change(model))
                        {
                            var _user = WithOutSessionBR.Singleton().Logon(model.Email, pass);
                            if (_user.UserHasAccess())
                            {
                                HttpContext.Session.Set("CurrentUser", _user);
                                return RedirectToAction("Index", "Home");
                            }
                        }

                        ViewData["Result"] = "Sorry, something extra in happen. Try in few minutes.";
                        return View();
                    }
                    else
                    {
                        ViewData["Result"] = "Your password has to be at least 8 characters long. Must contain at least one lower case letter, one upper case letter and one digit.";
                        return View();
                    }
                }


        */



        #region Base


        public AdminLogInController() { }


        #endregion Base

    }
}