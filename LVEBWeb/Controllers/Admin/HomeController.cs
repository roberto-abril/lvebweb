﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {

        [HttpGet("Admin")]
        [HttpGet("Admin/Index")]
        public IActionResult Index()
        {
            return View("Home/Index");
        }


        [HttpPost("Common/CitiesGet/{provinceCode}")]
        public JsonResult CitiesGet(string provinceCode)
        {
            var result = App_Core.Admin.BR.CommonBR.Singleton().CitiesListByProvince(CurrentUser, provinceCode);
            return Json(result);
        }

    }
}
