﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BR;

using LVEBWeb.App_Core.Admin.BE;

namespace LVEBWeb.Controllers.Admin
{
    public partial class AdminController
    {
        [HttpGet("Admin/Tasks/{code}")]
        public ActionResult Tasks(string code = "")
        {
            var model = TasksBR.Singleton().TaskGroupGet(CurrentUser, code);
            //return View("TasksGroups/TasksGroupsEdit", model);
            return View("Tasks/Tasks", model);
        }
        [HttpGet("Admin/Tasks/Next")]
        public JsonResult Tasks_Next(string taskCode, int lastTask)
        {
            var model = TasksBR.Singleton().TaskGetNext(CurrentUser, taskCode, lastTask);
            return Json(model);
        }


        [HttpPost("Admin/Tasks/StatusChange")]
        public JsonResult Tasks_StatusChange(int taskId, int status)
        {
            var result = TasksBR.Singleton().TaskStatusChange(CurrentUser, taskId, status);
            return Json(new JsonResultModel(result));
        }


        [HttpGet("Admin/Tasks/Search")]
        public JsonResult Tasks_Search(GridFilterModel gridModel)
        {
            try
            {
                var items = TasksBR.Singleton().TaskGroupsSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.Tasks_Search");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }



    }
}
