﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class PlaceController : _ControllerBase
    {

        [HttpGet("p/{code}")]
        [HttpGet("p/{code}/{name}")]
        [HttpGet("l/{code}")]
        [HttpGet("l/{code}/{name}")]
        public IActionResult Index(string code, string name)
        {
            var place = _PlaceBR.PlaceGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("p/{code}/eventos/{name}")]
        public IActionResult Events(string code, string name)
        {
            var place = _PlaceBR.PlaceGet(CurrentUser, code);
            place.Events = EventsBR.Singleton(_MemoryCache).EventsByPlace(CurrentUser, code);
            return View(place);
        }
        [HttpGet("p/{code}/fotos/{name}")]
        public IActionResult Photos(string code, string name)
        {
            var place = _PlaceBR.PlaceGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("p/{code}/videos/{name}")]
        public IActionResult Videos(string code, string name)
        {
            var place = _PlaceBR.PlaceGet(CurrentUser, code);
            return View(place);
        }
        [HttpGet("p/{code}/favoritos/{name}")]
        public IActionResult Favorites(string code, string name)
        {
            var place = _PlaceBR.PlaceGet(CurrentUser, code);
            return View(place);
        }

        #region Base
        private static IPlacesBR _PlaceBR;
        IMemoryCache _MemoryCache;

        public PlaceController(IMemoryCache memoryCache)
        {
            _MemoryCache = memoryCache;
            _PlaceBR = PlacesBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
