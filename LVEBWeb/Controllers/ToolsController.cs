﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class ToolsController : _ControllerBase
    {

        [HttpGet("tools/PlaceImprove")]
        public IActionResult PlaceImprove(string type, string placeCode)
        {
            ViewData["Type"] = type;
            switch (type)
            {
                case "localcerrado":
                case "propietario":
                    var place = PlacesBR.Singleton(_MemoryCache).PlaceGet(CurrentUser, placeCode);
                    ViewData["PlaceCode"] = place.Code;
                    ViewData["PlaceName"] = place.Name;
                    break;
            }
            return View();
        }

        [HttpPost("tools/PlaceImprove")]
        public IActionResult PlaceImprove(PlacesToImproveBE model)
        {
            var result = _ToolsBR.PlacesToImprove_Insert(CurrentUser, model);

            return View("ToolframeClose");
        }

        #region Base
        private static IToolsBR _ToolsBR;
        IMemoryCache _MemoryCache;

        public ToolsController(IMemoryCache memoryCache)
        {
            _MemoryCache = memoryCache;
            _ToolsBR = ToolsBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
