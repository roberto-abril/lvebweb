﻿using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Localization;

namespace LVEBWeb.Controllers
{
    public class _ControllerBase : Controller
    {

        protected System.Globalization.CultureInfo UserCulture;
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            ViewData["culture"] = "es";
            UserCulture = new System.Globalization.CultureInfo("es");



            string value;
            var hv = Request.Cookies.TryGetValue("lz", out value); // Location Zip Code
            if (hv)
            {
                CurrentUser.Location.Zip = value;
                hv = Request.Cookies.TryGetValue("lc", out value); // Location City
                CurrentUser.Location.City = value;
                hv = Request.Cookies.TryGetValue("llat", out value); // Location Latitude
                CurrentUser.Location.IPLatitude = Convert.ToDouble(value, System.Globalization.CultureInfo.InvariantCulture);
                hv = Request.Cookies.TryGetValue("llon", out value); // Location Longitude
                CurrentUser.Location.IPLongitude = Convert.ToDouble(value, System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                var aux = FunctionsLocation.UserLocationGet(CurrentUser.UserIP);
                CurrentUser.Location.City = aux.city;
                CurrentUser.Location.Zip = aux.zip;
                CurrentUser.Location.IPLatitude = aux.latitude;
                CurrentUser.Location.IPLongitude = aux.longitude;
            }



            hv = Request.Cookies.TryGetValue("rlat", out value);
            if (hv) CurrentUser.Location.RealLatitude = Convert.ToDouble(value, System.Globalization.CultureInfo.InvariantCulture);
            hv = Request.Cookies.TryGetValue("rlon", out value);
            if (hv) CurrentUser.Location.RealLongitude = Convert.ToDouble(value, System.Globalization.CultureInfo.InvariantCulture);

            if (CurrentUser != null)
            {
                CurrentUser.Location.RLatLonActive = CurrentUser.Location.RealLatitude > 0 || CurrentUser.Location.RealLatitude < 0 || CurrentUser.Location.RealLongitude > 0 || CurrentUser.Location.RealLongitude < 0;
            }
            if (CurrentUser.Location.RLatLonActive)
                CurrentUser.Location.LatLonActive = CurrentUser.Location.RLatLonActive;
            else
                CurrentUser.Location.LatLonActive = CurrentUser.Location.IPLatitude > 0 || CurrentUser.Location.IPLatitude < 0 || CurrentUser.Location.IPLongitude > 0 || CurrentUser.Location.IPLongitude < 0;

            base.OnActionExecuting(context);
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(100);
            /*     Request.Cookies.["lr"] =  CurrentUser.Location.Region;
                Request.Cookies.Append("lc", CurrentUser.Location.City,);
                */
            if (!string.IsNullOrEmpty(CurrentUser.Location.Zip))
            {
                Response.Cookies.Append("lc", CurrentUser.Location.City, option);
                Response.Cookies.Append("lz", CurrentUser.Location.Zip, option);
                Response.Cookies.Append("llat", CurrentUser.Location.IPLatitude.ToString(System.Globalization.CultureInfo.InvariantCulture), option);
                Response.Cookies.Append("llon", CurrentUser.Location.IPLongitude.ToString(System.Globalization.CultureInfo.InvariantCulture), option);
            }

            ViewData["CurrentUser"] = CurrentUser;



            var msg = HttpContext.Session.Get<Models.MSGModel>("MSG");
            ViewData["MSG"] = msg;

            base.OnActionExecuted(context);
        }


        protected void MessageShow(Models.MSGTypes type, string message)
        {
            HttpContext.Session.Set("MSG", new Models.MSGModel()
            {
                Message = message,
                Type = type
            });
        }

        protected void MessageClear()
        {
            HttpContext.Session.Remove("MSG");
        }

        //  protected SessionUserModel CurrentUser = new SessionUserModel();
        #region Variables
        /// <summary>
        /// Contiene el id del usuario en curso.
        /// </summary>
        SessionUserModel _CurrentUser;
        /// <summary>
        /// Obtiene o establece el usuario en curso. Gestionado y almacenado en la Session.
        /// </summary>
        protected SessionUserModel CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {
                    if (HttpContext.Session != null && HttpContext.Session.Get<SessionUserModel>("CurrentUser") != null)
                        _CurrentUser = HttpContext.Session.Get<SessionUserModel>("CurrentUser");
                    else
                    {
                        _CurrentUser = new SessionUserModel();
                        HttpContext.Session.Set("CurrentUser", _CurrentUser);
                    }

                    _CurrentUser.UserIP = UserIPGet();
                }

                return _CurrentUser;
            }
        }
        #endregion

        protected string UserIPGet()
        {
            var ip = HttpContext?.Connection?.RemoteIpAddress?.ToString();
            return ip;
        }
    }
}