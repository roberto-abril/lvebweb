﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using Microsoft.Extensions.Caching.Memory;
using LVEBWeb.App_Core.Main.BR;

namespace LVEBWeb.Controllers
{
    public class CompetitionsController : _ControllerBase
    {
        [HttpGet("Competiciones")]
        [HttpGet("Competitions")]
        public IActionResult Index()
        {
            var competitions = _MainBR.CompetitionsSearch(CurrentUser, new App_Core.Common.GridFilterModel());
            return View(competitions);
        }

        [HttpGet("Competicion/{code}")]
        [HttpGet("Competition/{code}")]
        [HttpGet("Competicion/{code}/{name}")]
        [HttpGet("Competition/{code}/{name}")]
        public IActionResult Details(string code)
        {
            var competitions = _MainBR.CompetitionGet(CurrentUser, code);
            return View(competitions);
        }


        [HttpGet("CompetitionsPoster/{code}")]
        [HttpGet("competicion-poster/{code}")]
        [ResponseCache(Duration = 86400)]
        public FileResult CompetitionsPoster(string code)
        {

            var path = System.IO.Path.Combine(App_Core.Common.Configuration.PathImagesCompetitions, code);
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                string contentType = "image/jpeg";
                return base.File(image, contentType);

            }
            else
                return null;
        }


        #region Base
        private static ICompetitionsBR _MainBR;


        public CompetitionsController(IMemoryCache memoryCache)
        {
            _MainBR = CompetitionsBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
