﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class SearchController : _ControllerBase
    {
        [HttpGet("/")]
        public IActionResult Index0()
        {
            var culture = UserCulture.ToString();
            if (culture != "es" && culture != "en")
                culture = "en";

            Response.Cookies.Append(
               Microsoft.AspNetCore.Localization.CookieRequestCultureProvider.DefaultCookieName,
                Microsoft.AspNetCore.Localization.CookieRequestCultureProvider.MakeCookieValue(new Microsoft.AspNetCore.Localization.RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return Redirect("/h");
        }

        [HttpGet("/es")]
        public IActionResult IndexES()
        {
            var culture = "es";

            Response.Cookies.Append(
               Microsoft.AspNetCore.Localization.CookieRequestCultureProvider.DefaultCookieName,
                Microsoft.AspNetCore.Localization.CookieRequestCultureProvider.MakeCookieValue(new Microsoft.AspNetCore.Localization.RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return Redirect("/h");
        }

        [HttpGet("/en")]
        public IActionResult IndexEN()
        {
            var culture = "en";

            Response.Cookies.Append(
               Microsoft.AspNetCore.Localization.CookieRequestCultureProvider.DefaultCookieName,
                Microsoft.AspNetCore.Localization.CookieRequestCultureProvider.MakeCookieValue(new Microsoft.AspNetCore.Localization.RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return Redirect("/h");
        }


        //[HttpGet("/en")]
        [HttpGet("h")]
        [HttpGet("places")]
        [HttpGet("salir-a-bailar")]
        [HttpGet("go-dancing")]
        [HttpGet("salir-a-bailar/cerca")]
        [HttpGet("aprender-a-bailar")]
        [HttpGet("aprender-a-bailar/cerca")]
        [HttpGet("eventos-fiestas")]
        [HttpGet("eventos-fiestas/cerca")]
        [HttpGet("cerca")]
        public IActionResult Index(string q)
        {
            var sfm = new SearchFilterModel() { CenterLatitude = CurrentUser.Location.Latitude(), CenterLongitude = CurrentUser.Location.Longitude() };

            switch (HttpContext.Request.Path.Value)
            {
                case "/salir-a-bailar":
                case "/go-dancing":
                case "/salir-a-bailar/cerca":
                case "/go-dancing/nearby":
                    sfm.Page = SearchSections.DanceHall;
                    ViewData["CurrentPage"] = "DanceHall";
                    break;
                case "/aprender-a-bailar":
                case "/aprender-a-bailar/cerca":
                    sfm.Page = SearchSections.DanceSchool;
                    ViewData["CurrentPage"] = "DanceSchool";
                    break;
                case "/eventos-fiestas":
                    sfm.Page = SearchSections.Events;
                    ViewData["CurrentPage"] = "Events";
                    break;
                default:
                    ViewData["CurrentPage"] = "";
                    break;
            }
            var hm = new SearchModel() { Page = sfm.Page };


            if (string.IsNullOrEmpty(q))
            {
                hm.Items = _SearchBR.SearchByBox(CurrentUser, sfm);
            }
            else
            {
                ViewData["q"] = q;
                hm.Items = _SearchBR.SearchFulltext(CurrentUser, sfm.Page, q);
            }

            return View("Index", hm);
        }

        [HttpGet("AutocompleteSearch/{value}")]
        public JsonResult AutocompleteSearch(string value, string referal)
        {
            var page = SearchSections.Home;
            switch (referal)
            {
                case "DanceHall":
                    page = SearchSections.DanceHall;
                    break;
                case "DanceSchool":
                    page = SearchSections.DanceSchool;
                    break;
                case "Events":
                    page = SearchSections.Events;
                    break;
            }
            var items = _SearchBR.AutocompleteSearch(CurrentUser, page, value);
            return Json(items);
        }


        #region Base
        private static IPlacesBR _PlaceBR;
        private static ISearchBR _SearchBR;


        public SearchController(IMemoryCache memoryCache)
        {
            _PlaceBR = PlacesBR.Singleton(memoryCache);
            _SearchBR = SearchBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
