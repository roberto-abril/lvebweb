﻿using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;

using LVEBWeb.App_Core.API.BR;
namespace LVEBWeb.Controllers.API
{

    public class _PublicController : Controller
    {
        [HttpGet("api")]
        public string API()
        {
            var info = "LVEB API\nv0.1\n" + System.Environment.MachineName;

            var fileGitCommit = App_Core.Common.Configuration.PathRoot("_apiversion.txt");
            if (System.IO.File.Exists(fileGitCommit))
                info += "\n" + System.IO.File.ReadAllText(fileGitCommit);

            return info;
        }
        [HttpGet("validate/p/{validatorCode}")]
        public ActionResult PV(string validatorCode)
        {
            var result = PublicBR.Singleton().PlaceSentValidate(validatorCode);
            return View();
        }

        [HttpGet("validate/e/{validatorCode}")]
        public ActionResult EV(string validatorCode)
        {
            var result = PublicBR.Singleton().EventSentValidate(validatorCode);
            return View();
        }


        #region Base
        public _PublicController()
        {

        }
        #endregion
    }
}