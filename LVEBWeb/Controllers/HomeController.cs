﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class HomeController : _ControllerBase
    {

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpGet("privacy-policy")]
        [HttpGet("politica-privacidad")]
        public IActionResult Privacity(string ln = "es")
        {
            return View();
        }

        [HttpGet("terms-of-use")]
        [HttpGet("termino-y-condiciones")]
        public IActionResult TermsOfuse(string ln = "es")
        {
            return View();
        }

        [HttpGet("msgok")]
        public string MessageOk()
        {
            MessageClear();
            return "ok";
        }
        [HttpGet("info")]
        public string Info()
        {
            return "";
        }

        [HttpGet("pi/{code}")]
        [ResponseCache(Duration = 86400)]
        public FileResult PlaceImage(string code)
        {
            var path = System.IO.Path.Combine(App_Core.Common.Configuration.PathImagePlaces(code));
            if (System.IO.File.Exists(path))
            {
                // var entityTag = new EntityTagHeaderValue("\"CalculatedEtagValue\"");

                var image = System.IO.File.ReadAllBytes(path);
                string contentType = "image/jpeg";
                var file = base.File(image, contentType, code + ".jpg");
                file.LastModified = DateTime.UtcNow.AddSeconds(-5);
                // entityTag: new Microsoft.Net.Http.Headers.EntityTagHeaderValue("\"MyCalculatedEtagValue\""));
                return file;
            }
            else
                return null;
        }
        [Route("sitemap.xml")]
        public ActionResult SitemapXml()
        {
            //var sitemapNodes = CommonBR.GetSitemapNodes();
            string xml = _CommonBR.SitemapGet();
            return this.Content(xml, "text/xml", System.Text.Encoding.UTF8);
        }

        #region Base
        private static ICommonBR _CommonBR;


        public HomeController(IMemoryCache memoryCache)
        {
            _CommonBR = CommonBR.Singleton();

        }
        #endregion Base

    }
}
