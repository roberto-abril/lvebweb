﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using LVEBWeb.App_Core.Main.BE;
using Microsoft.Extensions.Caching.Memory;
using LVEBWeb.App_Core.Common;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace LVEBWeb.Controllers
{
    public class ManagerController : _ControllerBaseRegister
    {

        [HttpGet("manager")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("manager/places")]
        public IActionResult Places()
        {
            return View();
        }

        [HttpGet("manager/places/Search")]
        public JsonResult PlacesSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = _ManagerBR.PlaceSearch(CurrentUser);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }

        [HttpGet("manager/events")]
        public IActionResult Events()
        {
            return View();
        }

        [HttpGet("manager/events/Search")]
        public JsonResult EventsSearch(GridFilterModel gridModel)
        {
            try
            {
                var items = _ManagerBR.EventsSearch(CurrentUser, gridModel);
                var jsonData = new
                {
                    total = items.Count,
                    page = gridModel.PN,
                    records = gridModel.TRs,
                    rows = items
                };

                return Json(jsonData);

            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "AdminController.UsersSelect");
                return Json(new
                {
                    total = 0,
                    page = 0,
                    records = 0
                });
            }
        }



        [HttpGet("manager/events/new")]
        public IActionResult EventNew(string pc)
        {
            if (string.IsNullOrEmpty(pc))
                return RedirectToAction("Events");
            var model = new EventBE()
            {
                PlaceCode = pc,
                DateStart = DateTime.Now.AddDays(5),
                Category = 250001
            };

            ViewData["Categories"] = CommonBR.Singleton().ValuesList(DICTables.EventCategories);
            return View(model);
        }

        [HttpPost("manager/events/new")]
        public IActionResult EventNew(EventBE model, IFormFile file)
        {
            if (!ModelState.IsValid)
                return View(model);

            ViewData["Categories"] = CommonBR.Singleton().ValuesList(DICTables.EventCategories);
            if (_ManagerBR.EventInsert(CurrentUser, model))
            {
                return Redirect("/manager/events/edit/" + model.Code);
            }
            return View(model);
        }

        [HttpGet("manager/events/edit/{code}")]
        public IActionResult EventEdit(string code)
        {
            var model = _ManagerBR.EventGet(CurrentUser, code);
            model.Code = code;

            ViewData["Categories"] = CommonBR.Singleton().ValuesList(DICTables.EventCategories);
            return View(model);
        }

        [HttpPost("manager/events/edit/{code}")]
        public IActionResult EventEdit(string code, EventBE model, IFormFile file)
        {
            if (!ModelState.IsValid)
                return View(model);
            if (model.DateStart < DateTime.Now)
            {
                ModelState.TryAddModelError("DateStart", "La fecha ha de ser futura.");
            }
            else
            {
                if (file != null && file.Length > 0)
                {
                    var pathLogo = Configuration.PathImagesEvents(model.PlaceCode, model.Code);
                    using (var stream = new FileStream(pathLogo, FileMode.Create))
                    {
                        file.CopyToAsync(stream);
                        Functions.ImagesCreateForEvents(pathLogo);
                    }
                    model.HavePoster = true;
                }
                if (!_ManagerBR.EventUpdate(CurrentUser, model))
                {
                    ModelState.TryAddModelError("Error", "Error al guardar los datos.");
                }

            }


            ViewData["Categories"] = CommonBR.Singleton().ValuesList(DICTables.EventCategories);
            return View(model);
        }


        [HttpGet("manager/events/img/{placeCode}/{code}")]
        [ResponseCache(Duration = 86400)]
        public FileResult EventPhoto(string placeCode, string code)
        {
            var path = App_Core.Common.Configuration.PathImagesEvents(placeCode, code) + "100";
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                string contentType = "image/jpeg";
                return base.File(image, contentType);
            }
            else
                return null;
        }



        #region Base
        private static IManagerBR _ManagerBR;


        public ManagerController(IMemoryCache memoryCache)
        {
            _ManagerBR = ManagerBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
