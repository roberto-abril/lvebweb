﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class UserController : _ControllerBaseRegister
    {

        [HttpGet("u")]
        public IActionResult Index()
        {
            ViewData["Action"] = "";
            return View(CurrentUser);
        }

        [HttpPost("u/registersend")]
        public IActionResult ManagerGetAccess(string message)
        {
            App_Core.Common.EmailsBR.Singleton().SendUserGetAccess(CurrentUser.UserCode, message);
            ViewData["Action"] = "ManagerGetAccess";
            return View("Index", CurrentUser);
        }



        #region Base
        private static IUserBR _MainBR;


        public UserController(IMemoryCache memoryCache)
        {
            _MainBR = UserBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
