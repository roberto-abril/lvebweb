﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.Models;
using LVEBWeb.App_Core.Main.Model;

using LVEBWeb.App_Core.Main.BR;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers
{
    public class UserRegisterController : _ControllerBase
    {

        [HttpGet("ur/LogIn")]
        public IActionResult LogIn()
        {
            return View();
        }

        [HttpGet("ur/logoff")]
        public IActionResult Logoff()
        {
            HttpContext.Session.Remove("CurrentUser");
            HttpContext.Session.Remove("UserRegister");

            return Redirect("/");

        }


        [HttpGet("ur/register")]
        public IActionResult Register()
        {
            var model = HttpContext.Session.Get<UserRegisterModel>("UserRegister");
            return View(model);
        }

        [HttpPost("ur/register")]
        public IActionResult Register(UserRegisterModel model)
        {
            var ur = HttpContext.Session.Get<UserRegisterModel>("UserRegister");
            ur.UserEmail = model.UserEmail;
            if (string.IsNullOrEmpty(model.UserName))
                ur.UserName = ur.UserEmail;
            else
                ur.UserName = model.UserName;

            if (_MainBR.UserRegister(CurrentUser, ur) != null)
            {
                var user = _MainBR.UserGetSessionModel(CurrentUser, ur.UserCode);
                HttpContext.Session.Set("CurrentUser", user);
                return View("WindowsReload");
            }
            else
            {
                ModelState.TryAddModelError("Email", "El email indicado esta en uso, indica otro email.");
            }
            return View(model);
        }



        [HttpGet("ur/register/fbend")]
        public IActionResult RegisterFBEnd(string userID, string name)
        {
            var userCode = _MainBR.UserCodeByConnectorId(CurrentUser, userID);
            if (userCode.Length > 0)
            {
                var user = _MainBR.UserGetSessionModel(CurrentUser, userCode);

                HttpContext.Session.Set("CurrentUser", user);

                return View("WindowsReload");
            }
            else
            {

                HttpContext.Session.Set("UserRegister", new UserRegisterModel()
                {
                    Connector = "google",
                    //  ConnectorToken = accessToken,
                    ConnectorId = userID,
                    UserName = name
                });


                return RedirectToAction("Register");
            }
        }


        [HttpGet("ur/register/googlestart")]
        public IActionResult RegisterGoogleStart()
        {
            return Redirect(App_Core.Common.RegisterGoogle.Singleton().UrlLogin());
        }

        [HttpGet("ur/register/googleend")]
        public IActionResult RegisterGoogleEnd(string code, string scope)
        {
            var up = App_Core.Common.RegisterGoogle.Singleton().UserProfileGet(code);
            if (string.IsNullOrEmpty(up.sub))
                return RedirectToAction("WindowsClose");

            var userCode = _MainBR.UserCodeByConnectorId(CurrentUser, up.sub);

            if (userCode.Length > 0)
            {
                var user = _MainBR.UserGetSessionModel(CurrentUser, userCode);

                HttpContext.Session.Set("CurrentUser", user);

                return View("WindowsReload");
            }
            else
            {

                HttpContext.Session.Set("UserRegister", new UserRegisterModel()
                {
                    Connector = "google",
                    ConnectorToken = code,
                    ConnectorId = up.sub,
                    UserName = up.name,
                    UserImage = up.picture
                });


                return RedirectToAction("WindowsClose");
            }
        }

        [HttpGet("ur/close")]
        public IActionResult WindowsClose()
        {
            return View();
        }


        [HttpGet("ur/vm")]
        public IActionResult ValidateEmail(string e, string t)
        {
            _MainBR.UserValidateEmail(CurrentUser, e, t);
            MessageShow(Models.MSGTypes.success, "Email " + e + " validado. Gracias. ");
            return Redirect("/");
        }



        #region Base
        private static IUserRegisterBR _MainBR;


        public UserRegisterController(IMemoryCache memoryCache)
        {
            _MainBR = UserRegisterBR.Singleton(memoryCache);

        }
        #endregion Base

    }
}
