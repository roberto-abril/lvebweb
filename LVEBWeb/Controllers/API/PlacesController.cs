﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.App_Core.API.BR;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.Models;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers.API
{
    public partial class PlacesController : _APIControllerBase
    {
        [HttpPost("api/Place/Get")]
        public JsonResult PlaceGet(SessionAPIModel user, SearchFilterModel filter)
        {
            var aux = _PlacesBR.PlaceGet(user, filter);
            return Json(aux);
        }

        [HttpPost("api/Place/Improve")]
        public IActionResult PlaceImprove(SessionAPIModel user, PlaceToImproveModel model)
        {
            var result = _PlacesBR.PlacesToImprove_Insert(user, model);

            return Json(new JsonResultModel(result));
        }
        #region Base
        private static PlacesBR _PlacesBR;

        public PlacesController(IMemoryCache memoryCache)
        {
            _PlacesBR = PlacesBR.Singleton(memoryCache);
        }
        #endregion
    }
}
