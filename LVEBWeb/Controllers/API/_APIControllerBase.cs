﻿using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Localization;

namespace LVEBWeb.Controllers.API
{
    public class _APIControllerBase : Controller
    {

        protected System.Globalization.CultureInfo UserCulture;
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {

            base.OnActionExecuted(context);
        }
        protected void WriteLog(string log)
        {
            Functions.ConsoleWriteLine(log);
        }


        //  protected SessionUserModel CurrentUser = new SessionUserModel();
        #region Variables
        /// <summary>
        /// Contiene el id del usuario en curso.
        /// </summary>
        SessionUserModel _CurrentUser;
        /// <summary>
        /// Obtiene o establece el usuario en curso. Gestionado y almacenado en la Session.
        /// </summary>
        protected SessionUserModel CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {
                    if (HttpContext.Session != null && HttpContext.Session.Get<SessionUserModel>("CurrentUser") != null)
                        _CurrentUser = HttpContext.Session.Get<SessionUserModel>("CurrentUser");
                    else
                    {
                        _CurrentUser = new SessionUserModel();
                        HttpContext.Session.Set("CurrentUser", _CurrentUser);
                    }

                    _CurrentUser.UserIP = UserIPGet();
                }

                return _CurrentUser;
            }
        }
        #endregion

        protected string UserIPGet()
        {
            var ip = HttpContext?.Connection?.RemoteIpAddress?.ToString();
            return ip;
        }
    }
}