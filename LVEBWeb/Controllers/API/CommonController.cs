﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.App_Core.API.BR;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.Models;

namespace LVEBWeb.Controllers.API
{
    // [LVEBWeb.Helpers.RedirectingActionSecureAttribute]
    public partial class CommonController : _APIControllerBase
    {

        [HttpPost("api/DLU")]
        public JsonResult DataLastUpdateList(SessionAPIModel user)
        {
            var aux = _CommonBR.DataLastUpdateList(user);
            return Json(aux);
        }

        //[HttpGet("api/CommonLocation")]
        [HttpPost("api/CommonLocation")]
        public JsonResult CommonLocation(SessionAPIModel user, long dateLastUpdate = 0)
        {
            var aux = _CommonBR.CommonLocationGet(user, dateLastUpdate);
            return Json(aux);
        }

        [HttpPost("api/CommonDICs")]
        public JsonResult CommonDICGet(SessionAPIModel user, long dateLastUpdate = 0)
        {
            var aux = _CommonBR.CommonDICGet(user, dateLastUpdate);
            return Json(aux);
        }
        [HttpPost("api/CommonDICsValues")]
        public JsonResult CommonDICGetValues(SessionAPIModel user, long dateLastUpdate = 0)
        {
            var aux = _CommonBR.CommonDICGetValues(user, dateLastUpdate);
            return Json(aux);
        }



        #region Base
        private static CommonBR _CommonBR;

        public CommonController()
        {
            _CommonBR = CommonBR.Singleton();
        }
        #endregion
    }
}
