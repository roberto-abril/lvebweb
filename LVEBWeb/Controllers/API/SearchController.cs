﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.App_Core.API.BR;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.Models;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers.API
{
    public partial class SearchController : _APIControllerBase
    {

        [HttpPost("api/Search/ByLocation")]
        [HttpPost("api/Search/FullText")]
        [HttpPost("api/Search")]
        public JsonResult SearchByBox(SessionAPIModel user, SearchFilterModel filter)
        {
            WriteLog("api/Search: " + filter.Section + " " + filter.Query + " " + filter.Latitude + " " + filter.Longitude + ", CityCode:" + filter.CityCode);

            List<SearchItemModel> items;
            if (string.IsNullOrEmpty(filter.Query))
            {
                if (filter.LocationActive)
                    items = _SearchBR.SearchByBox(user, filter);
                else
                    items = _SearchBR.SearchFullText(user, filter);
            }
            else
            {
                items = _SearchBR.SearchFullText(user, filter);
            }

            return Json(items);
        }


        #region Base
        private static ISearchBR _SearchBR;


        public SearchController(IMemoryCache memoryCache)
        {
            _SearchBR = SearchBR.Singleton(memoryCache);

        }
        #endregion Base
    }
}
