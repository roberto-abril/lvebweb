﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.App_Core.API.BR;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.Models;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers.API
{
    // [LVEBWeb.Helpers.RedirectingActionSecureAttribute]
    public partial class APIControllerOLD : _APIControllerBase
    {

        [HttpPost("api/PlacesByProvince/{province}")]
        [HttpPost("api/Places")]
        public JsonResult PlacesByProvince(SessionAPIModel user, string province = "", long dateLastUpdate = 0)
        {
            var aux = PlacesBR.Singleton(_MemoryCache).PlacesList(user, province, dateLastUpdate);
            return Json(aux);
        }
        [HttpGet("api/PlaceLogo/{code}")]
        [HttpGet("api/PlaceLogo/{code}/{size}")]
        [ResponseCache(Duration = 86400)]
        public FileResult PlaceLogo(string code, string size = "")
        {
            var path = App_Core.Common.Configuration.PathImagePlaces(code + size);
            if (!System.IO.File.Exists(path))
                path = System.IO.Path.Combine(App_Core.Common.Configuration.PathRoot("wwwroot", "images"), "_empty.jpeg");

            var image = System.IO.File.ReadAllBytes(path);
            string contentType = "image/jpeg";
            return base.File(image, contentType);



        }

        [HttpPost("api/PlaceSentToCheck")]
        public JsonResult PlaceSentToCheck(SessionAPIModel user, PlaceToCheckBE place)
        {
            var aux = PlacesBR.Singleton(_MemoryCache).PlaceToCheckInsert(user, place);
            return Json(new JsonResultModel(aux));
        }

        [HttpPost("api/Events/{province}")]
        [HttpPost("api/Events")]
        public JsonResult Events(SessionAPIModel user, string province = "", long dateLastUpdate = 0)
        {
            var aux = EventsBR.Singleton(_MemoryCache).EventsList(user, province, dateLastUpdate);
            return Json(aux);
        }

        [HttpPost("api/EventSentToCheck")]
        public JsonResult EventSentToCheck(SessionAPIModel user, EventToCheckBE activity)
        {
            var aux = EventsBR.Singleton(_MemoryCache).EventToCheckInsert(user, activity);
            return Json(new JsonResultModel(aux));
        }

        [HttpPost("api/Competitions")]
        public JsonResult Competitions(SessionAPIModel user, long dateLastUpdate = 0)
        {
            var aux = CompetitionsBR.Singleton().CompetitionsList(user, dateLastUpdate);
            return Json(aux);
        }
        [HttpGet("api/CompetitionPoster/{code}")]
        [HttpGet("api/CompetitionPoster/{code}/{size}")]
        [ResponseCache(Duration = 86400)]
        public FileResult CompetitionPoster(string code, string size = "")
        {
            var path = System.IO.Path.Combine(App_Core.Common.Configuration.PathImagesCompetitions, code + size);
            if (!System.IO.File.Exists(path))
                path = System.IO.Path.Combine(App_Core.Common.Configuration.PathRoot("wwwroot", "images"), "_empty.jpeg");

            var image = System.IO.File.ReadAllBytes(path);
            string contentType = "image/jpeg";
            return base.File(image, contentType);



        }


        [HttpPost("api/SendMail")]
        public JsonResult SendMail(string subject, string message)
        {
            return null;
            //var result = AVAPIAdmin.App_Core.Common.EmailsBR.Singleton().SendInternalMessage(CurrentUser.CustomerId, CurrentUser.UserEmail, subject, message);
            //return Json(new JsonResultModel(result));
        }
        [HttpPost("api/InternalMessageInsert")]
        public JsonResult InternalMessageInsert(SessionAPIModel user, App_Core.API.BE.InternalMessageBE value)
        {
            var aux = InternalMessageBR.Singleton().InternalMessageInsert(user, value);
            return Json(new Models.JsonResultModel(aux));
        }



        #region Base
        private static CommonBR _CommonBR;
        IMemoryCache _MemoryCache;

        public APIControllerOLD(IMemoryCache memoryCache)
        {
            _MemoryCache = memoryCache;
            _CommonBR = CommonBR.Singleton();
        }
        #endregion
    }
}
