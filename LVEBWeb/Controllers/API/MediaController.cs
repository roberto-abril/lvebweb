﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using LVEBWeb.App_Core.API.BR;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.Models;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.Controllers.API
{
    public class MediaController : _APIControllerBase
    {

        [HttpGet("api/media/cp/{code}")]
        [ResponseCache(Duration = 86400)]
        public FileResult MediaCompetitionsPoster(string code)
        {

            string contentType = "image/jpeg";
            var path = System.IO.Path.Combine(App_Core.Common.Configuration.PathImagesCompetitions, code);
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                return base.File(image, contentType);
            }
            else
                return null;
            //  return base.File(App_Core.Common.Configuration.ImageEmpty, contentType);
        }
        [HttpGet("api/media/pi/{code}")]
        [ResponseCache(Duration = 86400)]
        public FileResult MediaPlaceImage(string code)
        {
            string contentType = "image/jpeg";
            var path = System.IO.Path.Combine(App_Core.Common.Configuration.PathImagePlaces(code));
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                return base.File(image, contentType);

            }
            else
                return null;
            // return base.File(App_Core.Common.Configuration.ImageEmpty, contentType);
        }
        [ResponseCache(Duration = 86400)]
        [HttpGet("api/media/ei/{placeCode}/{code}")]
        public FileResult MediaEventPhoto(string placeCode, string code)
        {
            string contentType = "image/jpeg";
            var path = App_Core.Common.Configuration.PathImagesEvents(placeCode, code);
            if (System.IO.File.Exists(path))
            {
                var image = System.IO.File.ReadAllBytes(path);
                return base.File(image, contentType);
            }
            else
            {
                path = App_Core.Common.Configuration.PathImagePlaces(placeCode);
                if (System.IO.File.Exists(path))
                {
                    var image = System.IO.File.ReadAllBytes(path);
                    return base.File(image, contentType);
                }
                else
                    return null;
                //return base.File(App_Core.Common.Configuration.ImageEmpty, contentType);
            }
        }
        #region Base
        // private static ISearchBR _SearchBR;


        public MediaController(IMemoryCache memoryCache)
        {
            // _SearchBR = SearchBR.Singleton(memoryCache);

        }
        #endregion Base
    }
}
