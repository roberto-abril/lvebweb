﻿using System;
using Newtonsoft.Json;
namespace LVEBWeb.Models
{
    public class JsonResultModel
    {
		public JsonResultModel(dynamic data)
        {
            WasOk = true;
			Data = data;
        }
        public JsonResultModel(bool wasOk)
        {
            WasOk = wasOk;
            if (!wasOk)
                Message = "Unexpected error, try it later.";
        }
        public JsonResultModel(bool wasOk, string message)
        {
            WasOk = wasOk;
            Message = message;
        }
        public string Message = "";
        public bool WasOk;
        
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public dynamic Data;
    }
}
