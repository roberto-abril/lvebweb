﻿using System;
namespace LVEBWeb.Models
{
    public class MSGModel
    {
        public string Message;
        public MSGTypes Type = MSGTypes.None;
    }
    public enum MSGTypes
    {
        None,
        warning,
        success,
        danger,
        primary
    }
}
