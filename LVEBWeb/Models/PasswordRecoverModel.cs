﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LVEBWeb.Models
{
    public class PasswordRecoverModel
    {
        [Required]
        public string Email { get; set; }

        public string Key { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        public string PasswordConfirmation { get; set; }


        public bool RememberMe { get; set; }
    }
}