﻿function PageNewTab(url) {
    window.open(url, '_blank');
    window.focus();
};
function PageLoad(url) {
    window.location = url;
};

function wlogs(log) {
    console.log(log);
}
function Info(cb) {
    $.get( '/info', 
    function(data) {
        if(cb)
            cb(data);
    }, 'json' );
}
function InfoInterval() {
    setInterval(Info,60000);
}

function DICLoadCities(id,province) {
    $.post( '/Common/CitiesGet/'+province, {}, 
    function(data) {
        $("#"+id).html('');
        for(var x = 0; x < data.length; x++){
            $("#"+id).append('<option value="'+data[x].code+'">'+data[x].name+'</option>');
        } 
    },
    'json' 
    );
}

function FormatDate(date) {
    if(date == 0) return "0";
    date = date.toString();
    return date.substr(6,2)+'/'+date.substr(4,2)+'/'+date.substr(0,4);
}



function TabStart(tabId) {
    $("#"+ tabId + " a").click(function(){TabChange(this,tabId)});
}
function TabChange(tab,tabId) {
    $("#"+ tabId + " a").removeClass('active');
    $(tab).addClass('active');
    $("."+ tabId + " > div").hide();

    $("."+ tab.id).show();
}