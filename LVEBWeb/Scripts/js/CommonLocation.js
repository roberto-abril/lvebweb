﻿function LocationGet(){
    if ("geolocation" in navigator){ //check Geolocation available 
        //try to get user current location using getCurrentPosition() method
        navigator.geolocation.getCurrentPosition(function(position){ 
            console.log("Found your location \nLat : "+position.coords.latitude+" \nLang :"+ position.coords.longitude);
        },function(error){
            console.log(error);
        },{timeout:10000});
    }else{
        console.log("Geolocation not available!");
    }
}