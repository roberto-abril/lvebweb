﻿(function (global, factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = global.document ? factory(global, true) : function (w) {
            if (!w.document) {
                throw new Error("Tripleando requires a window with a document");
            }
            return factory(w);
        };
    } else {
        factory(global);
    }
    // Format function used for this script.
    if (!String.prototype.vxf) {
        String.prototype.vxf = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
        };
    }
}(typeof window !== "undefined" ? window : this, function (window, noGlobal) {
    TD = document;
    AIG = {
        CookieSet: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            TD.cookie = cname + "=" + cvalue + "; " + expires;
     },
     CookieGet: function (cname) {
         var name = cname + "=";
         var ca = TD.cookie.split(';');
         for (var i = 0; i < ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') {
                 c = c.substring(1);
             }
             if (c.indexOf(name) != -1) {
                 return c.substring(name.length, c.length);
             }
         }
         return "";
     },
     CookieClear: function () {
            TD.cookie.setCookie('vxpoverh', '');
            TD.cookie.setCookie('vxlbtn', '');
        },
        // return the current time all as a number
        Now: function () {
            return (new Date()).toISOString().slice(0, 19).replace(/-|:|T/g, "");
        },
        // is the Common Configuration, url, host, lanauge ut, and the list of maps and lse.
        Conf: {},
        PageBack: function() {
         window.history.go(-1);
    },
    PageLoad: function(url) {
         window.location = url;
    },
    PageLoadNewTab:function(url) {
         window.open(url, '_blank');
         window.focus();
    },
    PageReload: function(key) {
         if(key){
             var ax = window.location.href.split('#');
             window.location = ax[0]+key;
             window.location.reload(false);
         }
         else
             window.location.reload(false);
    },
    Replace :function(text,searchValue,replaceValue) { 
         var reg = new RegExp( searchValue, 'g' ); 
         return text.replace(reg,replaceValue);
    },
    Split :function(val) {
        return val.split( /,\s*/ );
    },
    ExtractLast: function ( term ) {
        return AIG.Split( term ).pop();
    },
    LocationGet: function(cb){
        if ("geolocation" in navigator){ //check Geolocation available 
            navigator.geolocation.getCurrentPosition(function(position){ 
            console.log(position);
                cb({NActive:true,LActive:true,Latitude:position.coords.latitude,Longitude:position.coords.longitude});
                //console.log("Found your location \nLat : "+position.coords.latitude+" \nLang :"+ position.coords.longitude);
            },function(e){
                cb({NActive:false});
            },{timeout:10000});
        }else{
            cb({NActive:false});
        }
    },
    Istouch : function() {
                return 'ontouchstart' in window        // works on most browsers
                    || !!navigator.maxTouchPoints;       // works on IE10/11 and Surface
    }};
    return AIG;
}));



