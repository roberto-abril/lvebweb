﻿

CREATE TABLE public.adminusers
(
    userid SERIAL NOT NULL,
    username text COLLATE pg_catalog."default" NOT NULL,
    userpassword text COLLATE pg_catalog."default" NOT NULL,
    useremail text COLLATE pg_catalog."default" NOT NULL,
    roles text COLLATE pg_catalog."default" NOT NULL,
    updatedate bigint NOT NULL DEFAULT '201801010000'::bigint,
    updateuser integer NOT NULL DEFAULT 0,
    CONSTRAINT adminusers_pkey PRIMARY KEY (userid),
    CONSTRAINT adminusers_useremail_key UNIQUE (useremail)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.adminusers
    OWNER to dm;
    
    
