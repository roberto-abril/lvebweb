--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Debian 11.4-1)
-- Dumped by pg_dump version 11.1

-- Started on 2019-08-24 17:16:45 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 214 (class 1259 OID 16996)
-- Name: eventstocheck; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.eventstocheck (
    id integer NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(250) NOT NULL,
    description text NOT NULL,
    addcitycode character(5) NOT NULL,
    address character varying(250) NOT NULL,
    placecode character varying(20) NOT NULL,
    datestart timestamp(4) without time zone NOT NULL,
    duration integer NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    latitude real DEFAULT 0 NOT NULL,
    longitude real DEFAULT 0 NOT NULL,
    reporter_name character varying(50) DEFAULT ''::character varying NOT NULL,
    reporter_email character varying(250) DEFAULT ''::character varying NOT NULL,
    reporter_phone character varying(15) DEFAULT ''::character varying NOT NULL,
    reporter_relation character varying(50) DEFAULT ''::character varying NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    validatorcode character varying(50) DEFAULT ''::character varying NOT NULL,
    statusdate date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    checkstatus integer DEFAULT 0 NOT NULL,
    checkstatusdate date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.eventstocheck OWNER TO dm;

--
-- TOC entry 213 (class 1259 OID 16994)
-- Name: activities_tmp_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.activities_tmp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activities_tmp_id_seq OWNER TO dm;

--
-- TOC entry 3941 (class 0 OID 0)
-- Dependencies: 213
-- Name: activities_tmp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.activities_tmp_id_seq OWNED BY public.eventstocheck.id;


--
-- TOC entry 229 (class 1259 OID 17492)
-- Name: adminusers; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.adminusers (
    userid integer NOT NULL,
    username text NOT NULL,
    userpassword text NOT NULL,
    useremail text NOT NULL,
    roles text NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.adminusers OWNER TO dm;

--
-- TOC entry 226 (class 1259 OID 17444)
-- Name: userslogs; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.userslogs (
    id integer NOT NULL,
    email text NOT NULL,
    success boolean NOT NULL,
    dateinsert date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.userslogs OWNER TO dm;

--
-- TOC entry 231 (class 1259 OID 17513)
-- Name: adminuserslogs_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.adminuserslogs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.adminuserslogs_id_seq OWNER TO dm;

--
-- TOC entry 3942 (class 0 OID 0)
-- Dependencies: 231
-- Name: adminuserslogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.adminuserslogs_id_seq OWNED BY public.userslogs.id;


--
-- TOC entry 230 (class 1259 OID 17504)
-- Name: adminuserslogs; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.adminuserslogs (
    id integer DEFAULT nextval('public.adminuserslogs_id_seq'::regclass) NOT NULL,
    email text NOT NULL,
    success boolean NOT NULL,
    dateinsert date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.adminuserslogs OWNER TO dm;

--
-- TOC entry 223 (class 1259 OID 17413)
-- Name: companies; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.companies (
    code text NOT NULL,
    name text,
    businessname text,
    vatnumber text,
    type integer DEFAULT 210000 NOT NULL,
    status integer DEFAULT 220001 NOT NULL,
    addcitycode text,
    addprovincecode text,
    addstreet text,
    addstreetnumber text,
    addcp text,
    latitude real NOT NULL,
    longitude real NOT NULL,
    contactweb text,
    contactphone text,
    contactemail text,
    contactothers text,
    notes text,
    extra1 text,
    extra2 text,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.companies OWNER TO dm;

--
-- TOC entry 233 (class 1259 OID 17915)
-- Name: companiesplaces; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.companiesplaces (
    companycode text NOT NULL,
    placecode text NOT NULL,
    companyplacetype integer NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.companiesplaces OWNER TO dm;

--
-- TOC entry 227 (class 1259 OID 17454)
-- Name: companiesusers; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.companiesusers (
    companycode text NOT NULL,
    usercode text NOT NULL,
    companyusertype integer NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.companiesusers OWNER TO dm;

--
-- TOC entry 196 (class 1259 OID 16395)
-- Name: competitions; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.competitions (
    code text NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    modalities text NOT NULL,
    oficialsite text NOT NULL,
    oficialsiteurl text NOT NULL,
    datestart date NOT NULL,
    dateend date NOT NULL,
    addprovincecode text NOT NULL,
    addressname text NOT NULL,
    addresslink text NOT NULL,
    latitude real DEFAULT 0 NOT NULL,
    longitud real DEFAULT 0 NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    haveposter boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    status integer DEFAULT 190001 NOT NULL
);


ALTER TABLE public.competitions OWNER TO dm;

--
-- TOC entry 197 (class 1259 OID 16406)
-- Name: datalastupdate; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.datalastupdate (
    type text NOT NULL,
    datelastupdate bigint NOT NULL
);


ALTER TABLE public.datalastupdate OWNER TO dm;

--
-- TOC entry 198 (class 1259 OID 16412)
-- Name: diccities; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.diccities (
    countrycode text NOT NULL,
    provincecode text NOT NULL,
    code text NOT NULL,
    name text,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.diccities OWNER TO dm;

--
-- TOC entry 199 (class 1259 OID 16418)
-- Name: dicprovinces; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.dicprovinces (
    countrycode text NOT NULL,
    code text NOT NULL,
    name text,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.dicprovinces OWNER TO dm;

--
-- TOC entry 200 (class 1259 OID 16424)
-- Name: dictables; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.dictables (
    tablecode integer NOT NULL,
    tablename text NOT NULL
);


ALTER TABLE public.dictables OWNER TO dm;

--
-- TOC entry 201 (class 1259 OID 16430)
-- Name: dictablevalues; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.dictablevalues (
    code integer NOT NULL,
    tablecode integer NOT NULL,
    name text,
    updateuser integer DEFAULT 0 NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.dictablevalues OWNER TO dm;

--
-- TOC entry 219 (class 1259 OID 17191)
-- Name: emailssent; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.emailssent (
    id integer NOT NULL,
    emailto text NOT NULL,
    template integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    parameters text NOT NULL,
    dateinsert date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.emailssent OWNER TO dm;

--
-- TOC entry 218 (class 1259 OID 17189)
-- Name: emailssent_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.emailssent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emailssent_id_seq OWNER TO dm;

--
-- TOC entry 3943 (class 0 OID 0)
-- Dependencies: 218
-- Name: emailssent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.emailssent_id_seq OWNED BY public.emailssent.id;


--
-- TOC entry 217 (class 1259 OID 17178)
-- Name: emailswaiting; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.emailswaiting (
    id integer NOT NULL,
    emailto text NOT NULL,
    template integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    parameters text NOT NULL,
    dateinsert date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.emailswaiting OWNER TO dm;

--
-- TOC entry 216 (class 1259 OID 17176)
-- Name: emailswaiting_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.emailswaiting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emailswaiting_id_seq OWNER TO dm;

--
-- TOC entry 3944 (class 0 OID 0)
-- Dependencies: 216
-- Name: emailswaiting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.emailswaiting_id_seq OWNED BY public.emailswaiting.id;


--
-- TOC entry 215 (class 1259 OID 17010)
-- Name: events; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.events (
    code character varying(50) NOT NULL,
    name character varying(250) NOT NULL,
    description text NOT NULL,
    addcitycode character(5) NOT NULL,
    address character varying(250) NOT NULL,
    placecode character varying(20) NOT NULL,
    managercode character varying(20) NOT NULL,
    datestart timestamp(4) without time zone NOT NULL,
    duration integer NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    latitude real DEFAULT 0 NOT NULL,
    longitude real DEFAULT 0 NOT NULL,
    status integer DEFAULT 200001 NOT NULL,
    photocodehome text DEFAULT ''::text NOT NULL,
    category integer DEFAULT 250001 NOT NULL,
    haveposter boolean DEFAULT false NOT NULL
);


ALTER TABLE public.events OWNER TO dm;

--
-- TOC entry 234 (class 1259 OID 17928)
-- Name: eventstags; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.eventstags (
    eventcode text NOT NULL,
    eventtagcode integer NOT NULL,
    updatedate date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.eventstags OWNER TO dm;

--
-- TOC entry 210 (class 1259 OID 16603)
-- Name: internalmessages; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.internalmessages (
    messageid character varying(50) NOT NULL,
    contactemail character varying(50) DEFAULT ''::character varying NOT NULL,
    contactphone character varying(50) DEFAULT ''::character varying NOT NULL,
    messagetype character varying(50) DEFAULT ''::character varying NOT NULL,
    messagebody text DEFAULT ''::text NOT NULL,
    inserted date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.internalmessages OWNER TO dm;

--
-- TOC entry 202 (class 1259 OID 16454)
-- Name: places; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.places (
    code text NOT NULL,
    name character varying(250),
    updateuser integer DEFAULT 0 NOT NULL,
    addcitycode character varying(50),
    addprovincecode character varying(2),
    addstreet character varying(250),
    addstreetnumber character varying(20),
    addcp character varying(10),
    description text,
    latitude real,
    longitude real,
    addcc character varying(2) DEFAULT 'es'::text,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    status integer DEFAULT 180001 NOT NULL,
    havelogo boolean DEFAULT false NOT NULL,
    isstore boolean DEFAULT false NOT NULL,
    isdancehall boolean DEFAULT false NOT NULL,
    isdanceschool boolean DEFAULT false NOT NULL,
    contactweb text DEFAULT ''::text NOT NULL,
    contactphone text DEFAULT ''::text NOT NULL,
    contactemail text DEFAULT ''::text NOT NULL,
    contactother text DEFAULT ''::text NOT NULL,
    openhours text DEFAULT ''::text NOT NULL,
    photocodehome text DEFAULT ''::text NOT NULL,
    photocodeicon text DEFAULT ''::text NOT NULL,
    descriptionfull text DEFAULT ''::text NOT NULL,
    score integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.places OWNER TO dm;

--
-- TOC entry 212 (class 1259 OID 16796)
-- Name: placestocheck; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.placestocheck (
    id integer NOT NULL,
    code character varying(50) NOT NULL,
    name character varying(250) NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    addcitycode character varying(50) NOT NULL,
    addprovincecode character varying(2) NOT NULL,
    addstreet character varying(250) NOT NULL,
    addstreetnumber character varying(20) NOT NULL,
    addcp character varying(10) NOT NULL,
    description text NOT NULL,
    latitude real NOT NULL,
    longitude real NOT NULL,
    addcc character varying(2) DEFAULT 'es'::text NOT NULL,
    categories text NOT NULL,
    tags text NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    reporter_name character varying(50) DEFAULT ''::character varying NOT NULL,
    reporter_email character varying(250) DEFAULT ''::character varying NOT NULL,
    reporter_phone character varying(15) DEFAULT ''::character varying NOT NULL,
    reporter_relation character varying(50) DEFAULT ''::character varying NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    validatorcode character varying(50) DEFAULT ''::character varying NOT NULL,
    statusdate date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    checkstatus integer DEFAULT 0 NOT NULL,
    checkstatusdate date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.placestocheck OWNER TO dm;

--
-- TOC entry 211 (class 1259 OID 16794)
-- Name: places_tmp_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.places_tmp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.places_tmp_id_seq OWNER TO dm;

--
-- TOC entry 3945 (class 0 OID 0)
-- Dependencies: 211
-- Name: places_tmp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.places_tmp_id_seq OWNED BY public.placestocheck.id;


--
-- TOC entry 203 (class 1259 OID 16464)
-- Name: placescategories; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.placescategories (
    placecode text NOT NULL,
    placecategorycode integer NOT NULL,
    updatedate date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.placescategories OWNER TO dm;

--
-- TOC entry 228 (class 1259 OID 17474)
-- Name: placesphotos; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.placesphotos (
    photocode text NOT NULL,
    placecode text NOT NULL,
    name text NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.placesphotos OWNER TO dm;

--
-- TOC entry 204 (class 1259 OID 16473)
-- Name: placessocios; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.placessocios (
    sociocode text NOT NULL,
    placecode text NOT NULL,
    placesociotypecode integer NOT NULL,
    updatedate date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.placessocios OWNER TO dm;

--
-- TOC entry 205 (class 1259 OID 16482)
-- Name: placestags; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.placestags (
    placecode text NOT NULL,
    placetagcode integer NOT NULL,
    updatedate date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.placestags OWNER TO dm;

--
-- TOC entry 235 (class 1259 OID 18431)
-- Name: placestoimprove; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.placestoimprove (
    id integer DEFAULT nextval('public.places_tmp_id_seq'::regclass) NOT NULL,
    placecode character varying(50) NOT NULL,
    usercode text NOT NULL,
    notes text NOT NULL,
    insertdate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.placestoimprove OWNER TO dm;

--
-- TOC entry 236 (class 1259 OID 18438)
-- Name: placestoimprove_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.placestoimprove_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.placestoimprove_id_seq OWNER TO dm;

--
-- TOC entry 3946 (class 0 OID 0)
-- Dependencies: 236
-- Name: placestoimprove_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.placestoimprove_id_seq OWNED BY public.placestoimprove.id;


--
-- TOC entry 206 (class 1259 OID 16491)
-- Name: socios; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.socios (
    code text NOT NULL,
    name text,
    surnames text,
    legalid text,
    legalidtypecode integer,
    addcitycode text,
    addprovincecode text,
    addstreet text,
    addstreetnumber text,
    addcp text,
    deleted integer DEFAULT 0 NOT NULL,
    nickname text DEFAULT ''::text NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.socios OWNER TO dm;

--
-- TOC entry 207 (class 1259 OID 16501)
-- Name: socioscontacts; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.socioscontacts (
    sociocode text NOT NULL,
    contacttypecode integer NOT NULL,
    contactvalue text NOT NULL,
    updateuser date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.socioscontacts OWNER TO dm;

--
-- TOC entry 208 (class 1259 OID 16509)
-- Name: sociospaymentmethods; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.sociospaymentmethods (
    sociocode text NOT NULL,
    paymenttypecode integer NOT NULL,
    paymentnumber text NOT NULL,
    paymentdetails text NOT NULL,
    updateuser date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.sociospaymentmethods OWNER TO dm;

--
-- TOC entry 222 (class 1259 OID 17387)
-- Name: tasks; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.tasks (
    id integer NOT NULL,
    taskgroupcode text NOT NULL,
    relatedcode text NOT NULL,
    relatednote text NOT NULL,
    status integer DEFAULT 220001 NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.tasks OWNER TO dm;

--
-- TOC entry 221 (class 1259 OID 17385)
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.tasks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO dm;

--
-- TOC entry 3947 (class 0 OID 0)
-- Dependencies: 221
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- TOC entry 220 (class 1259 OID 17372)
-- Name: tasksgroups; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.tasksgroups (
    code text NOT NULL,
    name text,
    notes text,
    status integer DEFAULT 220001 NOT NULL,
    type integer DEFAULT 230000 NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.tasksgroups OWNER TO dm;

--
-- TOC entry 224 (class 1259 OID 17426)
-- Name: users; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.users (
    code text NOT NULL,
    name text NOT NULL,
    nickname text NOT NULL,
    password text DEFAULT ''::text NOT NULL,
    useremail text NOT NULL,
    userphone text DEFAULT ''::text NOT NULL,
    birthdate timestamp(4) without time zone,
    notes text DEFAULT ''::text NOT NULL,
    addcitycode text,
    addprovincecode text,
    updatedate bigint DEFAULT '201801010000'::bigint NOT NULL,
    updateuser integer DEFAULT 0 NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO dm;

--
-- TOC entry 232 (class 1259 OID 17903)
-- Name: usersconnectors; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.usersconnectors (
    usercode text NOT NULL,
    connectorid text DEFAULT ''::text NOT NULL,
    connector text DEFAULT ''::text NOT NULL,
    inserted date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    useddate date DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.usersconnectors OWNER TO dm;

--
-- TOC entry 225 (class 1259 OID 17442)
-- Name: userslogs_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public.userslogs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.userslogs_id_seq OWNER TO dm;

--
-- TOC entry 3948 (class 0 OID 0)
-- Dependencies: 225
-- Name: userslogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public.userslogs_id_seq OWNED BY public.userslogs.id;


--
-- TOC entry 209 (class 1259 OID 16534)
-- Name: userstaskvalidation; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.userstaskvalidation (
    key text NOT NULL,
    type integer NOT NULL,
    value text NOT NULL,
    dateinsert date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dateexpire date NOT NULL
);


ALTER TABLE public.userstaskvalidation OWNER TO dm;

--
-- TOC entry 237 (class 1259 OID 18458)
-- Name: usersvalidateemail; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public.usersvalidateemail (
    usercode text NOT NULL,
    useremail text DEFAULT ''::text NOT NULL,
    token text DEFAULT ''::text NOT NULL,
    insertdate bigint DEFAULT '201801010000'::bigint NOT NULL,
    validateddate bigint DEFAULT '201801010000'::bigint NOT NULL
);


ALTER TABLE public.usersvalidateemail OWNER TO dm;

--
-- TOC entry 3692 (class 2604 OID 17194)
-- Name: emailssent id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.emailssent ALTER COLUMN id SET DEFAULT nextval('public.emailssent_id_seq'::regclass);


--
-- TOC entry 3689 (class 2604 OID 17181)
-- Name: emailswaiting id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.emailswaiting ALTER COLUMN id SET DEFAULT nextval('public.emailswaiting_id_seq'::regclass);


--
-- TOC entry 3665 (class 2604 OID 16999)
-- Name: eventstocheck id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.eventstocheck ALTER COLUMN id SET DEFAULT nextval('public.activities_tmp_id_seq'::regclass);


--
-- TOC entry 3651 (class 2604 OID 16799)
-- Name: placestocheck id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.placestocheck ALTER COLUMN id SET DEFAULT nextval('public.places_tmp_id_seq'::regclass);


--
-- TOC entry 3700 (class 2604 OID 17390)
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- TOC entry 3716 (class 2604 OID 17447)
-- Name: userslogs id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.userslogs ALTER COLUMN id SET DEFAULT nextval('public.userslogs_id_seq'::regclass);


--
-- TOC entry 3801 (class 2606 OID 17503)
-- Name: adminusers adminusers_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.adminusers
    ADD CONSTRAINT adminusers_pkey PRIMARY KEY (userid);


--
-- TOC entry 3803 (class 2606 OID 17501)
-- Name: adminusers adminusers_useremail_key; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.adminusers
    ADD CONSTRAINT adminusers_useremail_key UNIQUE (useremail);


--
-- TOC entry 3805 (class 2606 OID 17512)
-- Name: adminuserslogs adminuserslogs_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.adminuserslogs
    ADD CONSTRAINT adminuserslogs_pkey PRIMARY KEY (id);


--
-- TOC entry 3789 (class 2606 OID 17425)
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (code);


--
-- TOC entry 3809 (class 2606 OID 17925)
-- Name: companiesplaces companiesplaces_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.companiesplaces
    ADD CONSTRAINT companiesplaces_pkey PRIMARY KEY (companycode, placecode);


--
-- TOC entry 3797 (class 2606 OID 17464)
-- Name: companiesusers companiesusers_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.companiesusers
    ADD CONSTRAINT companiesusers_pkey PRIMARY KEY (companycode, usercode);


--
-- TOC entry 3745 (class 2606 OID 16559)
-- Name: competitions competitions_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.competitions
    ADD CONSTRAINT competitions_pkey PRIMARY KEY (code);


--
-- TOC entry 3747 (class 2606 OID 16561)
-- Name: datalastupdate datalastupdate_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.datalastupdate
    ADD CONSTRAINT datalastupdate_pkey PRIMARY KEY (type);


--
-- TOC entry 3749 (class 2606 OID 16563)
-- Name: diccities diccities_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.diccities
    ADD CONSTRAINT diccities_pkey PRIMARY KEY (code, countrycode, provincecode);


--
-- TOC entry 3751 (class 2606 OID 16565)
-- Name: dicprovinces dicprovinces_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.dicprovinces
    ADD CONSTRAINT dicprovinces_pkey PRIMARY KEY (code, countrycode);


--
-- TOC entry 3753 (class 2606 OID 16567)
-- Name: dictables dictables_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.dictables
    ADD CONSTRAINT dictables_pkey PRIMARY KEY (tablecode);


--
-- TOC entry 3755 (class 2606 OID 16569)
-- Name: dictablevalues dictablevalues_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.dictablevalues
    ADD CONSTRAINT dictablevalues_pkey PRIMARY KEY (code);


--
-- TOC entry 3783 (class 2606 OID 17201)
-- Name: emailssent emailssent_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.emailssent
    ADD CONSTRAINT emailssent_pkey PRIMARY KEY (id);


--
-- TOC entry 3781 (class 2606 OID 17188)
-- Name: emailswaiting emailswaiting_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.emailswaiting
    ADD CONSTRAINT emailswaiting_pkey PRIMARY KEY (id);


--
-- TOC entry 3779 (class 2606 OID 17022)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (code);


--
-- TOC entry 3777 (class 2606 OID 17009)
-- Name: eventstocheck events_tmp_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.eventstocheck
    ADD CONSTRAINT events_tmp_pkey PRIMARY KEY (id);


--
-- TOC entry 3811 (class 2606 OID 17938)
-- Name: eventstags eventstags_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.eventstags
    ADD CONSTRAINT eventstags_pkey PRIMARY KEY (eventcode, eventtagcode);


--
-- TOC entry 3773 (class 2606 OID 16617)
-- Name: internalmessages internalmessages_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.internalmessages
    ADD CONSTRAINT internalmessages_pkey PRIMARY KEY (messageid);


--
-- TOC entry 3757 (class 2606 OID 16575)
-- Name: places places_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.places
    ADD CONSTRAINT places_pkey PRIMARY KEY (code);


--
-- TOC entry 3775 (class 2606 OID 16808)
-- Name: placestocheck places_tmp_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.placestocheck
    ADD CONSTRAINT places_tmp_pkey PRIMARY KEY (id);


--
-- TOC entry 3759 (class 2606 OID 16577)
-- Name: placescategories placescategories_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.placescategories
    ADD CONSTRAINT placescategories_pkey PRIMARY KEY (placecode, placecategorycode);


--
-- TOC entry 3799 (class 2606 OID 17484)
-- Name: placesphotos placesphotos_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.placesphotos
    ADD CONSTRAINT placesphotos_pkey PRIMARY KEY (photocode);


--
-- TOC entry 3761 (class 2606 OID 16579)
-- Name: placessocios placessocios_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.placessocios
    ADD CONSTRAINT placessocios_pkey PRIMARY KEY (placecode, sociocode, placesociotypecode);


--
-- TOC entry 3763 (class 2606 OID 16581)
-- Name: placestags placestags_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.placestags
    ADD CONSTRAINT placestags_pkey PRIMARY KEY (placecode, placetagcode);


--
-- TOC entry 3765 (class 2606 OID 16583)
-- Name: socios socios_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.socios
    ADD CONSTRAINT socios_pkey PRIMARY KEY (code);


--
-- TOC entry 3767 (class 2606 OID 16585)
-- Name: socioscontacts socioscontacts_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.socioscontacts
    ADD CONSTRAINT socioscontacts_pkey PRIMARY KEY (sociocode, contacttypecode, contactvalue);


--
-- TOC entry 3769 (class 2606 OID 16587)
-- Name: sociospaymentmethods sociospaymentmethods_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.sociospaymentmethods
    ADD CONSTRAINT sociospaymentmethods_pkey PRIMARY KEY (sociocode, paymenttypecode, paymentnumber);


--
-- TOC entry 3787 (class 2606 OID 17399)
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 3785 (class 2606 OID 17384)
-- Name: tasksgroups tasksgroups_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.tasksgroups
    ADD CONSTRAINT tasksgroups_pkey PRIMARY KEY (code);


--
-- TOC entry 3791 (class 2606 OID 17437)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (code);


--
-- TOC entry 3793 (class 2606 OID 17439)
-- Name: users users_useremail_key; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_useremail_key UNIQUE (useremail);


--
-- TOC entry 3807 (class 2606 OID 17914)
-- Name: usersconnectors usersconnectors_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.usersconnectors
    ADD CONSTRAINT usersconnectors_pkey PRIMARY KEY (usercode, connectorid);


--
-- TOC entry 3795 (class 2606 OID 17453)
-- Name: userslogs userslogs_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.userslogs
    ADD CONSTRAINT userslogs_pkey PRIMARY KEY (id);


--
-- TOC entry 3771 (class 2606 OID 16595)
-- Name: userstaskvalidation userstaskvalidation_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.userstaskvalidation
    ADD CONSTRAINT userstaskvalidation_pkey PRIMARY KEY (key);


--
-- TOC entry 3813 (class 2606 OID 18469)
-- Name: usersvalidateemail usersvalidateemail_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.usersvalidateemail
    ADD CONSTRAINT usersvalidateemail_pkey PRIMARY KEY (usercode, useremail);


--
-- TOC entry 3814 (class 2606 OID 18412)
-- Name: usersconnectors usersconnectors_users; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public.usersconnectors
    ADD CONSTRAINT usersconnectors_users FOREIGN KEY (usercode) REFERENCES public.users(code);


-- Completed on 2019-08-24 17:16:56 CEST

--
-- PostgreSQL database dump complete
--

