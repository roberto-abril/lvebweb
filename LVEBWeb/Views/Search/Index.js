﻿$(function () {
    // LocationGet();
    CheckGeolocation();
    SearchStart();
    $('#btnSearch').click(function(){ 
    if($('#search').val() != '')
        $('#search-box').submit()
        else{
            $('#search').focus();
            $('#search').addClass('error');
        }
    });
    
    $(".nearmy").click(SearchNear);
});

function CheckGeolocation(){
    if (!("geolocation" in navigator)){
        $(".search-box-main").removeClass('col-8');
        $(".search-box-main").addClass('col-11');
        $(".neartmy").remove();
    }
}



function SearchStart(){
    $( "#search").on( "keydown", function( event ) {
    $('#search').removeClass('error');
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
    }).autocomplete({
        source: function( request, response ) {
          $.getJSON( "/AutocompleteSearch/"+request.term, {referal:currentPage},response );
        },
        select: function( event, ui ) {
            this.value = ui.item.label +" "+ui.item.value1;
            //$('#search-box').submit()
            SearchSelect(ui.item);
        }
    })
}
function SearchSelect(value) {
    var label = AIG.Replace(value.label, ' ', '-');
    AIG.PageLoad('/l/'+value.code+'/'+label);
    //AIG.PageLoadNewTab('/l/'+value.code+'/'+label);
}

function SearchNear() {
    AIG.LocationGet(function(value){
        if(value.NActive && value.LActive){
            AIG.CookieSet('rlat',value.Latitude);
            AIG.CookieSet('rlon',value.Longitude);
            //AIG.PageLoad('/cerca');
            console.log('/cerca');
            window.location = "/cerca";
            window.location.reload(false)
        }else{
            console.log(value);
            if(!value.NActive){
                return;
            }
            if(!value.LActive){
                alert('Deactivate');
            }
        }
    });
}

function ShareLoad(div) {
    div = $(div);
    $("#shareIcons_" + div.attr("data-code")).jsSocials({
        url: div.attr("data-url"),
        text: div.attr("data-text"),
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "linkedin", "pinterest", "whatsapp"]
    });
    div.remove();
}