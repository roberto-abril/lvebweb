﻿$(function () {
    GridMainStart();
    $('#StatusCheckbox').change(function() {
      $('#Status').val( $(this).prop('checked')?'200002':'200001')
    })
});

function GridMainStart() {

    $("#GridMain").jsGrid({
        width: "100%",
        height:250,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        noDataContent: "No tienes eventos.",
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Places/Search?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteCategory").val()+"&FK4="+$("#filteBaileTags").val(),
                    dataType: "json"
                }).done(function(response) {
                    d.resolve(response.rows);
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ 
            //{ name: "code", type: "text",title:"Id", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "addCity", type: "text",title:"Ciudad", width: 80},
            //{ name: "categoriesString", type: "text",title:"Categorias", width: 80},
            //{ name: "tagsString", type: "text",title:"Estilos", width: 120},
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
           // { name: "updateUser", type: "text",title:"Update User", width: 50 }
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};