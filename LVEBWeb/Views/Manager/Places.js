﻿$(function () {
    GridMainStart();
});

function GridMainStart() {

    $("#GridMain").jsGrid({
        width: "100%",
        height:250,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        noDataContent: "No tienes locales.",
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/manager/places/Search",
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                });
                return d.promise();
            }
        },
        fields:
          [ 
            { name: "placeName", type: "text",title:"Nombre", width: 50 },
            { name: "placeCity", type: "text",title:"Ciudad", width: 50},
            { name: "companyName", type: "text",title:"Empresa", width: 50},
            { name: "options", type: "text",title:"", width: 120,itemTemplate: function(value,doc) {
                return '<a class="btn btn-outline-primary btn-sm" href="/manager/events/new?pc=' + doc.placeCode + '" >Publicar Evento</a> ' +
                    ' <button type="button" class="btn btn-outline-primary btn-sm" target=_blank onclick="PlaceImprove();">Mejorar Información</button> ';
            } },
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};



function PlaceImprove() {
    $("#modalManager .modal-title").text('Mejorar información');
    $("#modalManager .modal-body").html('<p>Funcionalidad aun no disponible.</p><p>Estará activa en las proximas semanas. </p><p>Disculpa las molestias. </p>');
    $("#modalManager,#bck-full").show();
    $("#modalManager button").click(PlaceImproveClose);
}

function PlaceImproveClose() {
    $("#modalManager button").unbind('click')
    $("#modalManager,#bck-full").hide();
}