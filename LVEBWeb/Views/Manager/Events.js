﻿$(function () {
    GridMainStart();
});

function GridMainStart() {

    $("#GridMain").jsGrid({
        width: "100%",
        height:250,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        noDataContent: "No tienes eventos.",
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/manager/events/Search?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteCategory").val()+"&FK4="+$("#filteBaileTags").val(),
                    dataType: "json"
                }).done(function(response) {
                    d.resolve(response.rows);
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ 
            { name: "eventName", type: "text",title:"Nombre", width: 80 },
                {
                    name: "dateStart", type: "text", title: "Fecha", width: 50, itemTemplate: function (value, doc) {
                        var a = doc.dateStart.substr(12).split(':');
                        return doc.dateStart.substr(8, 2) + '/' + doc.dateStart.substr(5, 2) + '/'+doc.dateStart.substr(0,4) +' ' + a[0]+':'+a[1];
               // return doc.dateStart.substr(0,10).replace('T',' ');
            }},
            { name: "placeName", type: "text",title:"Local", width: 80},
            { name: "statusName", type: "text",title:"Estado", width: 50},
            { name: "options", type: "text",title:"", width: 50,itemTemplate: function(value,doc) {
                return '<a class="btn btn-outline-primary btn-sm" href="/manager/events/edit/' + doc.eventCode + '" >Editar Evento</a> ';
            } },
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};