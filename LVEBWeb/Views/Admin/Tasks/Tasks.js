﻿$(function () {
    TasksSearch();
    $("#frameTask").height($(window).height()-80);
});

var TaskId = 0;
function TaskNext(){
    var taskType = $("#TaskGroupType").val();
    $.get( '/Admin/Tasks/Next?taskCode='+$("#TaskGroupCode").val()+'&lastTask='+TaskId, function(data) {
        if(data.id > 0){
            var url = '';
            switch(taskType){
                case '240002':
                    url = "/Admin/Places/Edit/" + data.relatedCode + "?hideMenu=true";
                break;
            }
            $("#frameTask").show();
            $("#frameMain").attr("src",url);
   
        TaskId = data.id;
        }            
    },'json');
}

function TaskClose(){
    $("#frameTask").hide();
}

function TaskChangeValidateAndNext(){
    TaskStatusChange(230003);
}
function TaskChangeNext(){
    TaskStatusChange(230002);
}


function TaskStatusChange(status){
    $.post( '/Admin/Tasks/StatusChange?taskId=' + TaskId + '&status=' + status, {}, function(data) {
        TaskNext();
    },'json');
}


function TasksSearch() {

    $("#GridTasks").jsGrid({
        width: "100%",
        height:400,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/TasksGroups/TasksSearch?FK1="+$("#TaskGroupCode").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
         fields:
          [ 
            { name: "id", type: "text",title:"Id", width: 80 },
            { name: "relatedCode", type: "text",title:"RelatedCode", width: 80 },
            { name: "relatedNote", type: "text",title:"RelatedNote", width: 80},
            { name: "status", type: "text",title:"Estado", width: 80},
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            ]  


    });
}



/*
function TaskGroupSaveStart() {
    if($("#form").valid()){
        $.ajax({
            type: 'POST',
            url: '/Admin/TasksGroups/Edit/'+$("#EditCode").val(),
            data: new FormData($("#form")[0]),
            processData: false, 
            contentType: false, 
            success: function(data) {
                if(data.wasOk){
                    if(data.newCode && data.newCode != ''){
                        PageLoad('/Admin/TasksGroups/Edit/'+data.newCode);
                    }
                    TaskGroupSaveEnd('BtnSave','Guardado Ok');
                }
                else
                {
                    TaskGroupSaveEnd('BtnSave','Error');
                }
             }
        });
   }
}

function TaskGroupSaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}


*/





