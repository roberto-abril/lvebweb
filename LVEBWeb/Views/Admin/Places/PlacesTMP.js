﻿$(function () {

    h = $(window).height() - 150;
    $("#map_canvas").height(h);
    GridMainStart();

    $("#ddlTables").change(GridMainStart);
});
var h;

function GridMainStart() {
    if (h < 300)
        h = 300;
    $("#GridMain").jsGrid({
        width: "100%",
        height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        /*rowDoubleClick: function(value) {
            PageNewTab('/Places/Edit/'+value.item.code);
        },*/
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/PlacesTMP/Search?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteCategory").val()+"&FK4="+$("#filteBaileTags").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                    POIsRefresh();
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Id", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "checkStatus", type: "text",title:"Check Status", width: 80},
            { name: "checkStatusDate", type: "text",title:"Date", width: 80},
            { name: "reporterName", type: "text",title:"R.Name", width: 80},
            { name: "reporterEmail", type: "text",title:"R.Email", width: 80},
            { name: "reporterPhone", type: "text",title:"R.Phone", width: 80},
            { name: "reporterRelation", type: "text",title:"R.Relation", width: 80},
            { name: "addCity", type: "text",title:"Ciudad", width: 80},
            { name: "addProvince", type: "text",title:"Provincia", width: 80},
            { name: "categoriesString", type: "text",title:"Categorias", width: 80},
            { name: "tagsString", type: "text",title:"Estilos", width: 120},
            { name: "updateDate", type: "text",title:"Update Date", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "updateUser", type: "text",title:"Update User", width: 50 }
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


function ListShow() {
    $("#map_canvas").hide();
    $("#gridContent").show();
}
var MapStarted = false;
var map;
var mapData = {MapZoom:12,lat:41.332024,lon:2.104219};
function MapShow() {
    $("#map_canvas").show();
    $("#gridContent").hide();
    if(MapStarted) return;
    

    var tiles = L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&z={z}&x={x}&y={y}&s=Ga', {
                maxZoom: 18,
                attribution: '',
                id: 'tiles_gmap'
            }),
            latlng = L.latLng(mapData.lat, mapData.lon);
    map = L.map('map_canvas', {center: latlng, zoom: mapData.MapZoom, layers: [tiles]});
    
    MapStarted = true;
    POIsRefresh();



};


var MapPois = [];
/*
var icon = {
    url: "/images/blank.png", // url
    size: new google.maps.Size(12,20),
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(12, 20) // anchor
};*/
var iconPOI = L.icon({iconUrl: '/images/blank.png', iconSize:[12, 20],iconAnchor:[6, 20]});

function POIsRefresh() {
    if(!MapStarted) return;
    wlogs('POIClear');
    for(var x = 0; x < MapPois.length; x++){
         map.removeLayer(MapPois[x]);
    }
    MapPois = [];

    for (var x = 0; x < DataRows.length; x++) {

        var marker = L.marker([DataRows[x].latitude, DataRows[x].longitude],{icon: iconPOI,draggable: true,title: DataRows[x].name});
        marker.data = DataRows[x];
        marker.addTo(map);
        marker.on('dblclick', function(event){
            PageNewTab('/Admin/Places/Edit/'+this.data.code);
        });
        MapPois.push(marker);

    };
};