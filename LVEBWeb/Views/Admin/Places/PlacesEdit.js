﻿$(function () {
    MapStart();
    //PlaceSaveStart();
    $("#BtnSave").click(PlaceSaveStart);

    $("#EditLat,#EditLon").bind("paste", LatLonPaste);
    
    $("#EditLat,#EditLon").change(LatLonChange);

    $( "#vxvenue_save" ).validate( {
        rules: {
            Name: "required",
            AddStreet: "required",
            Description:"required"
        },
        messages: {
            Name: {
                required: "Campo requerido"
            },
            AddStreet: {
                required: "Campo requerido"
            },
            Description: {
                required: "Campo requerido"
            }
        }
    });
    
    $('#Categories').magicsearch({
        dataSource: LocalCategorias,
        fields: ['Name'],
        id: 'Code',
        format: '%Name%',
        multiple: true,
        focusShow: true,
        multiField: 'Name',
        multiStyle: {
            space: 5,
           width: 200
        }
    });
    $('#TagsBailes').magicsearch({
        dataSource: LocalTagsBailes,
        fields: ['Name'],
        id: 'Code',
        format: '%Name%',
        multiple: true,
        focusShow: true,
        multiField: 'Name',
        multiStyle: {
            space: 5,
            width: 130
        }
        });
    $('#TagsProfile').magicsearch({
        dataSource: LocalTagsProfiles,
        fields: ['Name'],
        id: 'Code',
        format: '%Name%',
        multiple: true,
        focusShow: true,
        multiField: 'Name',
        multiStyle: {
            space: 5,
            width: 130
        }
        });

    TabStart('tab-edit');

    HoursStart();

    GridPhotosStart();
    PhotoAddStart();
    
    $(".calendarCopy").click(HoursCopy);
    $(".calendarClear").click(HoursClear);
    
    
    $("#AddProvinceCode").change(function(){
        DICLoadCities('AddCityCode',$("#AddProvinceCode").val());
    });
    
});

function HoursStart() {
    if($("#OpenHours").val() != ''){
        var hours = JSON.parse($("#OpenHours").val());
        for(var x = 0; x < hours.length; x++){
            if(hours[x])
            for(var y = 0; y < hours[x].length; y++){
                $("#time"+x+y).val(hours[x][y]);
            }
        }
    }

    $("#OpenHoursTable input").change(HoursChange);
}
function HoursChange() {
    var hours = [];
    $("#OpenHoursTable input").each(function(){
        if($( this ).val() != ''){
            var x = parseInt(this.id[4]);
            var y = parseInt(this.id[5]);
            if(!hours[x]) hours[x] = [];
            hours[x][y] = $( this ).val();
        }
    });
    $("#OpenHours").val(JSON.stringify(hours));
    console.log(hours);
}
function HoursCopy() {
    var id = this.id;
    var idP = 'time'+(1*id.substr(4) - 1);
    $("#"+id+'0').val( $("#"+idP+'0').val());
    $("#"+id+'1').val( $("#"+idP+'1').val());
    $("#"+id+'2').val( $("#"+idP+'2').val());
    $("#"+id+'3').val( $("#"+idP+'3').val());
    HoursChange();
}
function HoursClear() {
    var id = this.id;
    $("#"+id+'0').val('');
    $("#"+id+'1').val('');
    $("#"+id+'2').val('');
    $("#"+id+'3').val('');
    HoursChange();
}

var map;
var marker;
var mapData = {MapZoom:15};


var iconPOI = L.icon({iconUrl: '/images/marker.png', iconSize:[25, 41],iconAnchor:[12, 41]});
function MapStart() {
    try {
        var lat = parseFloat($("#EditLat").val().replace(',','.'));
        var lon = parseFloat($("#EditLon").val().replace(',','.'));

        var tiles = L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&z={z}&x={x}&y={y}&s=Ga', {
            maxZoom: 18,
            attribution: '',
            id: 'tiles_gmap'
        }),
            latlng = L.latLng(lat, lon);
        map = L.map('map_canvas', { center: latlng, zoom: mapData.MapZoom, layers: [tiles] });

        marker = L.marker([lat, lon], { icon: iconPOI, draggable: true });
        marker.addTo(map);
        marker.on('dragend', function (event) {
            var position = event.target.getLatLng();
            //marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
            //map.panTo(new L.LatLng(position.lat, position.lng))

            $("#EditLat").val((position.lat+'').replace('.',','));
            $("#EditLon").val((position.lng+'').replace('.',','));
        });
    } catch (ex) {}
};

function LatLonChange(){
    var latlon = new L.LatLng(parseFloat($("#EditLat").val().replace(',','.')), parseFloat($("#EditLon").val().replace(',','.')));
    map.panTo(latlon);
    marker.setLatLng(latlon);
}


function PlaceSaveStart() {
    if($("#form").valid()){
        $("#TagsBaileIds").val($("#TagsBailes").attr('data-id'));
        $("#CategoriesIds").val($("#Categories").attr('data-id'));
        $("#TagsProfileIds").val($("#TagsProfile").attr('data-id'));
        var data =  new FormData($("#form")[0]);
        $.ajax({
            type: 'POST',
            url: '/Admin/Places/Edit/'+$("#EditCode").val(),
            data: data,
            processData: false, 
            contentType: false, 
            success: function(data) {
                if(data.wasOk){
                    if(data.newCode && data.newCode != '')
                        PageLoad('/Admin/Places/Edit/'+data.newCode+'?hideMenu='+$("#HideMenu").val()+'&'+Math.random());
    
                    BTNSaveEnd('BtnSave','Guardado Ok');
                }
                else
                    BTNSaveEnd('BtnSave','Error');
             }
        });
   }
}
function BTNSaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}

function PlaceDelete(){
    if (confirm('El local sera eliminado de forma permanente. Estas seguro?')) {
        $.post( '/Admin/Places/Delete/'+$("#EditCode").val(), {}, function(data) {
            if(data.wasOk)
                $("#body").html('Local Eliminado!');
            else
                BTNSaveEnd('BtnSave','Error');
           },
           'json' 
        );
    }
    return false;
}




function GridPhotosStart() {
    $("#GridPhotos").jsGrid({
        width: "100%",
        height:300,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            //PageNewTab('/Admin/Places/PhotoList/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Places/PhotoList?placeCode="+$("#EditCode").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "photo", type: "text",title:"", width: 500,itemTemplate: function(value,doc) {
                    return '<img src="/Admin/PlacePhoto/'+doc.photoCode+'" >';
            } },
            { name: "photoCode", type: "text",title:"Id", width: 80 },
            { name: "updateDate", type: "text",title:"Update Date", width: 80,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "updateDate", type: "text",title:"Update Date", width: 80,itemTemplate: function(value,doc) {
                    return '<button onclick="PhotoDelete(\''+doc.photoCode+'\');return false;">Eliminar</button>';
            } },
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


function PhotoAddStart(){
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#photo_status');
       
    $('#form-photo').ajaxForm({
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function() {
            var percentVal = '100%';
            bar.width(percentVal)
            percent.html(percentVal);
            GridPhotosStart();
        },
        complete: function(xhr) {
            status.html(xhr.responseText);
        }
    }); 
}
function PhotoDelete(photoCode){
    if (confirm('La foto sera eliminada de forma permanente. Estas seguro?')) {
        $.post( '/Admin/PlacePhoto/Delete/'+photoCode, {}, function(data) {
            GridPhotosStart();
           },
           'json' 
        );
    }
    return false;
}


function LatLonPaste(e) {
    var pastedData = e.originalEvent.clipboardData.getData('text');
    var l = pastedData.split('.').length;
    if (l == 2) {
        $(this).val(pastedData.replace('.',','));
        LatLonChange();
        return false;
    }
    if(l>2){
        var ll = pastedData.split(',');
        $('#EditLat').val(ll[0].replace('.',','));
        $('#EditLon').val(ll[1].replace('.',','));
        LatLonChange();
        return false;
    }
}