﻿$(function () {
    GridMainStart();
    $("select").change(GridMainStart);
});
var h;

function GridMainStart() {
    if (h < 300)
        h = 300;
    $("#GridMain").jsGrid({
        width: "100%",
        height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            PageNewTab('/Admin/Users/UserEdit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Users/UsersSearch?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteType").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Código", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "nickname", type: "text",title:"Nick", width: 80},
            { name: "addCity", type: "text",title:"Ciudad", width: 80},   { name: "customer", type: "text",title:"", width: 80, filtering: false
            ,itemTemplate: function(value,doc) {
                var btns = $('<div>');
               // btns.append($("<a>").attr("href", '/Admin/Customer/Details/'+doc.customerId).text("Customer").attr("style","margin-right: 5px;"));
                btns.append($("<a>").attr("href", '/Admin/users/UserEmulator?UserCode='+doc.code).text("Emulate User").attr("style","margin-right: 5px;").attr("target","_black"));
               return btns;
            }},
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            ]  
            
            


    });

    $(".jsgrid-grid-body").attr("height",null);
};


