﻿
function UsersSearchStart() {
    $("#user_list").hide();
    $("#user_search").show();
    $("#BtnUserAdd").hide();
    $("#BtnUserCancel").show();
    UsersSearch();
}

function UsersLoad() {
    if($("#EditCode").val() == '') return;
    //if (h < 300)
    //    h = 300;
    $("#GridUsers").jsGrid({
        width: "100%",
       // height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Companies/UsersByCompanyList?code="+$("#EditCode").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response);
                   // DataRows = response.rows;
                });
                return d.promise();
            }
        },
        //pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ 
            { name: "userCode", type: "text",title:"Código", width: 80 },
            { name: "userNickname", type: "text",title:"Nick", width: 80},
            { name: "userName", type: "text",title:"name", width: 80},
            { name: "companyUserTypeName", type: "text",title:"Tipo", width: 80 },
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "add", type: "text",title:"Borrar", width: 50,itemTemplate: function(value,doc) {
                    return ' <a href="#" class="btn btn-default" onclick="UsersRemove(this,\''+doc.userCode+'\',\''+doc.companyCode+'\');">Eliminar</a>';
            } },
            ]  


    });

   // $(".jsgrid-grid-body").attr("height",null);
};

function UsersAdd(btn,userCode,companyUserType) {
    var form = {
        CompanyCode : $("#EditCode").val(),
        UserCode : userCode,
        CompanyUserType :$("#UserAddType_"+userCode).val()
        }
    $.post( '/Admin/Companies/UserAdd', form, function(data) {
      if(data){
            $("#user_list").show();
            $("#user_search").hide();
            UsersLoad();
      } else {
          $(btn).addClass('error');
      }
    },
    'json' 
    );
}
function UsersRemove(btn,userCode,companyCode) {
    if (!confirm('El usuario sera eliminado de forma permanente. Estas seguro?')) {
        return;
    }
    var form = {
        CompanyCode : companyCode,
        UserCode : userCode
        }
    $.post( '/Admin/Companies/UserRemove', form, function(data) {
        if(data){
            UsersLoad();
      } else {
          $(btn).addClass('error');
      }
    },
    'json' 
    );
}

function UsersSearch() {

    $("#GridUserSearch").jsGrid({
        width: "100%",
        //height:400,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            PageNewTab('/Admin/Users/UserEdit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Users/UsersSearch?FK1="+$("#userFilterName").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Código", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "nickname", type: "text",title:"Nick", width: 80},
            { name: "addCity", type: "text",title:"Ciudad", width: 80},
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "add", type: "text",title:"Añadir", width: 50,itemTemplate: function(value,doc) {
                    return '<select type="text" id="UserAddType_'+doc.code+'" class="form-control"><option value="150003">Colaborador</option><option value="150002">Manager</option><option value="150001">Propietario</option></select> <a href="#" class="btn btn-default" onclick="UsersAdd(this,\''+doc.code+'\',\''+value+'\');">Añadir</a>';
            } },
            ]  


    });
}


function UsersCancel() {
    $("#user_list").show();
    $("#user_search").hide();
    $("#BtnUserAdd").show();
    $("#BtnUserCancel").hide();
}

