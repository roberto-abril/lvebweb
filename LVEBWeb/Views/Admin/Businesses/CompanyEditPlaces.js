﻿

function PlacesSearchStart() {
    $("#place_list").hide();
    $("#place_search").show();
    $("#BtnPlaceAdd").hide();
    $("#BtnPlaceCancel").show();
    PlacesSearch();
}

function PlacesLoad() {
    if($("#EditCode").val() == '') return;
    $("#GridPlaces").jsGrid({
        width: "100%",
        height:300,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Companies/PlacesByCompanyList?code="+$("#EditCode").val(),
                    dataType: "json"
                }).done(function(response) {
                    d.resolve(response);
                });
                return d.promise();
            }
        },
        fields:
          [ 
            { name: "placeCode", type: "text",title:"Código", width: 80 },
            { name: "placeName", type: "text",title:"name", width: 80},
            { name: "companyPlaceTypeName", type: "text",title:"Tipo", width: 80 },
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "add", type: "text",title:"Borrar", width: 50,itemTemplate: function(value,doc) {
                    return ' <a href="#" class="btn btn-default" onclick="PlaceRemove(this,\''+doc.placeCode+'\',\''+doc.companyCode+'\');">Eliminar</a>';
            } },
            ]  


    });

};

function PlaceAdd(btn,userCode,companyUserType) {
    var form = {
        CompanyCode : $("#EditCode").val(),
        PlaceCode : userCode,
        CompanyPlaceType :$("#PlaceAddType_"+userCode).val()
        }
    $.post( '/Admin/Companies/PlaceAdd', form, function(data) {
      if(data){
            PlaceCancel();
            PlacesLoad();
      } else {
          $(btn).addClass('error');
      }
    },
    'json' 
    );
}
function PlaceRemove(btn,userCode,companyCode) {
    if (!confirm('El local sera eliminado de forma permanente. Estas seguro?')) {
        return;
    }
    var form = {
        CompanyCode : companyCode,
        PlaceCode : userCode
        }
    $.post( '/Admin/Companies/PlaceRemove', form, function(data) {
        if(data){
            PlacesLoad();
      } else {
          $(btn).addClass('error');
      }
    },
    'json' 
    );
}

function PlacesSearch() {
    $("#GridPlacesSearch").jsGrid({
        width: "100%",
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            PageNewTab('/Admin/Places/Edit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Places/Search?FK1="+$("#placeFilterName").val()+"&FK2="+$("#placeFilterProvince").val()+"&FK3="+$("#placeFilteCategory").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Id", width: 60 },
            { name: "name", type: "text",title:"Nombre", width: 60 },
            { name: "addCity", type: "text",title:"Ciudad", width: 60},
            { name: "addProvince", type: "text",title:"Provincia", width: 60},
            { name: "categoriesString", type: "text",title:"Categorias", width: 60},
            { name: "updateDate", type: "text",title:"Updated", width: 30,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "add", type: "text",title:"Añadir", width: 60,itemTemplate: function(value,doc) {
                    return '<select type="text" id="PlaceAddType_'+doc.code+'" class="form-control"><option value="">--</option></select> <a href="#" class="btn btn-default" onclick="PlaceAdd(this,\''+doc.code+'\',\''+value+'\');">Añadir</a>';
            } },
          ]  


    });
}



function PlaceCancel() {
    $("#place_list").show();
    $("#place_search").hide();
    $("#BtnPlaceAdd").show();
    $("#BtnPlaceCancel").hide();
}



