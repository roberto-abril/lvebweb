﻿$(function () {
    $("#BtnSave").click(CompanySaveStart);
    $("#BtnDelete").click(CompanyDelete);

    TabStart('tab-edit');
            $( "#vxvenue_save" ).validate( {
                rules: {
                    Name: "required",
                    AddStreet: "required",
                    Description:"required",
                    AddProvinceCode:"required"
                },
                messages: {
                    Name: {
                        required: "Campo requerido"
                    },
                    AddStreet: {
                        required: "Campo requerido"
                    },
                    Description: {
                        required: "Campo requerido"
                    }
                }
            });


    $("#AddProvinceCode").change(function(){
        DICLoadCities('AddCityCode',$("#AddProvinceCode").val());
    });
    UsersLoad();
    PlacesLoad()
});

function CompanySaveStart() {
    if($("#vxvenue_save").valid()){
        $.post( '/Admin/Companies/CompanyEdit/'+$("#EditCode").val(), $('#vxvenue_save').serialize(), 
        function(data) {
            if(data.wasOk){
                if(data.newCode && data.newCode != '')
                    PageLoad('/Admin/Companies/CompanyEdit/'+data.newCode);

                CompanySaveEnd('BtnSave','Guardado Ok');
            }
            else
                CompanySaveEnd('BtnSave','Error');
        },
        'json' 
        );
    }
}

function CompanySaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}



function CompanyDelete(){
    if (confirm('La empresa sera eliminada de forma permanente. Estas seguro?')) {
        $.post( '/Admin/Companies/Delete/'+$("#EditCode").val(), {}, function(data) {
            if(data.wasOk)
                $("#body").html('Empresa Eliminada!');
            //else
            //    BTNSaveEnd('BtnSave','Error');
           },
           'json' 
        );
    }
    return false;
}

