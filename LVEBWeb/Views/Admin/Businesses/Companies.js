﻿$(function () {
    GridMainStart();
    $("select").change(GridMainStart);
});
var h;

function GridMainStart() {
    if (h < 300)
        h = 300;
    $("#GridMain").jsGrid({
        width: "100%",
        height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            PageNewTab('/Admin/Companies/CompanyEdit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Companies/CompaniesSearch?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteType").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Código", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "typeName", type: "text",title:"Tipo", width: 80},
            { name: "vatnumber", type: "text",title:"Vat Number", width: 80},
            { name: "businessname", type: "text",title:"Nombre Fiscal", width: 80},
            { name: "addCity", type: "text",title:"Ciudad", width: 80},
            { name: "addProvince", type: "text",title:"Provincia", width: 120},
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
           // { name: "updateUser", type: "text",title:"Update User", width: 50 }
            ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


