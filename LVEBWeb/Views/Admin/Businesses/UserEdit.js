﻿$(function () {
    $("#BtnSave").click(UserSaveStart);
    $("#BtnDelete").click(UserDelete);


    TabStart('tab-edit');
            $( "#vxvenue_save" ).validate( {
                rules: {
                    Name: "required",
                    AddStreet: "required",
                    Description:"required",
                    AddProvinceCode:"required"
                },
                messages: {
                    Name: {
                        required: "Campo requerido"
                    },
                    AddStreet: {
                        required: "Campo requerido"
                    },
                    Description: {
                        required: "Campo requerido"
                    }
                }
            });


    $("#AddProvinceCode").change(function(){
        DICLoadCities('AddCityCode',$("#AddProvinceCode").val());
    });
    CompaniesLoad();
});

function UserSaveStart() {
    if($("#vxvenue_save").valid()){
        $.post( '/Admin/Users/UserEdit/'+$("#EditCode").val(), $('#vxvenue_save').serialize(), 
        function(data) {
            if(data.wasOk){
                if(data.newCode && data.newCode != '')
                    PageLoad('/Admin/Users/UserEdit/'+data.newCode);

                CompanySaveEnd('BtnSave','Guardado Ok');
            }
            else
                CompanySaveEnd('BtnSave','Error');
        },
        'json' 
        );
    }
}

function UserSaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}



function UserDelete(){
    if (confirm('El usuario sera eliminada de forma permanente. Estas seguro?')) {
        $.post( '/Admin/Users/Delete/'+$("#EditCode").val(), {}, function(data) {
            if(data.wasOk)
                $("#body").html('Usuario Eliminado!');
            //else
            //    BTNSaveEnd('BtnSave','Error');
           },
           'json' 
        );
    }
    return false;
}



function CompaniesLoad() {
    if($("#EditCode").val() == '') return;
    //if (h < 300)
    //    h = 300;
    $("#GridCompanies").jsGrid({
        width: "100%",
       // height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/users/CompaniesByUserList?code="+$("#EditCode").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response);
                });
                return d.promise();
            }
        },
        //pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ 
            { name: "companyCode", type: "text",title:"Código", width: 80 },
            { name: "companyName", type: "text",title:"Empresa", width: 80},
            { name: "companyUserTypeName", type: "text",title:"Tipo", width: 80 },
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            { name: "add", type: "text",title:"Borrar", width: 50,itemTemplate: function(value,doc) {
                    return ' <a href="#" class="btn btn-default" onclick="CompanyRemove(this,\''+doc.userCode+'\',\''+doc.companyCode+'\');">Eliminar</a>';
            } },
            ]  


    });

   // $(".jsgrid-grid-body").attr("height",null);
};




function CompanyRemove(btn,userCode,companyCode) {
    if (!confirm('La empresa sera eliminada de forma permanente. Estas seguro?')) {
        return;
    }
    var form = {
        CompanyCode : companyCode,
        UserCode : userCode
        }
    $.post( '/Admin/Companies/UserRemove', form, function(data) {
        if(data){
            CompaniesLoad();
      } else {
          $(btn).addClass('error');
      }
    },
    'json' 
    );
}

