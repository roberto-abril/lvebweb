﻿$(function () {
    GridMainStart();
    $("#btnRefresh").click(function () {
        try {
            $("#GridMain").jsGrid("loadData");
        } catch (e) { }
        return false;
    });
});
var jv = null;


function GridMainStart() {
    $("#GridMain").jsGrid({
        //height: "100%",
        width: "100%",
        sorting: true,
        autoload: true,
        paging: true, 
        controller: {
            loadData: function() {
                var d = $.Deferred();
                $.ajax({
                    url: "/Admin/Tools/Exceptions_Select?FK1="+$("#txtFilter").val(),
                    dataType: "json"
                }).done(function(response) {d.resolve(response.rows);});
                return d.promise();
            }
        },
        fields: [
            { name: "dateInsert", type: "text",title:"DateInsert", width: 80 },
            { name: "method", type: "text",title:"Method", width: 80 },
            { name: "module", type: "textarea",title:"Module", width: 80 },
            { name: "exception", type: "textarea",title:"Exception", width: 200 },
            { name: "notes", type: "text",title:"Notes", width: 80 },

        ], 
        rowDoubleClick: function(args) {
            jv = JsonView('datajson', args.item, jv);
        },


    });

    $(".jsgrid-grid-body").attr("height",null);
};

