﻿$(function () {
    GridMainStart();
    $("#ddlTables").change(GridMainStart);
});


function GridMainStart() {
    $("#GridMain").jsGrid({
        width: "100%",
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            TableValueEdit(value.item);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/EmailsSearch?FK1="+$("#ddlTables").val()+"&FK2="+$("#filterEmailTo").val(),
                    dataType: "json"
                }).done(function(response) {d.resolve(response.rows);});
                return d.promise();
            }
        },
        fields:
          [ 
            { name: "id", type: "text",title:"Id", width: 50 },
            { name: "to", type: "text",title:"To", width: 120},
            { name: "template", type: "text",title:"Template", width: 50 },
            { name: "dateInsert", type: "text",title:"DateInsert", width: 80 },
            { name: "status", type: "text",title:"Status", width: 50 },
             { name: "parameters", type: "text",title:"Parameters", width: 100,itemTemplate: function(value,doc) {
                    return JSON.stringify(doc.parameters);
            } },
            //{ name: "parameters", type: "text",title:"Parameters", width: 200 }
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};

