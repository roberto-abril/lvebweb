﻿$(function () {
    $("#BtnSave").click(TaskGroupSaveStart);
    $("#BtnDelete").click(TaskGroupDelete);
    $( "#form" ).validate( {
        rules: {
            Name: "required",
            AddStreet: "required",
            Description:"required",
            AddProvinceCode:"required",
            Modalities:"required"
        },
        messages: {
            Name: {
                required: "Campo requerido"
            },
            AddStreet: {
                required: "Campo requerido"
            },
            Description: {
                required: "Campo requerido"
            }
        }
    });


    TasksSearch();
    TabStart('tab-edit');
});


function TaskGroupSaveStart() {
    if($("#form").valid()){
        $.ajax({
            type: 'POST',
            url: '/Admin/TasksGroups/Edit/'+$("#EditCode").val(),
            data: new FormData($("#form")[0]),
            processData: false, 
            contentType: false, 
            success: function(data) {
                if(data.wasOk){
                    if(data.newCode && data.newCode != ''){
                        PageLoad('/Admin/TasksGroups/Edit/'+data.newCode);
                    }
                    TaskGroupSaveEnd('BtnSave','Guardado Ok');
                }
                else
                {
                    TaskGroupSaveEnd('BtnSave','Error');
                }
             }
        });
   }
}

function TaskGroupSaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}

function TaskGroupDelete(){
    if (confirm('La competicion sera eliminada de forma permanente. Estas seguro?')) {
        $.post( '/Admin/TasksGroups/Delete/'+$("#EditCode").val(), {}, function(data) {
            if(data.wasOk)
                $("#body").html('Competición Eliminada!');
            else
                BTNSaveEnd('BtnSave','Error');
           },
           'json' 
        );
    }
    return false;
}





function TasksSearch() {

    $("#GridTasks").jsGrid({
        width: "100%",
        height:400,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/TasksGroups/TasksSearch?FK1="+$("#EditCode").val()+"&FK2="+$("#taskFilterCode").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
         fields:
          [ 
            { name: "id", type: "text",title:"Id", width: 80 },
            { name: "relatedCode", type: "text",title:"RelatedCode", width: 80 },
            { name: "relatedNote", type: "text",title:"RelatedNote", width: 80},
            { name: "status", type: "text",title:"Estado", width: 80},
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                    return FormatDate(doc.updateDate);
            } },
            ]  


    });
}






