﻿$(function () {
    GridMainStart();
});

function GridMainStart() {
    $("#GridMain").jsGrid({
        width: "100%",
        //height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            PageNewTab('/Admin/TasksGroups/Edit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/TasksGroups/Search?FK1="+$("#filterName").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        fields:
          [ 
            { name: "code", type: "text",title:"Código", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "start", type: "text",title:"Starty", width: 80,itemTemplate: function(value,doc) {
                    return '<a class="" href="/Admin/tasks/'+doc.code+'" >Comenzar</a>';
            } },
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                     return FormatDate(doc.updateDate);
            } },
           { name: "status", type: "text",title:"Status", width: 50 }
            ]  


    });
};


