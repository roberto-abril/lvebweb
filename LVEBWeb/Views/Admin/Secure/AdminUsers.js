﻿$(function () {
    GridMainStart();
    $("#txtName").keyup(function(e){
        var code = e.which;
        if(code==13) {
            e.preventDefault();
            GridMainStart();
        }
    });
});

function GridMainStart() {
    $("#GridMain").jsGrid({
        width: "100%",
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            UserEdit(value.item);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Secure/AdminUsersSearch?FK1="+$("#filterName").val(),
                    dataType: "json"
                }).done(function(response) {d.resolve(response.rows);});
                return d.promise();
            }
        },
        fields:
          [ //SymbolDetails
            { name: "userId", type: "text",title:"Id", width: 50 },
            { name: "userName", type: "text",title:"Nombre", width: 80, sorting: true},
            { name: "userEmail", type: "text",title:"Email", width: 80, sorting: true},
            { name: "roles", type: "text",title:"Roles", width: 80, sorting: true},
            { name: "updateDate", type: "text",title:"Update Date", width: 50
                //,itemTemplate: function(value,doc) {return doc.updateDate.substr(0,10).replace('T',' ');}
            },
          ]  

    });

    $(".jsgrid-grid-body").attr("height",null);
};


var CurrentDoc;
function UserNew(doc){
    CurrentDoc = doc;
    $("#editUserId,#editUserName,#editRoles").val('');
    $("#DIVUser").show();
}


function UserEdit(doc){
    CurrentDoc = doc;
    $("#editUserId").val(doc.userId);
    $("#editUserName").val(doc.userName);
    $("#editUserEmail").val(doc.userEmail);
    $("#editRoles").val(doc.roles);
    $("#DIVUser").show();
}

function UserEditEnd() {

    var form = {
        UserId : CurrentDoc? CurrentDoc.userId:0,
        UserName : $("#editUserName").val(),
        UserEmail : $("#editUserEmail").val(),
        Roles : $("#editRoles").val(),
        UserPassword : $("#editPassword").val()
    };

    $.post('/Admin/Secure/AdminUserSave',form,function(data){
        GridMainStart();
        $("#DIVUser").hide();
    });
}

