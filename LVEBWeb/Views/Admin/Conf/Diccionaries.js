﻿$(function () {
    GridMainStart();
    $("#ddlTables").change(GridMainStart);
});


function GridMainStart() {
    $("#GridMain").jsGrid({
        width: "100%",
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            TableValueEdit(value.item);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Conf/DiccionariesSearch?FK1="+$("#ddlTables").val(),
                    dataType: "json"
                }).done(function(response) {d.resolve(response.rows);});
                return d.promise();
            }
        },
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Code", width: 80 },
            { name: "name", type: "text",title:"Name", width: 80},
            { name: "updateDate", type: "text",title:"Update Date", width: 60 },
            { name: "updateUser", type: "text",title:"Update User", width: 60 }
          ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


function UpdateData(doc){
    var data = {
        DateLastDownload : doc.DateLastDownload,
        symbol : doc.Symbol
    };

    $.post('/Tinversion/Data/SymbolsWaitingDownload',data,function(data){
        console.log(data);
        GridMainStart();
    });
    
}
var CurrentDoc;
function TableValueNew(doc){
    CurrentDoc = doc;
    $("#editTable").val($("#ddlTables option:selected").text());
    $("#editName,#editCode").val('');
    $("#DIVTableValue").show();
}


function TableValueEdit(doc){
    CurrentDoc = doc;
    $("#editTable").val($("#ddlTables option:selected").text());
    $("#editCode").val(doc.code);
    $("#editName").val(doc.name);
    $("#DIVTableValue").show();
}

function TableValueEnd() {

    var form = {
        TableCode : $("#ddlTables").val(),
        Code : $("#editCode").val(),
        Name : $("#editName").val()
    };

    $.post('/Admin/Conf/DiccionarySave',form,function(data){
        GridMainStart();
        $("#DIVTableValue").hide();
    });
}