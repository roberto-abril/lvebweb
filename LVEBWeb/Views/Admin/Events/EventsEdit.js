﻿$(function () {
    MapStart();
    $("#BtnSave").click(EventSaveStart);
    $("#BtnDelete").click(EventDelete);
        $( "#vxvenue_save" ).validate( {
            rules: {
                Name: "required",
                AddStreet: "required",
                Description:"required",
                AddProvinceCode:"required"
            },
            messages: {
                Name: {
                    required: "Campo requerido"
                },
                AddStreet: {
                    required: "Campo requerido"
                },
                Description: {
                    required: "Campo requerido"
                }
            }
        });


    $("#AddProvinceCode").change(function(){
        DICLoadCities('AddCityCode',$("#AddProvinceCode").val());
    });
    AutoStart();

    TabStart('tab-edit');
});

function EventSaveStart() {
    if($("#vxvenue_save").valid()){
        var data =  new FormData($("#vxvenue_save")[0]);
        
        $.ajax({
            type: 'POST',
            url: '/Admin/Events/Edit/'+$("#EditCode").val(),
            data: data,
            processData: false, 
            contentType: false, 
            success: function(data) {
                if(data.wasOk){
                    if(data.newCode && data.newCode != '')
                        PageLoad('/Admin/Events/Edit/'+data.newCode);

                    BTNSaveEnd('BtnSave','Guardado Ok');
                }
                else
                    BTNSaveEnd('BtnSave','Error');
             }
        });
        
        /*$.post( '/Admin/Events/Edit/'+$("#EditCode").val(), data, 
        function(data) {
            if(data.wasOk){
                if(data.newCode && data.newCode != '')
                    PageLoad('/Admin/Events/Edit/'+data.newCode);

                BTNSaveEnd('BtnSave','Guardado Ok');
            }
            else
                BTNSaveEnd('BtnSave','Error');
        },
        'json' 
        );*/
    }
}

function BTNSaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}

var cache = {};
function AutoStart() {
    $( "#PlaceName" ).autocomplete({
        minLength: 2,
        source: function( request, response ) {
            var term = request.term;
            if ( term in cache ) {
              response( cache[ term ] );
              return;
            }
 
        $.getJSON( "/Admin/PlacesFindCodesFull", request, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      },
      select: function( event, ui ) {
        $("#PlaceCode").val(ui.item.code);
        $("#PlaceName").val(ui.item.label);
        $("#EditLat").val(ui.item.latitude);
        $("#EditLon").val(ui.item.longitude);
        $("#AddProvinceCode").val(ui.item.addProvinceCode);
        $("#AddStreet").val(ui.item.addStreet);
        if($("#AddCityCode option[value='"+ui.item.addCityCode+"']").length > 0)
            $("#AddCityCode").val(ui.item.addCityCode);
        else{
            $("#AddCityCode").append($('<option>', {value:ui.item.addCityCode, text:ui.item.addCityCode}));
            $("#AddCityCode").val(ui.item.addCityCode);
        }
            var lat = parseFloat($("#EditLat").val());
            var lon = parseFloat($("#EditLon").val());
            map.panTo(L.latLng(lat, lon));
            marker.setLatLng(L.latLng(lat, lon));
      }
    });
    
    
    $( "#ManagerName" ).autocomplete({
      minLength: 2,
      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[ term ] );
          return;
        }
 
        $.getJSON( "/Admin/BusinessFindCodes", request, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      },
      select: function( event, ui ) {
        $("#ManagerCode").val(ui.item.id);
      }
    });
}



var map;
var marker;
var mapData = {MapZoom:12};


var iconPOI = L.icon({iconUrl: '/images/marker.png', iconSize:[25, 41],iconAnchor:[12, 41]});
function MapStart() {
    var lat = parseFloat($("#EditLat").val().replace(',','.'));
    var lon = parseFloat($("#EditLon").val().replace(',','.'));

    var tiles = L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&z={z}&x={x}&y={y}&s=Ga', {
                maxZoom: 18,
                attribution: '',
                id: 'tiles_gmap'
            }),
            latlng = L.latLng(lat, lon);
    map = L.map('map_canvas', {center: latlng, zoom: mapData.MapZoom, layers: [tiles]});
    
    marker = L.marker([lat,lon],{icon: iconPOI,draggable: true});
    marker.addTo(map);
    marker.on('dragend', function(event){
        var position = event.target.getLatLng();
        //marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
        //map.panTo(new L.LatLng(position.lat, position.lng))
        
        $("#EditLat").val(position.lat);
        $("#EditLon").val(position.lng);
    });
     
};


function EventDelete(){
    if (confirm('El evento sera eliminado de forma permanente. Estas seguro?')) {
        $.post( '/Admin/Events/Delete/'+$("#EditCode").val(), {}, function(data) {
            if(data.wasOk)
                $("#body").html('Evento Eliminado!');
            // else BTNSaveEnd('BtnSave','Error');
           },
           'json' 
        );
    }
    return false;
}


