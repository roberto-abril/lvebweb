﻿$(function () {

    h = $(window).height() - 150;
    $("#map_canvas").height(h);
    GridMainStart();

    $("#ddlTables").change(GridMainStart);
});
var h;

function GridMainStart() {
    if (h < 300)
        h = 300;
    $("#GridMain").jsGrid({
        width: "100%",
        height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
       /* rowDoubleClick: function(value) {
            PageNewTab('/Events/Edit/'+value.item.code);
        },*/
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/EventsTMP/Search?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Código", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "checkStatus", type: "text",title:"Check Status", width: 80},
            { name: "checkStatusDate", type: "text",title:"Date", width: 80},
            { name: "reporterName", type: "text",title:"R.Name", width: 80},
            { name: "reporterEmail", type: "text",title:"R.Email", width: 80},
            { name: "reporterPhone", type: "text",title:"R.Phone", width: 80},
            { name: "reporterRelation", type: "text",title:"R.Relation", width: 80},
            { name: "placeName", type: "text",title:"Lugar", width: 80},
            { name: "managerName", type: "text",title:"Responsable", width: 80},
            //{ name: "dateStart", type: "text",title:"Fecha", width: 80},
            { name: "dateStart", type: "text",title:"Fecha", width: 70,itemTemplate: function(value,doc) {
                    return doc.dateStart.substr(0,16).replace('T',' ');
            } },
            { name: "duration", type: "text",title:"Duración", width: 50},
            { name: "updateDate", type: "text",title:"Update Date", width: 50,itemTemplate: function(value,doc) {
                     return FormatDate(doc.updateDate);
            } }
            ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


