﻿$(function () {

    h = $(window).height() - 150;
    $("#map_canvas").height(h);
    GridMainStart();

    $("#ddlTables").change(GridMainStart);
});
var h;

function GridMainStart() {
    if (h < 300)
        h = 300;
    $("#GridMain").jsGrid({
        width: "100%",
        height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
          //  PageNewTab('/Competitions/Edit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Messages/Search?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteType").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "messageId", type: "text",title:"Id", width: 80 },
            { name: "contactEmail", type: "text",title:"Email", width: 80 },
            { name: "contactPhone", type: "text",title:"Telefono", width: 80},
            { name: "messageType", type: "text",title:"Tipo", width: 80},
            { name: "messageBody", type: "text",title:"Data", width: 80},
            { name: "inserted", type: "text",title:"Insertado", width: 50,itemTemplate: function(value,doc) {
                    return doc.inserted.substr(0,10);
            } }
            ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


