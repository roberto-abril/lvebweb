﻿$(function () {
    $("#BtnSave").click(CompetitionSaveStart);
    $("#BtnDelete").click(CompetitionDelete);
    $( "#form" ).validate( {
        rules: {
            Name: "required",
            AddStreet: "required",
            Description:"required",
            AddProvinceCode:"required",
            Modalities:"required"
        },
        messages: {
            Name: {
                required: "Campo requerido"
            },
            AddStreet: {
                required: "Campo requerido"
            },
            Description: {
                required: "Campo requerido"
            }
        }
    });


    $("#AddProvinceCode").change(function(){
        DICLoadCities('AddCityCode',$("#AddProvinceCode").val());
    });


    TabStart('tab-edit');
});

function CompetitionSaveStart() {
    if($("#form").valid()){
        $.ajax({
            type: 'POST',
            url: '/Admin/Competitions/Edit/'+$("#EditCode").val(),
            data: new FormData($("#form")[0]),
            processData: false, 
            contentType: false, 
            success: function(data) {
                if(data.wasOk){
                    if(data.newCode && data.newCode != ''){
                        PageLoad('/Admin/Competitions/Edit/'+data.newCode);
                    }
                    CompetitionSaveEnd('BtnSave','Guardado Ok');
                }
                else
                {
                    CompetitionSaveEnd('BtnSave','Error');
                }
             }
        });
   }
}

function CompetitionSaveEnd(id, text){
    var cv = $("#"+id).text();
    $("#"+id).text(text);
    setTimeout(function(){
        $("#"+id).text(cv);
    },3000);
}

function CompetitionDelete(){
    if (confirm('La competicion sera eliminada de forma permanente. Estas seguro?')) {
        $.post( '/Admin/Competitions/Delete/'+$("#EditCode").val(), {}, function(data) {
            if(data.wasOk)
                $("#body").html('Competición Eliminada!');
            else
                BTNSaveEnd('BtnSave','Error');
           },
           'json' 
        );
    }
    return false;
}



