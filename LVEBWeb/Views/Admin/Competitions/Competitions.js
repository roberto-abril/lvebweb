﻿$(function () {

    h = $(window).height() - 150;
    $("#map_canvas").height(h);
    GridMainStart();

    $("#ddlTables").change(GridMainStart);
});
var h;

function GridMainStart() {
    if (h < 300)
        h = 300;
    $("#GridMain").jsGrid({
        width: "100%",
        height:h,
        sorting: true,
        autoload: true,
        filtering:false,
        paging: true, 
        rowDoubleClick: function(value) {
            PageNewTab('/Admin/Competitions/Edit/'+value.item.code);
        },
        controller: {
            loadData: function(filter) {
                
                var d = $.Deferred();
                delete filter.sortField;
                delete filter.sortOrder;
                $.ajax({
                    url: "/Admin/Competitions/Search?FK1="+$("#filterName").val()+"&FK2="+$("#filterProvince").val()+"&FK3="+$("#filteType").val(),
                    dataType: "json"
                }).done(function(response) {
                d.resolve(response.rows);
                    DataRows = response.rows;
                });
                return d.promise();
            }
        },
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total: {itemCount}",
        fields:
          [ //SymbolDetails
            { name: "code", type: "text",title:"Código", width: 80 },
            { name: "name", type: "text",title:"Nombre", width: 80 },
            { name: "modalities", type: "text",title:"Modalities", width: 80},
            { name: "oficialSite", type: "text",title:"OficialSite", width: 80},
            { name: "dateStart", type: "text",title:"Fecha Inicio", width: 50,itemTemplate: function(value,doc) {
                     return doc.dateStart.substr(0,10);
            } },
            { name: "dateEnd", type: "text",title:"Fecha Fin", width: 50,itemTemplate: function(value,doc) {
                     return doc.dateEnd.substr(0,10);
            } },
            { name: "updateDate", type: "text",title:"Updated", width: 50,itemTemplate: function(value,doc) {
                     return FormatDate(doc.updateDate);
            } },
           { name: "status", type: "text",title:"Status", width: 50 }
            ]  


    });

    $(".jsgrid-grid-body").attr("height",null);
};


