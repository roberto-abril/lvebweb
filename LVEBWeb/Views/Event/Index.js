﻿ 
$(function () {
    MapStart();
   
});
 
var iconPOI = L.icon({iconUrl: '/images/marker.png', iconSize:[25, 41],iconAnchor:[12, 41]});
function MapStart() {
    var tiles = L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&z={z}&x={x}&y={y}&s=Ga', {
                maxZoom: 18,
                attribution: '',
                id: 'tiles_gmap'
            }),
            latlng = L.latLng(lat, lon);
    map = L.map('map_canvas', {center: latlng, zoom: 15, layers: [tiles]});
    
    marker = L.marker([lat,lon],{icon: iconPOI});
    marker.addTo(map);

     
};