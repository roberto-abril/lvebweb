﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LVEBWeb.Helpers
{
    public class RedirectingActionSecureAttribute : ActionFilterAttribute
    {

        protected bool HasAccess(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session == null || filterContext.HttpContext.Session.Get<App_Core.Main.Model.SessionUserModel>("CurrentUser") == null)
                return false;


            //  var usersesion = filterContext.HttpContext.Session.Get<App_Core.Main.BE.SessionUserModel>("CurrentUser");
            /* if (!usersesion.UserHasAccess())
                 return false;*/


            return true;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //if (!filterContext.HttpContext.Request.Headers["Referer"].ToString().Contains("localhost"))
            if (!HasAccess(filterContext))
            {
                filterContext.Result = new RedirectResult("/LogIn");


            }
        }
    }


    public class RedirectingActionSecureAdminAttribute : ActionFilterAttribute
    {

        protected bool HasAccess(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session == null || filterContext.HttpContext.Session.Get<App_Core.Admin.BE.SessionAdminUserModel>("CurrentAdminUser") == null)
                return false;


            var usersesion = filterContext.HttpContext.Session.Get<App_Core.Admin.BE.SessionAdminUserModel>("CurrentAdminUser");
            if (!usersesion.UserHasAccess())
                return false;


            return true;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //if (!filterContext.HttpContext.Request.Headers["Referer"].ToString().Contains("localhost"))
            if (!HasAccess(filterContext))
            {
                filterContext.Result = new RedirectResult("/Admin/LogIn");


            }
        }
    }
    /*
    public class RedirectingActionSecureAdminAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var usersesion = filterContext.HttpContext.Session.Get<SessionUserModel>("CurrentAdminUser");
            if (usersesion != null && !usersesion.IsAdmin())
            {
                filterContext.Result = new RedirectResult("/Admin/Home");

            }
        }
    }*/


}