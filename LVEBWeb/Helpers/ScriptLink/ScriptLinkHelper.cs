﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LVEBWeb.Helpers
{

    public static class ScriptLinkHelper
    {

        private static System.Collections.Hashtable _Hashtable = new System.Collections.Hashtable();
        static object lockObject = new object();

        #region  CSS


        public static HtmlString CSSGetFileName(this IHtmlHelper helper)
        {
            List<string> filesType = ScriptLink_Conf.CSSFiles("css/");
            return CSSGetFileName("css_", filesType);
        }

        public static HtmlString CSSGetAdmin(this IHtmlHelper helper)
        {
            List<string> filesType = ScriptLink_Conf.CSSFiles("admin_css/");
            return CSSGetFileName("admin_css", filesType);
        }

        public static HtmlString CSSGetFileName(this IHtmlHelper helper, List<string> files)
        {
            string page = "css_" + helper.ViewContext.RouteData.Values.FirstOrDefault(c => c.Key == "controller").Value + "_" + helper.ViewContext.RouteData.Values.FirstOrDefault(c => c.Key == "action").Value;
            return CSSGetFileName(page, files);
        }
        private static HtmlString CSSGetFileName(string page, List<string> files)
        {

            if (page == "" || files.Count == 0) return new HtmlString("");
            page = page.ToLower();
            bool _isDev = false;
            string fileName = "";

            if (!Configuration.Environment.Equals("PRO"))
            {
                _isDev = true;
                fileName = page + DateTime.Now.ToString("ddMMyy_HHmmsss") + ".css";
            }
            else
            {
                fileName = page + Configuration.ScriptVersion + ".css";
            }

            if (_isDev || !_Hashtable.Contains(page))
            {
                lock (lockObject)
                {
                    //Si es debug eliinamos el fichero actual y creamos el nuevo.
                    if (_isDev && _Hashtable.Contains(page))
                    {
                        string _tmpFileName = (System.String)_Hashtable[page];
                        System.IO.File.Delete(Configuration.PathRoot("wwwroot", "_bs/") + fileName);
                        _Hashtable.Remove(page);
                    }

                    Stream output = File.Create(Configuration.PathRoot("wwwroot", "_bs/") + fileName);

                    using (StreamWriter sw = new StreamWriter(output))
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            if (files[i] != string.Empty)
                            {
                                string content = File.ReadAllText(Configuration.PathRoot(files[i]));
                                //content = content.Replace("{SPRITEVERSION}", Configuration.STYLE_SPRITE_VERSION);
                                content = content.Replace("{HOST}", Configuration.HOST_URL);
                                if (!_isDev && files[i].IndexOf(".min.") == -1)
                                {
                                    content = CSSMinify(content);
                                }
                                sw.WriteLine(content);
                            }
                        }
                    }

                    _Hashtable.Add(page, fileName);
                    fileName = Configuration.HOST_URL + "/_bs/" + fileName;
                }
            }
            else
            {
                fileName = (System.String)_Hashtable[page];
                fileName = Configuration.HOST_URL + "/_bs/" + fileName;
            }
            return new HtmlString("<link href=\"" + fileName + "\" rel=\"stylesheet\" type=\"text/css\" />");
            // return fileName;
        }

        private static string CSSMinify(string source)
        {
            //Remove comments
            string output = System.Text.RegularExpressions.Regex.Replace(source, "/\\*[^*]*\\*+([^/][^*]*\\*+)*/", string.Empty);

            output = output.Replace("" + (char)13 + "" + (char)10 + "", string.Empty);
            output = output.Replace("" + (char)13 + "", string.Empty);
            output = output.Replace("" + (char)10 + "", string.Empty);
            output = output.Replace("" + (char)9 + "", string.Empty);
            output = output.Replace(" : ", ":");
            output = output.Replace(" :", ":");
            output = output.Replace(": ", ":");

            return output;
        }
        #endregion  CSS


        #region JS

        #region JS Google
        /// <summary>
        /// Retorna la informacion para enlazar la api de gole map.
        /// http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyB85o_MxnL4RrcmwDCXGizqWH5DCC0YzAA&callback=MapGMStart
        /// </summary>
        public static HtmlString JSGoogleMap(this IHtmlHelper helper, bool places)
        {
            if (places)
                return new HtmlString("<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyB85o_MxnL4RrcmwDCXGizqWH5DCC0YzAA&sensor=false&libraries=places\"></script>");
            else
                return new HtmlString("<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyB85o_MxnL4RrcmwDCXGizqWH5DCC0YzAA&sensor=false\"></script>");

        }

        #endregion JS Google

        /*
        public static HtmlString JSGetAdmin(this IHtmlHelper helper)
        {
            List<string> _filesType = ScriptLink_Conf.JSFiles("admin_js/");
            return JSGetFileName("admjs_", _filesType);
        }*/
        public static HtmlString JSGetAdmin(this IHtmlHelper helper)
        {
            List<string> _filesType = ScriptLink_Conf.JSFiles("admin_js/");
            return JSGetFileName("admin_js", _filesType);
        }
        public static HtmlString JSGetFileName(this IHtmlHelper helper)
        {
            List<string> _filesType = ScriptLink_Conf.JSFiles("js/");
            return JSGetFileName("js_", _filesType);
        }
        public static HtmlString JSGetFileName(this IHtmlHelper helper, List<string> files)
        {
            string page = "js_" + helper.ViewContext.RouteData.Values.FirstOrDefault(c => c.Key == "controller").Value + "_" + helper.ViewContext.RouteData.Values.FirstOrDefault(c => c.Key == "action").Value;


            return JSGetFileName(page, files);
        }
        private static HtmlString JSGetFileName(string page, List<string> files)
        {

            if (page == "" || files.Count == 0) return new HtmlString("");
            page = page.ToLower();

            bool _isDev = false;
            string fileName = "";

            if (!Configuration.Environment.Equals("PRO"))
            {
                _isDev = true;
                fileName = page + DateTime.Now.ToString("ddMMyy_HHmmsss") + ".js";
            }
            else
            {
                fileName = page + Configuration.ScriptVersion + ".js";
            }


            if (_isDev || !_Hashtable.Contains(page))
            {
                ////string fileName = "";
                //if (!_Hashtable.Contains("js_" + page) || _isDev)
                //{
                lock (lockObject)
                {
                    //Si es debug eliinamos el fichero actual y creamos el nuevo.
                    if (_isDev && _Hashtable.Contains(page))
                    {
                        string _tmpFileName = (System.String)_Hashtable[page];
                        System.IO.File.Delete(Configuration.PathRoot("wwwroot", "_bs/") + fileName);
                        _Hashtable.Remove(page);
                    }

                    Stream output = File.Create(Configuration.PathRoot("wwwroot", "_bs/") + fileName);


                    using (StreamWriter sw = new StreamWriter(output))
                    {

                        for (int i = 0; i < files.Count; i++)
                        {

                            if (!string.IsNullOrEmpty(files[i]) && System.IO.File.Exists(Configuration.PathRoot(files[i])))
                            {

                                if (files[i].Trim().StartsWith("http://"))
                                {
                                    System.IO.StreamReader str = null;

                                    try
                                    {
                                        System.Net.HttpWebRequest fr = default(System.Net.HttpWebRequest);
                                        Uri targetURI = new Uri(files[i]);

                                        fr = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(targetURI);

                                        fr.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; es-ES; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)";

                                        fr.Method = "GET";
                                        fr.KeepAlive = false;
                                        fr.ProtocolVersion = System.Net.HttpVersion.Version10;
                                        fr.AllowAutoRedirect = true;
                                        fr.MaximumAutomaticRedirections = 10;
                                        fr.Timeout = (Int32)new TimeSpan(0, 0, 10).TotalMilliseconds;

                                        if ((fr != null))
                                        {
                                            str = new System.IO.StreamReader(fr.GetResponse().GetResponseStream());
                                            sw.WriteLine(str.ReadToEnd());
                                            str.Close();
                                        }
                                    }
                                    catch (System.Exception ex)
                                    {
                                        ExceptionManager.PublishAsync(ex, "JSGetHtmlTag");
                                        if ((str != null))
                                        {
                                            str.Close();
                                            str.Dispose();
                                        }

                                        if ((sw != null))
                                        {
                                            sw.Close();
                                            sw.Dispose();
                                        }

                                        File.SetCreationTime(Configuration.PathRoot(fileName), System.DateTime.Now.AddDays(-60));
                                    }
                                }
                                else
                                {
                                    string content = File.ReadAllText(Configuration.PathRoot(files[i]));
                                    content = content.Replace("{HOST}", Configuration.HOST_URL);


                                    System.Text.StringBuilder result = new System.Text.StringBuilder();

                                    if (!_isDev && files[i].IndexOf(".min.") == -1)
                                    {
                                        ScriptLink_JSMinifier js = new ScriptLink_JSMinifier();
                                        js.Minify(content, result);
                                        sw.WriteLine(result.ToString());
                                    }
                                    else
                                    {
                                        sw.WriteLine(content);
                                    }
                                }
                            }

                        }
                    }


                    _Hashtable.Add(page, fileName);
                    fileName = Configuration.HOST_URL + "/_bs/" + fileName;
                }
            }
            else
            {
                fileName = (System.String)_Hashtable[page];
                fileName = Configuration.HOST_URL + "/_bs/" + fileName;

            }

            return new HtmlString("<script type=\"text/javascript\" src=\"" + fileName + "\"></script>");



        }

        #endregion JS


    }
}