﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Common;

namespace LVEBWeb.Helpers
{
    /// <summary>
    /// Contine los grupos de ficheros segun el tipo de CSS o JS.
    /// </summary>
    public static class ScriptLink_Conf
    {
		public static List<string> CSSFiles(string path)
        {
            List<string> _rtn = new List<string>();


            string _path = Configuration.PathRoot("Scripts", path);
            string[] _files = System.IO.Directory.GetFiles(_path, "*.css");
            foreach (var f in _files.OrderBy(d => d))
            {
                _rtn.Add(_path + System.IO.Path.GetFileName(f));
            }
            return _rtn;
        }

        public static List<string> JSFiles(string path)
        {
            List<string> _rtn = new List<string>();
            string _path = Configuration.PathRoot("Scripts", path);
            string[] _files = System.IO.Directory.GetFiles(_path, "*.js");
            foreach (var f in _files.OrderBy(d => d))
            {
                _rtn.Add(_path + System.IO.Path.GetFileName(f));
            }
            return _rtn;
        }
    }

}