﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LVEBWeb.Helpers
{
    public static class ConfigHelper
    {

        public static string SiteName<TModel>(this IHtmlHelper<TModel> htmlHelper)
        {
            return "La Vida Es Baile";
        }
        public static string Config_HostName<TModel>(this IHtmlHelper<TModel> htmlHelper)
        {
            return Configuration.HOST_URL;
        }

        public static string IfElse<TModel>(this IHtmlHelper<TModel> htmlHelper, bool check, string isTrue, string isFalse)
        {
            return check ? isTrue : isFalse;
        }

        public static string Concatenate<TModel>(this IHtmlHelper<TModel> htmlHelper, string value)
        {
            return value;
        }
        public static string URLEncode<TModel>(this IHtmlHelper<TModel> htmlHelper, string value)
        {
            return System.Web.HttpUtility.UrlEncode(value);
        }


        static Dictionary<int, string> Months = new Dictionary<int, string>()
        {
            {1,"Enero"}, {2,"Febrero"}, {3,"Marzo"}, {4,"Abril"}, {5,"Mayo"}, {6,"Junio"}, {7,"Julio"}, {8,"Agosto"}, {9,"Septiembre"}, {10,"Octubre"}, {11,"Noviembre"}, {12,"Diciembre"}
        };
        public static string MonthName<TModel>(this IHtmlHelper<TModel> htmlHelper, int monthNumber)
        {
            if (Months.ContainsKey(monthNumber))
                return Months[monthNumber];
            return "";
        }
    }
}