﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class InternalMessageBE
    {
        public string MessageId { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string MessageType { get; set; }
        public string MessageBody { get; set; }
        public DateTime Inserted { get; set; }
    }
}
