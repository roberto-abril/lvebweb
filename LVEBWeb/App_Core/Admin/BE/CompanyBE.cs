﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class CompanyBE : BaseAddBE
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public string VatNumber { get; set; }
        public string BusinessName { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Notes { get; set; }
        public string ContactWeb { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string ContactOthers { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }

        public List<CompanyUserBE> Users { get; set; }
    }
}
