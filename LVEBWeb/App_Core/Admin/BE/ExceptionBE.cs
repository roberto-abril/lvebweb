﻿using System;
using MongoDB.Bson.Serialization.Attributes;
namespace LVEBWeb.App_Core.Admin.BE
{
    [BsonIgnoreExtraElements]
    public class ExceptionBE
    {
        //public string _id;
        public string CustomerId;
        public string Notes;
        public string Module;
        public string Exception;
        public string Method;
        public string DateInsert;
    }
}
