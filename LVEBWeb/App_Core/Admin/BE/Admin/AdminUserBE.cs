﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class AdminUserBE : BaseBE
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string UserEmail { get; set; }
        public string Roles { get; set; }
    }
}
