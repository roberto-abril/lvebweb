﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class TaskBE : BaseBE
    {
        public int Id { get; set; }
        public string RelatedCode { get; set; }
        public string RelatedNote { get; set; }
    }
}
