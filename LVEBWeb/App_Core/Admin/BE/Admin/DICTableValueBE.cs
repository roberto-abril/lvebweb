﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class DICTableValueBE : BaseBE
    {
        public int Code { get; set; }
        public int TableCode { get; set; }
        public string Name { get; set; }
    }
}
