﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class TaskGroupBE : BaseBE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public string Notes { get; set; }
    }
}
