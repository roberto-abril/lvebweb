﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class SocioPaymentMethod
    {

        public string SocioCode { get; set; }
        public int PaymentTypeCode { get; set; }
        public string PaymentType { get; set; }
        public string PaymentNumber { get; set; }
        public string PaymentDetails { get; set; }
    }
}
