﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class EventBE : BaseAddBE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string ManagerCode { get; set; }
        public string ManagerName { get; set; }
        public DateTime DateStart { get; set; }
        public int Duration { get; set; }


        public Boolean HavePoster { get; set; }
        public int Category { get; set; }
    }
}
