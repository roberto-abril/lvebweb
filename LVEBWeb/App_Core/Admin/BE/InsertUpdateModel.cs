﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class InsertUpdateModel
    {
        public InsertUpdateModel(bool wasOk)
        {
            WasOk = wasOk;
            NewCode = "";
        }
        public InsertUpdateModel(bool wasOk, string newCode)
        {
            WasOk = wasOk;
            NewCode = newCode;
        }
        public bool WasOk { get; set; }
        public string NewCode { get; set; }
    }
}
