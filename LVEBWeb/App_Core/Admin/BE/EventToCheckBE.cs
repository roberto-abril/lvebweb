﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class EventToCheckBE : EventBE
    {
        public DateTime CheckStatusDate;
        public int CheckStatus;
        public string ValidatorCode { get; set; }
        public string ReporterName { get; set; }
        public string ReporterPhone { get; set; }
        public string ReporterEmail { get; set; }
        public string ReporterRelation { get; set; }
    }
}
