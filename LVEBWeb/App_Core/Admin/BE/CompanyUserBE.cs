﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class CompanyUserBE : BaseBE
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string UserNickname { get; set; }
        public int CompanyUserType { get; set; }
        public string CompanyUserTypeName;

    }
}
