﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LVEBWeb.App_Core.Admin.BE
{
    public class SessionAdminUserModel
    {
        public int Result;
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        //admin,managescripts,manageapi,billing,manageusers
        public HashSet<string> UserRoles { get; set; }


        public bool IsAdmin()
        {
            if (this.UserRoles == null) return false;
            return this.UserRoles.Contains("admin");
        }

        public bool UserHasAccess()
        {
            return this.UserId > 0;
        }

        public int Attempts { get; set; }


    }
}
