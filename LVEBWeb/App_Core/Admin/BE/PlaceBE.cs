﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class PlaceBE : BaseAddBE
    {
        public PlaceBE()
        {
            Categories = new List<PlaceCategoryBE>();
            Socios = new List<PlaceSocioBE>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool HaveLogo { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }
        public List<PlaceCategoryBE> Categories { get; set; }
        //public string CategoriesString { get; set; }
        public string CategoriesIds { get; set; }
        public string TagsBaileIds { get; set; }
        public string TagsProfileIds { get; set; }
        public string TagsIds { get; set; }
        public List<PlaceSocioBE> Socios { get; set; }
        public string ContactWeb { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string ContactOther { get; set; }

        public string OpenHours { get; set; }

        public string PhotoCodeHome { get; set; }
        public string PhotoCodeIcon { get; set; }

        public int Score { get; set; }
    }
}
