﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class PlaceSocioBE : BaseBE
    {

        public string SocioCode { get; set; }
        public string PlaceCode { get; set; }
        public int PlaceSocioTypeCode { get; set; }
        public string PlaceSocioType { get; set; }
    }
}
