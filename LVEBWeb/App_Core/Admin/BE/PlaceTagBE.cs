﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class PlacePhotoBE : BaseBE
    {
        public string PhotoCode { get; set; }
        public string PlaceCode { get; set; }
        public string Name { get; set; }
    }
}
