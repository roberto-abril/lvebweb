﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class PlaceTagBE : BaseBE
    {
        public string PlaceCode { get; set; }
        public int PlaceTagCode { get; set; }
        public string PlaceTag { get; set; }
    }
}
