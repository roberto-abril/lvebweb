﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class BaseBE
    {
        public int UpdateUser;
        public long UpdateDate;
        public int Status { get; set; }

    }
    public class BaseAddBE : BaseBE
    {
        public string AddCityCode { get; set; }
        public string AddCity { get; set; }
        public string AddProvinceCode { get; set; }
        public string AddProvince { get; set; }
        public string AddStreet { get; set; }
        public string AddStreetNumber { get; set; }
        public string AddCP { get; set; }
    }
}
