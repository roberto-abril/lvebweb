﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class DICCodeName
    {
        public DICCodeName()
        {
        }
        public DICCodeName(string id, string label, string value)
        {
            Id = id;
            Label = label;
            Value = value;
        }
        public string Id { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
    }
}
