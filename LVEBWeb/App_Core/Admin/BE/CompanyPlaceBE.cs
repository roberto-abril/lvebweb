﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class CompanyPlaceBE : BaseBE
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public int CompanyPlaceType { get; set; }
        public string CompanyPlaceTypeName { get; set; }

    }
}
