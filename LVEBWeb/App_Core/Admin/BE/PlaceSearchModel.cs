﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class PlaceSearchModel
    {
        public string Code { get; set; }
        public string Label { get; set; }
        public string AddCityCode { get; set; }
        public string AddProvinceCode { get; set; }
        public string AddStreet { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
