﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace LVEBWeb.App_Core.Admin.BE
{
    public class UserSecureBE : BaseAddBE
    {
        public virtual string UserId { get; set; }
        public virtual string UserName { get; set; }
        public virtual string FullName { get; set; }
        public virtual string Password { get; set; }
        public virtual string Password2 { get; set; }
        public virtual string Email { get; set; }
        public virtual int StatusId { get; set; }
        public virtual byte Attempts { get; set; }


    }
}