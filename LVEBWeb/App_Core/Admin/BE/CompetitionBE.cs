﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class CompetitionBE : BaseBE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Modalities { get; set; }
        public string OficialSite { get; set; }
        public string OficialSiteUrl { get; set; }
        public bool HavePoster { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }


        public string AddProvinceCode { get; set; }
        public string AddProvince { get; set; }
        public string AddressName { get; set; }
        public string AddressLink { get; set; }
        public double Latitude { get; set; }
        public double Longitud { get; set; }

    }
}
