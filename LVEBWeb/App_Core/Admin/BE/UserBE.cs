﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace LVEBWeb.App_Core.Admin.BE
{
    public class UserBE : BaseAddBE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public string Notes { get; set; }

    }
}