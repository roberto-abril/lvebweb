﻿using System;
namespace LVEBWeb.App_Core.Admin.BE
{
    public class PlaceCategoryBE : BaseBE
    {
        public string PlaceCode { get; set; }
        public int PlaceCategoryCode { get; set; }
        public string PlaceCategory { get; set; }
    }
}
