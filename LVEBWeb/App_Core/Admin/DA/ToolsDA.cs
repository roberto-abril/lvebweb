﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
using MongoDB.Driver;
using MongoDB.Bson;
namespace LVEBWeb.App_Core.Admin.DA
{
    public partial class ToolsDA : App_Core.Admin.DA.BaseDA
    {
        public List<ExceptionBE> Exceptions_Search(GridFilterModel gridModel)
        {
            var dbc = DBGetCollection<ExceptionBE>(DBs.Exection, "Exceptions");

            var filters = new List<FilterDefinition<ExceptionBE>>();
            if (!string.IsNullOrEmpty(gridModel.FK1))
            {
                filters.Add(Builders<ExceptionBE>.Filter.Regex(d => d.Module, new BsonRegularExpression("/" + gridModel.FK1 + "/i")));
                filters.Add(Builders<ExceptionBE>.Filter.Regex(d => d.Method, new BsonRegularExpression("/" + gridModel.FK1 + "/i")));
            }
            else
            {
                filters.Add(Builders<ExceptionBE>.Filter.Empty);
            }

            var docs = dbc.Find(Builders<ExceptionBE>.Filter.Or(filters)).ToList();

            return docs;

        }

        public bool Exceptions_Clear()
        {
            var dbc = DBGetCollection<ExceptionBE>(DBs.Exection, "Exceptions");
            dbc.DeleteMany(_ => true);
            return true;
        }

        #region Base
        private static ToolsDA _ManagerSingleton;
        public static ToolsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ToolsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}