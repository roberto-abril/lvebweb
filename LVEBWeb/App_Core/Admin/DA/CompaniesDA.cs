﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class CompaniesDA : App_Core.Admin.DA.BaseDA
    {

        public List<DICCodeName> CompaniesFindCodes(SessionAdminUserModel user, string name)
        {
            var sql = "select s.Code,s.Name from Companies s where deleted = 0 and name like '" + name + "%'";
            var items = new List<DICCodeName>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICCodeName()
                            {
                                Id = reader.GetValue(0).ToString(),
                                Value = reader.GetValue(1).ToString(),
                                Label = reader.GetValue(1).ToString()
                            });
                        }
                    }
                }
            }
            return items;

        }
        public List<CompanyBE> CompaniesSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select c.code,c.Name,c.businessname,c.vatnumber,t.Name,AddCityCode,cit.Name,AddProvinceCode,pro.Name,AddStreet,AddStreetNumber,AddCP,c.UpdateDate,c.UpdateUser " +
              " from companies c " +
              " left join DICTableValues t on c.Type = t.code" +
              " left join DICProvinces pro on c.AddProvinceCode = pro.code " +
              " left join DICCities cit on c.AddCityCode = cit.code";

            sql += " order by c.Name limit 2000";
            var items = new List<CompanyBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new CompanyBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                BusinessName = reader.GetValue(2).ToString(),
                                VatNumber = reader.GetValue(3).ToString(),
                                TypeName = reader.GetValue(4).ToString(),
                                AddCityCode = reader.GetValue(5).ToString(),
                                AddCity = reader.GetValue(6).ToString(),
                                AddProvinceCode = reader.GetValue(7).ToString(),
                                AddProvince = reader.GetValue(8).ToString(),
                                AddStreet = reader.GetValue(9).ToString(),
                                AddStreetNumber = reader.GetValue(10).ToString(),
                                AddCP = reader.GetValue(11).ToString(),
                                UpdateDate = reader.GetInt64(12),
                                UpdateUser = reader.GetInt32(13)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public bool CompanyInsertOrUpdate(SessionAdminUserModel user, CompanyBE value, bool insert)
        {
            var sql = "update companies set Name=@Name,businessname=@businessname,vatnumber=@vatnumber,type=@type,AddCityCode=@AddCityCode,AddProvinceCode=@AddProvinceCode,AddStreet=@AddStreet" +
                ",AddStreetNumber=@AddStreetNumber,AddCP=@AddCP,latitude=@latitude,longitude=@longitude,contactweb=@contactweb,contactphone=@contactphone,contactemail=@contactemail,contactothers=@contactothers" +
                ",notes=@notes,extra1=@extra1,extra2=@extra2,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser" +
                " where Code = @Code ";
            if (insert)
                sql = "insert into companies (name, businessname, vatnumber,type,addcitycode, addprovincecode, addstreet, addstreetnumber, addcp, latitude, longitude" +
                    ", contactweb, contactphone, contactemail, contactothers, notes, extra1, extra2, updatedate, updateuser,code) " +
                    " values (@name, @businessname, @vatnumber,@type,@addcitycode, @addprovincecode, @addstreet, @addstreetnumber, @addcp, @latitude, @longitude" +
                    ", @contactweb, @contactphone, @contactemail, @contactothers, @notes, @extra1, @extra2, @updatedate, @updateuser,@code)  ";
            value.UpdateDate = Common.Functions.DateUpdate();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@businessname", value.BusinessName));
                    cmd.Parameters.Add(new NpgsqlParameter("@VatNumber", value.VatNumber));
                    cmd.Parameters.Add(new NpgsqlParameter("@Type", value.Type));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", value.AddCityCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddProvinceCode", value.AddProvinceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddStreet", value.AddStreet));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddStreetNumber", value.AddStreetNumber));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddCP", value.AddCP));
                    cmd.Parameters.Add(new NpgsqlParameter("@Latitude", value.Latitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@Longitude", value.Longitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@ContactWeb", value.ContactWeb));
                    cmd.Parameters.Add(new NpgsqlParameter("@ContactPhone", value.ContactPhone));
                    cmd.Parameters.Add(new NpgsqlParameter("@ContactEmail", value.ContactEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@ContactOthers", value.ContactOthers));
                    cmd.Parameters.Add(new NpgsqlParameter("@Notes", value.Notes));
                    cmd.Parameters.Add(new NpgsqlParameter("@Extra1", value.Extra1));
                    cmd.Parameters.Add(new NpgsqlParameter("@Extra2", value.Extra2));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public CompanyBE CompanyGet(SessionAdminUserModel user, string code)
        {
            var sql = "select code, name, businessname, vatnumber, type, addcitycode, addprovincecode, addstreet, addstreetnumber, addcp, latitude, longitude" +
                ", contactweb, contactphone, contactemail, contactothers, notes, extra1, extra2, updatedate, updateuser" +
                " from companies as c" +
                " where c.Code = @Code ";
            var company = new CompanyBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            company = new CompanyBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                BusinessName = reader.GetValue(2).ToString(),
                                VatNumber = reader.GetValue(3).ToString(),
                                Type = reader.GetInt32(4),
                                AddCityCode = reader.GetValue(5).ToString(),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddStreet = reader.GetValue(7).ToString(),
                                AddStreetNumber = reader.GetValue(8).ToString(),
                                AddCP = reader.GetValue(9).ToString(),
                                Latitude = reader.GetDouble(10),
                                Longitude = reader.GetDouble(11),
                                ContactWeb = reader.GetValue(12).ToString(),
                                ContactPhone = reader.GetValue(13).ToString(),
                                ContactEmail = reader.GetValue(14).ToString(),
                                ContactOthers = reader.GetValue(15).ToString(),
                                Notes = reader.GetValue(16).ToString(),
                                Extra1 = reader.GetValue(17).ToString(),
                                Extra2 = reader.GetValue(18).ToString(),
                                UpdateDate = reader.GetInt64(19),
                                UpdateUser = reader.GetInt32(20),
                                Users = new List<CompanyUserBE>()
                            };
                        }
                        else
                            return company;
                    }
                }
            }


            return company;
        }

        public List<CompanyUserBE> CompanyUserList(SessionAdminUserModel user, string companyCode, string userCode)
        {
            var sql = "select cu.companycode,c.name,cu.usercode,u.name,u.nickname,cu.companyusertype,ct.name,cu.UpdateDate,cu.UpdateUser" +
                 " from companiesusers cu " +
                 " left join users u on cu.usercode = u.code" +
                 " left join companies c on cu.companycode = c.code" +
                 " left join DICTableValues ct on cu.companyusertype = ct.code " +
                 " where cu.deleted = false and ";
            if (companyCode != null)
                sql += " cu.companycode = '" + companyCode + "' ";
            if (userCode != null)
                sql += " cu.userCode = '" + userCode + "' ";

            var companyUserList = new List<CompanyUserBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    //cmd.Parameters.Add( new NpgsqlParameter( "@companycode", code ) );
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            companyUserList.Add(new CompanyUserBE()
                            {
                                CompanyCode = reader.GetValue(0).ToString(),
                                CompanyName = reader.GetValue(1).ToString(),
                                UserCode = reader.GetValue(2).ToString(),
                                UserName = reader.GetValue(3).ToString(),
                                UserNickname = reader.GetValue(4).ToString(),
                                CompanyUserType = reader.GetInt32(5),
                                CompanyUserTypeName = reader.GetValue(6).ToString(),
                                UpdateDate = reader.GetInt64(7),
                                UpdateUser = reader.GetInt32(8)
                            });
                        }
                    }
                }
            }
            return companyUserList;
        }


        public bool CompanyUserInsert(SessionAdminUserModel user, CompanyUserBE value)
        {
            var sql = "insert into companiesusers (companycode, usercode, companyusertype, updateuser,updatedate) " +
                    "values (@companycode,@usercode,@companyusertype,@UpdateUser,@updatedate) " +
                    " on CONFLICT(companycode, usercode) DO UPDATE SET deleted = false,CompanyUserType=EXCLUDED.CompanyUserType, updateuser = EXCLUDED.updateuser, updatedate= @updatedate";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@companycode", value.CompanyCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@usercode", value.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@CompanyUserType", value.CompanyUserType));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@updatedate", Functions.DateUpdate()));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public bool CompanyUserDelete(SessionAdminUserModel user, CompanyUserBE value)
        {
            var sql = "update companiesusers set Deleted = true,UpdateUser = @UpdateUser  where companycode = @companycode and usercode = @usercode";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@CompanyCode", value.CompanyCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UserCode", value.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }



        public List<CompanyPlaceBE> CompanyPlaceList(SessionAdminUserModel user, string companyCode, string placeCode)
        {
            var sql = "select cp.companycode,c.name,cp.placeCode,p.name,cp.companyplacetype,ct.name,cp.UpdateDate,cp.UpdateUser" +
                 " from companiesplaces cp " +
                 " left join places p on cp.placecode = p.code" +
                 " left join companies c on cp.companycode = c.code" +
                 " left join DICTableValues ct on cp.companyplacetype = ct.code " +
                 " where cp.deleted = false and ";
            if (companyCode != null)
                sql += " cp.companycode = '" + companyCode + "' ";
            if (placeCode != null)
                sql += " cp.placeCode = '" + placeCode + "' ";

            var companyUserList = new List<CompanyPlaceBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    //cmd.Parameters.Add( new NpgsqlParameter( "@companycode", code ) );
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            companyUserList.Add(new CompanyPlaceBE()
                            {
                                CompanyCode = reader.GetValue(0).ToString(),
                                CompanyName = reader.GetValue(1).ToString(),
                                PlaceCode = reader.GetValue(2).ToString(),
                                PlaceName = reader.GetValue(3).ToString(),
                                CompanyPlaceType = reader.GetInt32(4),
                                CompanyPlaceTypeName = reader.GetValue(5).ToString(),
                                UpdateDate = reader.GetInt64(6),
                                UpdateUser = reader.GetInt32(7)
                            });
                        }
                    }
                }
            }
            return companyUserList;
        }


        public bool CompanyPlaceInsert(SessionAdminUserModel user, CompanyPlaceBE value)
        {
            var sql = "insert into companiesplaces (companycode, placecode, CompanyPlaceType, updateuser,updatedate) " +
                    "values (@companycode,@placecode,@CompanyPlaceType,@UpdateUser,@updatedate) " +
                    " on CONFLICT(companycode, placecode) DO UPDATE SET deleted = false,CompanyPlaceType=EXCLUDED.CompanyPlaceType, updateuser = EXCLUDED.updateuser, updatedate= @updatedate";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@companycode", value.CompanyCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@placecode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@CompanyPlaceType", value.CompanyPlaceType));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@updatedate", Functions.DateUpdate()));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public bool CompanyPlaceDelete(SessionAdminUserModel user, CompanyPlaceBE value)
        {
            var sql = "update companiesplaces set Deleted = true,UpdateUser = @UpdateUser  where companycode = @companycode and placecode = @placecode";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@CompanyCode", value.CompanyCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@placecode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }




        #region Base
        private static CompaniesDA _ManagerSingleton;
        public static CompaniesDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompaniesDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}