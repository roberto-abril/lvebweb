﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class UsersDA : App_Core.Admin.DA.BaseDA
    {

        public List<DICCodeName> UsersFindCodes(SessionAdminUserModel user, string name)
        {
            var sql = "select s.Code,s.Name from Users s where deleted = 0 and name like '" + name + "%'";
            var items = new List<DICCodeName>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICCodeName()
                            {
                                Id = reader.GetValue(0).ToString(),
                                Value = reader.GetValue(1).ToString(),
                                Label = reader.GetValue(1).ToString()
                            });
                        }
                    }
                }
            }
            return items;

        }

        public List<UserBE> UsersSeach(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select u.code,u.Name,u.nickname,BirthDate,cit.Name,u.useremail,u.userphone,u.UpdateDate " +
              " from Users u " +
              //" left join DICProvinces pro on u.AddProvinceCode = pro.code " +
              " left join DICCities cit on u.AddCityCode = cit.code";

            sql += " order by u.Name limit 2000";
            var items = new List<UserBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new UserBE()
                            {
                                Code = reader.IsDBNull(0) ? "" : reader.GetValue(0).ToString(),
                                Name = reader.IsDBNull(1) ? "" : reader.GetValue(1).ToString(),
                                Nickname = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                BirthDate = reader.IsDBNull(3) ? new DateTime() : reader.GetDateTime(3),
                                AddCity = reader.IsDBNull(4) ? "" : reader.GetValue(4).ToString(),
                                Email = reader.IsDBNull(5) ? "" : reader.GetValue(5).ToString(),
                                Phone = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                UpdateDate = reader.IsDBNull(7) ? 0 : reader.GetInt64(7)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public bool UserInsertOrUpdate(SessionAdminUserModel user, UserBE value, bool insert)
        {
            var sql = "update Users set Name=@Name,Nickname=@Nickname,BirthDate=@BirthDate,AddCityCode=@AddCityCode,AddProvinceCode=@AddProvinceCode" +
                ",UserEmail=@UserEmail,UserPhone=@UserPhone,Notes=@Notes,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser" +
                " where Code = @Code ";
            if (insert)
                sql = "insert into Users (name, Nickname, BirthDate,addcitycode, addprovincecode" +
                    ", UserEmail, UserPhone,Notes, updatedate, updateuser,code) " +
                    "values (@name, @Nickname, @BirthDate,@addcitycode, @addprovincecode" +
                    ", @UserEmail, @UserPhone,@Notes, @updatedate, @updateuser,@code)  ";
            value.UpdateDate = Common.Functions.DateUpdate();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@Nickname", value.Nickname));
                    cmd.Parameters.Add(new NpgsqlParameter("@BirthDate", value.BirthDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", value.AddCityCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddProvinceCode", value.AddProvinceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UserEmail", value.Email));
                    cmd.Parameters.Add(new NpgsqlParameter("@UserPhone", value.Phone));
                    cmd.Parameters.Add(new NpgsqlParameter("@Notes", value.Notes));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public UserBE UserGet(SessionAdminUserModel user, string code)
        {
            var sql = "select code, name, Nickname, BirthDate, addcitycode, addprovincecode" +
                ",UserEmail, UserPhone,Notes, updatedate, updateuser from Users as c" +
                " where c.Code = @Code ";
            var userModel = new UserBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            userModel = new UserBE()
                            {
                                Code = reader.IsDBNull(0) ? "" : reader.GetValue(0).ToString(),
                                Name = reader.IsDBNull(1) ? "" : reader.GetValue(1).ToString(),
                                Nickname = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                BirthDate = reader.IsDBNull(3) ? new DateTime() : reader.GetDateTime(3),
                                AddCityCode = reader.IsDBNull(4) ? "" : reader.GetValue(4).ToString(),
                                AddProvinceCode = reader.IsDBNull(5) ? "" : reader.GetValue(5).ToString(),
                                Email = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                Phone = reader.IsDBNull(7) ? "" : reader.GetValue(7).ToString(),
                                Notes = reader.IsDBNull(8) ? "" : reader.GetValue(8).ToString(),
                                UpdateDate = reader.IsDBNull(9) ? 0 : reader.GetInt64(9),
                                UpdateUser = reader.IsDBNull(10) ? 0 : reader.GetInt32(10)
                            };
                        }
                        else
                            return userModel;
                    }
                }
            }



            return userModel;
        }



        public bool UserDelete(SessionAdminUserModel user, UserBE value)
        {
            var sql = "update Users set Deleted = true,UpdateUser = @UpdateUser,UpdateDate=@UpdateDate where code = @code";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Functions.DateUpdate()));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }



        #region Base
        private static UsersDA _ManagerSingleton;
        public static UsersDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new UsersDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}