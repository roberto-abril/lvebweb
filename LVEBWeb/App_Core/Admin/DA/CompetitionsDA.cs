﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class CompetitionsDA : App_Core.Admin.DA.BaseDA
    {
        public List<CompetitionBE> CompetitionsSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select c.Code,c.Name,Description,Modalities,DateStart,DateEnd,AddProvinceCode,pro.Name,AddressName,OficialSite,HavePoster,c.UpdateDate,c.Status " +
              " from Competitions c " +
              " left join DICProvinces pro on c.AddProvinceCode = pro.code " +
              " where c.deleted = false";

            sql += " order by c.DateStart desc limit 2000";
            var items = new List<CompetitionBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new CompetitionBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                Modalities = reader.GetValue(3).ToString(),
                                DateStart = reader.GetDateTime(4),
                                DateEnd = reader.GetDateTime(5),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddProvince = reader.GetValue(7).ToString(),
                                AddressName = reader.GetValue(8).ToString(),
                                OficialSite = reader.GetValue(9).ToString(),
                                HavePoster = reader.GetBoolean(10),
                                UpdateDate = reader.GetInt64(11),
                                Status = reader.GetInt32(12)
                            });
                        }
                    }
                }
                return items;
            }
        }

        public bool CompetitionInsertOrUpdate(SessionAdminUserModel user, CompetitionBE value, bool insert)
        {
            var sql = "update Competitions set Name=@Name,Description=@Description,Modalities=@Modalities,DateStart=@DateStart,DateEnd=@DateEnd,OficialSite=@OficialSite,OficialSiteUrl=@OficialSiteUrl,AddProvinceCode=@AddProvinceCode," +
            "AddressName=@AddressName,AddressLink=@AddressLink,Latitude=@Latitude,Longitud=@Longitud,HavePoster=@HavePoster,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser,Status=@Status where Code = @Code ";
            if (insert)
                sql = "insert into Competitions (Name,Description,Modalities,DateStart,DateEnd,OficialSite,OficialSiteUrl,AddProvinceCode,AddressName,AddressLink,Latitude,Longitud,HavePoster,UpdateDate,UpdateUser,Status,Code) " +
                    "values (@Name,@Description,@Modalities,@DateStart,@DateEnd,@OficialSite,@OficialSiteUrl,@AddProvinceCode,@AddressName,@AddressLink,@Latitude,@Longitud,@HavePoster,@UpdateDate,@UpdateUser,@Status,@Code)  ";
            value.UpdateDate = Common.Functions.DateUpdate();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@Description", value.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("@Modalities", value.Modalities));
                    cmd.Parameters.Add(new NpgsqlParameter("@DateStart", value.DateStart));
                    cmd.Parameters.Add(new NpgsqlParameter("@DateEnd", value.DateEnd));
                    cmd.Parameters.Add(new NpgsqlParameter("@OficialSite", value.OficialSite));
                    cmd.Parameters.Add(new NpgsqlParameter("@OficialSiteUrl", value.OficialSiteUrl));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddProvinceCode", value.AddProvinceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddressName", value.AddressName));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddressLink", value.AddressLink));
                    cmd.Parameters.Add(new NpgsqlParameter("@Latitude", value.Latitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@Longitud", value.Longitud));
                    cmd.Parameters.Add(new NpgsqlParameter("@HavePoster", value.HavePoster));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", value.Status));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();

                }
            }

            this.DataLastUpdateSave(DataTypes.Competitions, value.UpdateDate);
            return true;
        }


        public CompetitionBE CompetitionGet(SessionAdminUserModel user, string code)
        {
            var sql = "select s.Code,s.Name,Description,Modalities,DateStart,DateEnd,AddProvinceCode,AddressName,AddressLink,OficialSite,OficialSiteUrl,Latitude,Longitud,HavePoster,s.UpdateDate,s.UpdateUser,s.Status" +
                " from Competitions as s" +
                //" left join DICTableValues tv on s.LegalIDTypeCode = tv.code " +
                " where s.Code = @Code ";


            var Competition = new CompetitionBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Competition = new CompetitionBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                Modalities = reader.GetValue(3).ToString(),
                                DateStart = reader.GetDateTime(4),
                                DateEnd = reader.GetDateTime(5),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddressName = reader.GetValue(7).ToString(),
                                AddressLink = reader.GetValue(8).ToString(),
                                OficialSite = reader.GetValue(9).ToString(),
                                OficialSiteUrl = reader.GetValue(10).ToString(),
                                Latitude = reader.GetDouble(11),
                                Longitud = reader.GetDouble(12),
                                HavePoster = reader.GetBoolean(13),
                                UpdateDate = reader.GetInt64(14),
                                UpdateUser = reader.GetInt32(15),
                                Status = reader.GetInt32(16)
                            };
                        }
                        else
                            return Competition;
                    }
                }
            }


            return Competition;
        }


        public bool CompetitionDelete(SessionAdminUserModel user, string code)
        {
            var sql = "update Competitions set Deleted = true,UpdateUser = @UpdateUser where code = @Code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }




        #region Base
        private static CompetitionsDA _ManagerSingleton;
        public static CompetitionsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompetitionsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}