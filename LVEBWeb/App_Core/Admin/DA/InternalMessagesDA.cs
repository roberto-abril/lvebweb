﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class InternalMessagesDA : App_Core.Admin.DA.BaseDA
    {
        public List<InternalMessageBE> MessagesSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select messageid,contactemail,contactphone,messagetype,messagebody,inserted " +
              " from internalmessages m ";

            sql += " order by inserted limit 2000";
            var items = new List<InternalMessageBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new InternalMessageBE()
                            {
                                MessageId = reader.GetValue(0).ToString(),
                                ContactEmail = reader.GetValue(1).ToString(),
                                ContactPhone = reader.GetValue(2).ToString(),
                                MessageType = reader.GetValue(3).ToString(),
                                MessageBody = reader.GetValue(4).ToString(),
                                Inserted = reader.GetDateTime(5)
                            });
                        }
                    }
                }
            }
            return items;
        }



        public InternalMessageBE MessageGet(SessionAdminUserModel user, string messageid)
        {
            var sql = "select messageid,contactemail,contactphone,messagetype,messagebody,inserted " +
              " from internalmessages m " +
                " where m.messageid = @messageid ";


            var Competition = new InternalMessageBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@messageid", messageid));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Competition = new InternalMessageBE()
                            {
                                MessageId = reader.GetValue(0).ToString(),
                                ContactEmail = reader.GetValue(1).ToString(),
                                ContactPhone = reader.GetValue(2).ToString(),
                                MessageType = reader.GetValue(3).ToString(),
                                MessageBody = reader.GetValue(4).ToString(),
                                Inserted = reader.GetDateTime(5)
                            };
                        }
                        else
                            return Competition;
                    }
                }
            }


            return Competition;
        }



        #region Base
        private static InternalMessagesDA _ManagerSingleton;
        public static InternalMessagesDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new InternalMessagesDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}