﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class ConfDA : App_Core.Admin.DA.BaseDA
    {
        public List<DICTableBE> TableList(SessionAdminUserModel user)
        {
            var sql = "select TableName, TableCode  from DICTables order by TableName ";

            var items = new List<DICTableBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICTableBE()
                            {
                                TableName = reader.GetValue(0).ToString(),
                                TableCode = reader.GetInt32(1)
                            });
                        }
                    }
                }
            }
            return items;
        }
        public List<DICTableValueBE> TableValueList(SessionAdminUserModel user, string tableCode)
        {

            var sql = "select Code,Name,UpdateDate,UpdateUser from DICTableValues where TableCode = '" + tableCode + "' order by Name ";

            var items = new List<DICTableValueBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICTableValueBE()
                            {
                                Code = reader.GetInt32(0),
                                Name = reader.GetValue(1).ToString(),
                                UpdateDate = reader.GetInt64(2),
                                UpdateUser = reader.GetInt32(3),
                                TableCode = Convert.ToInt32(tableCode)
                            });
                        }
                    }
                }
            }
            return items;
        }
        public int TableValueMaxCode(SessionAdminUserModel user, int tableCode)
        {

            var sql = "select max(Code) from DICTableValues where TableCode = '" + tableCode + "' ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    var aux = cmd.ExecuteScalar();
                    if (aux.ToString() == "")
                        return 0;
                    else
                        return Convert.ToInt32(aux);
                }
            }
        }

        public bool TableValueInsertOrUpdate(SessionAdminUserModel user, DICTableValueBE value, bool insert)
        {
            var sql = "update DICTableValues set Name = @Name, UpdateUser = @UpdateUser,UpdateDate=@UpdateDate where Code = @Code ";
            if (insert)
                sql = "insert into DICTableValues (Name,UpdateUser,Code,UpdateDate,TableCode) values (@Name,@UpdateUser,@Code,@UpdateDate,@TableCode)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Functions.DateUpdate()));
                    if (insert)
                        cmd.Parameters.Add(new NpgsqlParameter("@TableCode", value.TableCode));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }





        public List<Common.EmailBE> EmailList(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            if (gridModel.FK1 != "EmailsSent")
                gridModel.FK1 = "EmailsWaiting";
            var sql = "select Id,EmailTo,Template,Status,Parameters,DateInsert from " + gridModel.FK1;

            if (!string.IsNullOrEmpty(gridModel.FK2))
                sql += " where EmailTo = @EmailTo ";

            sql += " order by DateInsert limit 1000";

            var items = new List<Common.EmailBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();



                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    if (!string.IsNullOrEmpty(gridModel.FK2))
                        cmd.Parameters.Add(new NpgsqlParameter("@EmailTo", gridModel.FK2));

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(new Common.EmailBE()
                            {
                                Id = reader.GetInt64(0),
                                To = reader.GetString(1),
                                Template = (Common.EmailTemplates)reader.GetInt32(2),
                                Status = reader.GetInt32(3),
                                ParametersCopy = reader.GetString(4),
                                DateInsert = reader.GetDateTime(5)
                            });
                        }
                    }
                }
            }
            return items;
        }


        #region Base
        private static ConfDA _ManagerSingleton;
        public static ConfDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ConfDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}