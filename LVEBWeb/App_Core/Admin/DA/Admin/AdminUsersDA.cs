﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class AdminUsersDA : App_Core.Admin.DA.BaseDA
    {
        public List<AdminUserBE> UsersSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select UserId,UserName,UserEmail,Roles,UpdateDate,UpdateUser " +
              //" ,(select  Group_Concat(Name) from PlacesTags pt left join DICTableValues ptv on pt.PlaceTagCode = ptv.code where pt.PlaceCode = p.code)" +
              " from AdminUsers ";

            sql += " order by UserName limit 2000";
            var items = new List<AdminUserBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new AdminUserBE()
                            {
                                UserId = reader.GetInt32(0),
                                UserName = reader.GetValue(1).ToString(),
                                UserEmail = reader.GetValue(2).ToString(),
                                Roles = reader.GetValue(3).ToString(),
                                UpdateDate = reader.GetInt64(4),
                                UpdateUser = reader.GetInt32(5)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public bool UserInsertOrUpdate(SessionAdminUserModel user, AdminUserBE value, bool insert)
        {
            var sql = "update AdminUsers set UserName=@UserName,UserEmail=@UserEmail,Roles=@Roles,UpdateDate=@updatedate,UpdateUser=@UpdateUser";
            if (string.IsNullOrEmpty(value.UserPassword))
                sql += ",UserPassword=UserPassword";
            sql += " where UserId = @UserId ";

            if (insert)
                sql = "insert into AdminUsers (UserName,UserEmail,Roles,UpdateDate,UpdateUser,UserPassword) " +
                    "values (@UserName,@UserEmail,@Roles,@updatedate,@UpdateUser,@UserPassword)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UserName", value.UserName));
                    cmd.Parameters.Add(new NpgsqlParameter("@UserEmail", value.UserEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@Roles", value.Roles));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Functions.DateUpdate()));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    if (!string.IsNullOrEmpty(value.UserPassword))
                        cmd.Parameters.Add(new NpgsqlParameter("@UserPassword", value.UserPassword));
                    //  if (value.UserId > 0)
                    if (!insert)
                        cmd.Parameters.Add(new NpgsqlParameter("@UserId", value.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }





        #region Base
        private static AdminUsersDA _ManagerSingleton;
        public static AdminUsersDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new AdminUsersDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}