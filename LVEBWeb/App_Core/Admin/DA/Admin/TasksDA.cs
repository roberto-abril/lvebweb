﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class TasksDA : App_Core.Admin.DA.BaseDA
    {
        public List<TaskGroupBE> TaskGroupsSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select code, name, status,type from TasksGroups where deleted = false order by UpdateDate desc ";

            var items = new List<TaskGroupBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new TaskGroupBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Status = reader.GetInt32(2),
                                Type = reader.GetInt32(3)
                            });
                        }
                    }
                }
            }
            return items;
        }
        public bool TaskGroupInsertOrUpdate(SessionAdminUserModel user, TaskGroupBE value, bool insert)
        {
            var sql = "update TasksGroups set Name = @Name,Notes=@Notes,status=@status,Type=@Type, UpdateUser = @UpdateUser,UpdateDate=@UpdateDate where Code = @Code ";
            if (insert)
                sql = "insert into TasksGroups (Code,Name,Notes,status,Type,UpdateUser,UpdateDate) values (@Code,@Name,@Notes,@status,@Type,@UpdateUser,@UpdateDate)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@Notes", value.Notes));
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", value.Status));
                    cmd.Parameters.Add(new NpgsqlParameter("@Type", value.Type));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Functions.DateUpdate()));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public TaskGroupBE TaskGroupGet(SessionAdminUserModel user, string code)
        {
            var sql = "select code, name,notes,status,type,UpdateDate,UpdateUser from TasksGroups where Code = @Code ";
            var company = new TaskGroupBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            company = new TaskGroupBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Notes = reader.GetValue(2).ToString(),
                                Status = reader.GetInt32(3),
                                Type = reader.GetInt32(4),
                                UpdateDate = reader.GetInt64(5),
                                UpdateUser = reader.GetInt32(6)
                            };
                        }
                        else
                            return company;
                    }
                }
            }


            return company;
        }



        public List<TaskBE> TaskSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select id, RelatedCode, RelatedNote, status,UpdateDate from Tasks where taskGroupCode = @taskGroupCode order by UpdateDate desc ";
            var items = new List<TaskBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@taskGroupCode", gridModel.FK1));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(new TaskBE()
                            {
                                Id = reader.GetInt32(0),
                                RelatedCode = reader.GetValue(1).ToString(),
                                RelatedNote = reader.GetValue(2).ToString(),
                                Status = reader.GetInt32(3)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public bool TaskGroupDelete(SessionAdminUserModel user, string code)
        {
            var sql = "update TasksGroups set Deleted = true,UpdateUser = @UpdateUser,UpdateDate=@UpdateDate  where code = @code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Functions.DateUpdate()));
                    cmd.Parameters.Add(new NpgsqlParameter("@code", code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool TaskStatusChange(SessionAdminUserModel user, int taskId, int status)
        {
            var sql = "update tasks set status = @status,UpdateUser = @UpdateUser,UpdateDate=@UpdateDate  where id = @taskId ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", status));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Functions.DateUpdate()));
                    cmd.Parameters.Add(new NpgsqlParameter("@taskId", taskId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public TaskBE TaskGetNext(SessionAdminUserModel user, string taskGroupCode, int lastTask)
        {
            var sql = @"select id,relatedcode 
            from tasks where taskgroupcode = @taskGroupCode and deleted = false and status in (230001,230002) limit 1  ";
            // SELECT id, taskgroupcode
            var taskNext = new TaskBE() { Id = 0 };
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@taskGroupCode", taskGroupCode));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            taskNext = new TaskBE()
                            {
                                Id = reader.GetInt32(0),
                                RelatedCode = reader.GetValue(1).ToString()
                            };
                        }
                        else
                            return taskNext;
                    }
                }
            }


            return taskNext;
        }



        #region Base
        private static TasksDA _ManagerSingleton;
        public static TasksDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new TasksDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}