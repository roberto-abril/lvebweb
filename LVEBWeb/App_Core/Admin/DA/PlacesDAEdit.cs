﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public partial class PlacesDA
    {
        public bool PlaceInsertOrUpdate(SessionAdminUserModel user, PlaceBE value, bool insert)
        {
            try
            {
                var sql = "update Places set Name=@Name,AddCityCode=@AddCityCode,AddProvinceCode=@AddProvinceCode,AddStreet=@AddStreet,AddStreetNumber=@AddStreetNumber,AddCP=@AddCP,Description=@Description,DescriptionFull=@DescriptionFull" +
                    ",Latitude=@Latitude,Longitude=@Longitude,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser,Status=@Status,Deleted=false,HaveLogo=@HaveLogo" +
                    ",ContactWeb=@ContactWeb,ContactPhone=@ContactPhone,ContactEmail=@ContactEmail,ContactOther=@ContactOther,OpenHours=@OpenHours,PhotoCodeHome=@PhotoCodeHome,PhotoCodeIcon=@PhotoCodeIcon,Score=@Score" +
                    " where Code = @Code ";
                if (insert)
                    sql = "insert into Places (Name,AddCityCode,AddProvinceCode,AddStreet,AddStreetNumber,AddCP,Description,DescriptionFull" +
                        ",Latitude,Longitude,UpdateDate,UpdateUser,Code,Status,HaveLogo" +
                        ",ContactWeb,ContactPhone,ContactEmail,ContactOther,OpenHours,PhotoCodeHome,PhotoCodeIcon) " +
                        "values (@Name,@AddCityCode,@AddProvinceCode,@AddStreet,@AddStreetNumber,@AddCP,@Description,@DescriptionFull" +
                        ",@Latitude,@Longitude,@UpdateDate,@UpdateUser,@Code,@Status,@HaveLogo" +
                        ",@ContactWeb,@ContactPhone,@ContactEmail,@ContactOther,@OpenHours,@PhotoCodeHome,@PhotoCodeIcon)  ";
                value.UpdateDate = Functions.DateUpdate();
                using (var conn = new NpgsqlConnection(Configuration.DBConnection))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                        cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", value.AddCityCode is null ? "" : value.AddCityCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@AddProvinceCode", value.AddProvinceCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@AddStreet", value.AddStreet is null ? "" : value.AddStreet));
                        cmd.Parameters.Add(new NpgsqlParameter("@AddStreetNumber", value.AddStreetNumber is null ? "" : value.AddStreetNumber));
                        cmd.Parameters.Add(new NpgsqlParameter("@AddCP", value.AddCP is null ? "" : value.AddCP));
                        cmd.Parameters.Add(new NpgsqlParameter("@Description", value.Description));
                        cmd.Parameters.Add(new NpgsqlParameter("@DescriptionFull", value.DescriptionFull));
                        cmd.Parameters.Add(new NpgsqlParameter("@Latitude", value.Latitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@Longitude", value.Longitude));
                        cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                        cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                        cmd.Parameters.Add(new NpgsqlParameter("@Status", value.Status));
                        cmd.Parameters.Add(new NpgsqlParameter("@HaveLogo", value.HaveLogo));
                        cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                        cmd.Parameters.Add(new NpgsqlParameter("@ContactWeb", value.ContactWeb));
                        cmd.Parameters.Add(new NpgsqlParameter("@ContactPhone", value.ContactPhone));
                        cmd.Parameters.Add(new NpgsqlParameter("@ContactEmail", value.ContactEmail));
                        cmd.Parameters.Add(new NpgsqlParameter("@ContactOther", value.ContactOther));
                        cmd.Parameters.Add(new NpgsqlParameter("@OpenHours", value.OpenHours));
                        cmd.Parameters.Add(new NpgsqlParameter("@PhotoCodeHome", value.PhotoCodeHome));
                        cmd.Parameters.Add(new NpgsqlParameter("@PhotoCodeIcon", value.PhotoCodeIcon));
                        cmd.Parameters.Add(new NpgsqlParameter("@Score", value.Score));
                        cmd.ExecuteNonQuery();
                    }
                }

                this.DataLastUpdateSave(DataTypes.Places, value.UpdateDate);
                return true;
            }
            catch (PostgresException ex)
            {
                return false;
            }
        }
        public bool PlaceDelete(SessionAdminUserModel user, string code)
        {
            var sql = "update Places set Deleted = true,UpdateUser = @UpdateUser, UpdateDate = @UpdateDate where Code = @Code";
            var updateDate = Common.Functions.DateUpdate();

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", updateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public PlaceBE PlaceGet(SessionAdminUserModel user, string code)
        {
            var sql = "select p.Code,p.Name,AddCityCode,cit.Name AddCity,AddProvinceCode,'' AddProvince,AddStreet,AddStreetNumber,AddCP,Description,DescriptionFull,Latitude,Longitude,p.UpdateDate,p.UpdateUser,p.HaveLogo" +
                 ",array_to_string(array(select placetagcode from PlacesTags pt left join DICTableValues ptv on pt.PlaceTagCode = ptv.code where pt.PlaceCode = p.code and pt.Deleted = false),',')" +
                 ",array_to_string(array(select placecategorycode from PlacesCategories pt left join DICTableValues ptv on pt.PlaceCategoryCode = ptv.code where pt.PlaceCode = p.code and pt.Deleted = false),',')" +
                 ",p.ContactWeb,p.ContactPhone,p.ContactEmail,p.ContactOther,p.OpenHours,p.PhotoCodeHome,p.PhotoCodeIcon,p.status,p.Score" +
            " from Places as p" +
            " left join DICCities cit on p.AddCityCode = cit.code" +
            " where p.deleted = false and p.Code = @Code ";

            var place = new PlaceBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {

                        if (reader.Read())
                        {

                            place = new PlaceBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                AddCityCode = reader.GetValue(2).ToString(),
                                AddCity = reader.GetValue(3).ToString(),
                                AddProvinceCode = reader.GetValue(4).ToString(),
                                AddProvince = reader.GetValue(5).ToString(),
                                AddStreet = reader.GetValue(6).ToString(),
                                AddStreetNumber = reader.GetValue(7).ToString(),
                                AddCP = reader.GetValue(8).ToString(),
                                Description = reader.GetValue(9).ToString(),
                                DescriptionFull = reader.GetValue(10).ToString(),
                                Latitude = reader.GetDouble(11),
                                Longitude = reader.GetDouble(12),
                                UpdateDate = reader.GetInt64(13),
                                UpdateUser = reader.GetInt32(14),
                                HaveLogo = reader.GetBoolean(15),
                                TagsIds = reader.GetValue(16).ToString(),
                                CategoriesIds = reader.GetValue(17).ToString(),
                                Socios = new List<PlaceSocioBE>(),
                                ContactWeb = reader.GetValue(18).ToString(),
                                ContactPhone = reader.GetValue(19).ToString(),
                                ContactEmail = reader.GetValue(20).ToString(),
                                ContactOther = reader.GetValue(21).ToString(),
                                OpenHours = reader.GetValue(22).ToString(),
                                PhotoCodeHome = reader.GetValue(23).ToString(),
                                PhotoCodeIcon = reader.GetValue(24).ToString(),
                                Status = reader.GetInt32(25),
                                Score = reader.GetInt32(26)

                            };




                        }
                        else
                            return place;
                    }
                }
            }
            /*
            sql = "select PlaceCode,PlaceCategoryCode,pc.UpdateDate,pc.UpdateUser, tv.name" +
                " from PlacesCategories pc " +
                " left join DICTableValues tv on pc.PlaceCategoryCode = tv.code " +
                " where PlaceCode = @Code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            place.Categories.Add(new PlaceCategoryBE()
                            {
                                PlaceCode = reader.GetValue(0).ToString(),
                                PlaceCategoryCode = reader.GetInt32(1),
                                UpdateDate = reader.GetDateTime(2),
                                UpdateUser = reader.GetValue(3).ToString(),
                                PlaceCategory = reader.GetValue(4).ToString()
                            });
                        }
                    }
                }
            }
*/

            sql = "select SocioCode,PlaceCode,PlaceSocioTypeCode,pc.UpdateDate,pc.UpdateUser, tv.name" +
                " from PlacesSocios pc " +
                " left join DICTableValues tv on pc.PlaceSocioTypeCode = tv.code " +
                " where PlaceCode = @Code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            place.Socios.Add(new PlaceSocioBE()
                            {
                                SocioCode = reader.GetValue(0).ToString(),
                                PlaceCode = reader.GetValue(1).ToString(),
                                PlaceSocioTypeCode = reader.GetInt32(2),
                                UpdateDate = reader.GetInt64(2),
                                UpdateUser = reader.GetInt32(3),
                                PlaceSocioType = reader.GetValue(4).ToString()
                            });
                        }
                    }
                }
            }

            return place;
        }

        public bool PlaceCategoryInsert(SessionAdminUserModel user, PlaceCategoryBE value)
        {
            if (value.PlaceCategoryCode == 0)
            {
                var sql = "insert into PlacesCategories (PlaceCode,PlaceCategoryCode,UpdateUser) " +
                          "values (@PlaceCode,(select code from DICTableValues where TableCode = 11 and name = @PlaceCategory),@UpdateUser)  ";

                using (var conn = new NpgsqlConnection(Configuration.DBConnection))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceCategory", value.PlaceCategory));
                        cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                var sql = "insert into PlacesCategories (PlaceCode,PlaceCategoryCode,UpdateUser) " +
                        "values (@PlaceCode,@PlaceCategoryCode,@UpdateUser)  ";

                using (var conn = new NpgsqlConnection(Configuration.DBConnection))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceCategoryCode", value.PlaceCategoryCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            return true;
        }
        public bool PlaceCategoryDelete(SessionAdminUserModel user, PlaceCategoryBE value)
        {
            var sql = "update PlacesCategories set Deleted = true,UpdateUser = @UpdateUser where PlaceCode = @PlaceCode and PlaceCategoryCode = @PlaceCategoryCode";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCategoryCode", value.PlaceCategoryCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool PlaceTagInsert(SessionAdminUserModel user, PlaceTagBE value)
        {
            if (value.PlaceTagCode == 0)
            {
                var sql = "insert into PlacesTags (PlaceCode,PlaceTagCode,UpdateUser) " +
                          "values (@PlaceCode,(select code from DICTableValues where TableCode = 11 and name = @PlaceCategory),@UpdateUser)  ";

                using (var conn = new NpgsqlConnection(Configuration.DBConnection))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceTag", value.PlaceTag));
                        cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                var sql = "insert into PlacesTags (PlaceCode,PlaceTagCode,UpdateUser) " +
                        "values (@PlaceCode,@PlaceTagCode,@UpdateUser)  ";

                using (var conn = new NpgsqlConnection(Configuration.DBConnection))
                {
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@PlaceTagCode", value.PlaceTagCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            return true;
        }
        public bool PlaceTagDelete(SessionAdminUserModel user, PlaceTagBE value)
        {
            var sql = "update PlacesTags set Deleted = true,UpdateUser = @UpdateUser where PlaceCode = @PlaceCode and PlaceTagCode = @PlaceTagCode";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceTagCode", value.PlaceTagCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }



        public bool PlaceSocioInsert(SessionAdminUserModel user, PlaceSocioBE value)
        {
            var sql = "insert into PlacesSocios (PlaceCode,SocioCode,PlaceSocioTypeCode,UpdateUser) " +
                    "values (@PlaceCode,@SocioCode,@PlaceSocioTypeCode,@UpdateUser)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@SocioCode", value.SocioCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceSocioTypeCode", value.PlaceSocioTypeCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        public bool PlaceSocioDelete(SessionAdminUserModel user, PlaceSocioBE value)
        {
            var sql = "update PlacesSocios set Deleted = true,UpdateUser = @UpdateUser where PlaceCode = @PlaceCode and SocioCode = @SocioCode and PlaceSocioTypeCode = @PlaceSocioTypeCode";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@SocioCode", value.SocioCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceSocioTypeCode", value.PlaceSocioTypeCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }




        public List<PlacePhotoBE> PlacePhotoList(SessionAdminUserModel user, string placeCode)
        {
            var sql = "select photocode,Name,updatedate,updateuser from placesphotos where deleted = false and placecode = @PlaceCode";
            var items = new List<PlacePhotoBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {

                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", placeCode));

                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new PlacePhotoBE()
                            {
                                PlaceCode = placeCode,
                                PhotoCode = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                UpdateDate = reader.GetInt64(2),
                                UpdateUser = reader.GetInt32(3),
                            });
                        }
                    }
                }
            }
            return items;

        }


        public bool PlacePhotoInsert(SessionAdminUserModel user, PlacePhotoBE value)
        {
            var sql = "insert into placesphotos (PhotoCode,PlaceCode,Name,UpdateUser,updatedate) " +
                    "values (@PhotoCode,@PlaceCode,@Name,@UpdateUser,@updatedate)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PhotoCode", value.PhotoCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@updatedate", Functions.DateUpdate()));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool PlacePhotoUpdate(SessionAdminUserModel user, PlacePhotoBE value)
        {
            var sql = "update placesphotos set Name = @Name, UpdateUser = @UpdateUser where PhotoCode = @PhotoCode";
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@PhotoCode", value.PhotoCode));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool PlacePhothoDelete(SessionAdminUserModel user, string photoCode)
        {
            var sql = "update placesphotos set Deleted = true,UpdateUser = @UpdateUser,updatedate=@updatedate where PhotoCode = @PhotoCode";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@updatedate", Functions.DateUpdate()));
                    cmd.Parameters.Add(new NpgsqlParameter("@PhotoCode", photoCode));

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }




    }
}