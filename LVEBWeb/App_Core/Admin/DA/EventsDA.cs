﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class EventsDA : App_Core.Admin.DA.BaseDA
    {


        public List<EventBE> EventsSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = "select e.Code,e.Name,e.PlaceCode,p.name,e.ManagerCode,s.nickname,DateStart,Duration,e.UpdateDate,e.UpdateUser,e.status " +
            " from Events as e" +
            " left join Places p on e.PlaceCode = p.code " +
            " left join Socios s on e.ManagerCode = s.code" +
            " where e.deleted = false";

            sql += "  order by e.Name limit 2000";
            var items = new List<EventBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new EventBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                PlaceCode = reader.GetValue(2).ToString(),
                                PlaceName = reader.GetValue(3).ToString(),
                                ManagerCode = reader.GetValue(4).ToString(),
                                ManagerName = reader.GetValue(5).ToString(),
                                DateStart = reader.GetDateTime(6),
                                Duration = reader.GetInt32(7),
                                UpdateDate = reader.GetInt64(8),
                                UpdateUser = reader.GetInt32(9),
                                Status = reader.GetInt32(10)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public bool EventInsertOrUpdate(SessionAdminUserModel user, EventBE value, bool insert)
        {
            var sql = "update Events set PlaceCode=@PlaceCode,ManagerCode=@ManagerCode,Name=@Name,AddCityCode=@AddCityCode,Address=@Address,Latitude=@Latitude,Longitude=@Longitude,Description=@Description,DateStart=@DateStart,Duration=@Duration,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser,Status=@Status,Category=@Category where Code = @Code ";
            if (insert)
                sql = "insert into Events (PlaceCode,ManagerCode,Name,AddCityCode,Address,Latitude,Longitude,Description,DateStart,Duration,UpdateDate,UpdateUser,Status,Category,Code) " +
                    "values (@PlaceCode,@ManagerCode,@Name,@AddCityCode,@Address,@Latitude,@Longitude,@Description,@DateStart,@Duration,@UpdateDate,@UpdateUser,@Status,@Category,@Code)  ";
            value.UpdateDate = Common.Functions.DateUpdate();

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@ManagerCode", value.ManagerCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));

                    cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", value.AddCityCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Address", value.AddStreet));
                    cmd.Parameters.Add(new NpgsqlParameter("@Latitude", value.Latitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@Longitude", value.Longitude));

                    cmd.Parameters.Add(new NpgsqlParameter("@Description", value.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("@DateStart", value.DateStart));
                    cmd.Parameters.Add(new NpgsqlParameter("@Duration", value.Duration));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", value.Status));
                    cmd.Parameters.Add(new NpgsqlParameter("@Category", value.Category));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();
                }
            }
            this.DataLastUpdateSave(DataTypes.Events, value.UpdateDate);
            return true;
        }


        public EventBE EventGet(SessionAdminUserModel user, string code)
        {
            var sql = "select PlaceCode,p.name,ManagerCode,s.name,a.Name,a.AddCityCode,a.Address,a.Latitude,a.Longitude,a.Description,DateStart,Duration,a.UpdateDate,a.UpdateUser,a.Status " +
                " from events as a" +
                " left join Places p on a.PlaceCode = p.code " +
                " left join Socios s on a.ManagerCode = s.code " +
                " where a.Code = @Code ";
            var v = new EventBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {

                        if (reader.Read())
                        {
                            v = new EventBE()
                            {
                                PlaceCode = reader.GetValue(0).ToString(),
                                PlaceName = reader.GetValue(1).ToString(),
                                ManagerCode = reader.GetValue(2).ToString(),
                                ManagerName = reader.GetValue(3).ToString(),
                                Name = reader.GetValue(4).ToString(),
                                AddCityCode = reader.GetValue(5).ToString(),
                                AddStreet = reader.GetValue(6).ToString(),
                                Latitude = reader.GetDouble(7),
                                Longitude = reader.GetDouble(8),
                                Description = reader.GetValue(9).ToString(),
                                DateStart = reader.GetDateTime(10),
                                Duration = reader.GetInt32(11),
                                UpdateDate = reader.GetInt64(12),
                                UpdateUser = reader.GetInt32(13),
                                Status = reader.GetInt32(14),
                                Code = code

                            };
                            if (v.AddCityCode.Length > 0)
                                v.AddProvinceCode = v.AddCityCode.Substring(0, 2);
                        }
                        else
                            return v;
                    }
                }
            }

            return v;
        }


        public bool EventDelete(SessionAdminUserModel user, string code)
        {
            var sql = "update events set Deleted = true,UpdateUser = @UpdateUser where code = @Code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserId));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }



        public List<EventToCheckBE> EventsTMPSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            var sql = @"select  CheckStatus,CheckStatusDate,ValidatorCode,reporter_name,reporter_email,reporter_phone,reporter_relation,
            	e.Code,e.Name,p.code,p.name,DateStart,Duration,e.UpdateDate,e.UpdateUser 
             	from EventsToCheck as e
             	left join Places as p on e.PlaceCode = p.code ";

            sql += "  order by e.Name limit 2000";
            var items = new List<EventToCheckBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new EventToCheckBE()
                            {
                                CheckStatus = reader.GetInt32(0),
                                CheckStatusDate = reader.GetDateTime(1),
                                ValidatorCode = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                ReporterName = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                ReporterEmail = reader.IsDBNull(4) ? "" : reader.GetValue(4).ToString(),
                                ReporterPhone = reader.IsDBNull(5) ? "" : reader.GetValue(5).ToString(),
                                ReporterRelation = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                Code = reader.GetValue(7).ToString(),
                                Name = reader.GetValue(8).ToString(),
                                PlaceCode = reader.GetValue(9).ToString(),
                                PlaceName = reader.GetValue(10).ToString(),
                                DateStart = reader.GetDateTime(11),
                                Duration = reader.GetInt32(12),
                                UpdateDate = reader.GetInt64(13),
                                UpdateUser = reader.GetInt32(14)

                            });
                        }
                    }
                }
            }
            return items;
        }




        #region Base
        private static EventsDA _ManagerSingleton;
        public static EventsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EventsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}