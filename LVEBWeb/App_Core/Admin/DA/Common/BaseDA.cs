﻿
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Npgsql;
using MongoDB.Bson;
using MongoDB.Driver;

namespace LVEBWeb.App_Core.Admin.DA
{
    public class BaseDA
    {


        protected bool DataLastUpdateSave(DataTypes type, long dateUpdate)
        {
            var sql = "Update DataLastUpdate set DateLastUpdate =  " + dateUpdate +
                    " where Type = '" + type.ToString() + "' ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        protected IMongoCollection<T> DBGetCollection<T>(DBs db, string collection)
        {
            var dbConnection = "";
            var dbName = "";
            switch (db)
            {
                case DBs.Exection:
                    dbConnection = Configuration.MDB_Exection_Connection;
                    dbName = Configuration.MDB_Exection;
                    break;
            }
            var client = new MongoClient(dbConnection);
            var clientdb = client.GetDatabase(dbName);
            var dbc = clientdb.GetCollection<T>(collection);
            return dbc;
        }


        /* public bool CopyFromDelete( BsonDocument doc, string type )
         {
             var dbc = DBGetCollection<BsonDocument>( DBs.Admin, "BackupFromDelete" );
             var mddoc = new BsonDocument();
             mddoc.Add( "type", type );
             mddoc.Add( "doc", doc );
             mddoc.Add( "di", DateTime.UtcNow );

             dbc.InsertOne( mddoc );
             return true;
         }*/

    }
}