﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class CommonDA : App_Core.Admin.DA.BaseDA
    {
        public List<DICValueBE> ValuesList(SessionAdminUserModel user, DICTables tableCode)
        {
            var sql = "select Code,Name from DICTableValues where TableCode = " + (int)tableCode + " order by Name limit 2000";
            var items = new List<DICValueBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICValueBE()
                            {
                                Code = reader.GetInt32(0),
                                Name = reader.GetValue(1).ToString()
                            });
                        }
                    }
                }
            }
            return items;
        }
        public List<DICProvinceBE> ProvincesList(SessionAdminUserModel user)
        {
            var sql = "select Code,Name from DICProvinces order by Name limit 2000";
            var items = new List<DICProvinceBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICProvinceBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString()
                            });
                        }
                    }
                }
            }
            return items;
        }

        public List<DICCityBE> CitiesListByProvince(SessionAdminUserModel user, string provinceCode)
        {
            var sql = "select Code,Name from DICCities where ProvinceCode = @ProvinceCode order by Name limit 2000";
            var items = new List<DICCityBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@ProvinceCode", provinceCode));
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICCityBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                ProvinceCode = provinceCode
                            });
                        }
                    }
                }
            }
            return items;
        }


        #region Base
        private static CommonDA _ManagerSingleton;
        public static CommonDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CommonDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}