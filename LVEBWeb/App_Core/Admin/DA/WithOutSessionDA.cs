﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
namespace LVEBWeb.App_Core.Admin.DA
{
    public class WithOutSessionDA : BaseDA
    {


        public SessionAdminUserModel Logon(string email, string password)
        {
            var _return = new SessionAdminUserModel();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                var sql = "select UserId,Username,userEmail,roles" +
                    " from AdminUsers where UserEmail = '" + email + "' and UserPassword = '" + password + "' ";

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _return.UserId = reader.GetInt32(0);
                            _return.UserName = reader.GetString(1);
                            _return.UserEmail = reader.GetString(2);
                            _return.UserRoles = new HashSet<string>(reader.GetString(3).Split(','));
                        }
                    }
                }
            }
            return _return;
        }

        public bool User_Email_Exist(string email)
        {
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                var sql = "select count(0)" +
                    " from AdminUsers where userEmail = '" + email + "' ";

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    return Convert.ToInt32(cmd.ExecuteScalar()) > 0;
                }
            }
        }
        public bool User_Password_Update(string email, string password)
        {
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                var sql = "update AdminUsers set UserPassword = @UserPassword where UserEmail = @UserEmail ";

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UserEmail", email));
                    cmd.Parameters.Add(new NpgsqlParameter("@UserPassword", password));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public bool LoginLogInsert(string email, bool success)
        {
            var sql = "insert into AdminUsersLogs (Email,Success) " +
                    "values (@Email,@Success)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Email", email));
                    cmd.Parameters.Add(new NpgsqlParameter("@Success", success));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        /*
        public bool ResetPasswordStart( string key, string email )
        {
            var sql = "insert into AdminUsersTaskValidation (Key,Type,Value,DateExpire) " +
                        "values (@Key,@Type,@Value,@DateExpire)  ";

            using (var conn = new NpgsqlConnection( Configuration.DBConnection ))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand( sql, conn ))
                {
                    cmd.Parameters.Add( new NpgsqlParameter( "@Key", key ) );
                    cmd.Parameters.Add( new NpgsqlParameter( "@Type", 1 ) );
                    cmd.Parameters.Add( new NpgsqlParameter( "@Value", email ) );
                    cmd.Parameters.Add( new NpgsqlParameter( "@DateExpire", DateTime.Now.AddDays( 2 ) ) );
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public bool ResetPasswordCheck( string email, string key )
        {

            using (var conn = new NpgsqlConnection( Configuration.DBConnection ))
            {
                conn.Open();
                var sql = "select count(0)" +
                    " from AdminUsersTaskValidation where Key = @Key and Value = @Value ";

                using (var cmd = new NpgsqlCommand( sql, conn ))
                {
                    cmd.Parameters.Add( new NpgsqlParameter( "@Key", key ) );
                    cmd.Parameters.Add( new NpgsqlParameter( "@Value", email ) );
                    return Convert.ToInt32( cmd.ExecuteScalar() ) > 0;
                }
            }

        }

        public bool ResetPasswordEnd( string key )
        {
            var sql = "delete from AdminUsersTaskValidation where Key = @Key ";

            using (var conn = new NpgsqlConnection( Configuration.DBConnection ))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand( sql, conn ))
                {
                    cmd.Parameters.Add( new NpgsqlParameter( "@Key", key ) );
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }
        */

        #region Base
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static WithOutSessionDA _ManagerSingleton;
        /// <summary>

        /// </summary>
        public static WithOutSessionDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new WithOutSessionDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}