﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.DA
{
    public partial class PlacesDA : App_Core.Admin.DA.BaseDA
    {
        public List<DICCodeName> PlacesFindCodes(SessionAdminUserModel user, string name)
        {
            var sql = "select s.Code,s.Name from Places s where deleted = false and name ILIKE '%" + name + "%'";
            var items = new List<DICCodeName>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(new DICCodeName()
                            {
                                Id = reader.GetValue(0).ToString(),
                                Value = reader.GetValue(1).ToString(),
                                Label = reader.GetValue(1).ToString()
                            });
                        }
                    }
                }
            }
            return items;
        }

        public List<PlaceSearchModel> PlacesFindCodesFull(SessionAdminUserModel user, string name)
        {
            var sql = "select s.Code,s.Name,AddProvinceCode,AddCityCode,AddStreet,AddStreetNumber,Latitude,Longitude  from Places s where deleted = false and name like '%" + name + "%'";
            var items = new List<PlaceSearchModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new PlaceSearchModel()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Label = reader.GetValue(1).ToString(),
                                AddProvinceCode = reader.GetValue(2).ToString(),
                                AddCityCode = reader.GetValue(3).ToString(),
                                AddStreet = reader.GetValue(4).ToString() + "," + reader.GetValue(5).ToString(),
                                Latitude = reader.GetDouble(6),
                                Longitude = reader.GetDouble(7)
                            });
                        }
                    }
                }
            }
            return items;

        }

        public List<PlaceBE> PlacesSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            //var sql = "select Code,Name,AddCityCode,AddProvinceCode,AddStreet,AddStreetNumber,AddCP,Description,Latitude,Longitude,UpdateDate,UpdateUser  from Places  ";
            var sql = @"select p.Code,p.Name,AddCityCode,cit.Name,AddProvinceCode,pro.name,AddStreet,Latitude,Longitude,p.UpdateDate
            	,array_to_string(array(select Name from PlacesTags pt left join DICTableValues ptv on pt.PlaceTagCode = ptv.code where pt.PlaceCode = p.code),',')
                ,array_to_string(array(select Name from PlacesCategories pt left join DICTableValues ptv on pt.PlaceCategoryCode = ptv.code where pt.PlaceCode = p.code),',')
                ,p.status,Score
                from Places p  
                left join DICProvinces pro on p.AddProvinceCode = pro.code
                left join DICCities cit on p.AddCityCode = cit.code";

            if (!string.IsNullOrEmpty(gridModel.FK3))
                sql += " inner join PlacesCategories pc on p.Code = pc.PlaceCode and pc.PlaceCategoryCode = '" + gridModel.FK3 + "'";

            if (!string.IsNullOrEmpty(gridModel.FK4))
                sql += " inner join PlacesTags pt on p.Code = pt.PlaceCode and pt.PlaceTagCode = '" + gridModel.FK4 + "'";

            sql += " where p.deleted = false";

            if (!string.IsNullOrEmpty(gridModel.FK1))
                sql += " and p.name like @Name";

            if (!string.IsNullOrEmpty(gridModel.FK2))
                sql += " and AddProvinceCode = @ProvinceCode";


            sql += " order by p.Name limit 2000";
            var items = new List<PlaceBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    if (!string.IsNullOrEmpty(gridModel.FK1))
                        cmd.Parameters.Add(new NpgsqlParameter("@Name", "%" + gridModel.FK1 + "%"));

                    if (!string.IsNullOrEmpty(gridModel.FK2))
                        cmd.Parameters.Add(new NpgsqlParameter("@ProvinceCode", gridModel.FK2));


                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new PlaceBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                AddCity = reader.IsDBNull(3) ? reader.GetValue(2).ToString() : reader.GetValue(3).ToString(),
                                AddProvince = reader.IsDBNull(5) ? reader.GetValue(4).ToString() : reader.GetValue(5).ToString(),
                                AddStreet = reader.GetValue(6).ToString(),
                                //AddStreetNumber = reader.GetValue(5).ToString(),
                                //AddCP = reader.GetValue(6).ToString(),
                                //Description = reader.GetValue(7).ToString(),
                                Latitude = reader.GetDouble(7),
                                Longitude = reader.GetDouble(8),
                                UpdateDate = reader.GetInt64(9),
                                TagsIds = reader.GetValue(10).ToString(),
                                CategoriesIds = reader.GetValue(11).ToString(),
                                Status = reader.GetInt32(12),
                                Score = reader.GetInt32(13)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public List<PlaceToCheckBE> PlacesTMPSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            //var sql = "select Code,Name,AddCityCode,AddProvinceCode,AddStreet,AddStreetNumber,AddCP,Description,Latitude,Longitude,UpdateDate,UpdateUser  from Places  ";
            var sql = @"select CheckStatus,CheckStatusDate,ValidatorCode,reporter_name,reporter_email,reporter_phone,reporter_relation
            	,p.Code,p.Name,cit.Name,pro.name,AddStreet,Latitude,Longitude,p.status
                from PlacesToCheck p 
                left join DICProvinces pro on p.AddProvinceCode = pro.code
                left join DICCities cit on p.AddCityCode = cit.code";


            sql += " order by p.updatedate limit 200";
            var items = new List<PlaceToCheckBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            // p.Code,p.Name,AddCityCode,cit.Name,AddProvinceCode,pro.name,AddStreet,Latitude,Longitude,p.UpdateDate
                            items.Add(new PlaceToCheckBE()
                            {
                                CheckStatus = reader.GetInt32(0),
                                CheckStatusDate = reader.GetDateTime(1),
                                ValidatorCode = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                ReporterName = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                ReporterEmail = reader.IsDBNull(4) ? "" : reader.GetValue(4).ToString(),
                                ReporterPhone = reader.IsDBNull(5) ? "" : reader.GetValue(5).ToString(),
                                ReporterRelation = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                Code = reader.GetValue(7).ToString(),
                                Name = reader.GetValue(8).ToString(),
                                AddCity = reader.IsDBNull(9) ? "" : reader.GetValue(9).ToString(),
                                AddProvince = reader.IsDBNull(10) ? "" : reader.GetValue(10).ToString(),
                                AddStreet = reader.GetValue(11).ToString(),
                                Latitude = reader.GetDouble(12),
                                Longitude = reader.GetDouble(13),
                                Status = reader.GetInt32(14)

                            });
                        }
                    }
                }
            }
            return items;
        }

        #region Base
        private static PlacesDA _ManagerSingleton;
        public static PlacesDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PlacesDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}