﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class UsersBR
    {
        public List<DICCodeName> UsersFindCodes( SessionAdminUserModel user, string name )
        {
            return UsersDA.Singleton().UsersFindCodes( user, name );

        }

        public List<UserBE> UsersSeach( SessionAdminUserModel user, GridFilterModel gridModel )
        {
            return UsersDA.Singleton().UsersSeach( user, gridModel );

        }
        public InsertUpdateModel UserInsertOrUpdate( SessionAdminUserModel user, UserBE value )
        {
            //return _ManagerSingleton.UserInsertOrUpdate( user, value,insert );
            if (value.Nickname == null) value.Nickname = value.Name;
            if (value.AddProvinceCode == null) value.AddProvinceCode = "";
            if (value.AddCityCode == null) value.AddCityCode = "";
            if (value.Email == null) value.Email = "";
            if (value.Phone == null) value.Phone = "";
            if (value.Notes == null) value.Notes = "";

            if (string.IsNullOrEmpty( value.Code ))
            {
                value.Code = Common.Functions.CreateId();
                return new InsertUpdateModel( UsersDA.Singleton().UserInsertOrUpdate( user, value, true ), value.Code );
            }
            else
                return new InsertUpdateModel( UsersDA.Singleton().UserInsertOrUpdate( user, value, false ), "" );

        }


        public UserBE UserGet( SessionAdminUserModel user, string code )
        {
            return UsersDA.Singleton().UserGet( user, code );

        }



        public bool UserDelete( SessionAdminUserModel user, UserBE value )
        {
            return UsersDA.Singleton().UserDelete( user, value );

        }




        #region Base
        private static UsersBR _ManagerSingleton;
        public static UsersBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new UsersBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
