﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class EventsBR
    {
        public List<EventBE> EventsSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return EventsDA.Singleton().EventsSelect(user, gridModel);
        }


        public InsertUpdateModel ActivityInsertOrUpdate(SessionAdminUserModel user, EventBE value)
        {
            if (string.IsNullOrEmpty(value.PlaceCode)) value.PlaceCode = "";
            if (string.IsNullOrEmpty(value.ManagerCode)) value.ManagerCode = "";
            if (string.IsNullOrEmpty(value.AddCityCode)) value.AddCityCode = "";
            if (string.IsNullOrEmpty(value.AddProvinceCode)) value.AddProvinceCode = "";

            InsertUpdateModel rtn;
            if (string.IsNullOrEmpty(value.Code))
            {
                value.Code = Common.Functions.CreateId();
                rtn = new InsertUpdateModel(EventsDA.Singleton().EventInsertOrUpdate(user, value, true), value.Code);
            }
            else
            {
                rtn = new InsertUpdateModel(EventsDA.Singleton().EventInsertOrUpdate(user, value, false));
                _cache.Remove(CacheKeys.Events + value.Code);
            }
            return rtn;
        }


        public EventBE EventGet(SessionAdminUserModel user, string code)
        {
            return EventsDA.Singleton().EventGet(user, code);
        }

        public bool EventDelete(SessionAdminUserModel user, string code)
        {
            var rtn = EventsDA.Singleton().EventDelete(user, code);
            _cache.Remove(CacheKeys.Events + code);
            return rtn;
        }
        public List<EventToCheckBE> EventsTMPSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return EventsDA.Singleton().EventsTMPSelect(user, gridModel);
        }

        #region Base
        private IMemoryCache _cache;
        private static EventsBR _ManagerSingleton;
        public static EventsBR Singleton(IMemoryCache memoryCache)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EventsBR(memoryCache);
            }
            return _ManagerSingleton;
        }
        public EventsBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;

        }
        #endregion Base
    }
}
