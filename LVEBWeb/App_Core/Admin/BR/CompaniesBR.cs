﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class CompaniesBR
    {
        public List<DICCodeName> CompaniesFindCodes(SessionAdminUserModel user, string name)
        {
            return CompaniesDA.Singleton().CompaniesFindCodes(user, name);
        }
        public List<CompanyBE> CompaniesSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return CompaniesDA.Singleton().CompaniesSelect(user, gridModel);
        }


        public InsertUpdateModel CompanyInsertOrUpdate(SessionAdminUserModel user, CompanyBE value)
        {
            if (value.BusinessName == null) value.BusinessName = "";
            if (value.VatNumber == null) value.VatNumber = "";
            if (value.AddCityCode == null) value.AddCityCode = "";
            if (value.AddStreet == null) value.AddStreet = "";
            if (value.AddStreetNumber == null) value.AddStreetNumber = "";
            if (value.AddCP == null) value.AddCP = "";
            if (value.ContactWeb == null) value.ContactWeb = "";
            if (value.ContactEmail == null) value.ContactEmail = "";
            if (value.ContactPhone == null) value.ContactPhone = "";
            if (value.ContactOthers == null) value.ContactOthers = "";
            if (value.Notes == null) value.Notes = "";
            if (value.Extra1 == null) value.Extra1 = "";
            if (value.Extra2 == null) value.Extra2 = "";

            if (string.IsNullOrEmpty(value.Code))
            {
                value.Code = Common.Functions.CreateId();
                return new InsertUpdateModel(CompaniesDA.Singleton().CompanyInsertOrUpdate(user, value, true), value.Code);
            }
            else
                return new InsertUpdateModel(CompaniesDA.Singleton().CompanyInsertOrUpdate(user, value, false), "");
        }

        public CompanyBE CompanyGet(SessionAdminUserModel user, string code)
        {
            return CompaniesDA.Singleton().CompanyGet(user, code);
        }
        public List<CompanyUserBE> CompanyUserList(SessionAdminUserModel user, string companyCode, string userCode)
        {
            return CompaniesDA.Singleton().CompanyUserList(user, companyCode, userCode);
        }

        public bool CompanyUserInsert(SessionAdminUserModel user, CompanyUserBE value)
        {
            return CompaniesDA.Singleton().CompanyUserInsert(user, value);
        }

        public bool CompanyUserDelete(SessionAdminUserModel user, CompanyUserBE value)
        {
            return CompaniesDA.Singleton().CompanyUserDelete(user, value);
        }

        public List<CompanyPlaceBE> CompanyPlaceList(SessionAdminUserModel user, string companyCode, string placeCode)
        {
            return CompaniesDA.Singleton().CompanyPlaceList(user, companyCode, placeCode);
        }
        public bool CompanyPlaceInsert(SessionAdminUserModel user, CompanyPlaceBE value)
        {
            return CompaniesDA.Singleton().CompanyPlaceInsert(user, value);
        }
        public bool CompanyPlaceDelete(SessionAdminUserModel user, CompanyPlaceBE value)
        {
            return CompaniesDA.Singleton().CompanyPlaceDelete(user, value);
        }

        #region Base
        private static CompaniesBR _ManagerSingleton;
        public static CompaniesBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompaniesBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
