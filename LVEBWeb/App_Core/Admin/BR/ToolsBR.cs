﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class ToolsBR
    {

        public List<ExceptionBE> Exceptions_Search(GridFilterModel gridModel)
        {

            return _MainDA.Exceptions_Search(gridModel);
        }
        public bool Exceptions_Clear()
        {
            return _MainDA.Exceptions_Clear();
        }

        #region Base

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ToolsBR _Singleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ToolsDA _MainDA;

        /// <summary>
        /// </summary>
        public static ToolsBR Singleton(ToolsDA managerDA)
        {
            if (_Singleton == null)
            {
                _Singleton = new ToolsBR(managerDA);
            }
            return _Singleton;
        }
        public static ToolsBR Singleton()
        {
            return Singleton(ToolsDA.Singleton());
        }

        public ToolsBR()
        {
            _MainDA = new ToolsDA();
        }
        public ToolsBR(ToolsDA mainDA)
        {
            _MainDA = mainDA;
        }

        #endregion
    }
}
