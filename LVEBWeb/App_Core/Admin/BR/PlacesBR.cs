﻿using System;
using System.Linq;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Admin.BR
{
    public partial class PlacesBR
    {
        public List<DICCodeName> PlacesFindCodes(SessionAdminUserModel user, string name)
        {
            return PlacesDA.Singleton().PlacesFindCodes(user, name);
        }
        public List<PlaceSearchModel> PlacesFindCodesFull(SessionAdminUserModel user, string name)
        {
            return PlacesDA.Singleton().PlacesFindCodesFull(user, name);
        }
        public List<PlaceBE> PlacesSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return PlacesDA.Singleton().PlacesSelect(user, gridModel);
        }



        public List<PlaceToCheckBE> PlacesTMPSelect(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return PlacesDA.Singleton().PlacesTMPSelect(user, gridModel);
        }




        #region Base
        private IMemoryCache _cache;
        private static PlacesBR _ManagerSingleton;
        public static PlacesBR Singleton(IMemoryCache memoryCache)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PlacesBR(memoryCache);
            }
            return _ManagerSingleton;
        }

        public PlacesBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;

        }
        #endregion Base
    }
}
