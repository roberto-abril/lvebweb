﻿using System;
using System.Linq;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;

namespace LVEBWeb.App_Core.Admin.BR
{
    public partial class PlacesBR
    {



        public InsertUpdateModel PlaceInsertOrUpdate(SessionAdminUserModel user, PlaceBE value)
        {

            if (string.IsNullOrEmpty(value.AddProvince)) value.AddProvince = "";
            if (string.IsNullOrEmpty(value.ContactEmail)) value.ContactEmail = "";
            if (string.IsNullOrEmpty(value.ContactOther)) value.ContactOther = "";
            if (string.IsNullOrEmpty(value.ContactPhone)) value.ContactPhone = "";
            if (string.IsNullOrEmpty(value.ContactWeb)) value.ContactWeb = "";
            if (string.IsNullOrEmpty(value.Description)) value.Description = "";
            if (string.IsNullOrEmpty(value.DescriptionFull)) value.DescriptionFull = "";
            if (string.IsNullOrEmpty(value.OpenHours)) value.OpenHours = "";
            if (string.IsNullOrEmpty(value.PhotoCodeHome)) value.PhotoCodeHome = "";
            if (string.IsNullOrEmpty(value.PhotoCodeIcon)) value.PhotoCodeIcon = "";
            if (string.IsNullOrEmpty(value.TagsBaileIds)) value.TagsBaileIds = "";
            if (string.IsNullOrEmpty(value.TagsProfileIds)) value.TagsProfileIds = "";


            InsertUpdateModel rtn;
            if (string.IsNullOrEmpty(value.Code))
            {
                value.Code = Common.Functions.CreateId();
                rtn = new InsertUpdateModel(PlacesDA.Singleton().PlaceInsertOrUpdate(user, value, true), value.Code);
                if (!rtn.WasOk)
                    return rtn;

                string[] ids;
                if (value.TagsIds != null)
                {
                    ids = value.TagsIds.Split(',');
                    for (var x = 0; x < ids.Length; x++)
                        PlacesDA.Singleton().PlaceTagInsert(user, new PlaceTagBE() { PlaceCode = value.Code, PlaceTagCode = Convert.ToInt32(ids[x]) });
                }
                if (value.CategoriesIds != null)
                {
                    ids = value.CategoriesIds.Split(',');
                    for (var x = 0; x < ids.Length; x++)
                        PlacesDA.Singleton().PlaceCategoryInsert(user, new PlaceCategoryBE() { PlaceCode = value.Code, PlaceCategoryCode = Convert.ToInt32(ids[x]) });
                }
            }
            else
            {
                var place = PlacesDA.Singleton().PlaceGet(user, value.Code);
                rtn = new InsertUpdateModel(PlacesDA.Singleton().PlaceInsertOrUpdate(user, value, false), "");

                if (value.TagsBaileIds.Length > 0)
                {
                    value.TagsIds = value.TagsBaileIds;
                    if (value.TagsProfileIds != null && value.TagsProfileIds.Length > 0)
                        value.TagsIds += "," + value.TagsProfileIds;

                }
                else
                {
                    value.TagsIds = value.TagsProfileIds;
                }

                var idsNew = value.TagsIds == "" ? new string[0] : value.TagsIds.Split(',');
                var idsOlds = place.TagsIds == "" ? new string[0] : place.TagsIds.Split(',');
                for (var x = 0; x < idsOlds.Length; x++)
                    if (!idsNew.Contains(idsOlds[x]))
                    {
                        PlacesDA.Singleton().PlaceTagDelete(user, new PlaceTagBE() { PlaceCode = value.Code, PlaceTagCode = Convert.ToInt32(idsOlds[x]) });
                    }
                for (var x = 0; x < idsNew.Length; x++)
                    if (!idsOlds.Contains(idsNew[x]))
                    {
                        PlacesDA.Singleton().PlaceTagInsert(user, new PlaceTagBE() { PlaceCode = value.Code, PlaceTagCode = Convert.ToInt32(idsNew[x]) });
                    }


                idsNew = value.CategoriesIds == "" ? new string[0] : value.CategoriesIds.Split(',');
                idsOlds = place.CategoriesIds == "" ? new string[0] : place.CategoriesIds.Split(',');
                for (var x = 0; x < idsOlds.Length; x++)
                    if (!idsNew.Contains(idsOlds[x]))
                    {
                        PlacesDA.Singleton().PlaceCategoryDelete(user, new PlaceCategoryBE() { PlaceCode = value.Code, PlaceCategoryCode = Convert.ToInt32(idsOlds[x]) });
                    }
                for (var x = 0; x < idsNew.Length; x++)
                    if (!idsOlds.Contains(idsNew[x]))
                    {
                        PlacesDA.Singleton().PlaceCategoryInsert(user, new PlaceCategoryBE() { PlaceCode = value.Code, PlaceCategoryCode = Convert.ToInt32(idsNew[x]) });
                    }


                _cache.Remove(CacheKeys.Places + value.Code);
            }
            return rtn;
        }
        public bool PlaceDelete(SessionAdminUserModel user, string code)
        {
            return PlacesDA.Singleton().PlaceDelete(user, code);
        }

        public PlaceBE PlaceGet(SessionAdminUserModel user, string code)
        {
            var place = PlacesDA.Singleton().PlaceGet(user, code);
            place.HaveLogo = System.IO.File.Exists(System.IO.Path.Combine(Configuration.PathImagesCompetitions, code));

            var tags = place.TagsIds.Split(',');
            place.TagsBaileIds = string.Join(",", tags.Select(z => z).Where(z => z.StartsWith("16")));
            place.TagsProfileIds = string.Join(",", tags.Select(z => z).Where(z => z.StartsWith("17")));
            return place;
        }

        /*
        public bool PlaceCategoryDelete(SessionUserModel user, PlaceCategoryBE value)
        {
            return PlacesDA.Singleton().PlaceCategoryDelete(user, value);
        }
        */


        public bool PlaceSocioInsert(SessionAdminUserModel user, PlaceSocioBE value)
        {
            return PlacesDA.Singleton().PlaceSocioInsert(user, value);
        }

        public bool PlaceSocioDelete(SessionAdminUserModel user, PlaceSocioBE value)
        {
            return PlacesDA.Singleton().PlaceSocioDelete(user, value);
        }


        public bool PlacePhotoInsert(SessionAdminUserModel user, PlacePhotoBE value)
        {
            return PlacesDA.Singleton().PlacePhotoInsert(user, value);
        }

        public bool PlacePhotoUpdate(SessionAdminUserModel user, PlacePhotoBE value)
        {
            return PlacesDA.Singleton().PlacePhotoUpdate(user, value);
        }

        public bool PlacePhothoDelete(SessionAdminUserModel user, string photoCode)
        {
            return PlacesDA.Singleton().PlacePhothoDelete(user, photoCode);
        }

        public List<PlacePhotoBE> PlacePhotoList(SessionAdminUserModel user, string placeCode)
        {
            return PlacesDA.Singleton().PlacePhotoList(user, placeCode);
        }

    }
}
