﻿using LVEBWeb.App_Core.Admin.DA;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LVEBWeb.Models;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class WithOutSessionBR
    {


        public SessionAdminUserModel Logon( string email, string password )
        {
            if (String.IsNullOrEmpty( email )) throw new ArgumentException( "Value cannot be null or empty.", "email" );
            if (String.IsNullOrEmpty( password )) throw new ArgumentException( "Value cannot be null or empty.", "password" );

            password = Common.Functions.MD5Encode( password );
            var _secureUser = WithOutSessionDA.Singleton().Logon( email, password );


            if (string.IsNullOrEmpty( _secureUser.UserEmail ))
            {
                return new SessionAdminUserModel() { UserEmail = "" };
            }
            else
            {
                return _secureUser;
            }
        }

        /*
        public bool PasswordForgot_SendMail(PasswordRecoverModel model)
        {
            if (string.IsNullOrEmpty(model.Email))
                return false;
            model.Email = model.Email.ToLower();

            if (WithOutSessionDA.Singleton().User_Email_Exist(model.Email))
            {

                var key = Functions.CreateId();
                WithOutSessionDA.Singleton().ResetPasswordStart(key, model.Email);
                EmailsBR.Singleton().SendPasswordReset(model.Email, key);
                return true;

            }
            return false;
        }
        public bool PasswordForgot_Check(string email, string key)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            email = email.ToLower();
            return WithOutSessionDA.Singleton().ResetPasswordCheck(email, key);
        }
        public bool PasswordForgot_Change(PasswordRecoverModel model)
        {
            if (string.IsNullOrEmpty(model.Email))
                return false;
            model.Email = model.Email.ToLower();

            if (WithOutSessionDA.Singleton().User_Email_Exist(model.Email))
            {
                if (!string.IsNullOrEmpty(model.Password)
                    && Functions.MatchGoodPassword(model.Password)
                    && model.Password == model.PasswordConfirmation)
                {
                    model.Password = Functions.MD5Encode(model.Password);
                    if (WithOutSessionDA.Singleton().User_Password_Update(model.Email, model.Password))
                        WithOutSessionDA.Singleton().ResetPasswordEnd(model.Key);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        */

        #region Base
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static WithOutSessionBR _ManagerSingleton;
        /// <summary>

        /// </summary>
		public static WithOutSessionBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new WithOutSessionBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}