﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;


namespace LVEBWeb.App_Core.Admin.BR
{
    public class TasksBR
    {
        public List<TaskGroupBE> TaskGroupsSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return TasksDA.Singleton().TaskGroupsSearch(user, gridModel);
        }
        public InsertUpdateModel TaskGroupInsertOrUpdate(SessionAdminUserModel user, TaskGroupBE value)
        {
            if (string.IsNullOrEmpty(value.Code))
            {
                value.Code = Common.Functions.CreateId();
                return new InsertUpdateModel(TasksDA.Singleton().TaskGroupInsertOrUpdate(user, value, true), value.Code);
            }
            else
                return new InsertUpdateModel(TasksDA.Singleton().TaskGroupInsertOrUpdate(user, value, false), "");
        }

        public TaskGroupBE TaskGroupGet(SessionAdminUserModel user, string code)
        {
            return TasksDA.Singleton().TaskGroupGet(user, code);
        }
        public bool TaskGroupDelete(SessionAdminUserModel user, string code)
        {
            return TasksDA.Singleton().TaskGroupDelete(user, code);
        }
        public List<TaskBE> TaskSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return TasksDA.Singleton().TaskSearch(user, gridModel);
        }

        public TaskBE TaskGetNext(SessionAdminUserModel user, string taskGroupCode, int lastTask)
        {
            return TasksDA.Singleton().TaskGetNext(user, taskGroupCode, lastTask);
        }

        public bool TaskStatusChange(SessionAdminUserModel user, int taskId, int status)
        {
            return TasksDA.Singleton().TaskStatusChange(user, taskId, status);
        }
        #region Base
        private static TasksBR _ManagerSingleton;
        public static TasksBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new TasksBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
