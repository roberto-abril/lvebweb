﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;


namespace LVEBWeb.App_Core.Admin.BR
{
    public class ConfBR
    {
        public List<DICTableBE> TableList(SessionAdminUserModel user)
        {
            return ConfDA.Singleton().TableList(user);
        }
        public List<DICTableValueBE> TableValueList(SessionAdminUserModel user, string tableCode)
        {
            return ConfDA.Singleton().TableValueList(user, tableCode);
        }

        public bool TableValueSave(SessionAdminUserModel user, DICTableValueBE value)
        {
            var result = false;
            if (value.Code == 0)
            {
                var max = ConfDA.Singleton().TableValueMaxCode(user, value.TableCode);
                var tableBaseCode = (value.TableCode * 10000);
                if (max == 0)
                    value.Code = tableBaseCode + 1;
                else
                {

                    value.Code = tableBaseCode + (max - tableBaseCode) + 1;
                }

                result = ConfDA.Singleton().TableValueInsertOrUpdate(user, value, true);

            }
            else
            {
                result = ConfDA.Singleton().TableValueInsertOrUpdate(user, value, false);
            }
            Common.Configuration.LoadDiccionaries();
            return result;
        }
        public List<Common.EmailBE> EmailList(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return ConfDA.Singleton().EmailList(user, gridModel);
        }


        #region Base
        private static ConfBR _ManagerSingleton;
        public static ConfBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ConfBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
