﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;
using LVEBWeb.Models;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Admin.BR
{
    public class AdminUsersBR
    {
        public List<AdminUserBE> UsersSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return AdminUsersDA.Singleton().UsersSelect(user, gridModel);
        }


        public bool UserInsertOrUpdate(SessionAdminUserModel user, AdminUserBE value)
        {
            if (value.Roles == null)
                value.Roles = "";
            if (string.IsNullOrEmpty(value.UserPassword))
            {
                if (value.UserId == 0)
                    value.UserPassword = "dd";
            }
            else
                value.UserPassword = Common.Functions.MD5Encode(value.UserPassword);


            return AdminUsersDA.Singleton().UserInsertOrUpdate(user, value, value.UserId == 0);
        }





        #region Base
        private static AdminUsersBR _ManagerSingleton;
        public static AdminUsersBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new AdminUsersBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}