﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class CompetitionsBR
    {
        public List<CompetitionBE> CompetitionsSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return CompetitionsDA.Singleton().CompetitionsSelect(user, gridModel);
        }


        public InsertUpdateModel CompetitionInsertOrUpdate(SessionAdminUserModel user, CompetitionBE value)
        {
            if (value.AddressName == null) value.AddressName = "";
            if (value.AddressLink == null) value.AddressLink = "";
            if (value.OficialSiteUrl == null) value.OficialSiteUrl = "";
            if (value.OficialSite == null) value.OficialSite = "";


            InsertUpdateModel iu;
            if (string.IsNullOrEmpty(value.Code))
            {
                value.Code = Common.Functions.CreateId();
                iu = new InsertUpdateModel(CompetitionsDA.Singleton().CompetitionInsertOrUpdate(user, value, true), value.Code);
            }
            else
            {
                iu = new InsertUpdateModel(CompetitionsDA.Singleton().CompetitionInsertOrUpdate(user, value, false), "");
                _cache.Remove(CacheKeys.Competitions + value.Code);
            }

            _cache.Remove(CacheKeys.Competitions);
            return iu;
        }

        public CompetitionBE CompetitionGet(SessionAdminUserModel user, string code)
        {
            return CompetitionsDA.Singleton().CompetitionGet(user, code);
        }

        public bool CompetitionDelete(SessionAdminUserModel user, string code)
        {
            var rtn = CompetitionsDA.Singleton().CompetitionDelete(user, code);
            _cache.Remove(CacheKeys.Competitions);
            _cache.Remove(CacheKeys.Competitions + code);
            return rtn;
        }

        #region Base

        private IMemoryCache _cache;
        private static CompetitionsBR _ManagerSingleton;
        public static CompetitionsBR Singleton(IMemoryCache memoryCache)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompetitionsBR(memoryCache);
            }
            return _ManagerSingleton;
        }
        public CompetitionsBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        #endregion Base
    }
}
