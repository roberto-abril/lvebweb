﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class InternalMessagesBR
    {
        public List<InternalMessageBE> MessagesSearch(SessionAdminUserModel user, GridFilterModel gridModel)
        {
            return InternalMessagesDA.Singleton().MessagesSearch(user, gridModel);
        }



        public InternalMessageBE MessageGet(SessionAdminUserModel user, string code)
        {
            return InternalMessagesDA.Singleton().MessageGet(user, code);
        }


        #region Base
        private static InternalMessagesBR _ManagerSingleton;
        public static InternalMessagesBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new InternalMessagesBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
