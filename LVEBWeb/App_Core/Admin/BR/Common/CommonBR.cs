﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Admin.BE;
using LVEBWeb.App_Core.Admin.DA;

namespace LVEBWeb.App_Core.Admin.BR
{
    public class CommonBR
    {
        public List<DICValueBE> ValuesList(SessionAdminUserModel user, DICTables tableCode)
        {
            return CommonDA.Singleton().ValuesList(user, tableCode);
        }
        public List<DICProvinceBE> ProvincesList(SessionAdminUserModel user)
        {
            return CommonDA.Singleton().ProvincesList(user);
        }

        public List<DICCityBE> CitiesListByProvince(SessionAdminUserModel user, string provinceCode)
        {
            return CommonDA.Singleton().CitiesListByProvince(user, provinceCode);
        }



        #region Base
        private static CommonBR _ManagerSingleton;
        public static CommonBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CommonBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
