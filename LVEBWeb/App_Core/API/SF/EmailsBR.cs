﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.BE;

namespace LVEBWeb.App_Core.API.SF
{
    public class EmailsBR
    {
        public static bool SendPlaceChange(SessionAPIModel user, PlaceToCheckBE place)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("ReporterName", place.ReporterName);
            parameters.Add("ReporterEmail", place.ReporterEmail);
            parameters.Add("Name", place.Name);
            if (place.Status == 170003)
            {
                parameters.Add("Description", "Local cerrado de forma permanente.");
                parameters.Add("AddStreet", "");
                parameters.Add("AddStreetNumber", "");
            }
            else
            {
                parameters.Add("Description", place.Description);
                parameters.Add("AddStreet", place.AddStreet);
                parameters.Add("AddStreetNumber", place.AddStreetNumber);
            }
            parameters.Add("ValidatorCode", place.ValidatorCode);
            parameters.Add("url", App_Core.Common.Configuration.HOST_URL + "/validate/p/" + place.ValidatorCode);
            if (place.Code.Length == 0)
                return LVEBWeb.App_Core.Common.EmailsBR.Singleton().Send(place.ReporterEmail, App_Core.Common.EmailTemplates.DataPlaceAdd, parameters);
            else
                return LVEBWeb.App_Core.Common.EmailsBR.Singleton().Send(place.ReporterEmail, App_Core.Common.EmailTemplates.DataPlaceEdit, parameters);
        }

        public static bool SendEventChange(SessionAPIModel user, EventToCheckBE place)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("ReporterName", place.ReporterName);
            parameters.Add("ReporterEmail", place.ReporterEmail);
            parameters.Add("Name", place.Name);
            parameters.Add("Description", place.Description);
            parameters.Add("AddStreet", place.AddStreet);
            parameters.Add("DateStart", place.DateStart.ToString("dd/MM/yyyy HH:mm"));
            parameters.Add("ValidatorCode", place.ValidatorCode);
            parameters.Add("url", App_Core.Common.Configuration.HOST_URL + "/validate/e/" + place.ValidatorCode);
            if (place.Code.Length == 0)
                return LVEBWeb.App_Core.Common.EmailsBR.Singleton().Send(place.ReporterEmail, App_Core.Common.EmailTemplates.DataEventAdd, parameters);
            else
                return LVEBWeb.App_Core.Common.EmailsBR.Singleton().Send(place.ReporterEmail, App_Core.Common.EmailTemplates.DataEventEdit, parameters);
        }
    }
}
