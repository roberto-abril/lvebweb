﻿using System;
namespace LVEBWeb.App_Core.API.SF
{
    public class SessionAPIModel
    {
        public string Token { get; set; }
        public string UserEmail { get; set; }
        public string UserId { get; set; }

    }
}
