﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.API.DA;

namespace LVEBWeb.App_Core.API.BR
{
    public class PublicBR
    {
        public bool PlaceSentValidate(string validatorCode)
        {
            return PublicDA.Singleton().PlaceSentValidate(validatorCode);
        }


        public bool EventSentValidate(string validatorCode)
        {
            return PublicDA.Singleton().EventSentValidate(validatorCode);
        }


        #region Base
        private static PublicBR _ManagerSingleton;
        public static PublicBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PublicBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
