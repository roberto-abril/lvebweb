﻿using System;
using System.Collections.Generic;
using System.Linq;
using LVEBWeb.App_Core.API.DA;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.App_Core.API.SF;
//using Lucene.Net.Analysis.Standard.StandardAnalyzer;
//using IndexWriter = Lucene.Net.Index.IndexWriter;
using LVEBWeb.App_Core.Common;
using Microsoft.Extensions.Caching.Memory;




namespace LVEBWeb.App_Core.API.BR
{
    public class SearchBR : ISearchBR
    {
        public List<SearchAutocompleteModel> AutocompleteSearch(SessionAPIModel user, SearchSections page, string value)
        {
            if (string.IsNullOrEmpty(value)) return new List<SearchAutocompleteModel>();

            return _ManagerDA.AutocompleteSearch(user, page, value);
        }


        public List<SearchItemModel> SearchByBox(SessionAPIModel user, SearchFilterModel filter)
        {
            List<SearchItemModel> items;
            switch (filter.Section)
            {
                case SearchSections.DanceHall:
                case SearchSections.DanceSchool:
                    items = SearchFormatPlaces(user, filter, _ManagerDA.PlacesSearchByBox(user, filter));
                    break;
                case SearchSections.Events:
                    items = SearchFormatEvents(user, filter, _ManagerDA.EventsSearchByBox(user, filter));
                    if (items.Count == 0)
                    {
                        filter.Query = "";
                        items = SearchFormatEvents(user, filter, _ManagerDA.EventsFullSearch(user, filter.Section, filter.Query));
                    }
                    break;
                case SearchSections.Competitions:
                    items = SearchFormatCompetitions(user, _ManagerDA.CompetitionsSearch(user, filter));
                    break;
                case SearchSections.Home:
                    var values = new List<SearchItemModel>();
                    values.AddRange(SearchFormatPlaces(user, filter, _ManagerDA.PlacesSearchByBox(user, filter)));
                    values.AddRange(SearchFormatEvents(user, filter, _ManagerDA.EventsSearchByBox(user, filter)));
                    values.AddRange(SearchFormatCompetitions(user, _ManagerDA.CompetitionsSearch(user, filter)));

                    items = values.OrderByDescending(x => x.Score).Take(50).Select(x => x).ToList();
                    break;
                default:
                    items = new List<SearchItemModel>();
                    break;
            }
            return items;
        }
        private string SearchUserLocation(SessionAPIModel user, SearchFilterModel filter)
        {
            if (string.IsNullOrEmpty(filter.CityCode)) return "";
            return filter.CityCode + " " + filter.CityCode.Substring(0, 2);
        }

        public List<SearchItemModel> SearchFullText(SessionAPIModel user, SearchFilterModel filter)
        {
            var cityFilter = "";

            if (!string.IsNullOrEmpty(filter.CityCode))
                cityFilter = filter.CityCode + " " + (filter.CityCode.Length > 2 ? filter.CityCode.Substring(0, 2) : "");

            if (string.IsNullOrEmpty(filter.Query))
                filter.Query = cityFilter;
            else
                filter.Query += " " + cityFilter;


            List<SearchItemModel> items;
            switch (filter.Section)
            {
                case SearchSections.DanceHall:
                case SearchSections.DanceSchool:
                    items = SearchFormatPlaces(user, filter, _ManagerDA.PlacesFullSearch(user, filter.Section, filter.Query));
                    break;
                case SearchSections.Events:
                    items = SearchFormatEvents(user, filter, _ManagerDA.EventsFullSearch(user, filter.Section, filter.Query));
                    break;
                case SearchSections.Competitions:
                    items = SearchFormatCompetitions(user, _ManagerDA.CompetitionsSearch(user, filter));
                    break;
                case SearchSections.Home:
                    var values = _ManagerDA.HomeFullSearch(user, filter.Query);

                    for (var x = 0; x < values.Count; x++)
                    {
                        switch (values[x].Type)
                        {
                            case SearchItemTypes.DanceHall:
                            case SearchItemTypes.DanceSchool:
                                values[x] = SearchFormatPlace(user, filter, (SearchItemPlaceModel)values[x]);
                                break;
                            case SearchItemTypes.Event:
                                values[x] = SearchFormatEvent(user, filter, (SearchItemEventModel)values[x]);
                                break;
                            case SearchItemTypes.Competition:
                                break;
                        }
                    }
                    items = values;//.OrderByDescending(x => x.Score).Take(50).Select(x => x).ToList();
                    break;
                default:
                    items = new List<SearchItemModel>();
                    break;
            }
            return items.OrderByDescending(x => x.Score).Take(50).Select(x => x).ToList();
        }

        private List<SearchItemModel> SearchFormatPlaces(SessionAPIModel user, SearchFilterModel filter, List<SearchItemPlaceModel> items)
        {
            for (var x = 0; x < items.Count; x++)
            {
                SearchFormatPlace(user, filter, items[x]);
            }
            if (filter.LocationActive)
            {
                items.Sort((x, y) => (int)(x.Distance - y.Distance));
            }
            var values = items.Take(30).Select(x => (SearchItemModel)x).ToList();
            return values;
        }
        private SearchItemPlaceModel SearchFormatPlace(SessionAPIModel user, SearchFilterModel filter, SearchItemPlaceModel item)
        {
            if (filter.LocationActive)
            {
                item.Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(item.Latitude, item.Longitude, filter.Latitude, filter.Longitude), 2);
                if (item.Distance == 0.0)
                    item.Distance = 0.05;
            }
            if (item.CategoriesList != null)
                foreach (var cat in item.CategoriesList)
                {
                    if (Configuration.DicLocalCategories.ContainsKey(cat))
                        item.CategoriesListString.Add(Configuration.DicLocalCategories[cat]);
                }
            for (var y = 0; y < item.TagsList.Count; y++)
            {
                if (Configuration.DicLocalTagsBailes.ContainsKey(item.TagsList[y]))
                    item.TagsBailesList.Add(Configuration.DicLocalTagsBailes[item.TagsList[y]]);

                if (Configuration.DicLocalTagsProfiles.ContainsKey(item.TagsList[y]))
                    item.TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[item.TagsList[y]]);
            }

            var defaultCode = "";
            var defaultUse = true;
            if (item.PhotoCodeHome.Length > 0)
            {
                defaultUse = false;
                item.PhotoCodeHome = "/api/media/pi/" + item.PhotoCodeHome + "1000";
            }
            if (defaultUse)
            {
                var v = long.Parse(item.Code, System.Globalization.NumberStyles.HexNumber);
                defaultCode = (v % 10).ToString();
                if (item.IsDanceHall)
                    item.PhotoCodeHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                else
                    item.PhotoCodeHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
            }
            defaultUse = true;
            if (item.PhotoCodeIcon.Length > 0)
            {
                defaultUse = false;
                item.PhotoCodeIcon = "/api/media/pi/" + item.PhotoCodeIcon + "100";
            }
            if (defaultUse)
            {
                if (item.IsDanceHall)
                    item.PhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                else
                    item.PhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
            }
            return item;
        }

        private List<SearchItemModel> SearchFormatEvents(SessionAPIModel user, SearchFilterModel filter, List<SearchItemEventModel> items)
        {

            for (var x = 0; x < items.Count; x++)
            {
                SearchFormatEvent(user, filter, items[x]);

            }
            if (filter.LocationActive)
            {
                items.Sort((x, y) => (int)(x.Distance - y.Distance));

            }

            var values = items.Take(50).Select(x => (SearchItemModel)x).ToList();

            return values;
        }
        private SearchItemEventModel SearchFormatEvent(SessionAPIModel user, SearchFilterModel filter, SearchItemEventModel item)
        {
            if (filter.LocationActive && item.LocationActive)
            {
                item.Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(item.Latitude, item.Longitude, filter.Latitude, filter.Longitude), 1);
                if (item.Distance == 0.0)
                    item.Distance = 0.05;
            }
            if (item.PlaceCategoriesList != null)
                foreach (var cat in item.PlaceCategoriesList)
                {
                    if (Configuration.DicLocalCategories.ContainsKey(cat))
                        item.PlaceCategoriesListString.Add(Configuration.DicLocalCategories[cat]);
                }

            item.Score = item.PlaceScore + 1;

            var defaultCode = "";
            var defaultUse = true;


            if (item.PlacePhotoCodeIcon.Length > 0)
            {
                defaultUse = false;
                //item.PlacePhotoCodeIcon =  item.PlacePhotoCodeIcon;
            }
            if (defaultUse)
            {
                if (item.PlaceIsDanceHall)
                    item.PlacePhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                else
                    item.PlacePhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
            }
            if (item.HavePoster)
            {
                item.EventPhotoHome = "/api/media/ei/" + item.PlaceCode + "/" + item.Code + "1000";
            }
            else
            {
                defaultUse = true;
                if (item.PlacePhotoCodeHome.Length > 0)
                {
                    defaultUse = false;
                    item.EventPhotoHome = "/api/media/pi/" + item.PlacePhotoCodeHome + "1000";
                }
                if (defaultUse)
                {
                    var v = long.Parse(item.PlaceCode, System.Globalization.NumberStyles.HexNumber);
                    defaultCode = (v % 10).ToString();
                    if (item.PlaceIsDanceHall)
                        item.EventPhotoHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                    else
                        item.EventPhotoHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
                }

            }
            defaultUse = true;
            if (item.PlacePhotoCodeHome.Length > 0)
            {
                defaultUse = false;
                item.PlacePhotoCodeHome = "/api/media/pi/" + item.PlacePhotoCodeHome + "1000";
            }
            if (defaultUse)
            {
                var v = long.Parse(item.Code, System.Globalization.NumberStyles.HexNumber);
                defaultCode = (v % 10).ToString();
                if (item.PlaceIsDanceHall)
                    item.PlacePhotoCodeHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                else
                    item.PlacePhotoCodeHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
            }
            defaultUse = true;
            if (item.PlacePhotoCodeIcon.Length > 0)
            {
                defaultUse = false;
                item.PlacePhotoCodeIcon = "/api/media/pi/" + item.PlacePhotoCodeIcon + "100";
            }
            if (defaultUse)
            {
                if (item.PlaceIsDanceHall)
                    item.PlacePhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                else
                    item.PlacePhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
            }
            return item;
        }

        private List<SearchItemModel> SearchFormatCompetitions(SessionAPIModel user, List<SearchItemCompetitionModel> items)
        {
            var values = items.Take(50).Select(x => (SearchItemModel)x).ToList();

            return values;
        }

        public List<SearchItemEventModel> EventsSearchByPlace(SessionAPIModel user, string placeCode)
        {
            return _ManagerDA.EventsSearchByPlace(user, placeCode);
        }


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static SearchBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ISearchDA _ManagerDA;


        public static SearchBR Singleton(IMemoryCache memoryCache, ISearchDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new SearchBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static SearchBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, SearchDA.Singleton());
        }

        public SearchBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new SearchDA();

        }
        public SearchBR(IMemoryCache memoryCache, ISearchDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base

    }
}