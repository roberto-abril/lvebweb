﻿using System.Collections.Generic;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.App_Core.API.SF;

namespace LVEBWeb.App_Core.API.BR
{
    public interface ISearchBR
    {

        List<SearchAutocompleteModel> AutocompleteSearch(SessionAPIModel user, SearchSections page, string value);

        List<SearchItemModel> SearchByBox(SessionAPIModel user, SearchFilterModel filter);
        List<SearchItemModel> SearchFullText(SessionAPIModel user, SearchFilterModel filter);

    }
}
