﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.API.DA;

namespace LVEBWeb.App_Core.API.BR
{
    public class InternalMessageBR
    {

        public bool InternalMessageInsert(SessionAPIModel user, InternalMessageBE value)
        {
            return InternalMessageDA.Singleton().InternalMessageInsert(user, value);
        }

        #region Base
        private static InternalMessageBR _ManagerSingleton;
        public static InternalMessageBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new InternalMessageBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
