﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.API.DA;

namespace LVEBWeb.App_Core.API.BR
{
    public class CompetitionsBR
    {

        public List<CompetitionBE> CompetitionsList(SessionAPIModel user, long dateLastUpdate)
        {
            return CompetitionsDA.Singleton().CompetitionsList(user, dateLastUpdate);
        }




        #region Base
        private static CompetitionsBR _ManagerSingleton;
        public static CompetitionsBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompetitionsBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
