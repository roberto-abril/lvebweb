﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.API.DA;
using Microsoft.Extensions.Caching.Memory;


namespace LVEBWeb.App_Core.API.BR
{
    public class EventsBR
    {


        // TODO eliminar
        public List<EvenetBE> EventsList(SessionAPIModel user, string province, long dateLastUpdate)
        {
            return EventsDA.Singleton().EventsList(user, province, dateLastUpdate);
        }
        // TODO eliminar
        public bool EventToCheckInsert(SessionAPIModel user, EventToCheckBE activity)
        {
            if (activity.Code == null) activity.Code = "";
            if (activity.AddStreet == null) activity.AddStreet = "";
            if (activity.AddCityCode == null) activity.AddCityCode = "";

            if (activity.ReporterName == null) activity.ReporterName = "";
            if (activity.ReporterEmail == null) activity.ReporterEmail = "";
            if (activity.ReporterPhone == null) activity.ReporterPhone = "";
            if (activity.ReporterRelation == null) activity.ReporterRelation = "";


            if (activity.ReporterName.Length > 49)
                activity.ReporterName = activity.ReporterName.Substring(0, 50);
            if (activity.ReporterEmail.Length > 250)
                activity.ReporterEmail = activity.ReporterEmail.Substring(0, 50);
            if (activity.ReporterPhone.Length > 15)
                activity.ReporterPhone = activity.ReporterPhone.Substring(0, 15);

            activity.ValidatorCode = App_Core.Common.Functions.CreateId();

            if (EventsDA.Singleton().EventToCheckInsert(user, activity))
            {
                SF.EmailsBR.SendEventChange(user, activity);
                return true;
            }
            return false;
        }


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static EventsBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IPlacesDA _ManagerDA;


        public static EventsBR Singleton(IMemoryCache memoryCache, IPlacesDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EventsBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static EventsBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, PlacesDA.Singleton());
        }

        public EventsBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new PlacesDA();

        }
        public EventsBR(IMemoryCache memoryCache, IPlacesDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
