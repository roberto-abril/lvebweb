﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.App_Core.API.DA;
using LVEBWeb.App_Core.Common;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.API.BR
{
    public class PlacesBR
    {
        public PlaceDetailsModel PlaceGet(SessionAPIModel user, SearchFilterModel filter)
        {
            var item = _ManagerDA.PlaceGet(user, filter.Code);
            SearchFormatPlace(user, filter, item);
            item.Events = SearchBR.Singleton(_cache).EventsSearchByPlace(user, filter.Code);

            for (var x = 0; x < item.Events.Count; x++)
            {
                var defaultCode = "";
                var defaultUse = true;
                if (item.Events[x].HavePoster)
                {
                    item.Events[x].EventPhotoHome = "/api/media/ei/" + item.Events[x].PlaceCode + "/" + item.Events[x].Code + "1000";
                }
                else
                {
                    if (item.Events[x].PlacePhotoCodeHome.Length > 0)
                    {
                        defaultUse = false;
                        item.Events[x].EventPhotoHome = "/api/media/pi/" + item.Events[x].PlacePhotoCodeHome + "1000";
                    }
                    if (defaultUse)
                    {
                        var v = long.Parse(item.Events[x].PlaceCode, System.Globalization.NumberStyles.HexNumber);
                        defaultCode = (v % 10).ToString();
                        if (item.Events[x].PlaceIsDanceHall)
                            item.Events[x].EventPhotoHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                        else
                            item.Events[x].EventPhotoHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
                    }
                }
            }

            return item;
        }
        private PlaceDetailsModel SearchFormatPlace(SessionAPIModel user, SearchFilterModel filter, PlaceDetailsModel item)
        {
            if (filter.LocationActive)
            {
                item.Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(item.Latitude, item.Longitude, filter.Latitude, filter.Longitude), 2);
                if (item.Distance == 0.0)
                    item.Distance = 0.05;
            }
            if (item.CategoriesList != null)
                foreach (var cat in item.CategoriesList)
                {
                    if (Configuration.DicLocalCategories.ContainsKey(cat))
                        item.CategoriesListString.Add(Configuration.DicLocalCategories[cat]);
                }
            for (var y = 0; y < item.TagsList.Count; y++)
            {
                if (Configuration.DicLocalTagsBailes.ContainsKey(item.TagsList[y]))
                    item.TagsBailesList.Add(Configuration.DicLocalTagsBailes[item.TagsList[y]]);

                if (Configuration.DicLocalTagsProfiles.ContainsKey(item.TagsList[y]))
                    item.TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[item.TagsList[y]]);
            }

            var defaultCode = "";
            var defaultUse = true;
            if (item.PhotoCodeHome.Length > 0)
            {
                defaultUse = false;
                item.PhotoCodeHome = "/api/media/pi/" + item.PhotoCodeHome + "1000";
            }
            if (defaultUse)
            {
                var v = long.Parse(item.Code, System.Globalization.NumberStyles.HexNumber);
                defaultCode = (v % 10).ToString();
                if (item.IsDanceHall)
                    item.PhotoCodeHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                else
                    item.PhotoCodeHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
            }
            defaultUse = true;
            if (item.PhotoCodeIcon.Length > 0)
            {
                defaultUse = false;
                item.PhotoCodeIcon = "/api/media/pi/" + item.PhotoCodeIcon + "100";
            }
            if (defaultUse)
            {
                if (item.IsDanceHall)
                    item.PhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                else
                    item.PhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
            }
            return item;
        }

        // TODO delete
        public List<PlaceBE> PlacesList(SessionAPIModel user, string provinceCode, long dateLastUpdate)
        {
            return PlacesDA.Singleton().PlacesList(user, provinceCode, dateLastUpdate);
        }
        // TODO delete
        public bool PlaceToCheckInsert(SessionAPIModel user, PlaceToCheckBE place)
        {

            if (place.Code == null) place.Code = "";
            if (place.AddCP == null) place.AddCP = "";
            if (place.AddCityCode == null) place.AddCityCode = "";
            if (place.AddProvinceCode == null) place.AddProvinceCode = "";
            if (place.AddStreet == null) place.AddStreet = "";
            if (place.AddStreetNumber == null) place.AddStreetNumber = "";

            if (place.ReporterName == null) place.ReporterName = "";
            if (place.ReporterEmail == null) place.ReporterEmail = "";
            if (place.ReporterPhone == null) place.ReporterPhone = "";
            if (place.ReporterRelation == null) place.ReporterRelation = "";


            if (place.ReporterName.Length > 49)
                place.ReporterName = place.ReporterName.Substring(0, 50);
            if (place.ReporterEmail.Length > 250)
                place.ReporterEmail = place.ReporterEmail.Substring(0, 50);
            if (place.ReporterPhone.Length > 15)
                place.ReporterPhone = place.ReporterPhone.Substring(0, 15);


            place.ValidatorCode = App_Core.Common.Functions.CreateId();

            if (PlacesDA.Singleton().PlaceToCheckInsert(user, place))
            {
                SF.EmailsBR.SendPlaceChange(user, place);
                return true;
            }
            return false;
        }


        public bool PlacesToImprove_Insert(SessionAPIModel user, PlaceToImproveModel model)
        {
            if (model.UserCode == null) model.UserCode = "";
            if (model.Notes == null) model.Notes = "";

            return _ManagerDA.PlacesToImprove_Insert(user, model);
        }

        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static PlacesBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IPlacesDA _ManagerDA;


        public static PlacesBR Singleton(IMemoryCache memoryCache, IPlacesDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PlacesBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static PlacesBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, PlacesDA.Singleton());
        }

        public PlacesBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new PlacesDA();

        }
        public PlacesBR(IMemoryCache memoryCache, IPlacesDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
