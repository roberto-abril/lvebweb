﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.API.DA;

namespace LVEBWeb.App_Core.API.BR
{
    public class CommonBR
    {

        public Models.CommonLocationModel CommonLocationGet(SessionAPIModel user, long dateLastUpdate)
        {
            var model = new Models.CommonLocationModel();
            model.Provinces = CommonDA.Singleton().ProvincesList(user, dateLastUpdate);
            model.Cities = CommonDA.Singleton().CitiesList(user, dateLastUpdate);
            return model;
        }

        public Models.CommonDICModel CommonDICGet(SessionAPIModel user, long dateLastUpdate)
        {
            var model = new Models.CommonDICModel();
            model.LocalCategorias = CommonDA.Singleton().ValuesList(user, App_Core.Common.DICTables.LocalCategorias, dateLastUpdate);

            model.LocalTags = CommonDA.Singleton().ValuesList(user, App_Core.Common.DICTables.LocalTagsBailes, dateLastUpdate);
            return model;
        }
        public List<DICValueBE> CommonDICGetValues(SessionAPIModel user, long dateLastUpdate)
        {
            return CommonDA.Singleton().ValuesList(user, App_Core.Common.DICTables.None, dateLastUpdate);
        }

        public Dictionary<string, long> DataLastUpdateList(SessionAPIModel user)
        {
            return CommonDA.Singleton().DataLastUpdateList(user);
        }

        #region Base
        private static CommonBR _ManagerSingleton;
        public static CommonBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CommonBR();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
