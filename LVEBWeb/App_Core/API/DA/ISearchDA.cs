﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;

namespace LVEBWeb.App_Core.API.DA
{
    public interface ISearchDA
    {

        List<SearchAutocompleteModel> AutocompleteSearch(SessionAPIModel user, SearchSections page, string value);

        List<SearchItemModel> HomeFullSearch(SessionAPIModel user, string value);


        List<SearchItemPlaceModel> PlacesSearchByBox(SessionAPIModel user, SearchFilterModel filter);
        List<SearchItemPlaceModel> PlacesFullSearch(SessionAPIModel user, SearchSections page, string value);

        List<SearchItemEventModel> EventsSearchByBox(SessionAPIModel user, SearchFilterModel filter);
        List<SearchItemEventModel> EventsFullSearch(SessionAPIModel user, SearchSections page, string value);

        List<SearchItemCompetitionModel> CompetitionsSearch(SessionAPIModel user, SearchFilterModel filter);


        List<SearchItemEventModel> EventsSearchByPlace(SessionAPIModel user, string placeCode);

    }
}
