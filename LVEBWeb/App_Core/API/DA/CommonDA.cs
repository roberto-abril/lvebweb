﻿using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.BE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.Models;
using Newtonsoft.Json;
using LVEBWeb.App_Core.Common;
using Npgsql;
namespace LVEBWeb.App_Core.API.DA
{
    public class CommonDA : App_Core.API.DA.BaseDA
    {
        public List<DICValueBE> ValuesList(SessionAPIModel user, App_Core.Common.DICTables tableCode, long dateLastUpdate)
        {
            var sql = "select Code,Name,updatedate from DICTableValues where " + (int)tableCode + " = 0 or TableCode = " + (int)tableCode + " and updatedate > " + dateLastUpdate + "  order by Name limit 2000";
            var items = new List<DICValueBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICValueBE() { Code = reader.GetInt32(0).ToString(), Name = reader.GetValue(1).ToString(), UpdateDate = reader.GetInt64(2) });
                        }
                    }
                }
            }
            return items;
        }
        public List<DICValueBE> ProvincesList(SessionAPIModel user, long dateLastUpdate)
        {
            var sql = "select Code,Name,updatedate from DICProvinces where updatedate > " + dateLastUpdate + " order by Name limit 2000";
            var items = new List<DICValueBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICValueBE() { Code = reader.GetValue(0).ToString(), Name = reader.GetValue(1).ToString(), UpdateDate = reader.GetInt64(2) });
                        }
                    }
                }
            }
            return items;
        }

        public List<DICValueBE> CitiesList(SessionAPIModel user, long dateLastUpdate)
        {
            var sql = "select Code,Name,updatedate from DICCities where updatedate > " + dateLastUpdate + "  order by Code limit 10000";
            var items = new List<DICValueBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new DICValueBE() { Code = reader.GetValue(0).ToString(), Name = reader.GetValue(1).ToString(), UpdateDate = reader.GetInt64(2) });
                        }
                    }
                }
            }
            return items;
        }

        public Dictionary<string, long> DataLastUpdateList(SessionAPIModel user)
        {
            var sql = "select Type,DateLastUpdate from DataLastUpdate order by Type limit 2000";
            var items = new Dictionary<string, long>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(reader.GetString(0), reader.GetInt64(1));
                        }
                    }
                }
            }
            return items;
        }

        #region Base
        private static CommonDA _ManagerSingleton;
        public static CommonDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CommonDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}