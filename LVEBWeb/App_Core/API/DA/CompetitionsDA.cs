﻿using LVEBWeb.App_Core.API.SF;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.Common;
using Npgsql;
namespace LVEBWeb.App_Core.API.DA
{
    public class CompetitionsDA : App_Core.Admin.DA.BaseDA
    {


        public List<CompetitionBE> CompetitionsList(SessionAPIModel user, long dateLastUpdate)
        {
            var sql = @"select c.Code,c.Name,Description,Modalities,DateStart,DateEnd,AddProvinceCode,AddressName,AddressLink,OficialSite,OficialSiteUrl,Latitude,Longitud,HavePoster 
            ,c.updatedate,c.deleted
            from Competitions c where c.Status = 190001 and DateEnd > CURRENT_TIMESTAMP and c.updatedate > " + dateLastUpdate + " order by c.DateStart, c.Name limit 2000";
            var items = new List<CompetitionBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new CompetitionBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                Modalities = reader.GetValue(3).ToString(),
                                DateStart = reader.GetDateTime(4),
                                DateEnd = reader.GetDateTime(5),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddressName = reader.GetValue(7).ToString(),
                                AddressLink = reader.GetValue(8).ToString(),
                                OficialSite = reader.GetValue(9).ToString(),
                                OficialSiteUrl = reader.GetValue(10).ToString(),
                                Latitude = reader.GetDouble(11),
                                Longitud = reader.GetDouble(12),
                                HavePoster = reader.GetBoolean(13),
                                UpdateDate = reader.GetInt64(14),
                                Deleted = reader.GetBoolean(15)
                            });
                        }
                    }
                }
            }
            return items;
        }

        public bool InternalMessageInsert(SessionAPIModel user, InternalMessageBE value)
        {
            var sql = "insert into internalmessages (messageid,contactemail,contactphone,messagetype,messagebody) " +
                    "values (@messageid,@contactemail,@contactphone,@messagetype,@messagebody)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@messageid", value.MessageId));
                    cmd.Parameters.Add(new NpgsqlParameter("@contactemail", value.ContactEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@contactphone", value.ContactPhone));
                    cmd.Parameters.Add(new NpgsqlParameter("@messagetype", value.MessageType));
                    cmd.Parameters.Add(new NpgsqlParameter("@messagebody", value.MessageBody));
                    cmd.ExecuteNonQuery();
                }
            }
            // this.DataLastUpdateSave(DataTypes.Activities, );
            return true;
        }





        #region Base
        private static CompetitionsDA _ManagerSingleton;
        public static CompetitionsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompetitionsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}