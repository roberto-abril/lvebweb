﻿using System;
using LVEBWeb.App_Core.API.BE;
using Npgsql;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.API.SF;
namespace LVEBWeb.App_Core.API.DA
{
    public class PublicDA
    {

        public bool PlaceSentValidate(string validatorCode)
        {
            var sql = "update PlacesToCheck set CheckStatus = @CheckStatus, CheckStatusDate=@CheckStatusDate, ValidatorCode = '' where ValidatorCode=@ValidatorCode  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@CheckStatus", 1));
                    cmd.Parameters.Add(new NpgsqlParameter("@CheckStatusDate", DateTime.Now));
                    //cmd.Parameters.Add(new NpgsqlParameter("@Id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("@ValidatorCode", validatorCode));
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }


        public bool EventSentValidate(string validatorCode)
        {
            var sql = "update EventsToCheck set CheckStatus = @CheckStatus, CheckStatusDate=@CheckStatusDate, ValidatorCode = '' where ValidatorCode=@ValidatorCode  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@CheckStatus", 1));
                    cmd.Parameters.Add(new NpgsqlParameter("@CheckStatusDate", DateTime.Now));
                    //cmd.Parameters.Add(new NpgsqlParameter("@Id", id));
                    cmd.Parameters.Add(new NpgsqlParameter("@ValidatorCode", validatorCode));
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }


        #region Base
        private static PublicDA _ManagerSingleton;
        public static PublicDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PublicDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
