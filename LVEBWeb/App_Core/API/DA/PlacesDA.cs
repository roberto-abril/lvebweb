﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
using LVEBWeb.App_Core.API.SF;

namespace LVEBWeb.App_Core.API.DA
{
    public class PlacesDA : App_Core.API.DA.BaseDA, IPlacesDA
    {

        public PlaceDetailsModel PlaceGet(SessionAPIModel user, string code)
        {
            var sql = "select p.Code,p.Name,c.name,AddStreet,AddStreetNumber,Description,DescriptionFull,Latitude,Longitude" +
                " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate,p.deleted" +
                " ,photocodehome,photocodeicon" +
                ",OpenHours ,contactweb, contactphone, contactemail" +
                " from Places as p left join diccities as c on p.AddCityCode = c.code " +
                " where p.Code = @Code ";


            var place = new PlaceDetailsModel();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            place = new PlaceDetailsModel()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                AddressCityName = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                AddressStreet = reader.GetValue(3).ToString(),
                                AddressStreetNumber = reader.GetValue(4).ToString(),
                                Description = reader.GetValue(5).ToString(),
                                DescriptionFull = reader.GetValue(6).ToString(),
                                Latitude = reader.GetDouble(7),
                                Longitude = reader.GetDouble(8),
                                TagsList = reader.IsDBNull(9) || reader.GetValue(9).ToString() == "" ? new List<int>() : reader.GetValue(9).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                CategoriesList = reader.IsDBNull(10) || reader.GetValue(10).ToString() == "" ? new HashSet<int>() : new HashSet<int>(reader.GetValue(10).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),
                                PhotoCodeHome = reader.GetValue(13).ToString(),
                                PhotoCodeIcon = reader.GetValue(14).ToString(),
                                OpenHours = reader.GetValue(15).ToString(),
                                ContactWeb = reader.GetValue(16).ToString(),
                                ContactPhone = reader.GetValue(17).ToString(),
                                ContactEmail = reader.GetValue(18).ToString()

                            };
                        }
                        else
                            return place;
                    }
                }
            }


            return place;
        }

        public List<PlaceBE> PlacesList(SessionAPIModel user, string provinceCode, long dateLastUpdate)
        {
            var sql = "select p.Code,p.Name,AddCityCode,c.name,AddStreet,AddStreetNumber,Description,Latitude,Longitude,HaveLogo" +
                " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate,p.deleted" +
                " from Places as p left join diccities as c on p.AddCityCode = c.code ";
            if (!string.IsNullOrEmpty(provinceCode))
                sql += " where p.updatedate > " + dateLastUpdate + " and AddProvinceCode = '" + provinceCode + "'";
            else
                sql += " where p.updatedate > " + dateLastUpdate + " ";
            // TODO Descomentar las dos siguiente lines en unas semanas (20/01/2019)
            if (dateLastUpdate == 0)
                sql += " and p.deleted = false ";
            sql += " order by p.Name limit 2000";

            var items = new List<PlaceBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {
                                var deleted = reader.GetBoolean(13);
                                if (deleted)
                                {
                                    items.Add(new PlaceBE()
                                    {
                                        Code = reader.GetValue(0).ToString(),
                                        Name = reader.GetValue(1).ToString(),
                                        UpdateDate = reader.GetInt64(12),
                                        Deleted = reader.GetBoolean(13)
                                    });
                                }
                                else
                                    items.Add(new PlaceBE()
                                    {
                                        Code = reader.GetValue(0).ToString(),
                                        Name = reader.GetValue(1).ToString(),
                                        AddCityCode = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                        AddCityName = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                        AddStreet = reader.GetValue(4).ToString(),
                                        AddStreetNumber = reader.GetValue(5).ToString(),
                                        Description = reader.GetValue(6).ToString(),
                                        Latitude = reader.GetDouble(7),
                                        Longitude = reader.GetDouble(8),
                                        //HaveLogo = reader.GetBoolean(9),
                                        TagsList = reader.IsDBNull(10) || reader.GetValue(10).ToString() == "" ? new List<int>() : reader.GetValue(10).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                        CategoriesList = reader.IsDBNull(11) || reader.GetValue(11).ToString() == "" ? new HashSet<int>() : new HashSet<int>(reader.GetValue(11).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),
                                        UpdateDate = reader.GetInt64(12),
                                        Deleted = reader.GetBoolean(13)
                                    });
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }
                    }
                }
            }
            return items;
        }

        // TODO Delete method.
        public bool PlaceToCheckInsert(SessionAPIModel user, PlaceToCheckBE place)
        {
            var sql = "insert into PlacesToCheck (Name,AddCityCode,AddProvinceCode,AddStreet,AddStreetNumber,AddCP,Description,Latitude,Longitude,UpdateDate,Code,Categories,Tags,Status,Reporter_Name,reporter_email,reporter_phone,reporter_relation,ValidatorCode) " +
                    "values (@Name,@AddCityCode,@AddProvinceCode,@AddStreet,@AddStreetNumber,@AddCP,@Description,@Latitude,@Longitude,@UpdateDate,@Code,@Categories,@Tags,@Status,@Reporter_Name,@reporter_email,@reporter_phone,@reporter_relation,@ValidatorCode)  ";
            place.UpdateDate = App_Core.Common.Functions.DateUpdate();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", place.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", place.AddCityCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddProvinceCode", place.AddProvinceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddStreet", place.AddStreet));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddStreetNumber", place.AddStreetNumber));
                    cmd.Parameters.Add(new NpgsqlParameter("@AddCP", place.AddCP));
                    cmd.Parameters.Add(new NpgsqlParameter("@Description", place.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("@Latitude", place.Latitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@Longitude", place.Longitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", place.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", place.Code));
                    cmd.Parameters.Add(new NpgsqlParameter("@Categories", place.Categories));
                    cmd.Parameters.Add(new NpgsqlParameter("@Tags", place.Tags));
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", place.Status));
                    cmd.Parameters.Add(new NpgsqlParameter("@Reporter_Name", place.ReporterName));
                    cmd.Parameters.Add(new NpgsqlParameter("@reporter_email", place.ReporterEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@reporter_phone", place.ReporterPhone));
                    cmd.Parameters.Add(new NpgsqlParameter("@reporter_relation", place.ReporterRelation));
                    cmd.Parameters.Add(new NpgsqlParameter("@ValidatorCode", place.ValidatorCode));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool PlacesToImprove_Insert(SessionAPIModel user, PlaceToImproveModel model)
        {
            var sql = "insert into placestoimprove (placecode,usercode,notes,insertdate) " +
                    "values (@placecode,@usercode,@notes, @insertdate)  ";
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@placecode", model.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@usercode", model.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@notes", model.Notes));
                    cmd.Parameters.Add(new NpgsqlParameter("@insertdate", Common.Functions.DateUpdate()));
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }



        #region Base
        private static PlacesDA _ManagerSingleton;
        public static PlacesDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PlacesDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}