﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;

namespace LVEBWeb.App_Core.API.DA
{
    public interface IEventsDA
    {
        List<SearchItemEventModel> EventsByPlace(SessionAPIModel user, string placeCode);
    }
}
