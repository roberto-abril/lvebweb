﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using LVEBWeb.App_Core.API.Models;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.Common;
using Newtonsoft.Json;
using Npgsql;
//using Lucene.Net.Analysis.Standard.StandardAnalyzer;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
//using IndexWriter = Lucene.Net.Index.IndexWriter;
using Version = Lucene.Net.Util.Version;



namespace LVEBWeb.App_Core.API.DA
{
    public class SearchDA : BaseDA, ISearchDA
    {
        public List<SearchAutocompleteModel> AutocompleteSearch(SessionAPIModel user, SearchSections page, string value)
        {
            if (page == SearchSections.DanceHall)
                value = "+110002 " + value;
            if (page == SearchSections.DanceSchool)
                value = "+110001 " + value;
            if (page == SearchSections.Events)
                value = "+event " + value;


            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            var topDocs = searcher.Search(query, 10);

            int results = topDocs.ScoreDocs.Length;
            // Console.WriteLine("Found {0} results", results);

            var items = new List<SearchAutocompleteModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                items.Add(new SearchAutocompleteModel()
                {
                    Code = doc.Get("Id"),
                    Label = doc.Get("Name"),
                    Value1 = doc.Get("City")
                });

                Functions.ConsoleWriteLine($"Name: {doc.Get("Name")}, AddStreet: {doc.Get("AddStreet")}, City: {doc.Get("City")}");
            }

            return items;
        }


        public List<SearchItemModel> HomeFullSearch(SessionAPIModel user, string value)
        {
            if (value == "")
            {
                value = "110002 110001 event";
            }
            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            TopDocs topDocs = searcher.Search(query, 30);

            int results = topDocs.ScoreDocs.Length;

            var items = new List<SearchItemModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                var type = doc.Get("Type");
                var json = doc.Get("json");
                switch (type)
                {
                    case "1": // DanceSchool
                    case "2": // DanceHall
                        var place = JsonConvert.DeserializeObject<SearchItemPlaceModel>(json);
                        items.Add(place);
                        break;
                    case "3": // Events
                        var e = JsonConvert.DeserializeObject<SearchItemEventModel>(json);
                        items.Add(e);
                        break;
                    case "4": // Competitions
                        var competition = JsonConvert.DeserializeObject<SearchItemCompetitionModel>(json);
                        items.Add(competition);
                        break;
                }
            }
            return items;
        }

        public List<SearchItemPlaceModel> PlacesFullSearch(SessionAPIModel user, SearchSections page, string value)
        {
            if (page == SearchSections.DanceHall)
                value = "+110002 " + value;
            if (page == SearchSections.DanceSchool)
                value = "+110001 " + value;

            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            var topDocs = searcher.Search(query, 30);

            int results = topDocs.ScoreDocs.Length;

            var items = new List<SearchItemPlaceModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                var json = doc.Get("json");
                var item = JsonConvert.DeserializeObject<SearchItemPlaceModel>(json);

                if (page == SearchSections.DanceHall)
                    item.Type = SearchItemTypes.DanceHall;
                else
                    item.Type = SearchItemTypes.DanceSchool;

                items.Add(item);
            }

            items.Sort((x, y) => y.Score.CompareTo(x.Score));
            return items;
        }

        public List<SearchItemEventModel> EventsFullSearch(SessionAPIModel user, SearchSections page, string value)
        {
            value = "+event " + value;

            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            var topDocs = searcher.Search(query, 30);

            int results = topDocs.ScoreDocs.Length;

            var items = new List<SearchItemEventModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                var json = doc.Get("json");
                var item = JsonConvert.DeserializeObject<SearchItemEventModel>(json);

                items.Add(item);

            }
            items.Sort((x, y) => y.PlaceScore.CompareTo(x.PlaceScore));

            return items;
        }



        public List<SearchItemPlaceModel> PlacesSearchByBox(SessionAPIModel user, SearchFilterModel filter)
        {
            //var dateLastUpdate = 0;
            var sql = "select p.Code,p.Name,AddCityCode,c.name,AddStreet,AddStreetNumber,Description,Latitude,Longitude,photocodehome,photocodeicon" +
                " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate" +
                " ,p.score" +
                " from Places as p left join diccities as c on p.AddCityCode = c.code ";


            sql += " where p.deleted = false and p.status = 180001 ";
            if (filter.LocationActive)
                sql += " and " + string.Format(System.Globalization.CultureInfo.InvariantCulture, " latitude > {0} and latitude < {1} and longitude > {2} and longitude < {3}", filter.Latitude - 1, filter.Latitude + 1, filter.Longitude - 1, filter.Longitude + 1);

            if (filter.Section == SearchSections.Store)
                sql += " and p.IsStore = true ";
            if (filter.Section == SearchSections.DanceHall)
                sql += " and p.IsDanceHall = true ";
            if (filter.Section == SearchSections.DanceSchool)
                sql += " and p.IsDanceSchool = true ";


            sql += " order by p.Name limit 400";

            var items = new List<SearchItemPlaceModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {

                                var place = new SearchItemPlaceModel()
                                {
                                    Code = reader.GetValue(0).ToString(),
                                    Name = reader.GetValue(1).ToString(),
                                    AddressCity = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                    AddressStreet = reader.GetValue(4).ToString() + ", " + reader.GetValue(5).ToString(),
                                    Description = reader.GetValue(6).ToString(),
                                    Latitude = reader.GetDouble(7),
                                    Longitude = reader.GetDouble(8),
                                    PhotoCodeHome = reader.GetValue(9).ToString(),
                                    PhotoCodeIcon = reader.GetValue(10).ToString(),
                                    TagsList = reader.IsDBNull(11) || reader.GetValue(11).ToString() == "" ? new List<int>() : reader.GetValue(11).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                    CategoriesList = reader.IsDBNull(12) || reader.GetValue(12).ToString() == "" ? new HashSet<int>() : new HashSet<int>(reader.GetValue(12).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),
                                    Score = reader.GetInt32(14)
                                };
                                if (filter.Section == SearchSections.DanceHall)
                                    place.Type = SearchItemTypes.DanceHall;
                                else
                                    place.Type = SearchItemTypes.DanceSchool;

                                items.Add(place);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }
                        if (filter.LocationActive)
                            items.Sort((x, y) => x.Distance.CompareTo(y.Distance));
                    }
                }
            }

            return items;
        }

        public List<SearchItemEventModel> EventsSearchByBox(SessionAPIModel user, SearchFilterModel filter)
        {
            var sql = "select e.code,e.name,e.Description,e.datestart" +
                ",p.Code,p.Name,c.name,e.address,e.Latitude,e.Longitude,p.photocodehome,p.photocodeicon,e.HavePoster,p.score,e.Duration" +
                // " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate" +
                " from events as e inner join Places as p on e.placecode = p.code left join diccities as c on p.AddCityCode = c.code ";


            sql += " where e.deleted = false and e.status = 200002 and e.datestart > CURRENT_TIMESTAMP ";

            if (filter.LocationActive)
                sql += " and " + string.Format(System.Globalization.CultureInfo.InvariantCulture, " e.latitude > {0} and e.latitude < {1} and e.longitude > {2} and e.longitude < {3}", filter.Latitude - 1, filter.Latitude + 1, filter.Longitude - 1, filter.Longitude + 1);


            sql += " order by e.datestart limit 400";

            var items = new List<SearchItemEventModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {
                                var havePoster = reader.GetBoolean(12);
                                var e = new SearchItemEventModel()
                                {
                                    Code = reader.GetValue(0).ToString(),
                                    Name = reader.GetValue(1).ToString(),
                                    Description = reader.GetValue(2).ToString(),
                                    DateStart = reader.GetDateTime(3),

                                    PlaceCode = reader.GetValue(4).ToString(),
                                    PlaceName = reader.GetValue(5).ToString(),
                                    AddressCity = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                    AddressStreet = reader.GetValue(7).ToString(),
                                    Latitude = reader.GetDouble(8),
                                    Longitude = reader.GetDouble(9),
                                    PlacePhotoCodeHome = reader.GetValue(10).ToString(),
                                    PlacePhotoCodeIcon = reader.GetValue(11).ToString(),
                                    HavePoster = reader.GetBoolean(12),
                                    PlaceScore = reader.GetInt32(13),
                                    Duration = reader.GetInt32(14),
                                    PlaceCategoriesList = reader.IsDBNull(15) || reader.GetValue(15).ToString() == "" ? new HashSet<int>() : new HashSet<int>(reader.GetValue(15).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),

                                };

                                e.Type = SearchItemTypes.Event;

                                items.Add(e);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }
                        if (filter.LocationActive)
                            items.Sort((x, y) => x.Distance.CompareTo(y.Distance));
                    }
                }
            }

            return items;
        }
        public List<SearchItemEventModel> EventsSearchByPlace(SessionAPIModel user, string placeCode)
        {
            var sql = "select e.code,e.name,e.Description,e.datestart" +
                ",p.Code,p.Name,c.name,e.address,p.Latitude,p.Longitude,p.photocodehome,p.photocodeicon,e.HavePoster,e.Duration" +
                // " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate" +
                " from events as e inner join Places as p on e.placecode = p.code left join diccities as c on p.AddCityCode = c.code ";


            sql += " where e.deleted = false and e.status = 200002 and e.datestart > CURRENT_TIMESTAMP and e.placecode = '" + placeCode + "'";


            sql += " order by e.datestart limit 400";

            var items = new List<SearchItemEventModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {
                                var havePoster = reader.GetBoolean(12);
                                var e = new SearchItemEventModel()
                                {
                                    Code = reader.GetValue(0).ToString(),
                                    Name = reader.GetValue(1).ToString(),
                                    Description = reader.GetValue(2).ToString(),
                                    DateStart = reader.GetDateTime(3),

                                    PlaceCode = reader.GetValue(4).ToString(),
                                    PlaceName = reader.GetValue(5).ToString(),
                                    AddressCity = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                    AddressStreet = reader.GetValue(7).ToString(),
                                    Latitude = reader.GetDouble(8),
                                    Longitude = reader.GetDouble(9),
                                    PlacePhotoCodeHome = reader.GetValue(10).ToString(),
                                    PlacePhotoCodeIcon = reader.GetValue(11).ToString(),
                                    HavePoster = reader.GetBoolean(12),
                                    Duration = reader.GetInt32(13),
                                    PlaceCategoriesList = reader.IsDBNull(14) || reader.GetValue(14).ToString() == "" ? new HashSet<int>() : new HashSet<int>(reader.GetValue(14).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),

                                };

                                e.Type = SearchItemTypes.Event;

                                items.Add(e);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }

                    }
                }
            }

            return items;
        }

        public List<SearchItemCompetitionModel> CompetitionsSearch(SessionAPIModel user, SearchFilterModel filter)
        {
            var sql = "select c.Code,c.Name,Description,Modalities,DateStart,DateEnd,AddProvinceCode,pro.Name,AddressName,OficialSite,HavePoster,c.UpdateDate " +
              " from Competitions c " +
              " left join DICProvinces pro on c.AddProvinceCode = pro.code " +
              " where c.Status = 190001 and c.Dateend > @DateEnd";

            sql += " order by c.datestart, c.Name limit 10";
            var items = new List<SearchItemCompetitionModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("DateEnd", DateTime.Now.AddDays(1)));
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new SearchItemCompetitionModel()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                Modalities = reader.GetValue(3).ToString(),
                                DateStart = reader.GetDateTime(4),
                                DateEnd = reader.GetDateTime(5),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddProvince = reader.GetValue(7).ToString(),
                                AddressStreet = reader.GetValue(8).ToString(),
                                OficialSite = reader.GetValue(9).ToString(),
                                HavePoster = reader.GetBoolean(10)
                            });
                        }
                    }
                }
            }
            return items;
        }


        #region Base
        private static SearchDA _ManagerSingleton;
        public static SearchDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new SearchDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}