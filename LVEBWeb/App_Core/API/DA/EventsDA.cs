﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;

namespace LVEBWeb.App_Core.API.DA
{
    public class EventsDA : App_Core.Admin.DA.BaseDA
    {

        public List<EvenetBE> EventsList(SessionAPIModel user, string province, long dateLastUpdate)
        {
            var sql = "select a.Code,a.Name,a.PlaceCode,p.name,a.ManagerCode,'' nickname,DateStart,Duration " +
            " ,p.latitude,p.longitude,p.addstreet,p.addstreetnumber,a.Description" +
            ",(SELECT name FROM diccities where code = p.addcitycode) as city,a.updatedate,a.deleted,p.addcitycode,c.name" +
            " from Events as a" +
            " left join diccities c on a.addcitycode = c.code " +
            " left join Places p on a.PlaceCode = p.code ";
            //" left join Socios s on a.ManagerCode = s.code ";

            sql += " where DateStart >= CURRENT_TIMESTAMP and a.updatedate > " + dateLastUpdate + " order by a.Name limit 2000";
            var items = new List<EvenetBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var adds = reader.GetValue(10).ToString();
                            var addsn = reader.GetValue(11).ToString();
                            if (addsn.Length > 0)
                                adds += "," + addsn;
                            adds += "," + reader.GetValue(13).ToString();

                            items.Add(new EvenetBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                PlaceCode = reader.GetValue(2).ToString(),
                                PlaceName = reader.GetValue(3).ToString(),
                                ManagerCode = reader.GetValue(4).ToString(),
                                ManagerName = reader.GetValue(5).ToString(),
                                DateStart = reader.GetDateTime(6),
                                Duration = reader.GetInt32(7),
                                Latitude = reader.IsDBNull(8) ? 0 : reader.GetDouble(8),
                                Longitude = reader.IsDBNull(9) ? 0 : reader.GetDouble(9),
                                Description = reader.GetValue(12).ToString(),
                                UpdateDate = reader.GetInt64(14),
                                Deleted = reader.GetBoolean(15),
                                AddCityCode = reader.GetValue(16).ToString(),
                                AddCityName = reader.GetValue(17).ToString(),
                                AddStreet = adds
                            });
                        }
                    }
                }
            }
            return items;
        }


        public bool EventToCheckInsert(SessionAPIModel user, EventToCheckBE value)
        {
            var sql = "insert into EventsToCheck (PlaceCode,Name,AddCityCode,Address,Latitude,Longitude,Description,DateStart,Duration,UpdateDate,Code,Reporter_Name,reporter_email,reporter_phone,reporter_relation,ValidatorCode) " +
                    "values (@PlaceCode,@Name,@AddCityCode,@Address,@Latitude,@Longitude,@Description,@DateStart,@Duration,@UpdateDate,@Code,@Reporter_Name,@reporter_email,@reporter_phone,@reporter_relation,@ValidatorCode) ";
            value.UpdateDate = App_Core.Common.Functions.DateUpdate();

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    //cmd.Parameters.Add(new NpgsqlParameter("@ManagerCode", value.ManagerCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));

                    cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", value.AddCityCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Address", value.AddStreet));
                    cmd.Parameters.Add(new NpgsqlParameter("@Latitude", value.Latitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@Longitude", value.Longitude));

                    cmd.Parameters.Add(new NpgsqlParameter("@Description", value.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("@DateStart", value.DateStart));
                    cmd.Parameters.Add(new NpgsqlParameter("@Duration", value.Duration));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.Parameters.Add(new NpgsqlParameter("@Reporter_Name", value.ReporterName));
                    cmd.Parameters.Add(new NpgsqlParameter("@reporter_email", value.ReporterEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@reporter_phone", value.ReporterPhone));
                    cmd.Parameters.Add(new NpgsqlParameter("@reporter_relation", value.ReporterRelation));
                    cmd.Parameters.Add(new NpgsqlParameter("@ValidatorCode", value.ValidatorCode));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        #region Base
        private static EventsDA _ManagerSingleton;
        public static EventsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EventsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}