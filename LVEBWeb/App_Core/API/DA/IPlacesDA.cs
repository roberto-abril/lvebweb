﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.SF;
using LVEBWeb.App_Core.API.Models;

namespace LVEBWeb.App_Core.API.DA
{
    public interface IPlacesDA
    {
        PlaceDetailsModel PlaceGet(SessionAPIModel user, string code);
        bool PlacesToImprove_Insert(SessionAPIModel user, PlaceToImproveModel model);
    }
}
