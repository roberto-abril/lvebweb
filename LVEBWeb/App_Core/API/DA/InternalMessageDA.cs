﻿using LVEBWeb.App_Core.API.SF;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.App_Core.API.BE;
using LVEBWeb.App_Core.Common;
using Npgsql;
namespace LVEBWeb.App_Core.API.DA
{
    public class InternalMessageDA : App_Core.Admin.DA.BaseDA
    {
        public bool InternalMessageInsert(SessionAPIModel user, InternalMessageBE value)
        {
            var sql = "insert into internalmessages (messageid,contactemail,contactphone,messagetype,messagebody) " +
                    "values (@messageid,@contactemail,@contactphone,@messagetype,@messagebody)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                try
                {
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@messageid", value.MessageId));
                        cmd.Parameters.Add(new NpgsqlParameter("@contactemail", value.ContactEmail));
                        cmd.Parameters.Add(new NpgsqlParameter("@contactphone", value.ContactPhone));
                        cmd.Parameters.Add(new NpgsqlParameter("@messagetype", value.MessageType));
                        cmd.Parameters.Add(new NpgsqlParameter("@messagebody", value.MessageBody));
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return true;
        }


        #region Base
        private static InternalMessageDA _ManagerSingleton;
        public static InternalMessageDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new InternalMessageDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}