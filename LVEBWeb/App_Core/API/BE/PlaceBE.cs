﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.BE
{
    public class PlaceBE : BaseAddBE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int Status { get; set; }
        [JsonIgnore]
        public HashSet<int> CategoriesList { get; set; }
        public string Categories
        {
            get
            {
                if (CategoriesList == null) return "";
                return string.Join(",", CategoriesList);
            }
        }
        [JsonIgnore]
        public List<int> TagsList { get; set; }
        public string Tags
        {
            get
            {
                if (TagsList == null) return "";
                return string.Join(",", TagsList);
            }
        }
        public long UpdateDate { get; set; }
        public bool Deleted { get; set; }
        public string PhotoCodeHome { get; set; }
        public string PhotoCodeIcon { get; set; }
    }
}
