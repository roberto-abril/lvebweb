﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.BE
{
    public class EventToCheckBE : EvenetBE
    {
        public string ValidatorCode { get; set; }
        public string ReporterName { get; set; }
        public string ReporterPhone { get; set; }
        public string ReporterEmail { get; set; }
        public string ReporterRelation { get; set; }
    }
}
