﻿using System;
namespace LVEBWeb.App_Core.API.BE
{
    public enum PlaceCategories : int
    {
        None = 0,
        School = 110001,
        DanceHall = 110002,
        Store = 110005
    }
}
