﻿using System;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.BE
{
    public class BaseBE
    {
        //public string UpdateUser;
        // public DateTime UpdateDate;

    }
    public class BaseAddBE : BaseBE
    {

        double _Latitude;
        public double Latitude
        {
            get { return Math.Round(_Latitude, 6); }
            set { _Latitude = value; }
        }
        double _Longitude;
        public double Longitude
        {
            get { return Math.Round(_Longitude, 6); }
            set { _Longitude = value; }
        }

        public string AddCityCode { get; set; }
        public string AddCityName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddProvinceCode { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddProvinceName { get; set; }
        public string AddStreet { get; set; }
        public string AddStreetNumber { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddCP { get; set; }
    }
}
