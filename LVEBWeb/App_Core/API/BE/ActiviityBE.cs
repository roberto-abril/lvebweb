﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.BE
{
    public class EvenetBE
    {
        public string Code { get; set; }
        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string ManagerCode { get; set; }
        public string ManagerName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateStart { get; set; }
        public int Duration { get; set; }

        public string AddCityCode { get; set; }
        public string AddCityName { get; set; }
        public string AddStreet { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public long UpdateDate { get; set; }
        public bool Deleted { get; set; }
    }
}
