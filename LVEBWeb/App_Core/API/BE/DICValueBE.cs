﻿using System;
namespace LVEBWeb.App_Core.API.BE
{
    public class DICValueBE : BaseBE
    {
        public string Code;
        public string Name;
        public long UpdateDate { get; set; }
    }
}
