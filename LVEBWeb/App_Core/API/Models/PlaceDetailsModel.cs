﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.Models
{
    public class PlaceDetailsModel
    {
        public PlaceDetailsModel()
        {
            CategoriesListString = new List<string>();
            TagsBailesList = new List<string>();
            TagsProfilesList = new List<string>();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }

        public string ContactWeb { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public int Status { get; set; }

        public string AddressCityName { get; set; }
        public string AddressProvinceName { get; set; }
        public string AddressStreet { get; set; }
        public string AddressStreetNumber { get; set; }

        public double Distance { get; set; }
        double _Latitude;
        public double Latitude
        {
            get { return Math.Round(_Latitude, 6); }
            set { _Latitude = value; }
        }
        double _Longitude;
        public double Longitude
        {
            get { return Math.Round(_Longitude, 6); }
            set { _Longitude = value; }
        }
        public string LatitudeString
        {
            get
            {
                return Latitude.ToString(Common.Functions.NumberFormatDot);
            }
        }
        public string LongitudeString
        {
            get
            {
                return Longitude.ToString(Common.Functions.NumberFormatDot);
            }
        }

        //[JsonIgnore]
        public HashSet<int> CategoriesList { get; set; }

        public List<string> CategoriesListString { get; set; }

        //[JsonIgnore]
        public List<int> TagsList { get; set; }
        public List<string> TagsBailesList { get; set; }
        public List<string> TagsProfilesList { get; set; }

        public string PhotoCodeHome { get; set; }
        public string PhotoCodeIcon { get; set; }

        public string OpenHours { get; set; }

        public bool IsDanceHall { get { return CategoriesList == null ? false : CategoriesList.Contains((int)Common.DICTableCategorias.SalaBaile); } }
        public bool IsSchool { get { return CategoriesList == null ? false : CategoriesList.Contains((int)Common.DICTableCategorias.EscuelaBaile); } }
        public bool IsStore { get { return CategoriesList == null ? false : CategoriesList.Contains((int)Common.DICTableCategorias.SinDefinir); } }


        public List<SearchItemEventModel> Events;

    }
}
