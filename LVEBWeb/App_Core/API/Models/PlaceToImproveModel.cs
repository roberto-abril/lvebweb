﻿using System;
namespace LVEBWeb.App_Core.API.Models
{
    public class PlaceToImproveModel
    {
        public string Type { get; set; }
        public string PlaceCode { get; set; }
        public string UserCode { get; set; }
        public string Notes { get; set; }
    }
}
