﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.BE;
namespace LVEBWeb.App_Core.API.Models
{
    public class CommonDICModel
    {
        public List<DICValueBE> LocalCategorias;
        public List<DICValueBE> LocalTags;
    }
}
