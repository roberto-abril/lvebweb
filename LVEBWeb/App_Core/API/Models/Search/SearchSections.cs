﻿using System;
namespace LVEBWeb.App_Core.API.Models
{
    public enum SearchSections
    {
        Home = 0,
        DanceSchool = 1,
        DanceHall = 2,
        Events = 3,
        Competitions = 4,
        Store = 5
    }
}
