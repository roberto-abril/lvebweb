﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.Models
{
    public class SearchItemEventModel : SearchItemModel
    {
        public SearchItemEventModel()
        {
            PlaceCategoriesListString = new List<string>();
        }

        public string CategoryName { get; set; }
        public DateTime DateStart { get; set; }
        public bool HavePoster { get; set; }

        public int Duration { get; set; }

        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public int PlaceScore { get; set; }
        public string PlaceCategories
        {
            get
            {
                if (PlaceCategoriesListString == null)
                    return "";
                return string.Join(' ', PlaceCategoriesListString);
            }
        }
        public HashSet<int> PlaceCategoriesList { get; set; }
        public List<string> PlaceCategoriesListString { get; set; }

        public string EventPhotoHome { get; set; }
        public string PlacePhotoCodeHome { get; set; }
        public string PlacePhotoCodeIcon { get; set; }

        public bool PlaceIsDanceHall { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.SalaBaile); } }
        public bool PlaceIsSchool { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.EscuelaBaile); } }
        public bool PlaceIsStore { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.SinDefinir); } }

    }
}
