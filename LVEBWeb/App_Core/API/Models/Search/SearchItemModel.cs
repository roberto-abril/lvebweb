﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.Models
{
    public class SearchItemModel
    {
        public SearchItemTypes Type;
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int Score { get; set; }


        public string AddressCity { get; set; }
        public string AddressStreet { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Distance { get; set; }
        public bool LocationActive
        {
            get { return Latitude > 0 || Longitude > 0; }
        }
    }
}
