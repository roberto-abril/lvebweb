﻿using System;
namespace LVEBWeb.App_Core.API.Models
{
    public enum SearchItemTypes
    {
        None = 0,
        DanceHall = 1,
        DanceSchool = 2,
        Event = 3,
        Competition = 4,
        Promotion = 5
    }
}
