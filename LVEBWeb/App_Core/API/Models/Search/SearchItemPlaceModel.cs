﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.Models
{
    public class SearchItemPlaceModel : SearchItemModel
    {
        public SearchItemPlaceModel()
        {
            CategoriesListString = new HashSet<string>();
            TagsBailesList = new List<string>();
            TagsProfilesList = new List<string>();
        }

        public string DescriptionFull { get; set; }
        public string Categories
        {
            get
            {
                if (CategoriesListString == null)
                    return "";
                return string.Join(' ', CategoriesListString);
            }
        }
        public HashSet<int> CategoriesList { get; set; }
        public HashSet<string> CategoriesListString { get; set; }
        [JsonIgnore]
        public string Tags { get; set; }
        public List<int> TagsList { get; set; }
        public List<string> TagsBailesList { get; set; }
        public List<string> TagsProfilesList { get; set; }



        public string PhotoCodeHome { get; set; }
        public string PhotoCodeIcon { get; set; }

        public bool IsDanceHall { get { return CategoriesList == null ? false : CategoriesList.Contains((int)Common.DICTableCategorias.SalaBaile); } }
        public bool IsSchool { get { return CategoriesList == null ? false : CategoriesList.Contains((int)Common.DICTableCategorias.EscuelaBaile); } }
        public bool IsStore { get { return CategoriesList == null ? false : CategoriesList.Contains((int)Common.DICTableCategorias.SinDefinir); } }

    }
}
