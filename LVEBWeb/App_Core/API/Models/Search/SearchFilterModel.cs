﻿using System;
namespace LVEBWeb.App_Core.API.Models
{
    public class SearchFilterModel
    {
        public string Code { get; set; }

        public SearchSections Section { get; set; }

        public string Query { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string CityCode { get; set; }

        public bool LocationActive
        {
            get { return Latitude > 0 || Longitude > 0; }
        }


    }
}
