﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.API.Models
{
    public class SearchItemCompetitionModel : SearchItemModel
    {
        public SearchItemCompetitionModel()
        {
            Type = SearchItemTypes.Competition;
        }
        public string Modalities { get; set; }
        public string OficialSite { get; set; }
        public string OficialSiteUrl { get; set; }
        public bool HavePoster { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public string AddProvinceCode { get; set; }
        public string AddProvince { get; set; }
        public string AddressLink { get; set; }
    }
}
