﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.API.BE;
namespace LVEBWeb.App_Core.API.Models
{
    public class CommonLocationModel
    {
        public List<DICValueBE> Provinces;
        public List<DICValueBE> Cities;
    }
}
