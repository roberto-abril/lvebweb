﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.Model;

namespace LVEBWeb.App_Core.Main.DA
{
    public interface IUserRegisterDA
    {
        bool EmailExist(SessionUserModel user, string email);

        string UserCodeByEmailAndPassword(SessionUserModel user, string email, string password);
        string UserCodeByConnectorId(SessionUserModel user, string token);
        // bool UserInsert(SessionUserModel user, UserBE value);
        UserModel UserGet(SessionUserModel user, string code);

        bool UserRegister(SessionUserModel user, UserRegisterModel value);


        bool UserValidateEmailInsert(SessionUserModel user, string userCode, string userEmail, string token);
        string UserValidateEmailCheck(SessionUserModel user, string userEmail, string token);
        bool UserValidateEmailValidated(SessionUserModel user, string userCode);
    }
}
