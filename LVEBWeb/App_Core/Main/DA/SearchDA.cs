﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
//using Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Util;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
//using IndexWriter = Lucene.Net.Index.IndexWriter;
using Version = Lucene.Net.Util.Version;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using Npgsql;
using Newtonsoft.Json;



namespace LVEBWeb.App_Core.Main.DA
{
    public class SearchDA : BaseDA, ISearchDA
    {
        public List<SearchAutocompleteModel> AutocompleteSearch(SessionUserModel user, SearchSections page, string value)
        {
            //try
            // {

            if (page == SearchSections.DanceHall)
                value = "+110002 " + value;
            if (page == SearchSections.DanceSchool)
                value = "+110001 " + value;
            if (page == SearchSections.Events)
                value = "+event " + value;


            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            var topDocs = searcher.Search(query, 10);

            int results = topDocs.ScoreDocs.Length;
            // Console.WriteLine("Found {0} results", results);

            var items = new List<SearchAutocompleteModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                items.Add(new SearchAutocompleteModel()
                {
                    Code = doc.Get("Id"),
                    Label = doc.Get("Name"),
                    Value1 = doc.Get("City")
                });

                Console.WriteLine($"Name: {doc.Get("Name")}, AddStreet: {doc.Get("AddStreet")}, City: {doc.Get("City")}");
            }

            return items;
            /*  }
               catch (Exception ex)
               {
                   return new List<SearchAutocompleteModel>();
               }*/
        }

        public List<SearchItemPlaceModel> PlacesFullSearch(SessionUserModel user, SearchSections page, string value)
        {
            //try
            // {

            if (page == SearchSections.DanceHall)
                value = "+110002 " + value;
            if (page == SearchSections.DanceSchool)
                value = "+110001 " + value;



            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            var topDocs = searcher.Search(query, 30);

            int results = topDocs.ScoreDocs.Length;
            // Console.WriteLine("Found {0} results", results);

            var items = new List<SearchItemPlaceModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                var json = doc.Get("json");
                var place = JsonConvert.DeserializeObject<PlaceBE>(json);


                var item = new SearchItemPlaceModel()
                {
                    Code = doc.Get("Id"),
                    Name = place.Name,
                    City = place.AddCityName,
                    Address = place.AddStreet,
                    Description = place.Description,
                    Latitude = place.Latitude,
                    Longitude = place.Longitude,
                    PhotoCodeHome = place.PhotoCodeHome == null ? "" : place.PhotoCodeHome,
                    PhotoCodeIcon = place.PhotoCodeIcon == null ? "" : place.PhotoCodeIcon,
                    TagsList = place.TagsList == null ? new List<int>() : place.TagsList,
                    CategoriesList = place.CategoriesList == null ? new List<int>() : place.CategoriesList.ToList()

                };

                if (page == SearchSections.DanceHall)
                    item.Type = SearchItemTypes.DanceHall;
                else
                    item.Type = SearchItemTypes.DanceSchool;

                items.Add(item);
            }

            return items;
            /* }
              catch (Exception ex)
              {
                  return new List<PlaceModel>();
              }*/
        }

        public List<SearchItemEventModel> EventsFullSearch(SessionUserModel user, SearchSections page, string value)
        {
            //try
            // {

            value = "+event " + value;



            value = Functions.StringRemoveSigns(value);
            var directory = FSDirectory.Open(new DirectoryInfo(Configuration.PathLucenIndex));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "All" }, analyzer);
            var query = parser.Parse(value);
            var searcher = new IndexSearcher(directory, true);
            var topDocs = searcher.Search(query, 30);

            int results = topDocs.ScoreDocs.Length;
            // Console.WriteLine("Found {0} results", results);

            var items = new List<SearchItemEventModel>();
            for (int i = 0; i < results; i++)
            {
                var scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                var json = doc.Get("json");
                var eventModel = JsonConvert.DeserializeObject<EventBE>(json);


                var item = new SearchItemEventModel()
                {
                    Type = SearchItemTypes.Event,
                    Code = doc.Get("Id"),
                    Name = eventModel.Name,
                    PlaceCode = eventModel.PlaceCode,
                    PlaceName = eventModel.PlaceName,
                    City = eventModel.AddCityName,
                    Address = eventModel.AddStreet,
                    Description = eventModel.Description,
                    Latitude = eventModel.Latitude,
                    Longitude = eventModel.Longitude,
                    PlacePhotoCodeHome = eventModel.PlacePhotoCodeHome,
                    PlacePhotoCodeIcon = eventModel.PlacePhotoCodeIcon,
                    Category = eventModel.Category,
                    CategoryName = eventModel.CategoryName,
                    DateStart = eventModel.DateStart

                };



                items.Add(item);
            }

            return items;
            /* }
              catch (Exception ex)
              {
                  return new List<PlaceModel>();
              }*/
        }



        public List<SearchItemPlaceModel> PlacesSearchByBox(SessionUserModel user, SearchFilterModel filter)
        {
            //var dateLastUpdate = 0;
            var sql = "select p.Code,p.Name,AddCityCode,c.name,AddStreet,AddStreetNumber,Description,Latitude,Longitude,photocodehome,photocodeicon" +
                " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate" +
                " from Places as p left join diccities as c on p.AddCityCode = c.code ";


            sql += " where p.deleted = false and p.status = 180001 and " + string.Format(System.Globalization.CultureInfo.InvariantCulture, " latitude > {0} and latitude < {1} and longitude > {2} and longitude < {3}", filter.CenterLatitude - 1, filter.CenterLatitude + 1, filter.CenterLongitude - 1, filter.CenterLongitude + 1);

            if (filter.Page == SearchSections.Stores)
                sql += " and p.IsStore = true ";
            if (filter.Page == SearchSections.DanceHall)
                sql += " and p.IsDanceHall = true ";
            if (filter.Page == SearchSections.DanceSchool)
                sql += " and p.IsDanceSchool = true ";


            sql += " order by p.Name limit 400";

            var items = new List<SearchItemPlaceModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {

                                var place = new SearchItemPlaceModel()
                                {
                                    Code = reader.GetValue(0).ToString(),
                                    Name = reader.GetValue(1).ToString(),
                                    City = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                    Address = reader.GetValue(4).ToString() + ", " + reader.GetValue(5).ToString(),
                                    Description = reader.GetValue(6).ToString(),
                                    Latitude = reader.GetDouble(7),
                                    Longitude = reader.GetDouble(8),
                                    PhotoCodeHome = reader.GetValue(9).ToString(),
                                    PhotoCodeIcon = reader.GetValue(10).ToString(),
                                    TagsList = reader.IsDBNull(11) || reader.GetValue(11).ToString() == "" ? new List<int>() : reader.GetValue(11).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                    CategoriesList = reader.IsDBNull(12) || reader.GetValue(12).ToString() == "" ? new List<int>() : new List<int>(reader.GetValue(12).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),

                                };
                                if (filter.Page == SearchSections.DanceHall)
                                    place.Type = SearchItemTypes.DanceHall;
                                else
                                    place.Type = SearchItemTypes.DanceSchool;

                                items.Add(place);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }
                        if (user.Location.LatLonActive)
                            items.Sort((x, y) => x.Distance.CompareTo(y.Distance));
                    }
                }
            }

            return items;
        }



        public List<SearchItemEventModel> EventsSearchByBox(SessionUserModel user, SearchFilterModel filter)
        {
            var sql = "select e.code,e.name,e.Description,e.datestart" +
                ",p.Code,p.Name,c.name,e.address,e.Latitude,e.Longitude,p.photocodehome,p.photocodeicon,e.HavePoster" +
                // " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate" +
                " from events as e inner join Places as p on e.placecode = p.code left join diccities as c on p.AddCityCode = c.code ";


            sql += " where e.deleted = false and e.status = 200002 and e.datestart > CURRENT_TIMESTAMP ";

            if (filter.FilterByBox)
                sql += " and " + string.Format(System.Globalization.CultureInfo.InvariantCulture, " e.latitude > {0} and e.latitude < {1} and e.longitude > {2} and e.longitude < {3}", filter.CenterLatitude - 1, filter.CenterLatitude + 1, filter.CenterLongitude - 1, filter.CenterLongitude + 1);


            sql += " order by e.datestart limit 400";

            var items = new List<SearchItemEventModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {

                                var e = new SearchItemEventModel()
                                {
                                    Code = reader.GetValue(0).ToString(),
                                    Name = reader.GetValue(1).ToString(),
                                    Description = reader.GetValue(2).ToString(),
                                    DateStart = reader.GetDateTime(3),

                                    PlaceCode = reader.GetValue(4).ToString(),
                                    PlaceName = reader.GetValue(5).ToString(),
                                    City = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                    Address = reader.GetValue(7).ToString(),
                                    Latitude = reader.GetDouble(8),
                                    Longitude = reader.GetDouble(9),
                                    PlacePhotoCodeHome = reader.GetValue(10).ToString(),
                                    PlacePhotoCodeIcon = reader.GetValue(11).ToString(),
                                    HavePoster = reader.GetBoolean(12),
                                    //   TagsList = reader.IsDBNull(11) || reader.GetValue(11).ToString() == "" ? new List<int>() : reader.GetValue(11).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                    PlaceCategoriesList = reader.IsDBNull(13) || reader.GetValue(13).ToString() == "" ? new List<int>() : new List<int>(reader.GetValue(13).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),

                                };

                                e.Type = SearchItemTypes.Event;

                                items.Add(e);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }
                        if (user.Location.LatLonActive)
                            items.Sort((x, y) => x.Distance.CompareTo(y.Distance));
                    }
                }
            }

            return items;
        }


        #region Base
        private static SearchDA _ManagerSingleton;
        public static SearchDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new SearchDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}