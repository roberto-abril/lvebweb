﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LVEBWeb.Models;
using Npgsql;
using Newtonsoft.Json;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
namespace LVEBWeb.App_Core.Main.DA
{
    public class CommonDA : BaseDA, ICommonDA
    {
        public Dictionary<int, string> ValuesList(DICTables tableCode)
        {
            var sql = "select Code,Name from DICTableValues where TableCode = " + (int)tableCode + " order by Name limit 2000";
            var items = new Dictionary<int, string>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(reader.GetInt32(0), reader.GetValue(1).ToString());
                        }
                    }
                }
            }
            return items;
        }

        public List<dynamic[]> PlaceNameList()
        {
            var sql = "select Code,Name,updatedate from public.places where status = 180001 order by updatedate desc limit 2000";
            var items = new List<dynamic[]>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(new dynamic[]{
                             reader.GetValue(0) + "/" + reader.GetValue(1),
                             reader.GetInt64(2)
                            });
                        }
                    }
                }
            }
            return items;
        }



        #region Base
        private static CommonDA _ManagerSingleton;
        public static CommonDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CommonDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}