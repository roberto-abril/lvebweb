﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using Npgsql;


namespace LVEBWeb.App_Core.Main.DA
{
    public class PlacesDA : BaseDA, IPlacesDA
    {



        public PlaceBE PlaceGet(SessionUserModel user, string code)
        {
            var sql = "select p.Code,p.Name,AddCityCode,c.name,AddStreet,AddStreetNumber,Description,DescriptionFull,Latitude,Longitude,HaveLogo" +
                " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate,p.deleted" +
                " ,photocodehome,photocodeicon,isdancehall,isdanceschool,isstore" +
                ",OpenHours ,contactweb, contactphone, contactemail" +
                " from Places as p left join diccities as c on p.AddCityCode = c.code " +
                " where p.Code = @Code ";


            var place = new PlaceBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            place = new PlaceBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                AddCityCode = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                AddCityName = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                AddStreet = reader.GetValue(4).ToString(),
                                AddStreetNumber = reader.GetValue(5).ToString(),
                                Description = reader.GetValue(6).ToString(),
                                DescriptionFull = reader.GetValue(7).ToString(),
                                Latitude = reader.GetDouble(8),
                                Longitude = reader.GetDouble(9),
                                // HaveLogo = reader.GetBoolean(9),
                                TagsList = reader.IsDBNull(11) || reader.GetValue(11).ToString() == "" ? new List<int>() : reader.GetValue(11).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                CategoriesList = reader.IsDBNull(12) || reader.GetValue(12).ToString() == "" ? new HashSet<int>() : new HashSet<int>(reader.GetValue(12).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),
                                UpdateDate = reader.GetInt64(13),
                                Deleted = reader.GetBoolean(14),
                                PhotoCodeHome = reader.GetValue(15).ToString(),
                                PhotoCodeIcon = reader.GetValue(16).ToString(),
                                IsDanceHall = reader.GetBoolean(17),
                                IsSchool = reader.GetBoolean(18),
                                IsStore = reader.GetBoolean(19),
                                OpenHours = reader.GetValue(20).ToString(),
                                ContactWeb = reader.GetValue(21).ToString(),
                                ContactPhone = reader.GetValue(22).ToString(),
                                ContactEmail = reader.GetValue(23).ToString()

                            };
                        }
                        else
                            return place;
                    }
                }
            }


            return place;
        }



        #region Base
        private static PlacesDA _ManagerSingleton;
        public static PlacesDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PlacesDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}