﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
//using Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Util;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
//using IndexWriter = Lucene.Net.Index.IndexWriter;
using Version = Lucene.Net.Util.Version;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using Npgsql;
using Newtonsoft.Json;



namespace LVEBWeb.App_Core.Main.DA
{
    public class UserDA : BaseDA, IUserDA
    {

        public UserModel UserGet(SessionUserModel user, string code)
        {
            var sql = "select code, name, Nickname, BirthDate, addcitycode, addprovincecode" +
                ",UserEmail, UserPhone from Users as c" +
                " where c.Code = @Code ";
            var userModel = new Model.UserModel();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            userModel = new Model.UserModel()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Nickname = reader.GetValue(2).ToString(),
                                BirthDate = reader.GetDateTime(3),
                                AddCityCode = reader.GetValue(4).ToString(),
                                AddProvinceCode = reader.GetValue(5).ToString(),
                                Email = reader.GetValue(6).ToString(),
                                Phone = reader.GetValue(7).ToString()
                            };
                        }
                        else
                            return userModel;
                    }
                }
            }



            return userModel;
        }


        #region Base
        private static UserDA _ManagerSingleton;
        public static UserDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new UserDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}