﻿
using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Npgsql;

namespace LVEBWeb.App_Core.Main.DA
{
    public class BaseDA
    {


        protected bool DataLastUpdateSave(DataTypes type, long dateUpdate)
        {
            var sql = "Update DataLastUpdate set DateLastUpdate =  " + dateUpdate +
                    " where Type = '" + type.ToString() + "' ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}