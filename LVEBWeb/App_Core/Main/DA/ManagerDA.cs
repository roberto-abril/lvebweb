﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using Npgsql;


namespace LVEBWeb.App_Core.Main.DA
{
    public class ManagerDA : BaseDA, IManagerDA
    {

        public List<ManagerPlaceModel> PlaceSearch(SessionUserModel user)
        {
            var sql = @"select p.code, p.name placename, c.name placecity, co.name
from places as p
inner join companiesplaces as cp on p.code = cp.placecode
inner join companies as co on co.code = cp.companycode
inner join companiesusers as cu on cp.companycode = cp.companycode
inner join users as u on cu.usercode = u.code
left join diccities c on p.addcitycode = c.code
where u.code =  @UserCode order by p.Name limit 100";

            var items = new List<ManagerPlaceModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UserCode", user.UserCode));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var place = new ManagerPlaceModel()
                            {
                                PlaceCode = reader.IsDBNull(0) ? "" : reader.GetValue(0).ToString(),
                                PlaceName = reader.IsDBNull(1) ? "" : reader.GetValue(1).ToString(),
                                PlaceCity = reader.IsDBNull(2) ? "" : reader.GetValue(2).ToString(),
                                CompanyName = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString()
                            };
                            items.Add(place);
                        }
                    }
                }
            }
            return items;
        }



        public List<ManagerEventModel> EventsSearch(SessionUserModel user, GridFilterModel gridModel)
        {
            var sql = @"select e.code, e.name,e.datestart,p.name ,e.category,e.status,haveposter
from events as e
inner join companiesplaces as cp on e.placecode = cp.placecode
inner join places as p on p.code = e.placecode
inner join companiesusers as cu on cp.companycode = cp.companycode
inner join users as u on cu.usercode = u.code
where u.code =  @UserCode order by e.datestart  limit 100";

            var items = new List<ManagerEventModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UserCode", user.UserCode));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var eventModel = new ManagerEventModel()
                            {
                                EventCode = reader.IsDBNull(0) ? "" : reader.GetValue(0).ToString(),
                                EventName = reader.IsDBNull(1) ? "" : reader.GetValue(1).ToString(),
                                DateStart = reader.IsDBNull(2) ? new DateTime() : reader.GetDateTime(2),
                                PlaceName = reader.IsDBNull(3) ? "" : reader.GetValue(3).ToString(),
                                CategoryCode = reader.IsDBNull(4) ? 250001 : reader.GetInt32(4),
                                StatusCode = reader.IsDBNull(5) ? 200001 : reader.GetInt32(5),
                                HavePoster = reader.IsDBNull(6) ? false : reader.GetBoolean(6)
                            };
                            if (eventModel.StatusCode == 200002)
                                eventModel.StatusName = "Activo";
                            else
                                eventModel.StatusName = "Borrador";
                            items.Add(eventModel);
                        }
                    }
                }
            }
            return items;
        }



        public bool EventInsert(SessionUserModel user, EventBE value)
        {
            var sql = "insert into Events (PlaceCode,ManagerCode,Name,AddCityCode,Address,Latitude,Longitude,Description,DateStart,Duration,UpdateDate,UpdateUser,Status,Code) " +
                    "values (@PlaceCode,@ManagerCode,@Name,@AddCityCode,@Address,@Latitude,@Longitude,@Description,@DateStart,@Duration,@UpdateDate,0,@Status,@Code)  ";
            value.UpdateDate = Common.Functions.DateUpdate();

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", value.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@ManagerCode", value.ManagerCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));

                    cmd.Parameters.Add(new NpgsqlParameter("@AddCityCode", value.AddCityCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Address", value.AddStreet));
                    cmd.Parameters.Add(new NpgsqlParameter("@Latitude", value.Latitude));
                    cmd.Parameters.Add(new NpgsqlParameter("@Longitude", value.Longitude));

                    cmd.Parameters.Add(new NpgsqlParameter("@Description", value.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("@DateStart", value.DateStart));
                    cmd.Parameters.Add(new NpgsqlParameter("@Duration", value.Duration));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    //cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", 0));
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", value.Status));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();
                }
            }
            this.DataLastUpdateSave(DataTypes.Events, value.UpdateDate);
            return true;
        }


        public bool EventUpdate(SessionUserModel user, EventBE value)
        {
            var sql = "update Events set Name=@Name,Description=@Description,DateStart=@DateStart,Duration=@Duration,UpdateDate=@UpdateDate,Status=@Status,haveposter=@haveposter where Code = @Code ";

            value.UpdateDate = Common.Functions.DateUpdate();

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.Name));
                    cmd.Parameters.Add(new NpgsqlParameter("@Description", value.Description));
                    cmd.Parameters.Add(new NpgsqlParameter("@DateStart", value.DateStart));
                    cmd.Parameters.Add(new NpgsqlParameter("@Duration", value.Duration));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", value.UpdateDate));
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", value.Status));
                    cmd.Parameters.Add(new NpgsqlParameter("@haveposter", value.HavePoster));

                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.Code));
                    cmd.ExecuteNonQuery();
                }
            }
            this.DataLastUpdateSave(DataTypes.Events, value.UpdateDate);
            return true;
        }

        public EventBE EventGet(SessionUserModel user, string code)
        {
            var sql = "select e.name,e.Description,e.DateStart,e.Duration,e.UpdateDate,e.Status,placecode, e.haveposter " +
                " from events as e" +
                " where e.Code = @Code ";
            var v = new EventBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {

                        if (reader.Read())
                        {
                            v = new EventBE()
                            {
                                Name = reader.GetValue(0).ToString(),
                                Description = reader.GetValue(1).ToString(),
                                DateStart = reader.GetDateTime(2),
                                Duration = reader.GetInt32(3),
                                UpdateDate = reader.GetInt64(4),
                                Status = reader.GetInt32(5),
                                PlaceCode = reader.GetString(6),
                                HavePoster = reader.GetBoolean(7),
                                Code = code

                            };
                        }
                        else
                            return v;
                    }
                }
            }

            return v;
        }


        public bool EventDelete(SessionUserModel user, string code)
        {
            var sql = "update events set Deleted = true,UpdateUser = @UpdateUser where code = @Code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        public bool EventChangeStatus(SessionUserModel user, string code, int status)
        {
            var sql = "update events set status = @Status,UpdateUser = @UpdateUser where code = @Code ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Status", status));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateUser", user.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }



        #region Base
        private static ManagerDA _ManagerSingleton;
        public static ManagerDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ManagerDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}