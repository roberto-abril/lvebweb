﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;

using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BE;

using Npgsql;


namespace LVEBWeb.App_Core.Main.DA
{
    public class CompetitionsDA : BaseDA, ICompetitionsDA
    {
        public List<CompetitionBE> CompetitionsSelect(SessionUserModel user, GridFilterModel gridModel)
        {
            var sql = "select c.Code,c.Name,Description,Modalities,DateStart,DateEnd,AddProvinceCode,pro.Name,AddressName,OficialSite,HavePoster,c.UpdateDate " +
              " from Competitions c " +
              " left join DICProvinces pro on c.AddProvinceCode = pro.code " +
              " where c.Status = 190001 and c.Dateend >= @DateEnd";

            sql += " order by c.datestart, c.Name limit 10";
            var items = new List<CompetitionBE>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                var day = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                //day = day.AddDays(1);
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("DateEnd", day));
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            items.Add(new CompetitionBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                Modalities = reader.GetValue(3).ToString(),
                                DateStart = reader.GetDateTime(4),
                                DateEnd = reader.GetDateTime(5),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddProvince = reader.GetValue(7).ToString(),
                                AddressName = reader.GetValue(8).ToString(),
                                OficialSite = reader.GetValue(9).ToString(),
                                HavePoster = reader.GetBoolean(10)
                                //UpdateDate = reader.GetInt64(11)
                            });
                        }
                    }
                }
            }
            return items;
        }


        public CompetitionBE CompetitionGet(SessionUserModel user, string code)
        {
            var sql = "select s.Code,s.Name,Description,Modalities,DateStart,DateEnd,AddProvinceCode,AddressName,AddressLink,OficialSite,OficialSiteUrl,Latitude,Longitud,HavePoster" +
                " from Competitions as s" +
                //" left join DICTableValues tv on s.LegalIDTypeCode = tv.code " +
                " where s.Code = @Code ";


            var Competition = new CompetitionBE();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Competition = new CompetitionBE()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                Modalities = reader.GetValue(3).ToString(),
                                DateStart = reader.GetDateTime(4),
                                DateEnd = reader.GetDateTime(5),
                                AddProvinceCode = reader.GetValue(6).ToString(),
                                AddressName = reader.GetValue(7).ToString(),
                                AddressLink = reader.GetValue(8).ToString(),
                                OficialSite = reader.GetValue(9).ToString(),
                                OficialSiteUrl = reader.GetValue(10).ToString(),
                                Latitude = reader.GetDouble(11),
                                Longitude = reader.GetDouble(12),
                                HavePoster = reader.GetBoolean(13)
                            };
                        }
                        else
                            return Competition;
                    }
                }
            }


            return Competition;
        }



        #region Base
        private static CompetitionsDA _ManagerSingleton;
        public static CompetitionsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompetitionsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}