﻿using LVEBWeb.App_Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using Npgsql;


namespace LVEBWeb.App_Core.Main.DA
{
    public class EventsDA : BaseDA, IEventsDA
    {
        public DetailsEventModel EventGet(SessionUserModel user, string code)
        {
            var sql = @"select e.Code,e.Name,e.description,e.datestart,e.duration,e.address
,e.AddCityCode,city.name,e.Latitude,e.Longitude,e.category,e.HavePoster
,p.photocodeicon,p.PhotoCodeHome,p.code,p.name
,array_to_string(array(select et.EventTagCode from EventsTags et where et.EventCode = e.code),',') 
from events as e
inner join places p on e.placecode = p.code
left join diccities as city on e.AddCityCode = city.code  
where e.status = 200002 and e.deleted = false and e.Code = @Code ";
            var model = new DetailsEventModel();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            model = new DetailsEventModel()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Description = reader.GetValue(2).ToString(),
                                DateStart = reader.GetDateTime(3),
                                Duration = reader.GetInt32(4),
                                Address = reader.GetValue(5).ToString(),
                                AddCityCode = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                AddCityName = reader.IsDBNull(7) ? "" : reader.GetValue(7).ToString(),
                                Latitude = reader.GetDouble(8),
                                Longitude = reader.GetDouble(9),
                                Category = reader.GetValue(10).ToString(),
                                HavePoster = reader.GetBoolean(11),
                                PlacePhotoCodeIcon = reader.GetValue(12).ToString(),
                                PlacePhotoCodeHome = reader.GetValue(13).ToString(),
                                PlaceCode = reader.GetValue(14).ToString(),
                                PlaceName = reader.GetValue(15).ToString()
                             
                            };
                        }
                        else
                            return model;
                    }
                }
            }


            return model;
        }



        public List<SearchItemEventModel> EventsByPlace(SessionUserModel user,string placeCode)
        {
            var sql = "select e.code,e.name,e.Description,e.datestart" +
                ",p.Code,p.Name,c.name,e.address,e.Latitude,e.Longitude,p.photocodehome,p.photocodeicon,e.HavePoster" +
                // " ,array_to_string(array(select pt.PlaceTagCode from PlacesTags pt where pt.PlaceCode = p.code),',')" +
                " ,array_to_string(array(select pc.PlaceCategoryCode from PlacesCategories pc where pc.PlaceCode = p.code),','),p.updatedate" +
                " from events as e inner join Places as p on e.placecode = p.code left join diccities as c on p.AddCityCode = c.code ";


            sql += " where p.code = @PlaceCode and e.deleted = false and e.status = 200002 and e.datestart > CURRENT_TIMESTAMP";


            sql += " order by e.datestart limit 400";

            var items = new List<SearchItemEventModel>();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@PlaceCode", placeCode));
                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            try
                            {

                                var e = new SearchItemEventModel()
                                {
                                    Code = reader.GetValue(0).ToString(),
                                    Name = reader.GetValue(1).ToString(),
                                    Description = reader.GetValue(2).ToString(),
                                    DateStart = reader.GetDateTime(3),

                                    PlaceCode = reader.GetValue(4).ToString(),
                                    PlaceName = reader.GetValue(5).ToString(),
                                    City = reader.IsDBNull(6) ? "" : reader.GetValue(6).ToString(),
                                    Address = reader.GetValue(7).ToString(),
                                    Latitude = reader.GetDouble(8),
                                    Longitude = reader.GetDouble(9),
                                    PlacePhotoCodeHome = reader.GetValue(10).ToString(),
                                    PlacePhotoCodeIcon = reader.GetValue(11).ToString(),
                                    HavePoster = reader.GetBoolean(12),
                                    //   TagsList = reader.IsDBNull(11) || reader.GetValue(11).ToString() == "" ? new List<int>() : reader.GetValue(11).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList(),
                                    PlaceCategoriesList = reader.IsDBNull(13) || reader.GetValue(13).ToString() == "" ? new List<int>() : new List<int>(reader.GetValue(13).ToString().Split(',').Select(x => Convert.ToInt32(x)).ToList()),

                                };

                                e.Type = SearchItemTypes.Event;

                                items.Add(e);
                            }
                            catch (Exception ex)
                            {
                                Console.Write(ex);
                            }
                        }
                        if (user.Location.LatLonActive)
                            items.Sort((x, y) => x.Distance.CompareTo(y.Distance));
                    }
                }
            }

            return items;
        }


        #region Base
        private static EventsDA _ManagerSingleton;
        public static EventsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EventsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}