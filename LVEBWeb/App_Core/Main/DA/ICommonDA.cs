﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Common;
namespace LVEBWeb.App_Core.Main.DA
{
    public interface ICommonDA
    {
        Dictionary<int, string> ValuesList(DICTables tableCode);
        List<dynamic[]> PlaceNameList();
    }
}
