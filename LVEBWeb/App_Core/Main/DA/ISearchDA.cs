﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.Model;

namespace LVEBWeb.App_Core.Main.DA
{
    public interface ISearchDA
    {

        List<SearchAutocompleteModel> AutocompleteSearch(SessionUserModel user, SearchSections page, string value);

        List<SearchItemPlaceModel> PlacesSearchByBox(SessionUserModel user, SearchFilterModel filter);
        List<SearchItemPlaceModel> PlacesFullSearch(SessionUserModel user, SearchSections page, string value);

        List<SearchItemEventModel> EventsSearchByBox(SessionUserModel user, SearchFilterModel filter);
        List<SearchItemEventModel> EventsFullSearch(SessionUserModel user, SearchSections page, string value);
    }
}
