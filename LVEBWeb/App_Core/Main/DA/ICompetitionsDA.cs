﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Common;
namespace LVEBWeb.App_Core.Main.DA
{
    public interface ICompetitionsDA
    {
        List<CompetitionBE> CompetitionsSelect(SessionUserModel user, GridFilterModel gridModel);
        CompetitionBE CompetitionGet(SessionUserModel user, string code);
    }
}
