﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
//using Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Util;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
//using IndexWriter = Lucene.Net.Index.IndexWriter;
using Version = Lucene.Net.Util.Version;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using Npgsql;
using Newtonsoft.Json;



namespace LVEBWeb.App_Core.Main.DA
{
    public class UserRegisterDA : BaseDA, IUserRegisterDA
    {
        public bool EmailExist(SessionUserModel user, string email)
        {
            var sql = "select count(0) from Users where useremail = @email ";


            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@email", email));
                    return Convert.ToInt32(cmd.ExecuteScalar()) > 0;
                }
            }

        }

        public string UserCodeByEmailAndPassword(SessionUserModel user, string email, string password)
        {
            var sql = "select code from Users where useremail = @email and password = @password";


            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@email", email));
                    cmd.Parameters.Add(new NpgsqlParameter("@password", password));
                    var userCode = cmd.ExecuteScalar();

                    if (userCode != null)
                        return userCode.ToString();
                }
            }

            return "";
        }

        public string UserCodeByConnectorId(SessionUserModel user, string connectorId)
        {
            var sql = "select usercode from usersConnectors where connectorId = @connectorId";


            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@connectorId", connectorId));
                    var userCode = cmd.ExecuteScalar();

                    if (userCode != null)
                        return userCode.ToString();
                }
            }

            return "";
        }

        public bool UserRegister(SessionUserModel user, UserRegisterModel value)
        {
            var sql = "insert into Users (code,name,nickname,UserEmail, updatedate, updateuser) " +
                    "values (@code,@name,@nickname, @UserEmail, @updatedate, 0)  ";


            var saveOk = false;
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", value.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@Name", value.UserName));
                    cmd.Parameters.Add(new NpgsqlParameter("@nickname", value.UserName));
                    cmd.Parameters.Add(new NpgsqlParameter("@UserEmail", value.UserEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@UpdateDate", Common.Functions.DateUpdate()));
                    saveOk = cmd.ExecuteNonQuery() > 0;
                }

                if (saveOk)
                {
                    sql = "insert into usersConnectors (usercode,connectorId,connector) " +
                                        "values (@UserCode,@connectorId, @connector)  ";
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter("@UserCode", value.UserCode));
                        cmd.Parameters.Add(new NpgsqlParameter("@connectorId", value.ConnectorId));
                        cmd.Parameters.Add(new NpgsqlParameter("@connector", value.Connector));
                        saveOk = cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            return saveOk;
        }


        public UserModel UserGet(SessionUserModel user, string code)
        {
            var sql = @"select code, name, Nickname, BirthDate, addcitycode, addprovincecode,UserEmail, UserPhone
            ,array_to_string(array(select c.companycode from companiesusers as c where c.usercode = u.code),',')
             from Users as u
             where u.Code = @Code ";
            var userModel = new Model.UserModel();
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@Code", code));
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            userModel = new Model.UserModel()
                            {
                                Code = reader.GetValue(0).ToString(),
                                Name = reader.GetValue(1).ToString(),
                                Nickname = reader.GetValue(2).ToString(),
                                BirthDate = reader.IsDBNull(3) ? new DateTime() : reader.GetDateTime(3),
                                AddCityCode = reader.GetValue(4).ToString(),
                                AddProvinceCode = reader.GetValue(5).ToString(),
                                Email = reader.GetValue(6).ToString(),
                                Phone = reader.GetValue(7).ToString()
                            };

                            var companies = reader.GetValue(8).ToString();
                            if (companies.Length == 0)
                                userModel.Companies = new HashSet<string>();
                            else
                                userModel.Companies = new HashSet<string>(companies.Split(','));
                        }
                        else
                            return userModel;
                    }
                }
            }



            return userModel;
        }

        public bool UserValidateEmailInsert(SessionUserModel user, string userCode, string userEmail, string token)
        {
            var sql = "insert into usersvalidateemail (usercode,useremail,token,insertdate, validateddate) " +
                    "values (@usercode,@useremail,@token, @insertdate, 0)  ";


            var saveOk = false;
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@usercode", userCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@useremail", userEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@token", token));
                    cmd.Parameters.Add(new NpgsqlParameter("@insertdate", Common.Functions.DateUpdate()));
                    saveOk = cmd.ExecuteNonQuery() > 0;
                }

            }
            return saveOk;
        }
        public string UserValidateEmailCheck(SessionUserModel user, string userEmail, string token)
        {
            var sql = "select  usercode from  usersvalidateemail where useremail = @useremail and token = @token  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@useremail", userEmail));
                    cmd.Parameters.Add(new NpgsqlParameter("@token", token));
                    var code = cmd.ExecuteScalar();
                    if (code == null)
                        return "";
                    else
                        return code.ToString();
                }

            }
            //    return "";
        }
        public bool UserValidateEmailValidated(SessionUserModel user, string userCode)
        {
            var sql = @"update usersvalidateemail 
            set validateddate = @validateddate
            where usercode = @usercode  ";


            var saveOk = false;
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@validateddate", Common.Functions.DateUpdate()));
                    cmd.Parameters.Add(new NpgsqlParameter("@usercode", userCode));
                    saveOk = cmd.ExecuteNonQuery() > 0;
                }

            }
            return saveOk;
        }
        #region Base
        private static UserRegisterDA _ManagerSingleton;
        public static UserRegisterDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new UserRegisterDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}