﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
//using Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Util;
using StandardAnalyzer = Lucene.Net.Analysis.Standard.StandardAnalyzer;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
//using IndexWriter = Lucene.Net.Index.IndexWriter;
using Version = Lucene.Net.Util.Version;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Common;
using Npgsql;
using Newtonsoft.Json;



namespace LVEBWeb.App_Core.Main.DA
{
    public class ToolsDA : BaseDA, IToolsDA
    {

        public bool PlacesToImprove_Insert(SessionUserModel user, PlacesToImproveBE model)
        {
            var sql = "insert into placestoimprove (placecode,usercode,notes,insertdate) " +
                    "values (@placecode,@usercode,@notes, @insertdate)  ";
            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@placecode", model.PlaceCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@usercode", model.UserCode));
                    cmd.Parameters.Add(new NpgsqlParameter("@notes", model.Notes));
                    cmd.Parameters.Add(new NpgsqlParameter("@insertdate", Common.Functions.DateUpdate()));
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }


        #region Base
        private static ToolsDA _ManagerSingleton;
        public static ToolsDA Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ToolsDA();
            }
            return _ManagerSingleton;
        }
        #endregion Base

    }
}