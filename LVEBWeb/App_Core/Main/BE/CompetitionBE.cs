﻿using System;
namespace LVEBWeb.App_Core.Main.BE
{
    public class CompetitionBE
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Modalities { get; set; }
        public string OficialSite { get; set; }
        public string OficialSiteUrl { get; set; }
        public bool HavePoster { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }


        public string AddProvinceCode { get; set; }
        public string AddProvince { get; set; }
        public string AddressName { get; set; }
        public string AddressLink { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }


        public string LatitudeString
        {
            get
            {
                return Latitude.ToString(Common.Functions.NumberFormatDot);
            }
        }
        public string LongitudeString
        {
            get
            {
                return Longitude.ToString(Common.Functions.NumberFormatDot);
            }
        }


        public string Dates
        {
            get
            {
                var d = "";
                if (DateStart.Day == DateEnd.Day)
                    d = "El día " + DateStart.ToString("dd-MMMM").Replace("-", " de ");
                else
                {
                    var ms = DateStart.ToString("MMMM");
                    var me = DateEnd.ToString("MMMM");
                    if (ms == me)
                        d = "Desde el  " + DateStart.ToString("dd") + "  al " + DateEnd.ToString("dd") + " de " + me;
                    else
                        d = "Desde el  " + DateStart.ToString("dd") + " de " + ms + " al " + DateEnd.ToString("dd") + " de " + me;
                }
                return d;
            }
        }
    }
}
