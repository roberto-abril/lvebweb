﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Main.BE
{
    public class PlaceBE : BaseAddBE
    {
        public PlaceBE()
        {
            CategoriesListString = new List<string>();
            TagsBailesList = new List<string>();
            TagsProfilesList = new List<string>();
        }

        public string LatitudeString
        {
            get
            {
                return Latitude.ToString(Common.Functions.NumberFormatDot);
            }
        }
        public string LongitudeString
        {
            get
            {
                return Longitude.ToString(Common.Functions.NumberFormatDot);
            }
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }


        public bool IsDanceHall { get; set; }
        public bool IsSchool { get; set; }
        public bool IsStore { get; set; }

        public double Distance { get; set; }


        public string ContactWeb { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public int Status { get; set; }
        //[JsonIgnore]
        public HashSet<int> CategoriesList { get; set; }

        public List<string> CategoriesListString { get; set; }

        //[JsonIgnore]
        public List<int> TagsList { get; set; }
        public List<string> TagsBailesList { get; set; }
        public List<string> TagsProfilesList { get; set; }

        //public long UpdateDate { get; set; }
        public bool Deleted { get; set; }


        public string PhotoCodeHome { get; set; }
        public string PhotoCodeIcon { get; set; }


        public string OpenHours { get; set; }

        string[] _OpenHoursLines;
        public string[] OpenHoursLines
        {
            get
            {
                if (_OpenHoursLines == null)
                {
                    if (OpenHours == "")
                        _OpenHoursLines = new string[0];
                    else
                    {
                        var days = JsonConvert.DeserializeObject<List<string[]>>(OpenHours);
                        _OpenHoursLines = new string[7];
                        for (var x = 0; x < 7; x++)
                        {
                            if (days.Count < x + 1 || days[x] == null)
                            {
                                _OpenHoursLines[x] = "<span>" + Common.Languages.Days["es"][x] + "</span> cerrado";
                            }
                            else
                            {
                                var aux = (days[x][0] == null ? "" : days[x][0]) + "-" +
                                (days[x].Length < 2 || days[x][1] == null ? "" : days[x][1]);
                                if (aux != "-")
                                    _OpenHoursLines[x] = "<span>" + Common.Languages.Days["es"][x] + "</span> " + aux;

                                aux = (days[x].Length < 3 || days[x][2] == null ? "" : days[x][2]) + "-" +
                                (days[x].Length < 4 || days[x][3] == null ? "" : days[x][3]);
                                if (aux == "-")
                                {
                                    aux = "";
                                }
                                else
                                {
                                    if (_OpenHoursLines[x] == null)
                                        _OpenHoursLines[x] = "<span>" + Common.Languages.Days["es"][x] + "</span> " + aux;
                                    else
                                        _OpenHoursLines[x] += " " + aux;
                                }
                            }
                        }
                    }
                }

                return _OpenHoursLines;
            }
        }



        public List<Model.SearchItemEventModel> Events;

    }
}
