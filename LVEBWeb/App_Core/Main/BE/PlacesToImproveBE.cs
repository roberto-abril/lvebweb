﻿using System;
namespace LVEBWeb.App_Core.Main.BE
{
    public class PlacesToImproveBE
    {
        public string Type { get; set; }
        public string PlaceCode { get; set; }
        public string UserCode { get; set; }
        public string Notes { get; set; }
    }
}
