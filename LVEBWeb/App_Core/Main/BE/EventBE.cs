﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Main.BE
{
    public class EventBE : BaseAddBE
    {

        public string LatitudeString
        {
            get
            {
                return Latitude.ToString(Common.Functions.NumberFormatDot);
            }
        }
        public string LongitudeString
        {
            get
            {
                return Longitude.ToString(Common.Functions.NumberFormatDot);
            }
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Category { get; set; }
        public string CategoryName { get; set; }

        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string ManagerCode { get; set; }
        public string ManagerName { get; set; }
        public DateTime DateStart { get; set; }
        public int Duration { get; set; }
        public int Status { get; set; }
        public double Distance { get; set; }



        public string PlacePhotoCodeHome { get; set; }
        public string PlacePhotoCodeIcon { get; set; }
        public Boolean HavePosgter { get; set; }

    }

}
