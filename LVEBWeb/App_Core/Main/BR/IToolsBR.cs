﻿using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.Model;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface IToolsBR
    {
        bool PlacesToImprove_Insert(SessionUserModel user, PlacesToImproveBE model);
    }
}
