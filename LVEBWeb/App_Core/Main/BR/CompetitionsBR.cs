﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class CompetitionsBR : ICompetitionsBR
    {
        public List<CompetitionBE> CompetitionsSearch(SessionUserModel user, GridFilterModel gridModel)
        {

            var items = default(List<CompetitionBE>);
            if (!_cache.TryGetValue(CacheKeys.Competitions, out items))
            {
                items = _ManagerDA.CompetitionsSelect(user, gridModel);
                _cache.Set(CacheKeys.Competitions, items, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(Configuration.MemoryCacheExpirationMinutes)));
            }

            return items;
        }


        public CompetitionBE CompetitionGet(SessionUserModel user, string code)
        {
            var item = default(CompetitionBE);
            if (!_cache.TryGetValue(CacheKeys.Competitions + code, out item))
            {
                item = _ManagerDA.CompetitionGet(user, code);
                _cache.Set(CacheKeys.Competitions + code, item, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(Configuration.MemoryCacheExpirationMinutes)));
            }

            return item;
        }


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static CompetitionsBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ICompetitionsDA _ManagerDA;


        //private static IEmailBR _EmailBR;

        /// <summary>

        /// </summary>
        public static CompetitionsBR Singleton(IMemoryCache memoryCache, ICompetitionsDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CompetitionsBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static CompetitionsBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, CompetitionsDA.Singleton());
        }

        public CompetitionsBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new CompetitionsDA();

        }
        public CompetitionsBR(IMemoryCache memoryCache, ICompetitionsDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
