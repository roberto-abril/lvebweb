﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class ToolsBR : IToolsBR
    {
        public bool PlacesToImprove_Insert(SessionUserModel user, PlacesToImproveBE model)
        {
            switch (model.Type)
            {
                case "localcerrado":
                default:
                    model.Notes = model.Type;
                    break;
            }

            if (string.IsNullOrEmpty(user.UserCode))
                model.UserCode = "";
            else
                model.UserCode = user.UserCode;

            return _ManagerDA.PlacesToImprove_Insert(user, model);
        }


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ToolsBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IToolsDA _ManagerDA;


        public static ToolsBR Singleton(IMemoryCache memoryCache, IToolsDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ToolsBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static ToolsBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, ToolsDA.Singleton());
        }

        public ToolsBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new ToolsDA();

        }
        public ToolsBR(IMemoryCache memoryCache, IToolsDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
