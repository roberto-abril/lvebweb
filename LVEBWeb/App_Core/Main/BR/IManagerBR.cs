﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface IManagerBR
    {
        List<ManagerPlaceModel> PlaceSearch(SessionUserModel user);


        List<ManagerEventModel> EventsSearch(SessionUserModel user, GridFilterModel gridModel);
        bool EventInsert(SessionUserModel user, EventBE value);
        bool EventUpdate(SessionUserModel user, EventBE value);
        EventBE EventGet(SessionUserModel user, string code);
        //    bool EventDelete(SessionUserModel user, string code);
        //  bool EventChangeStatus(SessionUserModel user, string code, int status);
    }
}
