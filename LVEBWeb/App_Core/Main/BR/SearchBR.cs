﻿using System;
using System.Linq;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class SearchBR : ISearchBR
    {

        public List<SearchAutocompleteModel> AutocompleteSearch(SessionUserModel user, SearchSections page, string value)
        {
            switch (page)
            {
                case SearchSections.DanceHall:
                case SearchSections.DanceSchool:
                default:
                    return _ManagerDA.AutocompleteSearch(user, page, value);
            }

        }


        public List<SearchItemModel> SearchByBox(SessionUserModel user, SearchFilterModel filter)
        {
            List<SearchItemModel> items;
            switch (filter.Page)
            {
                case SearchSections.DanceHall:
                case SearchSections.DanceSchool:
                    items = SearchFormatPlaces(user, _ManagerDA.PlacesSearchByBox(user, filter));
                    break;
                case SearchSections.Events:
                    items = SearchFormatEvents(user, _ManagerDA.EventsSearchByBox(user, filter));
                    if (items.Count == 0)
                    {
                        filter.FilterByBox = false;
                        items = SearchFormatEvents(user, _ManagerDA.EventsSearchByBox(user, filter));
                    }
                    break;
                default:
                    items = SearchFormatPlaces(user, _ManagerDA.PlacesSearchByBox(user, filter));
                    // items = new List<SearchItemModel>();
                    break;
            }
            return items;
        }

        public List<SearchItemModel> SearchFulltext(SessionUserModel user, SearchSections page, string value)
        {
            /* var items = _ManagerDA.PlacesFullSearch(user, value);
             SearchFormat(user, items);
             return items;
             */
            List<SearchItemModel> items;
            switch (page)
            {
                case SearchSections.DanceHall:
                case SearchSections.DanceSchool:
                    items = SearchFormatPlaces(user, _ManagerDA.PlacesFullSearch(user, page, value));
                    break;
                case SearchSections.Events:
                    items = SearchFormatEvents(user, _ManagerDA.EventsFullSearch(user, page, value));
                    break;
                default:
                    items = SearchFormatPlaces(user, _ManagerDA.PlacesFullSearch(user, page, value));
                    break;
            }
            return items.OrderByDescending(x => x.Score).Take(50).Select(x => x).ToList();
        }

        private List<SearchItemModel> SearchFormatPlaces(SessionUserModel user, List<SearchItemPlaceModel> items)
        {

            for (var x = 0; x < items.Count; x++)
            {
                if (user.Location.RLatLonActive)
                    items[x].Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(items[x].Latitude, items[x].Longitude, user.Location.Latitude(), user.Location.Longitude()), 1);
                for (var y = 0; y < items[x].CategoriesList.Count; y++)
                {
                    if (Configuration.DicLocalCategories.ContainsKey(items[x].CategoriesList[y]))
                        items[x].CategoriesListString.Add(Configuration.DicLocalCategories[items[x].CategoriesList[y]]);
                }
                for (var y = 0; y < items[x].TagsList.Count; y++)
                {
                    if (Configuration.DicLocalTagsBailes.ContainsKey(items[x].TagsList[y]))
                        items[x].TagsBailesList.Add(Configuration.DicLocalTagsBailes[items[x].TagsList[y]]);

                    if (Configuration.DicLocalTagsProfiles.ContainsKey(items[x].TagsList[y]))
                        items[x].TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[items[x].TagsList[y]]);
                }

                var defaultCode = "";
                var defaultUse = true;
                if (items[x].PhotoCodeHome.Length > 0)
                {
                    defaultUse = false;
                    //var file = Configuration.PathImagePlace(items[x].PhotoCodeHome);
                    //if(System.IO.File.Exists(file))
                    items[x].PhotoCodeHome = "/pi/" + items[x].PhotoCodeHome + "1000";
                }
                if (defaultUse)
                {
                    var v = long.Parse(items[x].Code, System.Globalization.NumberStyles.HexNumber);
                    defaultCode = (v % 10).ToString();
                    if (items[x].IsDanceHall)
                        items[x].PhotoCodeHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                    else
                        items[x].PhotoCodeHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
                }
                defaultUse = true;
                if (items[x].PhotoCodeIcon.Length > 0)
                {
                    defaultUse = false;
                    items[x].PhotoCodeIcon = "/pi/" + items[x].PhotoCodeIcon + "100";
                }
                if (defaultUse)
                {
                    if (items[x].IsDanceHall)
                        items[x].PhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                    else
                        items[x].PhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
                }
                var rnd = new Random();
                items[x].Randon = (new Random()).Next(1000, 9999);
                //values.Add((SearchItemModel)items[x]);

            }
            if (user.Location.RLatLonActive)
            {
                return items.OrderBy(x => x.Distance).Take(30).Select(x => (SearchItemModel)x).ToList();
                //items.Sort((x, y) => (int)(x.Distance - y.Distance));
            }
            else
            {
                return items.OrderBy(x => x.Randon).Take(30).Select(x => (SearchItemModel)x).ToList();
                // items.Sort((x, y) => (int)(x.Randon - y.Randon));
            }

            //  var values = items.Take(30).Select(x => (SearchItemModel)x).ToList();

            //  return values;
        }
        private List<SearchItemModel> SearchFormatEvents(SessionUserModel user, List<SearchItemEventModel> items)
        {

            for (var x = 0; x < items.Count; x++)
            {
                if (user.Location.RLatLonActive)
                    items[x].Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(items[x].Latitude, items[x].Longitude, user.Location.Latitude(), user.Location.Longitude()), 1);
                /*  for (var y = 0; y < items[x].CategoriesList.Count; y++)
                  {
                      if (Configuration.DicLocalCategories.ContainsKey(items[x].CategoriesList[y]))
                          items[x].CategoriesListString.Add(Configuration.DicLocalCategories[items[x].CategoriesList[y]]);
                  }
                  for (var y = 0; y < items[x].TagsList.Count; y++)
                  {
                      if (Configuration.DicLocalTagsBailes.ContainsKey(items[x].TagsList[y]))
                          items[x].TagsBailesList.Add(Configuration.DicLocalTagsBailes[items[x].TagsList[y]]);

                      if (Configuration.DicLocalTagsProfiles.ContainsKey(items[x].TagsList[y]))
                          items[x].TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[items[x].TagsList[y]]);
                  }*/

                var defaultCode = "";
                var defaultUse = true;

                if (items[x].PlacePhotoCodeIcon.Length > 0)
                {
                    defaultUse = false;
                    items[x].PlacePhotoCodeIcon = "/pi/" + items[x].PlacePhotoCodeIcon + "100";
                }
                if (defaultUse)
                {
                    if (items[x].PlaceIsDanceHall)
                        items[x].PlacePhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                    else
                        items[x].PlacePhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
                }

                if (items[x].HavePoster)
                {
                    items[x].EventPhotoHome = "/ei/" + items[x].PlaceCode + "/" + items[x].Code + "1000";
                }
                else
                {
                    defaultUse = true;
                    if (items[x].PlacePhotoCodeHome.Length > 0)
                    {
                        defaultUse = false;
                        items[x].EventPhotoHome = "/pi/" + items[x].PlacePhotoCodeHome + "1000";
                    }
                    if (defaultUse)
                    {
                        var v = long.Parse(items[x].PlaceCode, System.Globalization.NumberStyles.HexNumber);
                        defaultCode = (v % 10).ToString();
                        if (items[x].PlaceIsDanceHall)
                            items[x].EventPhotoHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                        else
                            items[x].EventPhotoHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
                    }




                }


                //values.Add((SearchItemModel)items[x]);

            }
            if (user.Location.LatLonActive)
            {
                items.Sort((x, y) => (int)(x.Distance - y.Distance));

            }

            var values = items.Take(50).Select(x => (SearchItemModel)x).ToList();

            return values;
        }


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static SearchBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ISearchDA _ManagerDA;


        //private static IEmailBR _EmailBR;

        /// <summary>

        /// </summary>
        public static SearchBR Singleton(IMemoryCache memoryCache, ISearchDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new SearchBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static SearchBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, SearchDA.Singleton());
        }

        public SearchBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new SearchDA();

        }
        public SearchBR(IMemoryCache memoryCache, ISearchDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
