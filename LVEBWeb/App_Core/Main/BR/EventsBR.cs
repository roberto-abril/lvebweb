﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class EventsBR : IEventsBR
    {
        public DetailsEventModel EventGet(SessionUserModel user, string code)
        {
            var item = default(DetailsEventModel);
            if (!_cache.TryGetValue(CacheKeys.Events + code, out item))
            {
                item = _ManagerDA.EventGet(user, code);

                EventFormat(user, item);

                _cache.Set(CacheKeys.Events + code, item, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(Configuration.MemoryCacheExpirationMinutes)));
            }

            return item;
        }

        private void EventFormat(SessionUserModel user, DetailsEventModel item)
        {

            if (user.Location.RLatLonActive)
                item.Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(item.Latitude, item.Longitude, user.Location.Latitude(), user.Location.Longitude()), 1);

            /*foreach (var value in item.CategoriesList)
            {
                if (Configuration.DicLocalCategories.ContainsKey(value))
                    item.CategoriesListString.Add(Configuration.DicLocalCategories[value]);
            }
            for (var y = 0; y < item.TagsList.Count; y++)
            {
                if (Configuration.DicLocalTagsBailes.ContainsKey(item.TagsList[y]))
                    item.TagsBailesList.Add(Configuration.DicLocalTagsBailes[item.TagsList[y]]);

                if (Configuration.DicLocalTagsProfiles.ContainsKey(item.TagsList[y]))
                    item.TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[item.TagsList[y]]);
            }

            */


            if (item.HavePoster)
            {
                // var pathImage = Configuration.PathImagesEvents(item.PlaceCode, item.Code);
                item.EventPhotoHome = "/ei/" + item.PlaceCode + "/" + item.Code + "1000";
            }
            else
            {
                var defaultUse = true;
                if (item.PlacePhotoCodeHome != null && item.PlacePhotoCodeHome.Length > 0)
                {
                    defaultUse = false;
                    //var file = Configuration.PathImagePlace(item.PhotoCodeHome);
                    //if(System.IO.File.Exists(file))
                    item.EventPhotoHome = "/pi/" + item.PlacePhotoCodeHome + "1000";
                }
                if (defaultUse)
                {
                    var v = long.Parse(item.Code, System.Globalization.NumberStyles.HexNumber);
                    var defaultCode = (v % 10).ToString();
                    //if (item.IsDanceHall)
                    item.EventPhotoHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                    // else
                    //    item.PlacePhotoCodeHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
                }
            }


        }

        public List<SearchItemEventModel> EventsByPlace(SessionUserModel user, string placeCode)
        {
            return EventsByPlaceFormat(user, _ManagerDA.EventsByPlace(user, placeCode));
        }
        private List<SearchItemEventModel> EventsByPlaceFormat(SessionUserModel user, List<SearchItemEventModel> items)
        {

            for (var x = 0; x < items.Count; x++)
            {
                if (user.Location.RLatLonActive)
                    items[x].Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(items[x].Latitude, items[x].Longitude, user.Location.Latitude(), user.Location.Longitude()), 1);
                /*  for (var y = 0; y < items[x].CategoriesList.Count; y++)
                  {
                      if (Configuration.DicLocalCategories.ContainsKey(items[x].CategoriesList[y]))
                          items[x].CategoriesListString.Add(Configuration.DicLocalCategories[items[x].CategoriesList[y]]);
                  }
                  for (var y = 0; y < items[x].TagsList.Count; y++)
                  {
                      if (Configuration.DicLocalTagsBailes.ContainsKey(items[x].TagsList[y]))
                          items[x].TagsBailesList.Add(Configuration.DicLocalTagsBailes[items[x].TagsList[y]]);

                      if (Configuration.DicLocalTagsProfiles.ContainsKey(items[x].TagsList[y]))
                          items[x].TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[items[x].TagsList[y]]);
                  }*/

                if (items[x].HavePoster)
                {
                    items[x].EventPhotoHome = "/ei/" + items[x].PlaceCode + "/" + items[x].Code + "1000";
                }
                else
                {
                    var defaultCode = "";
                    var defaultUse = true;
                    if (items[x].PlacePhotoCodeHome.Length > 0)
                    {
                        defaultUse = false;
                        items[x].EventPhotoHome = "/pi/" + items[x].PlacePhotoCodeHome + "1000";
                    }
                    if (defaultUse)
                    {
                        var v = long.Parse(items[x].Code, System.Globalization.NumberStyles.HexNumber);
                        defaultCode = (v % 10).ToString();
                        if (items[x].PlaceIsDanceHall)
                            items[x].EventPhotoHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                        else
                            items[x].EventPhotoHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
                    }

                    defaultUse = true;
                    if (items[x].PlacePhotoCodeIcon.Length > 0)
                    {
                        defaultUse = false;
                        items[x].PlacePhotoCodeIcon = "/pi/" + items[x].PlacePhotoCodeIcon + "100";
                    }
                    if (defaultUse)
                    {
                        if (items[x].PlaceIsDanceHall)
                            items[x].PlacePhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                        else
                            items[x].PlacePhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
                    }

                }


                //values.Add((SearchItemModel)items[x]);

            }
            if (user.Location.LatLonActive)
            {
                items.Sort((x, y) => (int)(x.Distance - y.Distance));

            }

            return items;
        }

        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static EventsBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IEventsDA _ManagerDA;


        public static EventsBR Singleton(IMemoryCache memoryCache, IEventsDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EventsBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static EventsBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, EventsDA.Singleton());
        }

        public EventsBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new EventsDA();

        }
        public EventsBR(IMemoryCache memoryCache, IEventsDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
