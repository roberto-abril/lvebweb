﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class ManagerBR : IManagerBR
    {
        public List<ManagerPlaceModel> PlaceSearch(SessionUserModel user)
        {
            return _ManagerDA.PlaceSearch(user);
        }


        public List<ManagerEventModel> EventsSearch(SessionUserModel user, GridFilterModel gridModel)
        {
            return _ManagerDA.EventsSearch(user, gridModel);
        }

        public bool EventInsert(SessionUserModel user, EventBE value)
        {
            value.Code = Common.Functions.CreateId();
            value.Status = 200001;

            var place = PlacesBR.Singleton(_cache).PlaceGet(user, value.PlaceCode);

            value.AddCityCode = place.AddCityCode;
            value.AddCityName = place.AddCityName;
            value.AddProvinceCode = place.AddProvinceCode;
            value.AddCP = place.AddCP;
            value.AddStreet = place.AddStreet;
            value.Latitude = place.Latitude;
            value.Longitude = place.Longitude;
            value.ManagerCode = user.UserCode;

            return _ManagerDA.EventInsert(user, value);
        }
        public bool EventUpdate(SessionUserModel user, EventBE value)
        {
            return _ManagerDA.EventUpdate(user, value);
        }
        public EventBE EventGet(SessionUserModel user, string code)
        {
            var e = _ManagerDA.EventGet(user, code);
            if (e.ManagerCode != user.UserCode)
                e.Code = "";
            
            return e;
        }

        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ManagerBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IManagerDA _ManagerDA;


        //private static IEmailBR _EmailBR;

        /// <summary>
        /// </summary>
        public static ManagerBR Singleton(IMemoryCache memoryCache, IManagerDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new ManagerBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static ManagerBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, ManagerDA.Singleton());
        }

        public ManagerBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new ManagerDA();

        }
        public ManagerBR(IMemoryCache memoryCache, IManagerDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
