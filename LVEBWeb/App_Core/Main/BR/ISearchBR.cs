﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface ISearchBR
    {

        List<SearchAutocompleteModel> AutocompleteSearch(SessionUserModel user, SearchSections page, string value);
        List<SearchItemModel> SearchByBox(SessionUserModel user, SearchFilterModel filter);
        List<SearchItemModel> SearchFulltext(SessionUserModel user, SearchSections page, string value);
    }
}
