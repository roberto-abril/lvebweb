﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface IUserRegisterBR
    {
        SessionUserModel UserGetSessionModel(SessionUserModel user, string code);
        string UserCodeByConnectorId(SessionUserModel user, string token);
        SessionUserModel UserRegister(SessionUserModel user, UserRegisterModel model);
        bool UserValidateEmail(SessionUserModel user, string userEmail, string token);
    }
}
