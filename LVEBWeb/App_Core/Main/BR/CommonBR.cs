﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using Microsoft.Extensions.Caching.Memory;
using System.Xml;
using System.Xml.Linq;
using System.Globalization;

namespace LVEBWeb.App_Core.Main.BR
{
    public class CommonBR : ICommonBR
    {

        public Dictionary<int, string> ValuesList(DICTables tableCode)
        {
            return _ManagerDA.ValuesList(tableCode);
        }

        public string SitemapGet()
        {
            List<SitemapNode> nodes = new List<SitemapNode>();

            nodes.Add(
                new SitemapNode()
                {
                    Url = Common.Configuration.HOST_URL + "/h",
                    Priority = 1
                });

            var items = _ManagerDA.PlaceNameList();
            var n = DateTime.Now;
            for (var x = 0; x < items.Count; x++)
            {
                var d = Functions.DateFormatInt(items[x][1]);
                var diff = n - d;

                var priority = 0.3;
                if (diff.TotalDays < 5)
                    priority = 0.8;
                else if (diff.TotalDays < 10)
                    priority = 0.6;

                nodes.Add(
                   new SitemapNode()
                   {
                       Url = Common.Configuration.HOST_URL + "/p/" + items[x][0],
                       Frequency = SitemapFrequency.Weekly,
                       LastModified = Functions.DateFormatInt(items[x][1]),
                       Priority = priority //0.8
                   });
            }

            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");

            foreach (SitemapNode sitemapNode in nodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc", Uri.EscapeUriString(sitemapNode.Url)),
                    sitemapNode.LastModified == null ? null : new XElement(
                        xmlns + "lastmod",
                        sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    sitemapNode.Frequency == null ? null : new XElement(
                        xmlns + "changefreq",
                        sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                    sitemapNode.Priority == null ? null : new XElement(
                        xmlns + "priority",
                        sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                root.Add(urlElement);
            }

            XDocument document = new XDocument(root);
            return document.ToString();
        }

        public class SitemapNode
        {
            public SitemapFrequency? Frequency { get; set; }
            public DateTime? LastModified { get; set; }
            public double? Priority { get; set; }
            public string Url { get; set; }
        }

        public enum SitemapFrequency
        {
            Never,
            Yearly,
            Monthly,
            Weekly,
            Daily,
            Hourly,
            Always
        }


        #region Base

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static CommonBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static ICommonDA _ManagerDA;


        //private static IEmailBR _EmailBR;

        /// <summary>

        /// </summary>
        public static CommonBR Singleton(ICommonDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new CommonBR(managerDA);
            }
            return _ManagerSingleton;
        }
        public static CommonBR Singleton()
        {

            return Singleton(CommonDA.Singleton());
        }

        public CommonBR()
        {
            _ManagerDA = new CommonDA();

        }
        public CommonBR(ICommonDA managerDA)
        {
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
