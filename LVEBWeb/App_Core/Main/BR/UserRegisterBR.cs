﻿using System;
using System.Linq;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class UserRegisterBR : IUserRegisterBR
    {
        public SessionUserModel UserGetSessionModel(SessionUserModel user, string code)
        {
            var userm = _ManagerDA.UserGet(user, code);

            var su = new SessionUserModel()
            {
                UserCode = userm.Code,
                UserEmail = userm.Email,
                UserName = userm.Name,
                IsCustomer = userm.Companies.Count > 0,
                Companies = userm.Companies
            };

            return su;
        }
        public SessionUserModel UserRegister(SessionUserModel user, UserRegisterModel model)
        {
            if (_ManagerDA.EmailExist(user, model.UserEmail))
            {
                return null;
            }
            model.UserCode = Functions.CreateId();
            if (_ManagerDA.UserRegister(user, model))
            {
                var token = Functions.CreateId() + DateTime.Now.ToString("HHYYmm");
                _ManagerDA.UserValidateEmailInsert(user, model.UserCode, model.UserEmail, token);

                EmailsBR.Singleton().SendUserValidateEmail(model.UserName, model.UserEmail, token);
            }

            var userm = _ManagerDA.UserGet(user, model.UserCode);

            var su = new SessionUserModel()
            {
                UserCode = userm.Code,
                UserEmail = userm.Email,
                UserName = userm.Name
            };

            return su;
        }

        public string UserCodeByConnectorId(SessionUserModel user, string token)
        {
            return _ManagerDA.UserCodeByConnectorId(user, token);
        }

        //    bool UserValidateEmailInsert(SessionUserModel user, string userCode, string userEmail, string token);
        // string UserValidateEmailCheck(SessionUserModel user, string userEmail, string token);
        public bool UserValidateEmail(SessionUserModel user, string userEmail, string token)
        {
            var userCode = _ManagerDA.UserValidateEmailCheck(user, userEmail, token);
            if (userCode.Length > 0)
            {
                _ManagerDA.UserValidateEmailValidated(user, userCode);
                return true;
            }
            return false;
        }


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static UserRegisterBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IUserRegisterDA _ManagerDA;


        //private static IEmailBR _EmailBR;

        /// <summary>

        /// </summary>
        public static UserRegisterBR Singleton(IMemoryCache memoryCache, IUserRegisterDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new UserRegisterBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static UserRegisterBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, UserRegisterDA.Singleton());
        }

        public UserRegisterBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new UserRegisterDA();

        }
        public UserRegisterBR(IMemoryCache memoryCache, IUserRegisterDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
