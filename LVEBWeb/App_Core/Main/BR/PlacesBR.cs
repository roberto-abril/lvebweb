﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class PlacesBR : IPlacesBR
    {
        public PlaceBE PlaceGet(SessionUserModel user, string code)
        {
            var item = default(PlaceBE);
            if (!_cache.TryGetValue(CacheKeys.Places + code, out item))
            {
                item = _ManagerDA.PlaceGet(user, code);
                item.ContactEmail = item.ContactEmail.Replace("@", "(at)");

                PlaceFormat(user, item);

                _cache.Set(CacheKeys.Places + code, item, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(Configuration.MemoryCacheExpirationMinutes)));
            }

            return item;
        }

        private void PlaceFormat(SessionUserModel user, PlaceBE item)
        {

            if (user.Location.RLatLonActive)
                item.Distance = Math.Round(Common.FunctionsLocation.CalculateDistanceInkm(item.Latitude, item.Longitude, user.Location.Latitude(), user.Location.Longitude()), 1);
            foreach (var value in item.CategoriesList)
            {
                if (Configuration.DicLocalCategories.ContainsKey(value))
                    item.CategoriesListString.Add(Configuration.DicLocalCategories[value]);
            }
            for (var y = 0; y < item.TagsList.Count; y++)
            {
                if (Configuration.DicLocalTagsBailes.ContainsKey(item.TagsList[y]))
                    item.TagsBailesList.Add(Configuration.DicLocalTagsBailes[item.TagsList[y]]);

                if (Configuration.DicLocalTagsProfiles.ContainsKey(item.TagsList[y]))
                    item.TagsProfilesList.Add(Configuration.DicLocalTagsProfiles[item.TagsList[y]]);
            }

            var defaultCode = "";
            var defaultUse = true;
            if (item.PhotoCodeHome.Length > 0)
            {
                defaultUse = false;
                //var file = Configuration.PathImagePlace(item.PhotoCodeHome);
                //if(System.IO.File.Exists(file))
                item.PhotoCodeHome = "/pi/" + item.PhotoCodeHome + "1000";
            }
            if (defaultUse)
            {
                var v = long.Parse(item.Code, System.Globalization.NumberStyles.HexNumber);
                defaultCode = (v % 10).ToString();
                if (item.IsDanceHall)
                    item.PhotoCodeHome = "/photos/default-sala-h" + defaultCode + ".jpeg";
                else
                    item.PhotoCodeHome = "/photos/default-escuela-h" + defaultCode + ".jpeg";
            }
            defaultUse = true;
            if (item.PhotoCodeIcon.Length > 0)
            {
                defaultUse = false;
                item.PhotoCodeIcon = "/pi/" + item.PhotoCodeIcon + "100";
            }
            if (defaultUse)
            {
                if (item.IsDanceHall)
                    item.PhotoCodeIcon = "/photos/default-sala-i" + defaultCode + ".jpeg";
                else
                    item.PhotoCodeIcon = "/photos/default-escuela-i" + defaultCode + ".jpeg";
            }


        }

        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static PlacesBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IPlacesDA _ManagerDA;


        public static PlacesBR Singleton(IMemoryCache memoryCache, IPlacesDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new PlacesBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static PlacesBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, PlacesDA.Singleton());
        }

        public PlacesBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new PlacesDA();

        }
        public PlacesBR(IMemoryCache memoryCache, IPlacesDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
