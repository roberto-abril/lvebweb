﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.Model;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface ICompetitionsBR
    {
        List<CompetitionBE> CompetitionsSearch(SessionUserModel user, GridFilterModel gridModel);
        CompetitionBE CompetitionGet(SessionUserModel user, string code);
    }
}
