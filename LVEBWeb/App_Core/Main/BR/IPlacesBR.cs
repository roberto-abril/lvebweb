﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface IPlacesBR
    {

        PlaceBE PlaceGet(SessionUserModel user, string code);
    }
}
