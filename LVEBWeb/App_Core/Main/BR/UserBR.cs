﻿using System;
using System.Linq;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
using LVEBWeb.App_Core.Main.Model;
using Microsoft.Extensions.Caching.Memory;

namespace LVEBWeb.App_Core.Main.BR
{
    public class UserBR : IUserBR
    {


        #region Base

        private IMemoryCache _cache;
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static UserBR _ManagerSingleton;

        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static IUserDA _ManagerDA;


        //private static IEmailBR _EmailBR;

        /// <summary>

        /// </summary>
        public static UserBR Singleton(IMemoryCache memoryCache, IUserDA managerDA)
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new UserBR(memoryCache, managerDA);
            }
            return _ManagerSingleton;
        }
        public static UserBR Singleton(IMemoryCache memoryCache)
        {

            return Singleton(memoryCache, UserDA.Singleton());
        }

        public UserBR(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _ManagerDA = new UserDA();

        }
        public UserBR(IMemoryCache memoryCache, IUserDA managerDA)
        {
            _cache = memoryCache;
            _ManagerDA = managerDA;

        }

        #endregion Base
    }
}
