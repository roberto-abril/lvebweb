﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Common;
using LVEBWeb.App_Core.Main.BE;
using LVEBWeb.App_Core.Main.DA;
namespace LVEBWeb.App_Core.Main.BR
{
    public interface ICommonBR
    {
        Dictionary<int, string> ValuesList(DICTables tableCode);
        string SitemapGet();
    }
}
