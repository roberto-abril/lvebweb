﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Main.Model
{
    public class SessionUserModel
    {

        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public bool IsCustomer { get; set; }
        public HashSet<string> Companies { get; set; }

        public SessionUserModel()
        {
            Location = new LocationModel();
        }

        public bool UserHasAccess
        {
            get
            {
                return !string.IsNullOrEmpty(this.UserCode);
            }
        }


        public string UserIP { get; set; }

        public LocationModel Location { get; set; }

    }
    public class LocationModel
    {
        public string Zip { get; set; }
        public string City { get; set; }
        public double IPLatitude { get; set; }
        public double IPLongitude { get; set; }
        public bool LatLonActive { get; set; }
        public bool RLatLonActive { get; set; }
        public double RealLatitude { get; set; }
        public double RealLongitude { get; set; }

        public double Latitude()
        {
            if (RLatLonActive)
                return RealLatitude;
            else
                return IPLatitude;
        }

        public double Longitude()
        {
            if (RLatLonActive)
                return RealLongitude;
            else
                return IPLongitude;
        }
    }
}
