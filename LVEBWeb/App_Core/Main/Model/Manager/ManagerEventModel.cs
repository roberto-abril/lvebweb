﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Main.Model
{
    public class ManagerEventModel
    {
        public int CategoryCode { get; set; }
        public int StatusCode { get; set; }
        public string StatusName { get; set; }
        public string EventCode { get; set; }
        public string EventName { get; set; }
        public string PlaceName { get; set; }
        public DateTime DateStart { get; set; }
        public bool HavePoster { get; set; }

    }
}
