﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Main.Model
{
    public class ManagerPlaceModel
    {
        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string PlaceCity { get; set; }
        public string CompanyName { get; set; }

    }
}
