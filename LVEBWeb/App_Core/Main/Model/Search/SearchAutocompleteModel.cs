﻿using System;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Main.Model
{
    public class SearchAutocompleteModel
    {
        public string Label;
        public string Code;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Value1;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Value2;
    }
}
