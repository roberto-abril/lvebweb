﻿using System;
namespace LVEBWeb.App_Core.Main.Model
{
    public class SearchFilterModel
    {

        public SearchSections Page;

        public string Query { get; set; }

        public bool FilterByBox = true;

        double _CenterLatitude;
        public double CenterLatitude
        {
            get { return _CenterLatitude; }
            set { _CenterLatitude = Math.Round(value, 2); }
        }
        double _CenterLongitude;
        public double CenterLongitude
        {
            get { return _CenterLongitude; }
            set { _CenterLongitude = Math.Round(value, 2); }
        }

        public string CenterKey()
        {
            return Convert.ToInt32(CenterLatitude * 100) + "_" + Convert.ToInt32(CenterLatitude * 100);
        }

    }
}
