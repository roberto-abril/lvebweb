﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Main.Model
{
    public class SearchItemEventModel : SearchItemModel
    {
        public SearchItemEventModel()
        {
            /*  CategoriesListString = new List<string>();
              TagsBailesList = new List<string>();
              TagsProfilesList = new List<string>();*/
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Category { get; set; }
        public string CategoryName { get; set; }
        public DateTime DateStart { get; set; }
        public bool HavePoster { get; set; }
        /*  [JsonIgnore]
         public string Tags { get; set; }
         public List<int> TagsList { get; set; }
         public List<string> TagsBailesList { get; set; }
         public List<string> TagsProfilesList { get; set; }*/


        // Address
        public string City { get; set; }
        public string Address { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Distance { get; set; }


        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string PlaceCategories
        {
            get
            {
                if (PlaceCategoriesListString == null)
                    return "";
                return string.Join(' ', PlaceCategoriesListString);
            }
        }
        public List<int> PlaceCategoriesList { get; set; }
        public List<string> PlaceCategoriesListString { get; set; }
        public string EventPhotoHome { get; set; }
        public string PlacePhotoCodeHome { get; set; }
        public string PlacePhotoCodeIcon { get; set; }


        public bool PlaceIsDanceHall { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.SalaBaile); } }
        public bool PlaceIsSchool { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.EscuelaBaile); } }
        public bool PlaceIsStore { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.SinDefinir); } }


    }
}
