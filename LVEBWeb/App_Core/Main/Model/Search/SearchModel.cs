﻿using System;
using System.Collections.Generic;
using LVEBWeb.App_Core.Main.BE;

namespace LVEBWeb.App_Core.Main.Model
{
    /// <summary>
    /// Model usado por las paginas principales, home, salas, ecuelas, eventos y competiciones. 
    /// </summary>
    public class SearchModel
    {
        public SearchSections Page;
        public List<SearchItemModel> Items { get; set; }
    }
}
