﻿using System;
namespace LVEBWeb.App_Core.Main.Model
{
    public enum SearchSections
    {
        Home,
        DanceSchool,
        DanceHall,
        Events,
        Competitions,
        Stores
    }
}
