﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Main.Model
{
    public class DetailsEventModel
    {

        public double Latitude;
        public double Longitude;

        public string LatitudeString
        {
            get
            {
                return Latitude.ToString(Common.Functions.NumberFormatDot);
            }
        }
        public string LongitudeString
        {
            get
            {
                return Longitude.ToString(Common.Functions.NumberFormatDot);
            }
        }
        public string AddCityCode { get; set; }
        public string AddCityName { get; set; }

        public bool HavePoster { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Category { get; set; }


        public DateTime DateStart { get; set; }
        public int Duration { get; set; }
        public int Status { get; set; }
        public double Distance { get; set; }

        public List<int> TagsList { get; set; }


        public bool HasEventImage { get; set; }
        public string EventPhotoHome { get; set; }
        //public string EventPhotoCodeIcon { get; set; }


        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string PlaceCategories
        {
            get
            {
                if (PlaceCategoriesListString == null)
                    return "";
                return string.Join(' ', PlaceCategoriesListString);
            }
        }
        public List<int> PlaceCategoriesList { get; set; }
        public List<string> PlaceCategoriesListString { get; set; }
        public string PlacePhotoCodeHome { get; set; }
        public string PlacePhotoCodeIcon { get; set; }


        public bool PlaceIsDanceHall { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.SalaBaile); } }
        public bool PlaceIsSchool { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.EscuelaBaile); } }
        public bool PlaceIsStore { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)Common.DICTableCategorias.SinDefinir); } }



    }

}
