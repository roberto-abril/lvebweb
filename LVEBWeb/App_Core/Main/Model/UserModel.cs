﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


namespace LVEBWeb.App_Core.Main.Model
{
    public class UserModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public string Notes { get; set; }
        public HashSet<string> Companies { get; set; }

        public string AddCityCode { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddProvinceCode { get; set; }

    }
}