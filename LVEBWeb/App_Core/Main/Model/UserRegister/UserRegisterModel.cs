﻿using System;
namespace LVEBWeb.App_Core.Main.Model
{
    public class UserRegisterModel
    {
        /// <summary>
        /// google, facebook, email
        /// </summary>
        public string Connector;
        public string ConnectorToken;
        public string ConnectorId;
        public string UserCode;
        public string UserImage;
        public string UserEmail { get; set; }
        public string UserName { get; set; }
    }
}
