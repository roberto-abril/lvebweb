﻿using System;
using System.Threading;
using System.Web;
using MongoDB.Driver;
using MongoDB.Bson;

namespace LVEBWeb.App_Core.Common
{
    public static class ExceptionManager
    {

        public static void Publish(Exception exception, string methodName, string module, string customerId = "", string notes = "")
        {
            if (exception == null) return;

            try
            {
                
                var client = new MongoClient(Configuration.MDB_EX_Connection);
                var clientdb = client.GetDatabase(Configuration.MDB_EX);
                var dbc = clientdb.GetCollection<BsonDocument>("Exceptions");

                var doc = new BsonDocument();

                doc.Add("Exception", exception.Message);
                doc.Add("Module", module);
                doc.Add("CustomerId", customerId);
                doc.Add("Notes", notes);
                doc.Add("Method", methodName);
                doc.Add("DateInsert", DateTime.Now.ToString("dd/MM/yyyy"));

                dbc.InsertOne(doc);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PublishAsync(Exception exception, string methodName, string module = "", string customerId = "", string notes = "")
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                Publish(exception, methodName, module, customerId, notes);
            });
        }
    }
}
