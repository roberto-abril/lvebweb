﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Common
{
    public class EmailsBR
    {

        public bool SendPasswordReset(string email, string key)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("url", Configuration.HOST_URL + "/PasswordReset/" + key + "/" + email);
            // PasswordReset/5a7412e0caf2113a436c2598/ab@avuxi.com
            return Send(email, EmailTemplates.PasswordReset, parameters);

        }
        public bool SendUserGetAccess(string userCode, string message)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("userCode", userCode);
            parameters.Add("message", message);
            return Send("", EmailTemplates.Internal_UserGetAccess, parameters);
        }
        /*  public bool SendUserRegisted(string userEmail, string userName)
          {
              var parameters = new Dictionary<string, string>();
              parameters.Add("userEmail", userEmail);
              return Send(userEmail, EmailTemplates.RegisterValidated, parameters);
          }*/
        public bool SendUserValidateEmail(string userName, string userEmail, string token)
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("UserName", userName);
            parameters.Add("UserEmail", userEmail);
            //parameters.Add("Token", token);
            parameters.Add("URL", Configuration.HOST_URL + "/ur/vm?e=" + userEmail + "&t=" + token);
            return Send(userEmail, EmailTemplates.RegisterValidateEmail, parameters);
        }


        public bool Send(string to, EmailTemplates template, Dictionary<string, string> parameters)
        {
            var email = new EmailBE()
            {
                Template = template,
                To = to,
                Parameters = parameters
            };
            return _ManagerDA.EmailSend(email);
        }


        #region Base

        private static EmailsBR _ManagerSingleton;
        private static EmailsDA _ManagerDA;

        public static EmailsBR Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new EmailsBR();
            }
            return _ManagerSingleton;
        }

        public EmailsBR()
        {
            _ManagerDA = new EmailsDA();
        }


        #endregion
    }
}
