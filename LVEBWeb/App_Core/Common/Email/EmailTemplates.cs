﻿using System;
namespace LVEBWeb.App_Core.Common
{
    public enum EmailTemplates
    {
        None = 0,
        Internal_UserLogin = 1,
        Internal_UserGetAccess = 2,
        PasswordReset = 10,
        RegisterValidateEmail = 20,
        RegisterValidated = 21,
        DataPlaceAdd = 50,
        DataPlaceEdit = 51,
        DataEventAdd = 60,
        DataEventEdit = 61
    }
}
