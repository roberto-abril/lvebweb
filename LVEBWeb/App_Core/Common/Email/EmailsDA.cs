﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace LVEBWeb.App_Core.Common
{
    public class EmailsDA
    {


        public bool EmailSend(EmailBE email)
        {
            var sql = "insert into EmailsWaiting (EmailTo,Template,Parameters) " +
                    "values (@EmailTo,@Template,@Parameters)  ";

            using (var conn = new NpgsqlConnection(Configuration.DBConnection))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter("@EmailTo", email.To));
                    cmd.Parameters.Add(new NpgsqlParameter("@Template", (int)email.Template));
                    cmd.Parameters.Add(new NpgsqlParameter("@Parameters", email.PameterToString()));

                    cmd.ExecuteNonQuery();
                }
            }
            return true;
        }


        #region Base
        /// <summary>
        /// Clase singleton.
        /// </summary>
        private static EmailsDA _Singleton;
        /// <summary>

        /// </summary>
        public static EmailsDA Singleton()
        {
            if (_Singleton == null)
            {
                _Singleton = new EmailsDA();
            }
            return _Singleton;
        }
        #endregion
    }
}
