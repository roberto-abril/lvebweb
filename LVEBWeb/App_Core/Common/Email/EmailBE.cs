﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEBWeb.App_Core.Common
{
    public class EmailBE
    {
        public long Id;
        public string To;
        public EmailTemplates Template;
        public Dictionary<string, string> Parameters;

        public string ParametersCopy
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                    Parameters = new Dictionary<string, string>();
                else
                    Parameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(value);
            }
        }
        public string PameterToString()
        {
            if (Parameters == null) return "{}";
            return JsonConvert.SerializeObject(Parameters);
        }
        public int Status = 0;
        public DateTime DateInsert;

        public string TemplateString
        {
            get { return Template.ToString(); }
        }
    }
}
