﻿
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;


namespace LVEBWeb.App_Core.Common
{
    public enum DBs
    {
        Exection
    }
    public class Configuration
    {

        public static string ContentRootPath = "";
        public static string PathRoot(string path)
        {
            return System.IO.Path.Combine(ContentRootPath, path);
        }
        public static string PathRoot(string path, string path1)
        {
            return System.IO.Path.Combine(ContentRootPath, path, path1);
        }
        public static string PathRoot(string[] paths)
        {
            var aux = new List<string>() { ContentRootPath };
            aux.AddRange(paths);
            return System.IO.Path.Combine(aux.ToArray());
        }

        static string _Environment = "";
        public static string Environment { get { return _Environment; } }


        static string _ScriptVersion = "";
        public static string ScriptVersion { get { return _ScriptVersion; } }

        static string _HOST_URL;
        public static string HOST_URL { get { return _HOST_URL; } }


        static string _DBConnection;
        public static string DBConnection { get { return _DBConnection; } }


        public static int _MemoryCacheExpirationMinutes = 20;
        public static int MemoryCacheExpirationMinutes { get { return _MemoryCacheExpirationMinutes; } }


        public static string MDB_Exection_Connection;
        public static string MDB_Exection;

        static string _PathInstall;


        public static string PathImagesEvents(string code1, string code2)
        {
            var path = System.IO.Path.Combine(_PathImagesEvents, code1);
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);

            return System.IO.Path.Combine(path, code2);
        }
        static string _PathImagesEvents;

        public static string PathImagesCompetitions
        {
            get
            {
                if (!_PathImagesCompetitionsChecked)
                {
                    if (!System.IO.Directory.Exists(_PathImagesCompetitions))
                        System.IO.Directory.CreateDirectory(_PathImagesCompetitions);
                    _PathImagesCompetitionsChecked = true;
                }
                return _PathImagesCompetitions;
            }
        }
        static bool _PathImagesCompetitionsChecked;
        static string _PathImagesCompetitions;

        // public static string PathImagesPlaces { get { return _PathImagesPlaces; } }
        static string _PathImagesPlaces;

        public static string PathImagePlaces(string code)
        {
            if (code.Length < 4) return "";
            var pathPlace = System.IO.Path.Combine(_PathImagesPlaces, code.Substring(0, 4));
            if (!System.IO.Directory.Exists(pathPlace))
                System.IO.Directory.CreateDirectory(pathPlace);

            return System.IO.Path.Combine(pathPlace, code);
        }
        public static string PathImagePlacesBase(string code)
        {
            if (code.Length < 4) return "";
            return System.IO.Path.Combine(_PathImagesPlaces, code.Substring(0, 4));
        }


        static string _PathLucenIndex;
        public static string PathLucenIndex { get { return _PathLucenIndex; } }

        public static string AdminUserEmail = "";
        public static string AdminUserPassword = "";

        public static string VisorDBsString = "";
        public static Dictionary<string, VisorDBModel> VisorDBs;

        static string _IPStackKey;
        public static string IPStackKey { get { return _IPStackKey; } }


        public static string MDB_EX_Connection;
        public static string MDB_EX;

        public static bool CACHE_STOP;

        public static string ImageEmpty;

        public static void LoadConfig(IConfigurationRoot conf)
        {
            try
            {
                _Environment = conf["Base:ENVIRONMENT"];
                _DBConnection = conf["Base:DBConnection"];
                _PathInstall = conf["Base:PathInstall"];
                _PathImagesCompetitions = conf["Base:PathImagesCompetitions"];
                _PathImagesPlaces = conf["Base:PathImagesPlaces"];
                _PathImagesEvents = conf["Base:PathImagesEvents"];


                MDB_Exection_Connection = conf["Base:MDB_Exection_Connection"];
                MDB_Exection = conf["Base:MDB_Exection"];

                _PathLucenIndex = conf["Base:PathLucenIndex"];


                _HOST_URL = conf["Base:HOST_URL"];


                var fileVerion = PathRoot("version.txt");
                if (System.IO.File.Exists(fileVerion))
                    _ScriptVersion = System.IO.File.ReadAllText(fileVerion).Trim();

                if (System.Environment.MachineName == "")
                {
                    _Environment = "PRO";
                    _HOST_URL = "http://192.168.7.51:8090";
                }

                _IPStackKey = conf["Base:IPStackKey"];


                MDB_EX_Connection = conf["Base:MDB_EX_Connection"];
                MDB_EX = conf["Base:MDB_EX"];

                AdminUserEmail = conf["Base:AdminUserEmail"];
                AdminUserPassword = conf["Base:AdminUserPassword"];
                CACHE_STOP = Convert.ToBoolean(conf["Base:CACHE_STOP"]);

                ImageEmpty = PathRoot("") + "/wwwroot/images/favicon.ico";


                LoadDiccionaries();
            }
            catch (Exception ex)
            {
                ExceptionManager.PublishAsync(ex, "SF.Configuration");
            }
        }

        public static Dictionary<int, string> DicLocalCategories;
        public static Dictionary<int, string> DicLocalTagsBailes;
        public static Dictionary<int, string> DicLocalTagsProfiles;
        public static void LoadDiccionaries()
        {
            DicLocalCategories = App_Core.Main.BR.CommonBR.Singleton().ValuesList(DICTables.LocalCategorias);
            DicLocalTagsBailes = App_Core.Main.BR.CommonBR.Singleton().ValuesList(DICTables.LocalTagsBailes);
            DicLocalTagsProfiles = App_Core.Main.BR.CommonBR.Singleton().ValuesList(DICTables.LocalTagsProfiles);
        }
    }
}