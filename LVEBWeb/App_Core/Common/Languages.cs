﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Common
{
    public class Languages
    {
        public static Dictionary<string, Dictionary<int, string>> Days = new Dictionary<string, Dictionary<int, string>>()
        {
            {"es", new Dictionary<int, string>()
                {
                    {0,"Lunes"},
                    {1,"Martes"},
                    {2,"Miércoles"},
                    {3,"Jueves"},
                    {4,"Viernes"},
                    {5,"Sábado"},
                    {6,"Domingo"}
                }
            }
        };
    }
}
