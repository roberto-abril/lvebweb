﻿using System;

namespace LVEBWeb.App_Core.Common
{

    public static class ExtensionMethods
    {
        public static string Replace(this string s, char[] separators, string newVal)
        {
            string[] temp;

            temp = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            return String.Join(newVal, temp);
        }
        public static string Replace(this string s, string[] separators, string newVal)
        {
            string[] temp;

            temp = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            return String.Join(newVal, temp);
        }


        /*
		public static string Right(this string sValue, int iMaxLength)
		{
			//Check if the value is valid
			if (string.IsNullOrEmpty(sValue))
			{
				//Set valid empty string as string could be null
				sValue = string.Empty;
			}
			else if (sValue.Length > iMaxLength)
			{
				//Make the string no longer than the max length
				sValue = sValue.Substring(sValue.Length - iMaxLength, iMaxLength);
			}

			//Return the string
			return sValue;
		}*/
    }
}
