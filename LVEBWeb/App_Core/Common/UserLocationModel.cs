﻿using System;
namespace LVEBWeb.App_Core.Common
{
    public class UserLocationModel
    {
        public string ip;
        public string country_code;
        public string country_name;
        public string region_code;
        public string region_name;
        public string city;
        public string zip;
        public double latitude;
        public double longitude;
        //{"ip":"83.60.221.216","country_code":"ES","country_name":"Spain","region_code":"CT","region_name":"Catalonia",
        //"city":"Barcelona","zip_code":"08001","time_zone":"Europe/Madrid","latitude":41.3984,"longitude":2.1741,"metro_code":0}
    }
}
