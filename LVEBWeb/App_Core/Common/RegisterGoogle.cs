﻿using System;
using RestSharp;
using Newtonsoft.Json;

namespace LVEBWeb.App_Core.Common
{
    public class RegisterGoogle
    {
        string ClientId = "396159091719-0congu3btm0h7ebe349q9vif4jhc8efb.apps.googleusercontent.com";
        string ClientSecret = "n5eJoXMrr5hl_Q4kXXasNmLz";
        string Redirection_Url = Configuration.HOST_URL +  "/ur/register/googleend";
        public string UrlLogin()
        {
            //Redirection_Url = Configuration.HOST_URL + "/ur/register/googleend";
            return "https://accounts.google.com/o/oauth2/v2/auth?scope=profile&include_granted_scopes=true&redirect_uri=" + Redirection_Url + "&response_type=code&client_id=" + ClientId + "";

        }



        public GoogleUserInfo UserProfileGet(string code)
        {
            var client = new RestClient("https://accounts.google.com/o/oauth2/token");
            var request = new RestRequest("", Method.POST);
            request.AddParameter("code", code);
            request.AddParameter("client_id", ClientId);
            request.AddParameter("client_secret", ClientSecret);
            request.AddParameter("redirect_uri", Redirection_Url);
            request.AddParameter("grant_type", "authorization_code");


            var response = client.Execute(request);
            var serStatus = JsonConvert.DeserializeObject<GoogleAccessToken>(response.Content);


            var url = "https://www.googleapis.com/oauth2/v3/userinfo?alt=json&access_token=" + serStatus.access_token + "";

            client = new RestClient(url);
            request = new RestRequest("", Method.GET);
            response = client.Execute(request);
            var userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleUserInfo>(response.Content);
            return userInfo;

        }



        public class GoogleAccessToken
        {

            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string id_token { get; set; }
            public string refresh_token { get; set; }
        }

        public class GoogleUserInfo
        {

            public string sub { get; set; }
            public string email { get; set; }
            public string name { get; set; }
            public string picture { get; set; }
            public string locale { get; set; }
        }

        #region Base
        private static RegisterGoogle _ManagerSingleton;
        public static RegisterGoogle Singleton()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new RegisterGoogle();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}