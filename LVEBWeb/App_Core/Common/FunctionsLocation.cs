﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace LVEBWeb.App_Core.Common
{

    public class FunctionsLocation
    {

        static RestClient UserLocationRC;
        public static UserLocationModel UserLocationGet( string ip )
        {
            if (ip == "::1")
            {
                ip = "83.60.221.216";
            }

            if (ip == "127.0.0.1")
                return new UserLocationModel() { };

            if (UserLocationRC == null)
                UserLocationRC = new RestClient("http://api.ipstack.com/");
            try
            {
                var request = new RestRequest(ip + "?format=1&access_key=" + Configuration.IPStackKey, Method.GET);
                IRestResponse response = UserLocationRC.Execute(request);
                var dic = Newtonsoft.Json.JsonConvert.DeserializeObject<UserLocationModel>(response.Content);
                // dic.content = response.Content;
                return dic;
            }
            catch (Exception ex)
            {

                ExceptionManager.PublishAsync(ex, "AdminController.UserLocationGet");
                return new UserLocationModel()
                {
                    latitude = 40.4378698,
                    longitude = -3.8196207,
                    city = "",
                    zip = ""
                };
            }



            /*
            var ul = new BE.UserLocation();
            if (!dic.ContainsKey("latitude") && !dic.ContainsKey("city"))
                return ul;
            ul.ip = dic["ip"];
            ul.country_code = dic["country_code"];
            ul.country_name = dic["country_name"];
            ul.region_code = dic["region_code"];
            ul.region_name = dic["region_name"];
            ul.city = dic["city"];
            ul.zip_code = dic["zip_code"];
            ul.time_zone = dic["time_zone"];
            ul.latitude = dic["latitude"];
            ul.longitude = dic["longitude"];

            return ul;
            */
            // http://freegeoip.net/json/83.60.221.216
        }


        public static double DegreesToRadians( double value )
        {
            return value * Math.PI / 180.0;
        }
        const double EarthRadius = 6371.0;
        public static double CalculateDistanceInkm( double lat1, double lon1, double lat2, double lon2 )
        {

            var dLat = DegreesToRadians( lat2 - lat1 );
            var dLon = DegreesToRadians( lon2 - lon1 );
            lat1 = DegreesToRadians( lat1 );
            lat2 = DegreesToRadians( lat2 );

            var sdLat = Math.Sin( dLat / 2.0 );
            var sdLon = Math.Sin( dLon / 2.0 );
            var cLats = Math.Cos( lat1 ) * Math.Cos( lat2 );
            var a = sdLat * sdLat + sdLon * sdLon * cLats;
            var c = 2.0 * Math.Atan2( Math.Sqrt( a ), Math.Sqrt( 1.0 - a ) );
            return EarthRadius * c;
        }
        public static int CalculateDistanceInMeters( double lat1, double lon1, double lat2, double lon2 )
        {
            return Convert.ToInt32( CalculateDistanceInkm( lat1, lon1, lat2, lon2 ) * 1000 );
        }


    }
}
