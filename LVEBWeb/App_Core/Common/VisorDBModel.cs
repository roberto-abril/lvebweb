﻿using System;
using System.Collections.Generic;
namespace LVEBWeb.App_Core.Common
{
    public class VisorDBModel
    {
        public string Name;
        public string File;
        public Dictionary<string, VisorDBTableModel> Tables;
    }
    public class VisorDBTableModel
    {
        public string Name;
        public Dictionary<string, VisorDBFieldModel> Fields;
        //public List<VisorDBFilterModel> Filters;
        public bool IsView;
    }
    public class VisorDBFieldModel
    {
        public VisorDBFieldModel() { }
        public VisorDBFieldModel(string name, string type)
        {
            Name = name;
            Type = type;
        }
        public string Name;
        public string Type;
        //public bool Filter;
    }
}
