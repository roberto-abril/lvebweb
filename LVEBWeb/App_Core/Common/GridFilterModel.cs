﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LVEBWeb.App_Core.Common
{
	public class GridFilterModel
	{
		/// <summary>
		/// PageNumber
		/// </summary>
		public int PN { get; set; }
		/// <summary>
		/// PageSize
		/// </summary>
		public byte PS { get; set; }
		/// <summary>
		/// ColumnOrder
		/// </summary>
		public string CO { get; set; }
		/// <summary>
		/// ColumnOrderType
		/// </summary>
		public string COT { get; set; }

		public int COTNumber { get { if (COT == "desc") return 1; return 0; } }
		/// <summary>
		/// TotalRows
		/// </summary>
		public long TRs { get; set; }
		/// <summary>
		/// PageTotal
		/// </summary>
		public long PT
		{
			get
			{
				if (PS == 0) return 0;
				return TRs % PS == 0 ? TRs / PS : TRs / PS + 1;
			}
		}
		public string FV { get; set; }
		public string searchField { get; set; }
		public string searchString { get; set; }
		public string searchOper { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string FK1 { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string FK2 { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string FK3 { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string FK4 { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string FK5 { get; set; }
	}
}
