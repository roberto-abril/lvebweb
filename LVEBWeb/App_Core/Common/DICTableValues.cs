﻿using System;
namespace LVEBWeb.App_Core.Common
{
    public enum DICTableCategorias : int
    {
        Discoteca = 110003,
        EscuelaBaile = 110001,
        EspacioMultifuncional = 110004,
        SalaBaile = 110002,
        SinDefinir = 110000
    }
}
