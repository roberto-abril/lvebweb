﻿
namespace LVEBWeb.App_Core.Common
{
    public static class CacheKeys
    {
        public static string Competitions { get { return "Competitions_"; } }
        public static string Places { get { return "Places_"; } }
        public static string Events { get { return "Events_"; } }
    }
}
