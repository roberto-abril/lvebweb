﻿using System;
namespace LVEBWeb.App_Core.Common
{
    public enum DICTables : int
    {
        None = 0,
        ContactType = 10,
        LocalCategorias = 11,
        UsuarioTypes = 12,
        CompanyTypeLegalID = 13,
        CompanyTypePayment = 14,
        LocalUserType = 15,
        LocalTagsBailes = 16,
        LocalTagsProfiles = 17,
        LocalStatus = 18,
        CompetitionStatus = 19,
        EventStatus = 20,
        EventCategories = 25,
        CompanyType = 21,
        CompanyStatus = 22,
        TaskStatus = 23,
        TaskTypes = 24
    }
}
