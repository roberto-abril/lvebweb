﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using Newtonsoft.Json;
using RestSharp;

namespace LVEBWeb.App_Core.Common
{

    public class Functions
    {

        public static void ConsoleWriteLine(string line)
        {
            Console.WriteLine(line);
        }

        public static void CheckPath(string path)
        {
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
        }

        public static string CreateId()
        {
            var rnd = new Random();
            var id = Convert.ToInt64(DateTime.Now.ToString("yyMMddHHmm") + rnd.Next(1000, 9999));
            return String.Format("{0:X}", id);
        }
        public static long DateUpdate()
        {
            return Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmm"));
        }

        public static DateTime DateFormatInt(int date)
        {
            return DateFormatInt(date);
        }
        public static DateTime DateFormatInt(long date)
        {
            var aux = date.ToString().ToCharArray();
            return new DateTime(
                Convert.ToInt32(aux[0].ToString() + aux[1].ToString() + aux[2].ToString() + aux[3].ToString()),
                Convert.ToInt32(aux[4].ToString() + aux[5].ToString()),
                Convert.ToInt32(aux[6].ToString() + aux[7].ToString()),
                Convert.ToInt32(aux[8].ToString() + aux[9].ToString()),
                Convert.ToInt32(aux[10].ToString() + aux[11].ToString()),
                0
                );
        }

        public static string MD5Encode(string value)
        {
            var m = System.Security.Cryptography.MD5.Create();
            byte[] data = m.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sbuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sbuilder.Append(data[i].ToString("x2"));
            }
            return sbuilder.ToString();
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = (dateTime.ToUniversalTime() - unixStart).Ticks;
            return (double)unixTimeStampInTicks / TimeSpan.TicksPerSecond;
        }


        public static System.Globalization.NumberFormatInfo NumberFormatComa = new System.Globalization.NumberFormatInfo()
        {
            NumberDecimalSeparator = ","
        };
        public static System.Globalization.NumberFormatInfo NumberFormatDot = new System.Globalization.NumberFormatInfo()
        {
            NumberDecimalSeparator = "."
        };


        public static bool MatchGoodPassword(string password)
        {
            return MatchRegex(password, "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,50}$");
        }

        public static bool MatchRegex(string text, string pattern)
        {
            var r = new System.Text.RegularExpressions.Regex(pattern);
            var m = r.Match(text);
            return m.Success;
        }

        public static bool ImagesCreateCopies(string file)
        {
            var image = Image.FromFile(file);
            var fileextesion = System.IO.Path.GetExtension(file);
            if (fileextesion.Length > 0) fileextesion = "." + fileextesion;
            var fileBase = file.Split('.')[0];
            var h = image.Height;
            var w = image.Width;


            var imgt = new Bitmap(100, 100);
            var fline_new = fileBase + "100" + fileextesion;
            using (var graphics = Graphics.FromImage(imgt))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphics.DrawImage(image, 0, 0, 100, 100);
                imgt.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);

            }


            var w_new = 250;
            var h_new = w_new * h / w;
            fline_new = fileBase + "250" + fileextesion;
            var imagen = (Image)(new Bitmap(image, new Size(250, h_new)));
            imagen.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);


            w_new = 500;
            fline_new = fileBase + "500" + fileextesion;
            if (w_new < w)
            {
                h_new = w_new * h / w;
                imagen = (Image)(new Bitmap(image, new Size(500, h_new)));
                imagen.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
            {
                image.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }


            w_new = 1000;
            fline_new = fileBase + "1000" + fileextesion;
            if (w_new < w)
            {
                h_new = w_new * h / w;
                imagen = (Image)(new Bitmap(image, new Size(1000, h_new)));
                imagen.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
            {
                image.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }


            return true;
        }

        public static bool ImagesCreateForEvents(string file)
        {
            var image = Image.FromFile(file);
            var fileextesion = System.IO.Path.GetExtension(file);
            if (fileextesion.Length > 0) fileextesion = "." + fileextesion;
            var fileBase = file.Split('.')[0];
            var h = image.Height;
            var w = image.Width;


            var imgt = new Bitmap(100, 100);
            var fline_new = fileBase + "100" + fileextesion;
            using (var graphics = Graphics.FromImage(imgt))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphics.DrawImage(image, 0, 0, 100, 100);
                imgt.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);

            }


            var w_new = 250;
            var h_new = w_new * h / w;
            fline_new = fileBase + "250" + fileextesion;
            var imagen = (Image)(new Bitmap(image, new Size(250, h_new)));
            imagen.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);


            w_new = 500;
            fline_new = fileBase + "500" + fileextesion;
            if (w_new < w)
            {
                h_new = w_new * h / w;
                imagen = (Image)(new Bitmap(image, new Size(500, h_new)));
                imagen.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
            {
                image.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }


            w_new = 1000;
            fline_new = fileBase + "1000" + fileextesion;
            if (w_new < w)
            {
                h_new = w_new * h / w;
                imagen = (Image)(new Bitmap(image, new Size(1000, h_new)));
                imagen.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
            {
                image.Save(fline_new, System.Drawing.Imaging.ImageFormat.Jpeg);
            }


            return true;
        }


        public static string StringRemoveSigns(string value)
        {
            value = value.Replace("á", "a");
            value = value.Replace("é", "e");
            value = value.Replace("í", "i");
            value = value.Replace("ó", "o");
            value = value.Replace("ú", "u");

            value = value.Replace("à", "a");
            value = value.Replace("è", "e");
            value = value.Replace("ì", "i");
            value = value.Replace("ò", "o");
            value = value.Replace("ù", "u");
            value = value.Replace("'", " ");
            return value;
        }

        public static string WebDownlaodText(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest("", Method.GET);

            IRestResponse response = client.Execute(request);
            return response.Content;

            //return JsonConvert.DeserializeObject<T>(response.Content);
        }

        /*
        public void GoogleGetToken(string code)
        {
            string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            UTF8Encoding utfenc = new UTF8Encoding();
            byte[] bytes = utfenc.GetBytes(poststring);
            Stream outputstream = null;
            try
            {
                request.ContentLength = bytes.Length;
                outputstream = request.GetRequestStream();
                outputstream.Write(bytes, 0, bytes.Length);
            }
            catch { }
            var response = (HttpWebResponse)request.GetResponse();
            var streamReader = new StreamReader(response.GetResponseStream());
            string responseFromServer = streamReader.ReadToEnd();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
            GetuserProfile(obj.access_token);
        }
        public void GoogleGetuserProfile(string accesstoken)
        {
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accesstoken + "";
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            response.Close();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Userclass userinfo = js.Deserialize<Userclass>(responseFromServer);
            imgprofile.ImageUrl = userinfo.picture;
            lblid.Text = userinfo.id;
            lblgender.Text = userinfo.gender;
            lbllocale.Text = userinfo.locale;
            lblname.Text = userinfo.name;
            hylprofile.NavigateUrl = userinfo.link;
        }*/

    }
}