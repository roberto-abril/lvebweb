﻿using LVEBWeb.IntegrationTests.Common;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace LVEBWeb.IntegrationTests.TestsCommon
{

    public abstract class TestBase
    {
        public TestBase()
        {
            Configuration.Singleton().LoadConfiguration();
        }


        protected T RequestPost<T>(string method, NameValueCollection values)
        {
            IRestResponse response = default(RestResponse);
            try
            {
                var client = new RestClient(Configuration.ApiUrl);
                var request = new RestRequest(method, Method.POST);
                for (var i = 0; i < values.Count; i++)
                {
                    request.AddParameter(values.GetKey(i), values.Get(i));
                }
                response = client.Execute(request);
                if (string.IsNullOrEmpty(response.Content)) return default(T);
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception ex)
            {
                if (response != null) { Console.WriteLine(response.Content); }
                Console.WriteLine($"Method: {method} , Exception: {ex.Message}");
                return default(T);
            }
        }


        protected T RequestGet<T>(string method, NameValueCollection values)
        {
            try
            {
                var client = new RestClient(Configuration.ApiUrl);
                var request = new RestRequest(method, Method.GET);
                for (var i = 0; i < values.Count; i++)
                {
                    request.AddQueryParameter(values.GetKey(i), values.Get(i));
                }
                IRestResponse response = client.Execute(request);
                if (string.IsNullOrEmpty(response.Content)) return default(T);
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}