﻿using Microsoft.Extensions.Configuration;
using System;

namespace LVEBWeb.IntegrationTests.Common
{
    public class Configuration
    {
        public static string ApiUrl { get; set; }

        IConfigurationRoot _config;
        public Configuration()
        {
            _config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        }

        private static Configuration _configuration;
        public static Configuration Singleton()
        {
            if (_configuration == null)
            {
                _configuration = new Configuration();
            }
            return _configuration;
        }

        public void LoadConfiguration()
        {
            ApiUrl = _config["APIUrl"];
        }
    }
}
