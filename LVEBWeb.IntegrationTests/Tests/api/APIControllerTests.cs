﻿using System;
using LVEBWeb.IntegrationTests.TestsCommon;
using System.Collections.Specialized;
using Xunit;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LVEBWeb.IntegrationTests.Tests.api
{
    public class LocationInfoTests : TestBase
    {
        public LocationInfoTests() { }

        private class CommonLocationModel
        {
            public List<dynamic> provinces;
            public List<dynamic> cities;
        }

        [Fact]
        public void CommonLocation_OK()
        {
            var form = new NameValueCollection { };
            var response = RequestPost<CommonLocationModel>("/api/CommonLocation", form);
            Assert.NotNull(response);
            Assert.True(response.provinces != null && response.provinces.Count > 0);
            Assert.True(response.cities != null && response.cities.Count > 0);
        }

        private class CommonDICsModel
        {
            public List<dynamic> localCategorias;
            public List<dynamic> localTags;
        }

        [Fact]
        public void CommonDICs_OK()
        {
            var form = new NameValueCollection { };
            var response = RequestPost<CommonDICsModel>("/api/CommonDICs", form);
            Assert.NotNull(response);
            Assert.True(response.localCategorias != null && response.localCategorias.Count > 0);
            Assert.True(response.localTags != null && response.localTags.Count > 0);
        }


        [Fact]
        public void CommonDICsValues_OK()
        {
            var form = new NameValueCollection { };
            var response = RequestPost<List<dynamic>>("/api/CommonDICsValues", form);
            Assert.NotNull(response);
            Assert.True(response != null && response.Count > 0);
        }


        [Fact]
        public void Places_OK()
        {
            var form = new NameValueCollection
            {
                ["dateLastUpdate"] = "0"
            };
            var response = RequestPost<List<dynamic>>("/api/Places", form);
            Assert.NotNull(response);
            Assert.True(response != null && response.Count > 0);
        }


        [Fact]
        public void Competitions_OK()
        {
            var form = new NameValueCollection
            {
                ["dateLastUpdate"] = "0"
            };
            var response = RequestPost<List<dynamic>>("/api/Competitions", form);
            Assert.NotNull(response);
            Assert.True(response != null && response.Count > 0);
        }

        [Fact]
        public void DLU_OK()
        {
            var form = new NameValueCollection { };
            var response = RequestPost<Dictionary<string, dynamic>>("/api/DLU", form);
            Assert.NotNull(response);
            Assert.True(response.ContainsKey("Competitions") && Convert.ToInt64(response["Competitions"]) > 201900141549);
            Assert.True(response.ContainsKey("Events") && Convert.ToInt64(response["Events"]) > 201800141549);
            Assert.True(response.ContainsKey("Locations") && Convert.ToInt64(response["Locations"]) > 201800141549);
            Assert.True(response.ContainsKey("Places") && Convert.ToInt64(response["Places"]) > 201800141549);
            Assert.True(response.ContainsKey("TableValues") && Convert.ToInt64(response["TableValues"]) > 201800141549);
        }


    }
}
